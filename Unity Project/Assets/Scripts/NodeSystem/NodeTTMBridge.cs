using UnityEngine;
using DopplerInteractive.TidyTileMapper.Utilities;
using TransTech.Nodes;
using TransTech.System.Debug;
using System.Collections.Generic;

/// <summary>
/// Bridges the Tidy Tile Mapper (TTM) with the Node system
/// </summary>
public class NodeTTMBridge : MonoSingleton<NodeTTMBridge>
{
    /// <summary>
    /// The TTM BlockMap used to display the Nodes
    /// </summary>
	private BlockMap m_Map;

    /// <summary>
    /// Prefab for the Usable Block type (Set in Editor)
    /// </summary>
    public GameObject m_UsableBlockPrefab;
    /// <summary>
    /// Prefab for the Unusable Block type (Set in Editor)
    /// </summary>
    public GameObject m_UnusableBlockPrefab;
    /// <summary>
    /// Prefab for the Empty editor block type (Set in Editor)
    /// </summary>
    public GameObject m_EmptyEditorBlockPrefab;
    /// <summary>
    /// Prefab for the Invisible block type (Set in Editor)
    /// </summary>
    public GameObject m_EmptyInvisibleBlockPrefab;
	
    /// <summary>
    /// Initializes the object
    /// </summary>
	public override void Init ()
	{
		base.Init ();
        DontDestroyOnLoad(gameObject);
        // Commented out Asset Pooling until it is fully understood.
		//AssetPool.EnablePooling();
	}

    /// <summary>
    /// Creates an empty block map for the Editor, at the given x and y size, with Layers.COUNT number of layers.
    /// </summary>
    /// <param name="mapName">The name of the map</param>
    /// <param name="xSize">The size of the map in blocks on the X Axis</param>
    /// <param name="ySize">The size of the map in block of the Y Axiz (Z World Axis)</param>
    /// <returns></returns>
    public BlockMap CreateEmptyMap(string mapName, int xSize, int ySize)
    {
        // We create the map with 1x1 chunks to get around the backwards setup it has.
        m_Map = BlockUtilities.CreateBlockMap(mapName, Vector3.one, 1, 1, BlockMap.GrowthAxis.Forward);
        
        for (int x = 0; x < xSize; x++)
        {
            for (int y = 0; y < ySize; y++)
            {
                for (int depth = 0; depth < (int)Layers.COUNT; depth++)
                {
                    GameObject b;
                    if (depth == 0) // If we are on the bottom layer, spawn an empty selectable block
                    {
                        b = Instantiate(m_EmptyEditorBlockPrefab) as GameObject;//AssetPool.Instantiate(m_EmptyEditorBlockPrefab); // We have removed the AssetPool code until it is properly understood
                        b.AddComponent<EditorBlockTag>().BlockType = Blocks.EmptySelectableEditorBlock;
                    }
                    else // Otherwise spawn an empty invisible block
                    {
                        b = Instantiate(m_EmptyInvisibleBlockPrefab) as GameObject;//AssetPool.Instantiate(m_EmptyInvisibleBlockPrefab); // We have removed the AssetPool code until it is properly understood
                        b.AddComponent<EditorBlockTag>().BlockType = Blocks.EmptyInvisibleEditorBlock;
                    }
                    BlockUtilities.AddBlockToMap(m_Map, b.GetComponent<Block>(), false, 0, true, x, y, depth, false, true);
                }
            }
        }
        // We spin the map 180 degrees so that the block indices match world directions
        m_Map.transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        return m_Map;
    }

    /// <summary>
    /// Sets the block to the given Blocks type
    /// </summary>
    /// <param name="xPos">X Position of the block to set</param>
    /// <param name="yPos">Y Position of the block to set</param>
    /// <param name="depth">Depth Position of the block to set</param>
    /// <param name="blockType">The new type to set the block to</param>
    public void SetBlockInLayer(int xPos, int yPos, int depth, Blocks blockType)
    {
        var block = BlockUtilities.GetBlockAt(m_Map, xPos, yPos, depth);
        // If we were unable to get the block, or the existing block is a different type to the one provided
        if (block == null || block.GetComponent<EditorBlockTag>().BlockType != blockType)
        {
            //AssetPool.Destroy(block.gameObject); // We have removed the AssetPool code until it is properly understood
            // Spawn a block of the appropriate type
            switch (blockType)
            {
                case Blocks.EmptyInvisibleEditorBlock:
                    block = (Instantiate(m_EmptyInvisibleBlockPrefab) as GameObject).GetComponent<Block>();
                    break;
                case Blocks.EmptySelectableEditorBlock:
                    block = (Instantiate(m_EmptyEditorBlockPrefab) as GameObject).GetComponent<Block>();
                    break;
                case Blocks.GrassBlock:
                    block = (Instantiate(m_UsableBlockPrefab) as GameObject).GetComponent<Block>();
                    break;
                case Blocks.RockBlock:
                    block = (Instantiate(m_UsableBlockPrefab) as GameObject).GetComponent<Block>();
                    break;
                case Blocks.WaterBlock:
                    block = (Instantiate(m_UnusableBlockPrefab) as GameObject).GetComponent<Block>();
                    break;
                default:
                    TTDebug.LogError("NodeTTMBridge.cs : Unsupported Block Type");
                    return;
            }
            // Add it to the map at the given coordinates.  This will automatically Destroy the existing block
            BlockUtilities.AddBlockToMap(m_Map, block, true, 0, true, xPos, yPos, depth, false, false);
            
            // Set the tag on the block to it's block type
            var blockTag = block.GetComponent<EditorBlockTag>() ?? block.gameObject.AddComponent<EditorBlockTag>();
            blockTag.BlockType = blockType;
        }
    }

    /// <summary>
    /// Generates and stores internally a Block Map from a given NodeMesh object.  This is used in the actual game, not the editor.
    /// </summary>
    /// <param name="mesh">The NodeMesh object to generate the map from</param>
    /// <param name="xSize">The size of the mesh's X dimension</param>
    /// <param name="ySize">The size of the mesh's Y dimension</param>
    /// <returns>A dictionary of the NodeView components, referenced by their Node object</returns>
    public Dictionary<Node, NodeView> GenerateMap(NodeMesh mesh, int xSize, int ySize)
	{
        // Create the map and orient
        m_Map = BlockUtilities.CreateBlockMap("Block Map", Vector3.one, 1, 1, BlockMap.GrowthAxis.Forward);
        m_Map.transform.rotation = Quaternion.Euler(0f, 180f, 0f);

        // Grab all the nodes from the mesh
        var nodes = mesh.GetNodes();
        var dict = new Dictionary<Node, NodeView>();

        // Loop through all the nodes
        foreach (var kvp in nodes)
        {
            GameObject tile;
            // if the block is unusable or empty, we place the unusable block type
            if (kvp.Value.NodeType == (int)NodeType.Unusable || kvp.Value.NodeType == (int)NodeType.None)
            {
                tile = AssetPool.Instantiate(m_UnusableBlockPrefab);
            }
            else // Otherwise the usable block type
            {
                tile = AssetPool.Instantiate(m_UsableBlockPrefab);
            }
            // TTM switches Y and Z, so we pass the Z coordinate of the node into the Y coordinate of TTM
            var x = (int)kvp.Value.Coordinate.XPos;
            var z = (int)kvp.Value.Coordinate.ZPos;
            BlockUtilities.AddBlockToMap(m_Map, tile.GetComponent<Block>(), true, 0, true, x, z, 0, false, true);
            // Add the NodeView component to the newly spawned tile and add it to the dictionary
            var nodeView = tile.AddComponent<NodeView>();
            nodeView.Init(kvp.Value, mesh);
            dict.Add(kvp.Value, nodeView);
            TTDebug.Log("Added tile at coords : " + x + " " + -kvp.Value.Coordinate.YPos + " " + z);
        }
        return dict;
	}

    /// <summary>
    /// Gets the "Ground" or lowest layer piece at the given coordinate
    /// </summary>
    /// <param name="xPos">X Pos coordinate</param>
    /// <param name="yPos">Y Pos coordinate (Z World Pos)</param>
    /// <returns>The Gameobject/Tile at the given coordinate on the Ground layer</returns>
    public GameObject GetGroundPiece(int xPos, int yPos)
    {
        var block = BlockUtilities.GetBlockAt(m_Map, xPos, yPos, 0);
        if (block == null)
        {
            TTDebug.LogError("NodeTTMBridge.cs : Invalid block coordinate : " + xPos + " " + yPos);
            return null;
        }
        return block.gameObject;
    }
}


