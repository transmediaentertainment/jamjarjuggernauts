using UnityEngine;

/// <summary>
/// View Controller for a connection between two pieces
/// </summary>
public class ConnectionView : MonoBehaviour
{
    /// <summary>
    /// Minimum scale for the connection (When at lowest power)
    /// </summary>
    private Vector3 m_MinScale;
    /// <summary>
    /// Maximum scale for the connection (When at highest power)
    /// </summary>
    private Vector3 m_MaxScale;

    /// <summary>
    /// Initializes the controller
    /// </summary>
    /// <param name="minScale">Minimum scale for the conneciton</param>
    /// <param name="maxScale">Maximum scale for the connection</param>
    public void Init(Vector3 minScale, Vector3 maxScale)
    {
        m_MinScale = minScale;
        m_MaxScale = maxScale;
    }

    /// <summary>
    /// Sets the interpolation level between the min and max scale (0 to 1)
    /// </summary>
    /// <param name="lerp">A zero to one</param>
    public void SetLerp(float lerp)
    {
        lerp = Mathf.Clamp01(lerp);
        var val = Vector3.Lerp(m_MinScale, m_MaxScale, lerp);
        transform.localScale = val;
    }
}