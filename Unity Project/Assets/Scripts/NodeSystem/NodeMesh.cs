using System.Collections.Generic;
using TransTech.Delegates;
using TransTech.System.Debug;

namespace TransTech.Nodes
{
    /// <summary>
    /// Class containing a Mesh of Node objects
    /// </summary>
    public partial class NodeMesh
    {
        /// <summary>
        /// Dictionary of Nodes in the mesh, referenced by their Unique ID
        /// </summary>
        private Dictionary<uint, Node> m_Nodes = new Dictionary<uint, Node>();

        /// <summary>
        /// Returns a copy of the Nodes Dictionary;
        /// </summary>
        /// <returns>A copy of the Nodes Dictionary</returns>
        public Dictionary<uint, Node> GetNodes()
        {
            return new Dictionary<uint, Node>(m_Nodes);
        }

        /// <summary>
        /// Returns a Dictionary of the Nodes in the mesh of the given type
        /// </summary>
        /// <param name="type">Type of nodes to return</param>
        /// <returns>A Dictionary of the Nodes in the mesh of the given type</returns>
        public Dictionary<uint, Node> GetNodesOfType(int type)
        {
            var dict = new Dictionary<uint, Node>();
            foreach (var kvp in m_Nodes)
            {
                var node = kvp.Value;
                if (node.NodeType == type)
                    dict.Add(node.UniqueID, node);
            }
            return dict;
        }

        /// <summary>
        /// Indexer to get Node by it's Unique ID
        /// </summary>
        /// <param name="nodeID">Unique ID of the node to fetch</param>
        /// <returns>The given node, or null</returns>
        public Node this[uint nodeID]
        {
            get
            {
                if (!m_Nodes.ContainsKey(nodeID))
                {
                    TTDebug.LogWarning("NodeMesh.cs : Unable to find node with id : " + nodeID.ToString());
                    return null;
                }

                return m_Nodes[nodeID] as Node;
            }
        }

        /// <summary>
        /// Number of connections each node in the mesh should support
        /// </summary>
        private int m_NodeConnectionCount;
        /// <summary>
        /// Number of connections each node in the mesh should support
        /// </summary>
        public int NodeConnectionCount { get { return m_NodeConnectionCount; } }
        /// <summary>
        /// Default type to set each new node to 
        /// </summary>
        private int m_DefaultNodeType;
        /// <summary>
        /// Default type to set each new node to 
        /// </summary>
        public int DefaultNodeType { get { return m_DefaultNodeType; } }
        /// <summary>
        /// The last Unique ID assigned to a Node
        /// </summary>
        private uint m_LastUniqueID;
        /// <summary>
        /// The last Unique ID assigned to a Node
        /// </summary>
        public uint LastUniqueID { get { return m_LastUniqueID; } }
        /// <summary>
        /// Fired when a node on the board changes
        /// </summary>
        public event VoidDelegate BoardChangedEvent;

        /// <summary>
        /// Constructor for Initialization
        /// </summary>
        /// <param name="nodeConnectionCount">Number of connections each Node should support</param>
        /// <param name="defaultNodeType">Default type for each newly created node</param>
        public NodeMesh(int nodeConnectionCount, int defaultNodeType)
        {
            m_NodeConnectionCount = nodeConnectionCount;
            m_DefaultNodeType = defaultNodeType;
            m_LastUniqueID = uint.MinValue + 1;
        }

        /// <summary>
        /// Create a new node in the mesh
        /// </summary>
        /// <param name="nodeType">The type of node to create</param>
        /// <returns>The Newly Created node</returns>
        public Node CreateNewNode(int nodeType)
        {
            var newNode = new Node(this, m_NodeConnectionCount, m_LastUniqueID, nodeType);
            newNode.NodeTypeChangedEvent += BoardChanged;
            m_LastUniqueID++;
            m_Nodes.Add(newNode.UniqueID, newNode);
            return newNode;
        }

        /// <summary>
        /// Create a new node in the mesh
        /// </summary>
        public Node CreateNewNode()
        {
            return CreateNewNode(m_DefaultNodeType);
        }

        /// <summary>
        /// Called when a Node change's it's type
        /// </summary>
        private void BoardChanged()
        {
            if (BoardChangedEvent != null)
                BoardChangedEvent();
        }
    }
}