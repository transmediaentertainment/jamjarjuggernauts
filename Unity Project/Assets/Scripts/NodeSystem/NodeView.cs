using TransTech.Delegates;
using TransTech.Nodes;
using TransTech.System.Animators;
using TransTech.System.Debug;
using TransTech.System.Input;
using UnityEngine;

/// <summary>
/// View controller object for a Node.
/// </summary>
public class NodeView : MonoBehaviour, IRaycastReceiver
{
    #region IRaycastReceiver implementation
    /// <summary>
    /// Handles being hit by the Raycast system
    /// </summary>
    /// <param name="hit">The RaycastHit informations</param>
    public void HandleRaycastHit(RaycastHit hit)
    {
        // If the GameController's Node test says we can report
        if (GameController.Instance.CurrentNodeTest(m_Node))
        {
            // Fire our Tile Clicked event
            if (TileWasClickedEvent != null)
                TileWasClickedEvent(m_Node.UniqueID);
        }
    }

    #endregion IRaycastReceiver implementation

    /// <summary>
    /// Event fired when the tile is clicked
    /// </summary>
    public event UIntDelegate TileWasClickedEvent;

    /// <summary>
    /// Has the Position of the NodeView been set?
    /// </summary>
    public bool PosSet { get; private set; }
    /// <summary>
    /// The Node this view is displaying
    /// </summary>
    private Node m_Node;

    /// <summary>
    /// The Tile GameObject displaying the Node
    /// </summary>
    private GameObject m_Tile;
    /// <summary>
    /// The Player Piece currently on this Tile
    /// </summary>
    private GameObject m_PlayerPiece;
    /// <summary>
    /// Is this piece a King piece?
    /// </summary>
    private bool m_PlayerPieceIsKing;
    /// <summary>
    /// The Connection GameObject for our Right Connection
    /// </summary>
    private GameObject m_RightConnection;
    /// <summary>
    /// The Connection GameObject for our Down Right Connection
    /// </summary>
    private GameObject m_DownRightConnection;
    /// <summary>
    /// The Connection GameObject for our Down Connection
    /// </summary>
    private GameObject m_DownConnection;
    /// <summary>
    /// The Connection GameObject for our Down Left Connection
    /// </summary>
    private GameObject m_DownLeftConnection;

    /// <summary>
    /// The Animator for spinning the Player Piece object when selected
    /// </summary>
    private SpinAnimator m_SpinAnimator = new SpinAnimator();

    /// <summary>
    /// Called by Unity when the object is created
    /// </summary>
    private void Awake()
    {
        gameObject.layer = LayerMask.NameToLayer("GameTile");
    }

    /// <summary>
    /// Used to Initialize the View
    /// </summary>
    /// <param name="node">The Node we are displaying</param>
    /// <param name="nodeMesh">The Node Mesh the Node belongs to</param>
    public void Init(Node node, NodeMesh nodeMesh)
    {
        m_Node = node;
        m_Node.NodeTypeChangedEvent += HandleNodeTypeChange;
        // TTM Integration
        m_Tile = NodeTTMBridge.Instance.GetGroundPiece((int)node.Coordinate.XPos, (int)node.Coordinate.ZPos);

        // Register for changes in right, rightdown, down, and leftdown
        var right = m_Node[(int)NodeDirections.Right];
        if (right != null)
            right.NodeTypeChangedEvent += () => HandleOtherNodeTypeChange(right, NodeDirections.Right);
        var downRight = m_Node[(int)NodeDirections.DownRight];
        if (downRight != null)
            downRight.NodeTypeChangedEvent += () => HandleOtherNodeTypeChange(downRight, NodeDirections.DownRight);
        var down = m_Node[(int)NodeDirections.Down];
        if (down != null)
            down.NodeTypeChangedEvent += () => HandleOtherNodeTypeChange(down, NodeDirections.Down);
        var downLeft = m_Node[(int)NodeDirections.DownLeft];
        if (downLeft != null)
            downLeft.NodeTypeChangedEvent += () => HandleOtherNodeTypeChange(downLeft, NodeDirections.DownLeft);
    }

    /// <summary>
    /// Handles another node (one of our connections) changing
    /// </summary>
    /// <param name="otherNode">The Node that changed</param>
    /// <param name="dir">The direction our connection with that node is in</param>
    private void HandleOtherNodeTypeChange(Node otherNode, NodeDirections dir)
    {
        if (otherNode != null)
        {
            if (!IsConnected(otherNode, m_Node))
            {
                TurnOffConnection(dir);
            }
        }
    }

    /// <summary>
    /// Should these two nodes be connected?
    /// </summary>
    /// <param name="node1">First node to check</param>
    /// <param name="node2">Second node to check</param>
    /// <returns></returns>
    private bool IsConnected(Node node1, Node node2)
    {
        if (node1.NodeType == node2.NodeType)
            return true;

        // Check all regular to king cases
        switch ((NodeType)node1.NodeType)
        {
            case NodeType.Player1:
                return node2.NodeType == (int)NodeType.Player1King;
            case NodeType.Player1King:
                return node2.NodeType == (int)NodeType.Player1;
            case NodeType.Player2:
                return node2.NodeType == (int)NodeType.Player2King;
            case NodeType.Player2King:
                return node2.NodeType == (int)NodeType.Player2;
            case NodeType.Player3:
                return node2.NodeType == (int)NodeType.Player3King;
            case NodeType.Player3King:
                return node2.NodeType == (int)NodeType.Player3;
            case NodeType.Player4:
                return node2.NodeType == (int)NodeType.Player4King;
            case NodeType.Player4King:
                return node2.NodeType == (int)NodeType.Player4;
        }
        return false;
    }

    /// <summary>
    /// Handles our Node type changing
    /// </summary>
    private void HandleNodeTypeChange()
    {
        UpdateNodeType();
    }

    /// <summary>
    /// Updates the visual objects to the new node type
    /// </summary>
    private void UpdateNodeType()
    {
        if (m_PlayerPiece != null)
        {
            m_PlayerPiece.transform.parent = null;
            if (m_PlayerPieceIsKing)
                SkinManager.Instance.ReturnSkinPlayerKingPieceInstance(m_PlayerPiece);
            else
                SkinManager.Instance.ReturnSkinPlayerPieceInstance(m_PlayerPiece);
        }

        var skin = SkinManager.Instance.GetSkin((NodeType)m_Node.NodeType);
        m_PlayerPieceIsKing = m_Node.NodeType == (int)NodeType.Player1King || m_Node.NodeType == (int)NodeType.Player2King ||
                       m_Node.NodeType == (int)NodeType.Player3King || m_Node.NodeType == (int)NodeType.Player4King;

        // If we're empty or unusable, then we don't want to get a player piece
        if (m_Node.NodeType != (int)NodeType.Empty && m_Node.NodeType != (int)NodeType.None && m_Node.NodeType != (int)NodeType.Unusable)
        {
            if (m_PlayerPieceIsKing)
                m_PlayerPiece = skin.GetPlayerKingPiece();
            else
                m_PlayerPiece = skin.GetPlayerPiece();
            m_PlayerPiece.transform.parent = transform;
            m_PlayerPiece.transform.localPosition = new Vector3(0f, 0.55f, 0f);
        }
        else
            m_PlayerPiece = null;

        // If we have any connections, we need to turn them off before changing the internal nodetype
        var right = m_Node[(int)NodeDirections.Right];
        if (right != null)
            HandleOtherNodeTypeChange(right, NodeDirections.Right);
        var downRight = m_Node[(int)NodeDirections.DownRight];
        if (downRight != null)
            HandleOtherNodeTypeChange(downRight, NodeDirections.DownRight);
        var down = m_Node[(int)NodeDirections.Down];
        if (down != null)
            HandleOtherNodeTypeChange(down, NodeDirections.Down);
        var downLeft = m_Node[(int)NodeDirections.DownLeft];
        if (downLeft != null)
            HandleOtherNodeTypeChange(downLeft, NodeDirections.DownLeft);
    }

    /// <summary>
    /// Turns on the visuals for a connection between two nodes
    /// </summary>
    /// <param name="dir">The direction of the connection</param>
    public void TurnOnConnection(NodeDirections dir)
    {
        var skin = SkinManager.Instance.GetSkin((NodeType)m_Node.NodeType);
        GameObject newConnection = null;
        switch (dir)
        {
            case NodeDirections.Right:
                if (m_RightConnection != null)
                    break;
                newConnection = m_RightConnection = skin.GetConnection();
                break;

            case NodeDirections.DownRight:
                if (m_DownRightConnection != null)
                    break;
                newConnection = m_DownRightConnection = skin.GetConnection();
                break;

            case NodeDirections.Down:
                if (m_DownConnection != null)
                    break;
                newConnection = m_DownConnection = skin.GetConnection();
                break;

            case NodeDirections.DownLeft:
                if (m_DownLeftConnection != null)
                    break;
                newConnection = m_DownLeftConnection = skin.GetConnection();
                break;

            default:
                TTDebug.LogError("NodeView.cs : Invalid Node Direction passed in : " + dir);
                break;
        }

        if (newConnection != null)
        {
            newConnection.SetActive(true);
            newConnection.transform.position = transform.position + skin.GetConnectionPosOffset(dir);
            newConnection.transform.rotation = skin.GetConnectionRotation(dir);
        }
    }

    /// <summary>
    /// Turns off the visuals for a connection between two nodes
    /// </summary>
    /// <param name="dir">The direction to turn off</param>
    public void TurnOffConnection(NodeDirections dir)
    {
        switch (dir)
        {
            case NodeDirections.Right:
                if (m_RightConnection == null)
                    return;

                SkinManager.Instance.ReturnSkinConnectionInstance(m_RightConnection);
                m_RightConnection = null;
                break;
            case NodeDirections.DownRight:
                if (m_DownRightConnection == null)
                    return;

                SkinManager.Instance.ReturnSkinConnectionInstance(m_DownRightConnection);
                m_DownRightConnection = null;
                break;
            case NodeDirections.Down:
                if (m_DownConnection == null)
                    return;

                SkinManager.Instance.ReturnSkinConnectionInstance(m_DownConnection);
                m_DownConnection = null;
                break;
            case NodeDirections.DownLeft:
                if (m_DownLeftConnection == null)
                    return;

                SkinManager.Instance.ReturnSkinConnectionInstance(m_DownLeftConnection);
                m_DownLeftConnection = null;
                break;
            default:
                TTDebug.LogError("NodeView.cs : Invalid Node Direction passed in : " + dir);
                break;
        }
    }

    /// <summary>
    /// Sets the position of the Node View in world coordinates
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    public void SetPosition(float x, float y)
    {
        transform.position = new Vector3(x, 0f, y);
        PosSet = true;
    }

    /// <summary>
    /// Set the material of the tile to it's highlighted state
    /// </summary>
    public void SetHighlighted()
    {
        var skinManager = SkinManager.Instance;
        if (m_Node.NodeType == (int)NodeType.Unusable)
            SetGOColor(m_Tile, skinManager.GetBoardSkin().UnusableHighlightColor);
        else
            SetGOColor(m_Tile, skinManager.GetBoardSkin().UsableHighlightColor);

        if (m_PlayerPiece != null)
            SetGOColor(m_PlayerPiece, skinManager.GetSkin((NodeType)m_Node.NodeType).HighlightColor);
    }

    /// <summary>
    /// Set the material of the tile to it's dimmed state
    /// </summary>
    public void SetDimmed()
    {
        var skinManager = SkinManager.Instance;
        if (m_Node.NodeType == (int)NodeType.Unusable)
            SetGOColor(m_Tile, skinManager.GetBoardSkin().UnusableDimmedColor);
        else
            SetGOColor(m_Tile, skinManager.GetBoardSkin().UsableDimmedColor);

        if (m_PlayerPiece != null)
            SetGOColor(m_PlayerPiece, skinManager.GetSkin((NodeType)m_Node.NodeType).DimmedColor);
    }

    /// <summary>
    /// Set the material of the tile to it's regular state
    /// </summary>
    public void SetRegular()
    {
        var skinManager = SkinManager.Instance;
        if (m_Node.NodeType == (int)NodeType.Unusable)
            SetGOColor(m_Tile, skinManager.GetBoardSkin().UnusableRegularColor);
        else
            SetGOColor(m_Tile, skinManager.GetBoardSkin().UsableRegularColor);

        if (m_PlayerPiece != null)
            SetGOColor(m_PlayerPiece, skinManager.GetSkin((NodeType)m_Node.NodeType).RegularColor);
    }

    /// <summary>
    /// Set the colour of the object
    /// </summary>
    /// <param name="go">The GameObject to set the colour for</param>
    /// <param name="col">The colour to set</param>
    private void SetGOColor(GameObject go, Color col)
    {
        var r = go.GetComponentInChildren(typeof (Renderer)) as Renderer;
        if(r != null)
            r.material.color = col;
    }

    /// <summary>
    /// Sets the tile to it's selected state
    /// </summary>
    public void SetSelected()
    {
        m_SpinAnimator.StartSpin(m_PlayerPiece.transform, Vector3.up, 180f);
    }

    /// <summary>
    /// Sets the tile to it's Unselected state
    /// </summary>
    public void SetUnselected()
    {
        m_SpinAnimator.StopSpin();
    }

    /// <summary>
    /// Sets the size of the connection object
    /// </summary>
    /// <param name="connectionDir">Direction of the connection object</param>
    /// <param name="size">The size to set it to</param>
    public void SetConnectionSize(NodeDirections connectionDir, float size)
    {
        ConnectionView cv = null;
        switch (connectionDir)
        {
            case NodeDirections.Right:
                if (m_RightConnection != null)
                    cv = m_RightConnection.GetComponent<ConnectionView>();
                break;

            case NodeDirections.DownRight:
                if (m_DownRightConnection != null)
                    cv = m_DownRightConnection.GetComponent<ConnectionView>();
                break;

            case NodeDirections.Down:
                if (m_DownConnection != null)
                    cv = m_DownConnection.GetComponent<ConnectionView>();
                break;

            case NodeDirections.DownLeft:
                if (m_DownLeftConnection != null)
                    cv = m_DownLeftConnection.GetComponent<ConnectionView>();
                break;

            default:
                TTDebug.LogError("NodeView.cs : Invalid direction passed for connection : " + connectionDir.ToString());
                break;
        }
        if (cv != null)
            cv.SetLerp(size);
    }
}