using TransTech.Patterns.Memento;

namespace TransTech.Nodes
{
    /// <summary>
    /// Public Abstract class for the Memento Originator to return
    /// </summary>
    public abstract class NodeMeshMemento : MementoObject { }

    /// <summary>
    /// Class containing a Mesh of Node objects
    /// </summary>
    public partial class NodeMesh : IMementoOriginator<NodeMeshMemento>
    {
        /// <summary>
        /// Creates and returns a Memento object of the NodeMesh's current state
        /// </summary>
        /// <returns></returns>
        public NodeMeshMemento CreateMemento()
        {
            return new NodeMeshMementoImplementation(this);
        }

        /// <summary>
        /// Restores the NodeMesh to the state of the given Memento
        /// </summary>
        /// <param name="memento">The memento to restore the state from</param>
        public void SetMemento(NodeMeshMemento memento)
        {
            var imp = (NodeMeshMementoImplementation)memento;
            m_NodeConnectionCount = imp.NodeConnectionCount;
            m_DefaultNodeType = imp.DefaultNodeType;
            m_LastUniqueID = imp.LastUniqueID;
        }

        /// <summary>
        /// Private implementation of the Memento class.  Kept private so
        /// that only the NodeMesh knows about it's internals
        /// </summary>
        private class NodeMeshMementoImplementation : NodeMeshMemento
        {
            /// <summary>
            /// Number of connections each node contains
            /// </summary>
            public int NodeConnectionCount { get; private set; }
            /// <summary>
            /// Default type to use on New Nodes
            /// </summary>
            public int DefaultNodeType { get; private set; }
            /// <summary>
            /// The last Unique Identifier number handed out
            /// </summary>
            public uint LastUniqueID { get; private set; }

            /// <summary>
            /// Constructor for Initialization
            /// </summary>
            /// <param name="nodeMesh">The NodeMesh to store the state of</param>
            public NodeMeshMementoImplementation(NodeMesh nodeMesh)
            {
                NodeConnectionCount = nodeMesh.NodeConnectionCount;
                DefaultNodeType = nodeMesh.DefaultNodeType;
                LastUniqueID = nodeMesh.LastUniqueID;
            }
        }
    }
}