using UnityEngine;

namespace TransTech.System.Animators
{
    /// <summary>
    /// Class for animating an object around a given axis.
    /// </summary>
    public class SpinAnimator
    {
        /// <summary>
        /// Axis to rotate around
        /// </summary>
        private Vector3 m_SpinAxis;

        /// <summary>
        /// Rotation of the object in it's original state
        /// </summary>
        private Quaternion m_StartRotation;

        /// <summary>
        /// The speed to rotate at
        /// </summary>
        private float m_MaxSpeed;

        /// <summary>
        /// Current rotation speed (used for accelerating towards max speed_
        /// </summary>
        private float m_CurrentSpeed;

        /// <summary>
        /// Rate of acceleration
        /// </summary>
        private const float m_AccelerationRate = 360f * Mathf.Deg2Rad;

        /// <summary>
        /// Transform to rotate
        /// </summary>
        private Transform m_Transform;

        /// <summary>
        /// Timer used during the slow down period
        /// </summary>
        private float m_SlowDownTimer;

        /// <summary>
        /// Total time for the slow down lerp
        /// </summary>
        private const float m_SlowDownTotalTime = 0.2f;

        /// <summary>
        /// Rotation of the transform when we stop the rotation animation
        /// </summary>
        private Quaternion m_EndRotation;

        /// <summary>
        /// Starts the object spinning
        /// </summary>
        /// <param name="t">Transform to spin</param>
        /// <param name="spinAxis">The Axis the object should spin around</param>
        /// <param name="degreesPerSecond">How many degrees per second to rotate</param>
        public void StartSpin(Transform t, Vector3 spinAxis, float degreesPerSecond)
        {
            m_SpinAxis = spinAxis;
            m_MaxSpeed = degreesPerSecond * Mathf.Deg2Rad;
            m_Transform = t;
            m_StartRotation = t.rotation;
            m_CurrentSpeed = 0f;

            // Register for Update events
            SystemEventCenter.Instance.UpdateEvent += SpinUpdate;
        }

        /// <summary>
        /// Handles updating the rotation of the transform
        /// </summary>
        /// <param name="deltaTime">The time elapsed since the last frame</param>
        private void SpinUpdate(float deltaTime)
        {
            // Accelerate if we aren't at full speed
            if (m_CurrentSpeed < m_MaxSpeed)
            {
                m_CurrentSpeed += m_AccelerationRate * deltaTime;
                m_CurrentSpeed = Mathf.Clamp(m_CurrentSpeed, 0f, m_MaxSpeed);
            }

            // Rotate
            m_Transform.Rotate(m_SpinAxis, m_CurrentSpeed * deltaTime);
        }

        /// <summary>
        /// Slows down the spin and puts to object back in it's original position
        /// </summary>
        /// <param name="deltaTime">Time since the last frame</param>
        private void SlowDownUpdate(float deltaTime)
        {
            m_SlowDownTimer += deltaTime;
            m_Transform.rotation = Quaternion.Lerp(m_EndRotation, m_StartRotation, m_SlowDownTimer / m_SlowDownTotalTime);

            // If we have finished, clamp and deregister from updates
            if (m_SlowDownTimer >= m_SlowDownTotalTime)
            {
                m_Transform.rotation = m_StartRotation;
                SystemEventCenter.Instance.UpdateEvent -= SlowDownUpdate;
            }
        }

        /// <summary>
        /// Stop the spinning animation
        /// </summary>
        public void StopSpin()
        {
            // We deregister the SpinUpdate, and register our slow down delegate
            SystemEventCenter.Instance.UpdateEvent -= SpinUpdate;
            SystemEventCenter.Instance.UpdateEvent += SlowDownUpdate;
            m_SlowDownTimer = 0f;
            m_EndRotation = m_Transform.rotation;
        }
    }
}