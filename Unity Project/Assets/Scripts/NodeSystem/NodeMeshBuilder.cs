namespace TransTech.Nodes
{
    /// <summary>
    /// Types of Nodes available in the Game
    /// </summary>
    public enum NodeType
    {
        None = -2,
        Unusable,
        Empty,
        Player1,
        Player2,
        Player3,
        Player4,
        Player1King,
        Player2King,
        Player3King,
        Player4King
    }

    /// <summary>
    /// Directions for Node Connections
    /// </summary>
    public enum NodeDirections
    {
        None = -1,
        UpLeft,
        Up,
        UpRight,
        Left,
        Right,
        DownLeft,
        Down,
        DownRight
    }

    /// <summary>
    /// Utility class used for building NodeMeshes
    /// </summary>
    public static class NodeMeshBuilder
    {
        /// <summary>
        /// Creates a new NodeMesh with all Nodes setup and connected
        /// </summary>
        /// <param name="xSize">Number of Nodes in the X Dimension</param>
        /// <param name="ySize">Number of Nodes in the Y Dimension</param>
        /// <returns>The created Node Mesh</returns>
        public static NodeMesh Create2DMesh(int xSize, int ySize)
        {
            // Create the NodeMesh
            var mesh = new NodeMesh2DGrid(xSize, ySize, 8, (int) NodeType.None);
            //var mesh = new NodeMesh(8, (int)NodeType.None);
            var nodes = new Node[xSize, ySize];
            // Create all the Nodes in the Node mesh
            for (int x = 0; x < xSize; x++)
            {
                for (int y = 0; y < ySize; y++)
                {
                    nodes[x, y] = mesh.CreateNewNode();
                    nodes[x, y].Coordinate = new NodeCoordinate(x, 0f, y);
                    mesh.SetNodeAtPosition(nodes[x,y], x, y);
                }
            }

            // Setup all the connections in the Node Mesh
            for (int x = 0; x < xSize; x++)
            {
                for (int y = 0; y < ySize; y++)
                {
                    for (int xDif = -1; xDif <= 1; xDif++)
                    {
                        for (int yDif = -1; yDif <= 1; yDif++)
                        {
                            if (IsInRange(x + xDif, xSize) && IsInRange(y + yDif, ySize))
                            {
                                if (x != x + xDif || y != y + yDif)
                                {
                                    nodes[x, y].AddConnection(nodes[x + xDif, y + yDif], (int)GetNodeDirection(xDif, yDif));
                                }
                            }
                        }
                    }
                }
            }

            return mesh;
        }

        /// <summary>
        /// Is the Value with a zero to Max range?
        /// </summary>
        /// <param name="val">Value to check</param>
        /// <param name="max">Max value</param>
        /// <returns>If is in range</returns>
        private static bool IsInRange(int val, int max)
        {
            return (val >= 0 && val < max);
        }

        /// <summary>
        /// Returns the direction for the given values
        /// </summary>
        /// <param name="xDir">positive or negative x direction</param>
        /// <param name="yDir">positive or negative Y direction</param>
        /// <returns></returns>
        private static NodeDirections GetNodeDirection(int xDir, int yDir)
        {
            // If the x direction is negative it is on the left
            if (xDir <= -1)
            {
                if (yDir >= 1)
                    return NodeDirections.UpLeft;
                if (yDir == 0)
                    return NodeDirections.Left;
                if (yDir <= -1)
                    return NodeDirections.DownLeft;
            }

            // If the x direction is zero, it's above or below us
            if (xDir == 0)
            {
                if (yDir >= 1)
                    return NodeDirections.Up;
                if (yDir <= -1)
                    return NodeDirections.Down;
            }

            // If the X direction is position, it's on the right of use
            if (xDir >= 1)
            {
                if (yDir >= 1)
                    return NodeDirections.UpRight;
                if (yDir == 0)
                    return NodeDirections.Right;
                if (yDir <= -1)
                    return NodeDirections.DownRight;
            }

            return NodeDirections.None;
        }
    }
}