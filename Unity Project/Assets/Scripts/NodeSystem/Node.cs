using System;
using TransTech.Delegates;
using TransTech.System.Debug;

namespace TransTech.Nodes
{
    /// <summary>
    /// Class for a node that lives in a NodeMesh.  Abstract concept
    /// </summary>
    public partial class Node
    {
        /// <summary>
        /// Const value to id an Invalid Node (not setup correctly)
        /// </summary>
        public const uint InvalidNode = uint.MinValue;

        /// <summary>
        /// Reference to the NodeMesh this Node belongs to
        /// </summary>
        private NodeMesh m_Mesh;

        /// <summary>
        /// ID's of the Nodes that this node connects to
        /// </summary>
        private uint[] m_ConnectingNodes;

        /// <summary>
        /// Number of Nodes that this Node has connections with (NOTE : some might be invalid nodes)
        /// </summary>
        public int ConnectionCount { get { return m_ConnectingNodes.Length; } }

        /// <summary>
        /// Indexer for accessing connected nodes
        /// </summary>
        /// <param name="pos">The position (index) of the connecting node</param>
        /// <returns>The node at this index, or null if no node there.</returns>
        public Node this[int pos]
        {
            get
            {
                if (!IsIndexInRange(pos))
                    throw new IndexOutOfRangeException("Node.cs");
                // If our connection ID is 0, it means we don't have a connection, so return null
                if (m_ConnectingNodes[pos] == 0)
                    return null;
                return m_Mesh[m_ConnectingNodes[pos]];
            }
        }

        /// <summary>
        /// Unique ID number of this Node
        /// </summary>
        private uint m_UniqueID;

        /// <summary>
        /// Unique ID number of this Node
        /// </summary>
        public uint UniqueID
        {
            get { return m_UniqueID; }
            private set { m_UniqueID = value; }
        }

        /// <summary>
        /// Type of node (usually used with an Enum)
        /// </summary>
        private int m_NodeType;

        /// <summary>
        /// Type of node (usually used with an Enum
        /// </summary>
        public int NodeType
        {
            get { return m_NodeType; }
            set
            {
                // If the node type changes, fire an event
                var lastVal = m_NodeType;
                m_NodeType = value;
                if (lastVal != m_NodeType)
                    if (NodeTypeChangedEvent != null)
                        NodeTypeChangedEvent();
            }
        }

        /// <summary>
        /// Fired when the Node's Type is changed
        /// </summary>
        public event VoidDelegate NodeTypeChangedEvent;

        /// <summary>
        /// Coordinate of the Node.
        /// </summary>
        private NodeCoordinate m_Coordinate;

        /// <summary>
        /// Coordinate of the Node.
        /// </summary>
        public NodeCoordinate Coordinate
        {
            get { return m_Coordinate; }
            set { m_Coordinate = value; }
        }

        /// <summary>
        /// Constructor for Initialization
        /// </summary>
        /// <param name="mesh">The NodeMesh this Node belongs to</param>
        /// <param name="connectionCount">Number of connections this node should use</param>
        /// <param name="uniqueID">Unique ID number</param>
        /// <param name="nodeType">Type of Node</param>
        public Node(NodeMesh mesh, int connectionCount, uint uniqueID, int nodeType)
        {
            m_Mesh = mesh;
            m_ConnectingNodes = new uint[connectionCount];
            for (var i = 0; i < m_ConnectingNodes.Length; i++)
            {
                m_ConnectingNodes[i] = InvalidNode;
            }
            UniqueID = uniqueID;
            NodeType = nodeType;
        }

        /// <summary>
        /// Replaces an existing connection with a new connection
        /// </summary>
        /// <param name="newConnection">The new Node to connect to</param>
        /// <param name="index">The index of the connection to replace</param>
        /// <returns>If successful</returns>
        public bool ReplaceConnection(Node newConnection, int index)
        {
            return NewConnection(newConnection, index, true);
        }

        /// <summary>
        /// Adds a new connection
        /// </summary>
        /// <param name="newConnection">The Node to connect to</param>
        /// <param name="index">The index of the connection to add</param>
        /// <returns>If successful</returns>
        public bool AddConnection(Node newConnection, int index)
        {
            return NewConnection(newConnection, index, false);
        }

        /// <summary>
        /// Creates a New Connection with another node
        /// </summary>
        /// <param name="newConnection">The Node to connect to</param>
        /// <param name="index">Index of the connection to make</param>
        /// <param name="shouldReplace">Should this replace an existing connection?</param>
        /// <returns>If successful</returns>
        private bool NewConnection(Node newConnection, int index, bool shouldReplace)
        {
            if (!IsIndexInRange(index))
                return false;

            if (shouldReplace)
            {
                if (m_ConnectingNodes[index] != InvalidNode)
                {
                    TTDebug.LogError("Node.cs : Connecting Node not Invalid");
                    return false;
                }
            }

            m_ConnectingNodes[index] = newConnection.UniqueID;
            return true;
        }

        /// <summary>
        /// Removes a connection
        /// </summary>
        /// <param name="index">Index of the connection to move</param>
        /// <returns>If successful</returns>
        public bool RemoveConnection(int index)
        {
            if (!IsIndexInRange(index))
                return false;

            if (m_ConnectingNodes[index] == InvalidNode)
            {
                TTDebug.LogError("Node.cs : Connection was already Invalid");
                return false;
            }

            m_ConnectingNodes[index] = InvalidNode;
            return true;
        }

        /// <summary>
        /// Is the given index within the range of connections
        /// </summary>
        /// <param name="index">Index to check</param>
        /// <returns>Is the index in range?</returns>
        private bool IsIndexInRange(int index)
        {
            if (index < 0 || index >= m_ConnectingNodes.Length)
            {
                TTDebug.LogError("Node.cs : Index out of range : " + index);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets the type of the Node
        /// </summary>
        /// <param name="newNodeType">Type to set the node to</param>
        public void SetNodeType(int newNodeType)
        {
            NodeType = newNodeType;
        }
    }
}