using PathologicalGames;
using TransTech.Nodes;
using UnityEngine;

/// <summary>
/// Skin for a player's tile pieces
/// </summary>
public class PlayerSkin : MonoBehaviour
{
    /// <summary>
    /// Prefab for the Player's Tile Object
    /// </summary>
    [SerializeField]
    private GameObject m_TilePrefab;
    /// <summary>
    /// Prefab for the Player's Tile Object
    /// </summary>
    public GameObject TilePrefab { get { return m_TilePrefab; } }

    /// <summary>
    /// Prefab for the Player's King/Fortify Tile Object
    /// </summary>
    [SerializeField]
    private GameObject m_TileKingPrefab;

    /// <summary>
    /// Prefab for the Player's King/Fortify Tile Object
    /// </summary>
    public GameObject TileKingPrefab { get { return m_TileKingPrefab; } }

    /// <summary>
    /// The Connection Skin object used for this player
    /// </summary>
    [SerializeField]
    private GameObject m_ConnectionSkin;

    /// <summary>
    /// Regular tint color for the Player's tiles
    /// </summary>
    [SerializeField]
    private Color m_RegularColor;

    /// <summary>
    /// Regular tint color for the Player's tiles
    /// </summary>
    public Color RegularColor { get { return m_RegularColor; } }

    /// <summary>
    /// Highlight tint color for the Player's tiles
    /// </summary>
    [SerializeField]
    private Color m_HighlightColor;

    /// <summary>
    /// Highlight tint color for the Player's tiles
    /// </summary>
    public Color HighlightColor { get { return m_HighlightColor; } }

    /// <summary>
    /// Dimmed tint color for the Player's tiles
    /// </summary>
    [SerializeField]
    private Color m_DimmedColor;
    /// <summary>
    /// Dimmed tint color for the Player's tiles
    /// </summary>
    public Color DimmedColor { get { return m_DimmedColor; } }

    /// <summary>
    /// ID number of the Skin (for use in the Skin Manager)
    /// </summary>
    public uint SkinID { get; private set; }

    /// <summary>
    /// ID number of the Skin (for use in the Skin Manager)
    /// </summary>
    public void SetSkinID(uint id)
    {
        SkinID = id;
    }

    /// <summary>
    /// Creates and returns a Player Tile piece
    /// </summary>
    /// <returns>The new Player Tile piece</returns>
    public GameObject GetPlayerPiece()
    {
        var t = PoolManager.Pools["Skins"].Spawn(m_TilePrefab.transform);
        t.gameObject.SetActive(true);
        return t.gameObject;
    }

    /// <summary>
    /// Returns a Player Tile piee to the pool
    /// </summary>
    /// <param name="tile">The Player Tile piece to return</param>
    public void ReturnTile(GameObject tile)
    {
        PoolManager.Pools["Skins"].Despawn(tile.transform);
    }

    /// <summary>
    /// Creates and returns a connection piece
    /// </summary>
    /// <returns>The newly created connection piece</returns>
    public GameObject GetConnection()
    {
        var connSkin = m_ConnectionSkin.GetComponent<ConnectionSkin>();
        var go = connSkin.GetConnection();
        return go;
    }

    /// <summary>
    /// Returns a connection piece to it's pool
    /// </summary>
    /// <param name="connection">The connection piece to return</param>
    public void ReturnConnection(GameObject connection)
    {
        var connSkin = m_ConnectionSkin.GetComponent<ConnectionSkin>();
        connSkin.ReturnConnection(connection);
    }

    /// <summary>
    /// Returns the position offset for the connection when being used in the given direction
    /// </summary>
    /// <param name="dir">The direction to get the offset for</param>
    /// <returns>The offset position</returns>
    public Vector3 GetConnectionPosOffset(NodeDirections dir)
    {
        var connSkin = m_ConnectionSkin.GetComponent<ConnectionSkin>();
        return connSkin.GetConnectionPosOffset(dir);
    }

    /// <summary>
    /// Returns the rotation for the connection object when being used in the given direction
    /// </summary>
    /// <param name="dir">The direction to get the offset for</param>
    /// <returns>The offset rotation</returns>
    public Quaternion GetConnectionRotation(NodeDirections dir)
    {
        var connSkin = m_ConnectionSkin.GetComponent<ConnectionSkin>();
        return connSkin.GetConnectionRotation(dir);
    }

    /// <summary>
    /// Creates and returns a King/Fortified tile piece
    /// </summary>
    /// <returns>The newly created King/Fortify piece</returns>
    public GameObject GetPlayerKingPiece()
    {
        var t = PoolManager.Pools["Skins"].Spawn(m_TileKingPrefab.transform);
        t.gameObject.SetActive(true);
        return t.gameObject;
    }

    /// <summary>
    /// Returns a King/Fortified tile piece to the pool
    /// </summary>
    /// <param name="tileKingPiece">The King/Fortify tile piece to return</param>
    public void ReturnTileKingPiece(GameObject tileKingPiece)
    {
        PoolManager.Pools["Skins"].Despawn(tileKingPiece.transform);
    }
}