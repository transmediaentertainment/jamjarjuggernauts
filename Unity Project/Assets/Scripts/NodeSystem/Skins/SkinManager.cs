using PathologicalGames;
using TransTech.Nodes;
using UnityEngine;

/// <summary>
/// Manager for handling the different player's skins
/// </summary>
public class SkinManager : MonoSingleton<SkinManager>
{
    /// <summary>
    /// The different tile pieces that can be retrieved from the skin
    /// </summary>
    private enum SkinPiece
    {
        Tile,
        King,
        Connection
    }

    /// <summary>
    /// The skin to use for Player One
    /// </summary>
    [SerializeField]
    private PlayerSkin m_PlayerOneSkin;

    /// <summary>
    /// The skin to use for Player Two
    /// </summary>
    [SerializeField]
    private PlayerSkin m_PlayerTwoSkin;

    /// <summary>
    /// The skin to use for Player Three
    /// </summary>
    [SerializeField]
    private PlayerSkin m_PlayerThreeSkin;

    /// <summary>
    ///  The skin to use for Player Four
    /// </summary>
    [SerializeField]
    private PlayerSkin m_PlayerFourSkin;

    /// <summary>
    /// The skin to use for the Board
    /// </summary>
    [SerializeField]
    private BoardSkin m_BoardSkin;

    /// <summary>
    /// Counter to use for ID tagging the skins
    /// </summary>
    private uint m_IDCounter;

    /// <summary>
    /// Initialize the manager
    /// </summary>
    public override void Init()
    {
        m_IDCounter = uint.MinValue;
    }

    /// <summary>
    /// Set the skin for the given player (NodeType)
    /// </summary>
    /// <param name="skin">The skin to set for the player</param>
    /// <param name="type">The NodeType for the player</param>
    public void SetPlayerSkin(PlayerSkin skin, NodeType type)
    {
        switch (type)
        {
            case NodeType.Player1:
                m_PlayerOneSkin = skin;
                break;

            case NodeType.Player2:
                m_PlayerTwoSkin = skin;
                break;

            case NodeType.Player3:
                m_PlayerThreeSkin = skin;
                break;

            case NodeType.Player4:
                m_PlayerFourSkin = skin;
                break;
        }
        skin.SetSkinID(m_IDCounter);
        m_IDCounter++;
    }

    /// <summary>
    /// Set the skin to use for the board
    /// </summary>
    /// <param name="skin">The skin for the board</param>
    public void SetBoardSkin(BoardSkin skin)
    {
        m_BoardSkin = skin;
    }

    /// <summary>
    /// Returns the skin to use for the board
    /// </summary>
    /// <returns>The board skin</returns>
    public BoardSkin GetBoardSkin()
    {
        return m_BoardSkin;
    }

    /// <summary>
    /// Get the skin for the given player
    /// </summary>
    /// <param name="type">The NodeType for the skin to retreive</param>
    /// <returns>The specific PlayerSkin</returns>
    public PlayerSkin GetSkin(NodeType type)
    {
        switch (type)
        {
            case NodeType.Player1:
                return m_PlayerOneSkin;
            case NodeType.Player1King:
                return m_PlayerOneSkin;
            case NodeType.Player2:
                return m_PlayerTwoSkin;
            case NodeType.Player2King:
                return m_PlayerTwoSkin;
            case NodeType.Player3:
                return m_PlayerThreeSkin;
            case NodeType.Player3King:
                return m_PlayerThreeSkin;
            case NodeType.Player4:
                return m_PlayerFourSkin;
            case NodeType.Player4King:
                return m_PlayerFourSkin;
        }
        return null;
    }

    /// <summary>
    /// Returns a Player Piece from the board to the pool
    /// </summary>
    /// <param name="go">The player piece to return</param>
    public void ReturnSkinPlayerPieceInstance(GameObject go)
    {
        PoolManager.Pools["Skins"].Despawn(go.transform);
    }

    /// <summary>
    /// Returns a Connection piece from the board to the pool
    /// </summary>
    /// <param name="go"></param>
    public void ReturnSkinConnectionInstance(GameObject go)
    {
        PoolManager.Pools["Skins"].Despawn(go.transform);
    }

    /// <summary>
    /// Returns a Player's King/Fortify piece from the board to the pool
    /// </summary>
    /// <param name="go"></param>
    public void ReturnSkinPlayerKingPieceInstance(GameObject go)
    {
        PoolManager.Pools["Skins"].Despawn(go.transform);
    }
}