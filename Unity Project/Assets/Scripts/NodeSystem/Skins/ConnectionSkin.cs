using PathologicalGames;
using TransTech.Nodes;
using UnityEngine;

/// <summary>
/// Skin class for connection objects between nodes
/// </summary>
public class ConnectionSkin : MonoBehaviour
{
    /// <summary>
    /// Prefab for the connection object in the skin
    /// </summary>
    public GameObject m_ConnectionPrefab;

    /// <summary>
    /// Minimum size of the connection (when connection is at lowest count)
    /// </summary>
    public Vector3 m_MinScale = Vector3.one;
    /// <summary>
    /// Maximum size of the connection (when connection is at highest count)
    /// </summary>
    public Vector3 m_MaxScale = Vector3.one;

    /// <summary>
    /// Creates and returns a connection object
    /// </summary>
    /// <returns>A connection object</returns>
    public GameObject GetConnection()
    {
        var t = PoolManager.Pools["Skins"].Spawn(m_ConnectionPrefab.transform);
        var connView = t.gameObject.GetComponent<ConnectionView>();
        connView.Init(m_MinScale, m_MaxScale);
        return t.gameObject;
    }

    /// <summary>
    /// Returns a connection object to it's pool
    /// </summary>
    /// <param name="connection">The connection object to return</param>
    public void ReturnConnection(GameObject connection)
    {
        PoolManager.Pools["Skins"].Despawn(connection.transform);
    }

    /// <summary>
    /// Returns the position offset for the connection when being used in the given direction
    /// </summary>
    /// <param name="dir">The direction to get the offset for</param>
    /// <returns>The offset position</returns>
    public Vector3 GetConnectionPosOffset(NodeDirections dir)
    {
        switch (dir)
        {
            case NodeDirections.Right:
                return new Vector3(0.5f, 0.55f);
            case NodeDirections.DownRight:
                return new Vector3(0.5f, 0.55f, -0.5f);
            case NodeDirections.Down:
                return new Vector3(0f, 0.55f, -0.5f);
            case NodeDirections.DownLeft:
                return new Vector3(-0.5f, 0.55f, -0.5f);
        }

        return Vector3.zero;
    }

    /// <summary>
    /// Returns the rotation for the connection object when being used in the given direction
    /// </summary>
    /// <param name="dir">The direction to get the offset for</param>
    /// <returns>The offset rotation</returns>
    public Quaternion GetConnectionRotation(NodeDirections dir)
    {
        switch (dir)
        {
            case NodeDirections.Right:
                return Quaternion.Euler(0f, 0f, 90f);
            case NodeDirections.DownRight:
                return Quaternion.Euler(0f, 45f, 90f);
            case NodeDirections.Down:
                return Quaternion.Euler(0f, 90f, 90f);
            case NodeDirections.DownLeft:
                return Quaternion.Euler(0f, 135f, 90f);
        }

        return Quaternion.identity;
    }
}