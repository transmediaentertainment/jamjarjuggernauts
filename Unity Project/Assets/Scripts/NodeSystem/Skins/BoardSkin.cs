using PathologicalGames;
using UnityEngine;

/// <summary>
/// Skin for a Tiled Board
/// </summary>
public class BoardSkin : MonoBehaviour
{
    // NOTE : We use private fields with SerializeField attributes so that
    // we can populate the skin inside the Unity Editor, but also adhere to 
    // object encapsulation


    /// <summary>
    /// Prefab for a Usable Tile in the skin.
    /// </summary>
    [SerializeField]
    private GameObject m_UsableTilePrefab;

    /// <summary>
    /// Regular material color for Usable Tiles
    /// </summary>
    [SerializeField]
    private Color m_UsableRegularColor;

    /// <summary>
    /// Regular material color for Usable Tiles
    /// </summary>
    public Color UsableRegularColor { get { return m_UsableRegularColor; } }

    /// <summary>
    /// Highlight material color for Usable Tiles
    /// </summary>
    [SerializeField]
    private Color m_UsableHighlightColor;

    /// <summary>
    /// Highlight material color for Usable Tiles
    /// </summary>
    public Color UsableHighlightColor { get { return m_UsableHighlightColor; } }

    /// <summary>
    /// Dimmed material color for Usable Tiles
    /// </summary>
    [SerializeField]
    private Color m_UsableDimmedColor;

    /// <summary>
    /// Dimmed material color for Usable Tiles
    /// </summary>
    public Color UsableDimmedColor { get { return m_UsableDimmedColor; } }

    /// <summary>
    /// Prefab for Unusable Tiles in the skin
    /// </summary>
    [SerializeField]
    private GameObject m_UnusableTilePrefab;

    /// <summary>
    /// Regular material color for Unusable Tiles
    /// </summary>
    [SerializeField]
    private Color m_UnusableRegularColor;

    /// <summary>
    /// Regular material color for Unusable Tiles
    /// </summary>
    [SerializeField]
    public Color UnusableRegularColor { get { return m_UnusableRegularColor; } }

    /// <summary>
    /// Highlight material color for Unusable Tiles
    /// </summary>
    [SerializeField]
    private Color m_UnusableHighlightColor;

    /// <summary>
    /// Highlight material color for Unusable Tiles
    /// </summary>
    public Color UnusableHighlightColor { get { return m_UnusableHighlightColor; } }

    /// <summary>
    /// Dimmed material color for Unusable Tiles
    /// </summary>
    [SerializeField]
    private Color m_UnusableDimmedColor;

    /// <summary>
    /// Dimmed material color for Unusable Tiles
    /// </summary>
    public Color UnusableDimmedColor { get { return m_UnusableDimmedColor; } }

    /// <summary>
    /// Creates and returns a Usable Tile from the skin.
    /// </summary>
    /// <returns>A Usable Tile object</returns>
    public GameObject GetUsableTile()
    {
        return PoolManager.Pools["Skins"].Spawn(m_UsableTilePrefab.transform).gameObject;
    }

    /// <summary>
    /// Creates and returns an Unusable Tile from the skin.
    /// </summary>
    /// <returns>A. Unusable Tile object</returns>
    public GameObject GetUnusableTile()
    {
        return PoolManager.Pools["Skins"].Spawn(m_UnusableTilePrefab.transform).gameObject;
    }

    /// <summary>
    /// Returns a usable tile object to the pool of objects
    /// </summary>
    /// <param name="tile">The usable tile object to return</param>
    public void ReturnUsableTile(GameObject tile)
    {
        PoolManager.Pools["Skins"].Despawn(tile.transform);
    }

    /// <summary>
    /// Returns an unusable tile object to the pool of objects
    /// </summary>
    /// <param name="tile">The unusable tile object to return</param>
    public void ReturnUnusableTile(GameObject tile)
    {
        PoolManager.Pools["Skins"].Despawn(tile.transform);
    }
}