using UnityEngine;

namespace TransTech.Nodes
{
    /// <summary>
    /// Wrapper class for storing the coordinates of a Node
    /// </summary>
    public class NodeCoordinate
    {
        /// <summary>
        /// X Position of the Node
        /// </summary>
        public float XPos { get; set; }
        /// <summary>
        /// Y Position of the Node
        /// </summary>
        public float YPos { get; set; }
        /// <summary>
        /// Z Position of the Node
        /// </summary>
        public float ZPos { get; set; }

        /// <summary>
        /// Constructor for Initialization
        /// </summary>
        /// <param name="xPos">X Pos of the Node</param>
        /// <param name="yPos">Y Pos of the Node</param>
        /// <param name="zPos">Z Pos of the Node</param>
        public NodeCoordinate(float xPos, float yPos, float zPos)
        {
            XPos = xPos;
            YPos = yPos;
            ZPos = zPos;
        }

        /// <summary>
        /// Returns the Coordinate as a Vector3
        /// </summary>
        /// <returns>Vector 3 position of the coordinate</returns>
        public Vector3 ToVector3()
        {
            return new Vector3(XPos, YPos, ZPos);
        }
    }
}