using System;
using System.IO;
using TransTech.Patterns.Memento;
using TransTech.System.Debug;
using TransTech.System.IO;

namespace TransTech.Nodes
{
    /// <summary>
    /// public abstract class for the Memento Originator to return
    /// </summary>
    public abstract class NodeMemento : MementoObject, IBinarySavable
    {
        /// <summary>
        /// Abstract function signature for saving the memento as a binary stream
        /// </summary>
        /// <param name="bw">The Binary Writer to save with</param>
        /// <returns>If successful</returns>
        public abstract bool SaveWithBinaryWriter(BinaryWriter bw);
        /// <summary>
        /// Abstract function signature for loading the memento from a binary stream
        /// </summary>
        /// <param name="br">The binary reader to load from</param>
        /// <returns>If Successful</returns>
        public abstract bool LoadFromBinaryReader(BinaryReader br);
    }

    /// <summary>
    /// Class for a node that lives in a NodeMesh.  Abstract concept
    /// </summary>
    public partial class Node : IMementoOriginator<NodeMemento>
    {
        /// <summary>
        /// Creates a Memento of the current state of the node
        /// </summary>
        /// <returns>The created Memento object</returns>
        public NodeMemento CreateMemento()
        {
            return new NodeMementoImplementation(this);
        }

        /// <summary>
        /// Restores the Node to the state in the given memento
        /// </summary>
        /// <param name="memento">The memento to restore the state from</param>
        public void SetMemento(NodeMemento memento)
        {
            var imp = (NodeMementoImplementation)memento;
            UniqueID = imp.UniqueID;
            NodeType = imp.NodeType;
            Coordinate = imp.Coordinate;
            m_ConnectingNodes = new uint[imp.ConnectionCount];
            for (int i = 0; i < m_ConnectingNodes.Length; i++)
            {
                m_ConnectingNodes[i] = imp[i];
            }
        }

        /// <summary>
        /// Private implementation of the memento class.  Kept private so that 
        /// only the Node knows about it's internals
        /// </summary>
        private class NodeMementoImplementation : NodeMemento
        {
            /// <summary>
            /// Unique ID of the Node
            /// </summary>
            public uint UniqueID { get; private set; }

            /// <summary>
            /// Type of the Node
            /// </summary>
            public int NodeType { get; private set; }

            /// <summary>
            /// Coordinate of the Node
            /// </summary>
            public NodeCoordinate Coordinate { get; private set; }

            /// <summary>
            /// Connections of the Node to other Nodes
            /// </summary>
            private uint[] m_Connections;

            /// <summary>
            /// Number of connections to other nodes
            /// </summary>
            public int ConnectionCount { get { return m_Connections.Length; } }

            /// <summary>
            /// Indexer to get ID's of connected nodes
            /// </summary>
            /// <param name="pos"></param>
            /// <returns></returns>
            public uint this[int pos]
            {
                get
                {
                    return m_Connections[pos];
                }
            }

            /// <summary>
            /// Constructor for Initialization
            /// </summary>
            /// <param name="node">The Node to store the state of</param>
            public NodeMementoImplementation(Node node)
            {
                UniqueID = node.UniqueID;
                NodeType = node.NodeType;
                Coordinate = node.Coordinate;
                m_Connections = new uint[node.ConnectionCount];
                for (int i = 0; i < m_Connections.Length; i++)
                {
                    if (node[i] != null)
                        m_Connections[i] = node[i].UniqueID;
                }
            }

            /// <summary>
            /// Saves the Memento data to the given Binary stream
            /// </summary>
            /// <param name="bw">The Binary Writer to save with</param>
            /// <returns>If Successful</returns>
            public override bool SaveWithBinaryWriter(BinaryWriter bw)
            {
                try
                {
                    bw.Write(UniqueID);
                    bw.Write(NodeType);

                    // We split the node coordinate up
                    bw.Write(Coordinate.XPos);
                    bw.Write(Coordinate.YPos);
                    bw.Write(Coordinate.ZPos);
                    bw.Write(ConnectionCount);
                    for (int i = 0; i < ConnectionCount; i++)
                    {
                        bw.Write(m_Connections[i]);
                    }
                }
                catch (Exception e)
                {
                    TTDebug.LogError("NodeMemento.cs : Failed to write Node to Binary File : " + e.Message);
                    return false;
                }

                return true;
            }

            /// <summary>
            /// Loads data from a Binary stream and sets the memento to that data
            /// </summary>
            /// <param name="br">The Binary Stream to load from</param>
            /// <returns>If successful</returns>
            public override bool LoadFromBinaryReader(BinaryReader br)
            {
                try
                {
                    UniqueID = br.ReadUInt32();
                    NodeType = br.ReadInt32();
                    var x = br.ReadSingle();
                    var y = br.ReadSingle();
                    var z = br.ReadSingle();
                    Coordinate = new NodeCoordinate(x, y, z);
                    m_Connections = new uint[br.ReadInt32()];
                    for (int i = 0; i < m_Connections.Length; i++)
                    {
                        m_Connections[i] = br.ReadUInt32();
                    }
                }
                catch (Exception e)
                {
                    TTDebug.LogError("NodeMemento.cs : Failed to read Node from Binary file : " + e.Message);
                    return false;
                }

                return true;
            }
        }
    }
}