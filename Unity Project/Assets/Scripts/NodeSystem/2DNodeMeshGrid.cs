﻿using TransTech.System.Debug;

namespace TransTech.Nodes
{
	public class NodeMesh2DGrid : NodeMesh
	{
        private Node[,] m_Grid;

        public int XSize { get { return m_Grid == null ? 0 : m_Grid.GetLength(0); } }
        public int YSize { get { return m_Grid == null ? 0 : m_Grid.GetLength(1); } }
             

	    public NodeMesh2DGrid(int xSize, int ySize, int nodeConnectionCount, int defaultNodeType) 
            : base(nodeConnectionCount, defaultNodeType)
	    {
            m_Grid = new Node[xSize, ySize];
	    }

	    public void SetNodeAtPosition(Node n, int xPos, int yPos)
	    {
	        if(xPos < 0 || yPos < 0 || xPos >= m_Grid.GetLength(0) || yPos >= m_Grid.GetLength(1))
	        {
	            TTDebug.LogError("2DNodeMeshGrid.cs : Index out of range : " + xPos + " : " + yPos);
                return;
	        }

	        m_Grid[xPos, yPos] = n;
	    }

        public Node GetNodeAtPosition(int xPos, int yPos)
        {
            if (xPos < 0 || yPos < 0 || xPos >= m_Grid.GetLength(0) || yPos >= m_Grid.GetLength(1))
            {
                TTDebug.LogError("2DNodeMeshGrid.cs : Index out of range : " + xPos + " : " + yPos);
                return null;
            }
            return m_Grid[xPos, yPos];
        }
	}
}
