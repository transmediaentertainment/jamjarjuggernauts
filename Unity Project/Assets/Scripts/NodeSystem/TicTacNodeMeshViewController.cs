using System.Collections.Generic;
using TransTech.Delegates;
using TransTech.Nodes;
using UnityEngine;
using TransTech.System.Debug;

/// <summary>
/// View Controller for the Node Mesh
/// </summary>
public class TicTacNodeMeshViewController : MonoBehaviour
{
    /// <summary>
    /// Size of the mesh in the X dimension
    /// </summary>
    [SerializeField]
    private int m_XSize = 5;
    /// <summary>
    /// Size of the mesh in the X dimension
    /// </summary>
    public int XSize { get { return m_XSize; } }

    /// <summary>
    /// Size of the mesh in the Y (world Z) dimension
    /// </summary>
    [SerializeField]
    private int m_YSize = 5;
    /// <summary>
    /// Size of the mesh in the Y (world Z) dimension
    /// </summary>
    public int YSize { get { return m_YSize; } }

    /// <summary>
    /// World size of the board on the X Axis
    /// </summary>
    public float XWorldSize { get { return m_XSize * m_ObjSize + (m_XSize - 1) * m_Spacing; } }

    /// <summary>
    /// World size of the board on the Y Axis (world Z)
    /// </summary>
    public float YWorldSize { get { return m_YSize * m_ObjSize + (m_YSize - 1) * m_Spacing; } }

    /// <summary>
    /// Amount of world space to leave between tiles
    /// </summary>
    private float m_Spacing = 0.2f;
    /// <summary>
    /// World size of the tiles 
    /// </summary>
    private float m_ObjSize = 1f;

    /// <summary>
    /// The NodeMesh object to display
    /// </summary>
    private NodeMesh m_NodeMesh;
    /// <summary>
    /// The NodeMesh object to display
    /// </summary>
    public NodeMesh CurrentNodeMesh { get { return m_NodeMesh; } }
    /// <summary>
    /// Collection of all the individual NodeView objects in the NodeMesh
    /// </summary>
    private Dictionary<Node, NodeView> m_NodeViewDict = new Dictionary<Node, NodeView>();

    /// <summary>
    /// Does the NodeMesh have any empty nodes?
    /// </summary>
    private bool m_HasEmptyNodes;
    /// <summary>
    /// Event fired when there are no empty spaces left on the board.
    /// </summary>
    public event VoidDelegate NoEmptySpacesAvailableEvent;
    /// <summary>
    /// Event fired when there are empty spaces left on the board
    /// </summary>
    public event VoidDelegate EmptySpacesAvailableEvent;

    /// <summary>
    /// Has the View Controller been initialized?
    /// </summary>
    private bool m_Init = false;
	/// <summary>
	/// Byte Array representation of the map (TTM)
	/// </summary>
	private byte[,,] m_Map;

    /// <summary>
    /// Called by Unity the frame after the object is created
    /// </summary>
    private void Start()
    {
        Init();
    }
	
    /// <summary>
    /// Sets the Byte Array to use as the Map representation (from TTM and Save system)
    /// </summary>
    /// <param name="map"></param>
	public void SetBlockMap( byte[,,] map )
	{
		m_Map = map;
	}

    /// <summary>
    /// Initializes the View Controller
    /// </summary>
    public void Init()
    {
        // If it's already been initialized, early exit
        if (m_Init)
            return;
		
        // If we have map data set, pull out the lengths
		if( m_Map != null )
		{
			m_XSize = m_Map.GetLength(0);
			m_YSize = m_Map.GetLength(1);
		}

        // Create an empty NodeMesh at the desired size
        m_NodeMesh = NodeMeshBuilder.Create2DMesh(m_XSize, m_YSize);
		
		var nodes = m_NodeMesh.GetNodes();
		
        // Loop through the nodes, setting the positions, and block types
		foreach( var kvp in nodes )
		{
			var x = (int)kvp.Value.Coordinate.XPos;
			var y = (int)kvp.Value.Coordinate.ZPos;
			
			TTDebug.Log("Setting pos x : " + x + " y : " + y + " to block: " + (Blocks)m_Map[x,y,0] );
			
			switch( (Blocks)m_Map[x,y,0] )
			{
			case Blocks.EmptyInvisibleEditorBlock:
				kvp.Value.NodeType = (int)NodeType.Unusable;
				break;
			case Blocks.EmptySelectableEditorBlock:
				kvp.Value.NodeType = (int)NodeType.Unusable;
				break;
			case Blocks.GrassBlock:
				kvp.Value.NodeType = (int)NodeType.Empty;
				break;
			case Blocks.RockBlock:
				kvp.Value.NodeType = (int)NodeType.Empty;
				break;
			case Blocks.WaterBlock:
				kvp.Value.NodeType = (int)NodeType.Unusable;
				break;
			default:
				TTDebug.LogError("TicTacNodeMeshViewController.cs : Byte data not supported : " + m_Map[x,y,0] );
				break;
			}

            kvp.Value.NodeTypeChangedEvent += ScanForConnections;
		}

        // Generate the map tiles from the NodeMesh
        m_NodeViewDict = NodeTTMBridge.Instance.GenerateMap(m_NodeMesh, m_XSize, m_YSize);

        m_Init = true;
    }

    /// <summary>
    /// Scans the NodeMesh for possible connections, and shows them by spawning connection objects.
    /// </summary>
    private void ScanForConnections()
    {
        // we want to scan starting from top, right and left
        var nodes = m_NodeMesh.GetNodes();
        m_HasEmptyNodes = false;
        foreach (var kvp in nodes)
        {
            var node = kvp.Value;
            if (kvp.Value.NodeType == (int)NodeType.Empty)
            {
                m_HasEmptyNodes = true;
            }
            if (!CanNodeConnect(node))
                continue;
            var rightNode = node[(int)NodeDirections.Right];
            if (rightNode != null)
            {
                if (CanNodeConnect(rightNode))
                {
                    if (IsConnected(node, rightNode))
                        m_NodeViewDict[node].TurnOnConnection(NodeDirections.Right);
                }
            }
            var downRightNode = node[(int)NodeDirections.DownRight];
            if (downRightNode != null)
            {
                if (CanNodeConnect(downRightNode))
                {
                    if (IsConnected(node, downRightNode))
                        m_NodeViewDict[node].TurnOnConnection(NodeDirections.DownRight);
                }
            }
            var downNode = node[(int)NodeDirections.Down];
            if (downNode != null)
            {
                if (CanNodeConnect(downNode))
                {
                    if (IsConnected(node, downNode))
                        m_NodeViewDict[node].TurnOnConnection(NodeDirections.Down);
                }
            }
            var downLeftNode = node[(int)NodeDirections.DownLeft];
            if (downLeftNode != null)
            {
                if (CanNodeConnect(downLeftNode))
                {
                    if (IsConnected(node, downLeftNode))
                        m_NodeViewDict[node].TurnOnConnection(NodeDirections.DownLeft);
                }
            }
        }

        if (!m_HasEmptyNodes)
        {
            if (NoEmptySpacesAvailableEvent != null)
                NoEmptySpacesAvailableEvent();
        }
        else
        {
            if (EmptySpacesAvailableEvent != null)
                EmptySpacesAvailableEvent();
        }
    }

    /// <summary>
    /// Is this Node able to connect to other nodes?
    /// </summary>
    /// <param name="node">The Node to check</param>
    /// <returns>If the Node has the ability to make connections</returns>
    private bool CanNodeConnect(Node node)
    {
        // We check for empty, unusable and 'none' node types
        return node.NodeType != (int)NodeType.Empty && node.NodeType != (int)NodeType.Unusable && node.NodeType != (int)NodeType.None;
    }

    /// <summary>
    /// Are these two nodes connected?
    /// </summary>
    /// <param name="node1">First node to check</param>
    /// <param name="node2">Second node to check</param>
    /// <returns>If the nodes are connected</returns>
    private bool IsConnected(Node node1, Node node2)
    {
        if (node1.NodeType == node2.NodeType)
            return true;

        // Check if they are regular/king connections
        switch ((NodeType)node1.NodeType)
        {
            case NodeType.Player1:
                return node2.NodeType == (int)NodeType.Player1King;
            case NodeType.Player1King:
                return node2.NodeType == (int)NodeType.Player1;
            case NodeType.Player2:
                return node2.NodeType == (int)NodeType.Player2King;
            case NodeType.Player2King:
                return node2.NodeType == (int)NodeType.Player2;
            case NodeType.Player3:
                return node2.NodeType == (int)NodeType.Player3King;
            case NodeType.Player3King:
                return node2.NodeType == (int)NodeType.Player3;
            case NodeType.Player4:
                return node2.NodeType == (int)NodeType.Player4King;
            case NodeType.Player4King:
                return node2.NodeType == (int)NodeType.Player4;
        }
        return false;
    }

    /// <summary>
    /// Sets the entire board to it's regular material colour
    /// </summary>
    public void SetBoardRegular()
    {
        foreach (var kvp in m_NodeViewDict)
        {
            kvp.Value.SetRegular();
        }
    }

    /// <summary>
    /// Sets the given Node to it's Highlighted material colour
    /// </summary>
    /// <param name="node">The Node to set to Highlighted</param>
    public void SetNodeHighlighted(Node node)
    {
        var n = m_NodeViewDict[node];
        n.SetHighlighted();
    }

    /// <summary>
    /// Sets the given Node to it's Regular material colour
    /// </summary>
    /// <param name="node">The Node to set to Regular</param>
    public void SetNodeRegular(Node node)
    {
        var n = m_NodeViewDict[node];
        n.SetRegular();
    }

    /// <summary>
    /// Sets the given Node it it's Simmed material colour
    /// </summary>
    /// <param name="node">The Node to set to Dimmed</param>
    public void SetNodeDimmed(Node node)
    {
        var n = m_NodeViewDict[node];
        n.SetDimmed();
    }

    /// <summary>
    /// Sets the given Node to a 'Selected' State
    /// </summary>
    /// <param name="node">The Node to 'Select'</param>
    public void SetNodeSelected(Node node)
    {
        var n = m_NodeViewDict[node];
        n.SetSelected();
    }

    /// <summary>
    /// Sets the given Node to a 'Unselected' State
    /// </summary>
    /// <param name="node">The Node to 'Unselect'</param>
    public void SetNodeUnselected(Node node)
    {
        var n = m_NodeViewDict[node];
        n.SetUnselected();
    }

    /// <summary>
    /// Converts a NodeCoordinate to a World Position, taking spacing into account
    /// </summary>
    /// <param name="coord">The Coordinate to convert</param>
    /// <returns>The World Space position of the given Coordinate</returns>
    private Vector3 CoordToSpacedPos(NodeCoordinate coord)
    {
        var pos = coord.ToVector3();
        pos.x = pos.x * m_ObjSize + (m_Spacing * (pos.x - 1));
        pos.z = pos.z * m_ObjSize + (m_Spacing * (pos.z - 1));
        return pos;
    }

    /// <summary>
    /// Does the board have any empty nodes?
    /// </summary>
    /// <returns></returns>
    public bool HasEmptyNodes()
    {
        return m_HasEmptyNodes;
    }

    /// <summary>
    /// Sets the size of the connections between the given nodes
    /// </summary>
    /// <param name="nodes">The nodes that are connected</param>
    /// <param name="dir">The direction the nodes are connected in</param>
    public void SizeConnections(Node[] nodes, NodeDirections dir)
    {
        var min = 2;
        var max = XSize;
        var actual = nodes.Length;
        var lerp = (actual - min) / (float)(max - min);

        // We won't need to do the last node's connect   
        for (int i = 0; i < nodes.Length; i++)
        {
            m_NodeViewDict[nodes[i]].SetConnectionSize(dir, lerp); 
        }
    }
}