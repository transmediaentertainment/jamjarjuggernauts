using TransTech.Delegates;
using UnityEngine;
using TransTech.System.Debug;

/// <summary>
/// Singleton class that fires events for mouse clicks.
/// </summary>
public class InputMouseClickEvents : MonoSingleton<InputMouseClickEvents>
{
	/// <summary>
	/// Occurs when the left mouse button is pressed down.
	/// </summary>
    public event VoidDelegate LeftMouseDown;
	
	/// <summary>
	/// Occurs when the left mouse button is held down.
	/// </summary>
    public event VoidDelegate LeftMouseHeld;
	
	/// <summary>
	/// Occurs when the left mouse button is released.
	/// </summary>
    public event VoidDelegate LeftMouseUp;
	
	/// <summary>
	/// Occurs when the right mouse button is pressed down.
	/// </summary>
    public event VoidDelegate RightMouseDown;
	
	/// <summary>
	/// Occurs when the right mouse button is held down.
	/// </summary>
    public event VoidDelegate RightMouseHeld;
	
	/// <summary>
	/// Occurs when the right mouse button is released.
	/// </summary>
    public event VoidDelegate RightMouseUp;
	
	/// <summary>
	/// Occurs when the middle mouse button is pressed down.
	/// </summary>
    public event VoidDelegate MiddleMouseDown;
	
	/// <summary>
	/// Occurs when the middle mouse button is held down.
	/// </summary>
    public event VoidDelegate MiddleMouseHeld;
	
	/// <summary>
	/// Occurs when the middle mouse button is released.
	/// </summary>
    public event VoidDelegate MiddleMouseUp;

#if !UNITY_IOS && !UNITY_ANDROID

    private bool m_LeftMouse = false;
    private bool m_RightMouse = false;
    private bool m_MiddleMouse = false;
	
	/// <summary>
	/// Called on the first active frame by Unity.
	/// </summary>
    private void Awake()
    {
        NGUIInputReceiver.Instance.MouseClickEvent += HandleNGUIMouseClickEvent;
    }
	
	/// <summary>
	/// Handles the NGUI mouse click events.
	/// </summary>
	/// <param name='isDown'>If the button is down</param>
	/// <param name='id'>The button ID (number)</param>
    private void HandleNGUIMouseClickEvent(bool isDown, int id)
    {
        switch (id)
        {
            case -1:	// Left Mouse Button
                m_LeftMouse = isDown;
                if(m_LeftMouse)
                {
                    if (LeftMouseDown != null)
                        LeftMouseDown();
                }
                else
                {
                    if (LeftMouseUp != null)
                        LeftMouseUp();
                }
                break;
            case -2:	// Right Mouse Button
                m_RightMouse = isDown;
                if (m_RightMouse)
                {
                    if (RightMouseDown != null)
                        RightMouseDown();
                }
                else
                {
                    if (RightMouseUp != null)
                        RightMouseUp();
                }
                break;
            case -3:	// Middle Mouse Button
                m_MiddleMouse = isDown;
                if (m_MiddleMouse)
                {
                    if (MiddleMouseDown != null)
                        MiddleMouseDown();
                }
                else
                {
                    if (MiddleMouseUp != null)
                        MiddleMouseUp();
                }
                break;
            default:
                TTDebug.LogWarning("InputMouseClickEvents.cs : Unsupported mouse click ID : " + id);
                return;
        }
    }
	
	/// <summary>
	/// Called once per frame by Unity.
	/// </summary>
    private void Update()
    {
        if (m_LeftMouse)
            if (LeftMouseHeld != null)
                LeftMouseHeld();
        if (m_RightMouse)
            if (RightMouseHeld != null)
                RightMouseHeld();
        if (m_MiddleMouse)
            if (MiddleMouseHeld != null)
                MiddleMouseHeld();
    }
#endif
}