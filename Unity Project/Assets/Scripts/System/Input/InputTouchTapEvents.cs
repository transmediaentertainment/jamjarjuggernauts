using System.Collections.Generic;
using TransTech.Delegates.TicTac;
using TransTech.System.Input;
using UnityEngine;

/// <summary>
/// Singleton class that fires events for Touch Taps.
/// </summary>
public class InputTouchTapEvents : MonoSingleton<InputTouchTapEvents>
{
    /// <summary>
    /// Fired when a Tap begins.
    /// </summary>
    public event TouchStateDelegate TapBeganEvent;

    /// <summary>
    /// Fired when a Tap ends
    /// </summary>
    public event TouchStateDelegate TapEndedEvent;

    /// <summary>
    /// Fired when a Tap becomes stale
    /// </summary>
    public event TouchStateDelegate TapBecameStaleEvent;

    /// <summary>
    /// Internal storage for the current touches.
    /// </summary>
    private Dictionary<int, TouchState> m_TouchStates = new Dictionary<int, TouchState>(7);

    /// <summary>
    /// Called once per frame by Unity
    /// </summary>
    private void Update()
    {
        var touches = Input.touches;

        foreach (var touch in touches)
        {
            if (!m_TouchStates.ContainsKey(touch.fingerId))
            {
                var id = touch.fingerId;
                m_TouchStates.Add(id, new TouchState());
				m_TouchStates[id].NewTouchReceived(touch);
                if (TapBeganEvent != null)
                    TapBeganEvent(m_TouchStates[id]);

                m_TouchStates[id].TouchBecameStale += () => { if (TapBecameStaleEvent != null) { TapBecameStaleEvent(m_TouchStates[id]); } m_TouchStates.Remove(id); };
                m_TouchStates[id].DragTouchEnded += () => { m_TouchStates.Remove(id); };
                m_TouchStates[id].TapTouchEnded += () => { if (TapEndedEvent != null) TapEndedEvent(m_TouchStates[id]); m_TouchStates.Remove(id); };
            }
			else
			{
				m_TouchStates[touch.fingerId].NewTouchReceived(touch);
			}
        }
    }
}