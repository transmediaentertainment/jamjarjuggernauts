using TransTech.Delegates;
using TransTech.Delegates.Unity;
using UnityEngine;

/// <summary>
/// Singleton class that fires events for mouse movements.
/// </summary>
public class InputMouseMovementEvents : MonoSingleton<InputMouseMovementEvents>
{
	/// <summary>
	/// Fired when the scroll wheel moves.
	/// </summary>
    public event FloatDelegate ScrollWheelMovedEvent;
	
	/// <summary>
	/// Called on the first active frame by Unity.
	/// </summary>
    private void Awake()
    {
        NGUIInputReceiver.Instance.ScrollEvent += (delta) => { if (ScrollWheelMovedEvent != null) ScrollWheelMovedEvent(delta); };
    }
}