using System.Collections.Generic;
using TransTech.Delegates.TicTac;
using TransTech.System.Input;
using UnityEngine;

/// <summary>
/// Singleton class that fires events for Touch Drags.
/// </summary>
public class InputTouchDragEvents : MonoSingleton<InputTouchDragEvents>
{
    /// <summary>
    /// Fired when a touch is dragged.
    /// </summary>
    public event TouchStateDelegate TouchDragEvent;
	
	/// <summary>
	/// Fired when a touch drag ends.
	/// </summary>
	public event TouchStateDelegate TouchDragEndedEvent;

    /// <summary>
    /// Internal storage for the current touches.
    /// </summary>
    private Dictionary<int, TouchState> m_TouchStates = new Dictionary<int, TouchState>(7);

    /// <summary>
    /// Called once per frame by Unity
    /// </summary>
    private void Update()
    {
        var touches = Input.touches;

        foreach (var touch in touches)
        {
            // If we haven't added a touch for this finger ID, create and add one.
            if (!m_TouchStates.ContainsKey(touch.fingerId))
            {
                var id = touch.fingerId;
                m_TouchStates.Add(id, new TouchState());
                m_TouchStates[id].TouchDragged += () => { if (TouchDragEvent != null) TouchDragEvent(m_TouchStates[id]); };
				m_TouchStates[id].DragTouchEnded += () => { if (TouchDragEndedEvent != null) TouchDragEndedEvent(m_TouchStates[id]); };
            }
            m_TouchStates[touch.fingerId].NewTouchReceived(touch);
        }
    }
}