using UnityEngine;

namespace TransTech.System.Input
{
	/// <summary>
	/// Interface that requires a class handle a Raycast Hit
	/// </summary>
    public interface IRaycastReceiver
    {
        void HandleRaycastHit(RaycastHit hit);
    }
}