using TransTech.Delegates;
using UnityEngine;

namespace TransTech.System.Input
{
	/// <summary>
	/// The touch state.
	/// </summary>
    public class TouchState
    {
        /// <summary>
        /// Occurs when tap touch ended.
        /// </summary>
        public event VoidDelegate TapTouchEnded;

        /// <summary>
        /// Occurs when drag touch ended.
        /// </summary>
        public event VoidDelegate DragTouchEnded;

        /// <summary>
        /// Occurs every frame the touch is dragged.
        /// </summary>
        public event VoidDelegate TouchDragged;

        /// <summary>
        /// Occurs when touch became stale.
        /// </summary>
        public event VoidDelegate TouchBecameStale;

        /// <summary>
        /// ID of the touch
        /// </summary>
        public int ID { get; private set; }

        /// <summary>
        /// Starting position of the Touch
        /// </summary>
        public Vector2 StartPos { get; private set; }

        /// <summary>
        /// Current position of the Touch (last reported position)
        /// </summary>
        public Vector2 CurrentPos { get; private set; }

        /// <summary>
        /// Delta Position of the Touch, from Start Pos to Current Pos.
        /// </summary>
        public Vector2 DeltaPos
        {
            get
            {
                return StartPos - CurrentPos;
            }
        }

        /// <summary>
        /// The Delta movement from the Previous Frame ONLY
        /// </summary>
        public Vector2 LastFrameDelta { get; private set; }

        /// <summary>
        /// Is the touch in a dragging state?
        /// </summary>
        public bool IsDragging { get; private set; }

        /// <summary>
        /// Movement Threshold for a touch to become a drag in Centimeters
        /// </summary>
        private const float m_DragCmThreshold = 0.5f;

        /// <summary>
        /// The squared movement threshold for a touch to become a drag in pixels
        /// </summary>
        private float m_DragSqrPixelsThreshold;

        /// <summary>
        /// Timer for determining when a touch is stale
        /// </summary>
        private float m_LifeTimer;

        /// <summary>
        /// Max time before a Touch becomes stale
        /// </summary>
        private const float m_StaleTime = 0.2f;

        /// <summary>
        /// Is this Touch state stale?
        /// </summary>
        private bool m_IsStale = false;

        /// <summary>
        /// Is this Touch State stale?  Fires events when becoming stale.
        /// </summary>
        public bool IsStale
        {
            get { return m_IsStale; }
            set
            {
                if (!m_IsStale && value)
                {
                    if (IsDragging)
                    {
                        if (DragTouchEnded != null)
                            DragTouchEnded();
                    }
                    else
                    {
                        if (TapTouchEnded != null)
                            TapTouchEnded();
                    }

                    SystemEventCenter.Instance.UpdateEvent -= Update;
                }
                m_IsStale = value;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TransTech.System.Input.TouchState"/> class.
        /// </summary>
        public TouchState()
        {
            var dpi = Screen.dpi;
            var dpcm = dpi * 0.393700787f; // Converts the inches to cm
            m_DragSqrPixelsThreshold = dpcm * m_DragCmThreshold;
            m_DragSqrPixelsThreshold *= m_DragSqrPixelsThreshold;
            m_IsStale = true;
        }

        /// <summary>
        /// Initialise the touch.  ALlows re-use
        /// </summary>
        /// <param name='t'>
        /// The touch to initialise the state with
        /// </param>
        private void Init(Touch t)
        {
            ID = t.fingerId;
            m_LifeTimer = 0f;
            IsStale = false;
            StartPos = t.position;
            CurrentPos = t.position;
            IsDragging = false;
            SystemEventCenter.Instance.UpdateEvent += Update;
        }

        /// <summary>
        /// Handles a new touch event being received.
        /// </summary>
        /// <param name='t'>The new touch</param>
        public void NewTouchReceived(Touch t)
        {
            // If our touch is stale, re-initialise
            if (IsStale)
                Init(t);

            // Otherwise handle the event.
            else
            {
                switch (t.phase)
                {
                    case TouchPhase.Began:
                        LastFrameDelta = t.deltaPosition;
                        break;

                    case TouchPhase.Ended:
                        CurrentPos = t.position;
                        LastFrameDelta = t.deltaPosition;
                        IsStale = true;
                        break;

                    case TouchPhase.Moved:
                        CurrentPos = t.position;
                        LastFrameDelta = t.deltaPosition;

                        // We limit drag events until we have hit out threshold
                        if (!IsDragging && DeltaPos.sqrMagnitude > m_DragSqrPixelsThreshold)
                        {
                            IsDragging = true;
                        }
                        if (IsDragging && TouchDragged != null)
                            TouchDragged();
                        break;

                    case TouchPhase.Stationary:
                        LastFrameDelta = t.deltaPosition;
                        break;

                    case TouchPhase.Canceled:
                        CurrentPos = t.position;
                        LastFrameDelta = t.deltaPosition;
                        IsStale = true;
                        break;
                }
            }

            // Make sure to reset the life timer each time a new event is received
            // so the touch doesn't become stale.
            m_LifeTimer = 0f;
        }

        /// <summary>
        /// Called once per frame when hooked up to the system events system.
        /// </summary>
        /// <param name='deltaTime'> Delta time since last frame.</param>
        private void Update(float deltaTime)
        {
            m_LifeTimer += deltaTime;

            // If we're over our timer, set the touch to stale.
            // We do this just in case a touch ended or canceled event is missed.
            if (m_LifeTimer > m_StaleTime)
            {
                IsStale = true;
            }
        }
    }
}