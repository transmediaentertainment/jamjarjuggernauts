using System.Collections.Generic;
using TransTech.System.Input;
using UnityEngine;

/// <summary>
/// Singleton class that controls the input.
/// </summary>
public class InputController : MonoSingleton<InputController>
{
	/// <summary>
	/// The input raycast layers (post bitshift).
	/// </summary>
    private int m_RaycastLayers;
	
	/// <summary>
	/// The list of input layers to bitshift into raycastLayers.
	/// </summary>
    private List<int> m_CurrentLayers = new List<int>();
	
	/// <summary>
	/// The list of valid touch IDs
	/// </summary>
    private List<int> m_ValidTouchIDs = new List<int>();
	
	/// <summary>
	/// Invalidates all of the current touches.
	/// </summary>
    public void InvalidateCurrentTouches()
    {
        m_ValidTouchIDs.Clear();
    }
	
	/// <summary>
	/// Adds a raycast collision layer based on name.
	/// </summary>
	/// <param name='layerName'>The layer name.</param>
    public void AddRaycastCollisionLayer(string layerName)
    {
        int layer = LayerMask.NameToLayer(layerName);
        if (layer >= 0)
        {
            AddRaycastCollisionLayer(layer);
        }
    }
	
	/// <summary>
	/// Adds a raycast collision layer based on index.
	/// </summary>
	/// <param name='layer'>The layer index.</param>
    public void AddRaycastCollisionLayer(int layer)
    {
        if (m_CurrentLayers.Contains(layer))
            return;
        m_CurrentLayers.Add(layer);
        UpdateCollisionLayer();
    }
	
	/// <summary>
	/// Removes a raycast collision layer based on name.
	/// </summary>
	/// <param name='layerName'>The layer name.</param>
    public void RemoveRaycastCollisionLayer(string layerName)
    {
        int layer = LayerMask.NameToLayer(layerName);
        if (layer >= 0)
        {
            RemoveRaycastCollisionLayer(layerName);
        }
    }
	
	/// <summary>
	/// Removes a raycast collision layer based on index.
	/// </summary>
	/// <param name='layer'>The layer index.</param>
    public void RemoveRaycastCollisionLayer(int layer)
    {
        int index = m_CurrentLayers.FindIndex(i => i == layer);
        if (index >= 0)
        {
            m_CurrentLayers.RemoveAt(index);
            UpdateCollisionLayer();
        }
    }
	
	/// <summary>
	/// Updates the raycast layers by bitshifting the current input layers.
	/// </summary>
    private void UpdateCollisionLayer()
    {
        m_RaycastLayers = 0;
        for (int i = 0; i < m_CurrentLayers.Count; i++)
        {
            m_RaycastLayers = m_RaycastLayers | 1 << m_CurrentLayers[i];
        }
    }
	
	/// <summary>
	/// Registers the tap began for raycast event handler.
	/// </summary>
    public void RegisterTapBeganForRaycast()
    {
        InputTouchTapEvents.Instance.TapBeganEvent += TapBeganEventHandler;
    }
	
	/// <summary>
	/// Deregisters the tap began for raycast event handler.
	/// </summary>
    public void DeregisterTapBeganForRaycast()
    {
        InputTouchTapEvents.Instance.TapBeganEvent -= TapBeganEventHandler;
    }
	
	/// <summary>
	/// Registers the tap ended for raycast event handler.
	/// </summary>
    public void RegisterTapEndedForRaycast()
    {
        InputTouchTapEvents.Instance.TapEndedEvent += TapEndedEventHandler;
    }
	
	/// <summary>
	/// Deregisters the tap for raycast event handler.
	/// </summary>
    public void DeregisterTapForRaycast()
    {
        InputTouchTapEvents.Instance.TapEndedEvent -= TapEndedEventHandler;
    }
	
	/// <summary>
	/// Registers the left click for raycast event handler.
	/// </summary>
    public void RegisterLeftClickForRaycast()
    {
        InputMouseClickEvents.Instance.LeftMouseDown += LeftMouseDown;
    }
	
	/// <summary>
	/// Deregisters the left click for raycast event handler.
	/// </summary>
    public void DeregisterLeftClickForRaycast()
    {
        InputMouseClickEvents.Instance.LeftMouseDown -= LeftMouseDown;
    }
	
	/// <summary>
	/// Raycasts based on the mouse pos.
	/// </summary>
    private void LeftMouseDown()
    {
        var mousePos = Input.mousePosition;
        RaycastCameras(mousePos);
    }
	
	/// <summary>
	/// The handler for a tap began event.
	/// </summary>
	/// <param name='t'>The touch.</param>
    private void TapBeganEventHandler(TouchState t)
    {
        m_ValidTouchIDs.Add(t.ID);
    }
	
	/// <summary>
	/// The handler for a tap ended event.
	/// </summary>
	/// <param name='t'>The touch.</param>
    private void TapEndedEventHandler(TouchState t)
    {
        if (m_ValidTouchIDs.Contains(t.ID))
        {
            var screenPos = new Vector3(t.CurrentPos.x, t.CurrentPos.y);
            RaycastCameras(screenPos);
        }
    }
	
	/// <summary>
	/// Raycasts based on each camera, passing the data to the hitInterface.
	/// </summary>
	/// <param name='screenPos'>The screen position.</param>
    private void RaycastCameras(Vector3 screenPos)
    {
        var cameras = CameraManager.Instance.GetCamerasForRaycasting(screenPos);
        RaycastHit hit;
        foreach (var cam in cameras)
        {
            Ray ray = cam.ScreenPointToRay(screenPos);
            if (Physics.Raycast(ray, out hit))
            {
                var hitInterface = hit.collider.GetComponent(typeof(IRaycastReceiver)) as IRaycastReceiver;
                if (hitInterface != null)
                    hitInterface.HandleRaycastHit(hit);
            }
        }
    }
}