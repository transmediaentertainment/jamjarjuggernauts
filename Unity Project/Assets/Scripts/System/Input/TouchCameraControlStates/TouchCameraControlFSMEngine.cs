using TransTech.FiniteStateMachine;
using TransTech.System.Debug;
using UnityEngine;

namespace TransTech.System.Input
{
	/// <summary>
	/// The touch camera control FSM engine.
	/// </summary>
	public class TouchCameraControlFSMEngine : FSM
	{
		/// <summary>
		/// ID of the first touch
		/// </summary>
		private int m_TouchIDOne;
		/// <summary>
		/// ID of the second touch
		/// </summary>
		private int m_TouchIDTwo;
		
		/// <summary>
		/// State of the first touch
		/// </summary>
		private TouchState m_TouchStateOne;
		/// <summary>
		/// State of the second touch
		/// </summary>
		private TouchState m_TouchStateTwo;
		
		/// <summary>
		/// Default Idle State (for no touches)
		/// </summary>
		private IdleFSMState m_IdleState;
		/// <summary>
		/// Touch camera's panning state (for one touch)
		/// </summary>
		private TouchCameraPanFSMState m_PanState;
		/// <summary>
		/// Touch camera's zooming state (for two touches)
		/// </summary>
		private TouchCameraZoomFSMState m_ZoomState;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="TransTech.System.Input.TouchCameraControlFSMEngine"/> class.
		/// </summary>
		/// <param name='cam'>The main camera</param>
		/// <param name='minX'>The minimum x position (left horizontal side) the camera can transform to</param>
		/// <param name='maxX'>The maximum x position (right horizontal side) the camera can transform to</param>
		/// <param name='minY'>The minimum y position (height) the camera can transform to</param>
		/// <param name='maxY'>The maximum y position (height) the camera can transform to</param>
		/// <param name='minZ'>The minimum z postion (bottom vertical side) the camera can transform to</param>
		/// <param name='maxZ'>The maximum z position (top vertical side) the camera can transform to</param>
		/// <param name='zoomSpeed'>The modifier amount which will be multiplied against the zoom speed</param>
		public TouchCameraControlFSMEngine( Camera cam, float minX, float maxX, float minY, float maxY, float minZ, float maxZ, float zoomSpeed )
		{
			// Set the touch IDs to -1 (invalid)
			m_TouchIDOne = -1;
			m_TouchIDTwo = -1;
			
			// Hook up the events
			InputTouchTapEvents.Instance.TapBeganEvent += HandleTouchTapBegan;
			InputTouchTapEvents.Instance.TapEndedEvent += HandleTouchTapEnded;
			InputTouchTapEvents.Instance.TapBecameStaleEvent += HandleTouchTapEnded;
			InputTouchDragEvents.Instance.TouchDragEndedEvent += HandleTouchTapEnded;
			
			// Init the states
			m_IdleState = new IdleFSMState();
			m_PanState = new TouchCameraPanFSMState( cam, minX, maxX, minZ, maxZ );
			m_ZoomState = new TouchCameraZoomFSMState( cam.transform, minY, maxY, zoomSpeed );
		}
		
		/// <summary>
		/// Handles the touch tap began event
		/// </summary>
		/// <param name='touch'>The touch state to handle</param>
		private void HandleTouchTapBegan(TouchState touch)
		{
			// If we haven't assigned the first touch, assign the ID and state to the first touch
			if( m_TouchIDOne == -1 )
			{
				m_TouchIDOne = touch.ID;
				m_TouchStateOne = touch;
			}
			// Else if the first touch has been assign and the second touch hasn't, assign the ID and state to the second touch
			else if( m_TouchIDTwo == -1 )
			{
				m_TouchIDTwo = touch.ID;
				m_TouchStateTwo = touch;
			}
			
			UpdateState();
		}
		
		/// <summary>
		/// Handles the touch tap ended event
		/// </summary>
		/// <param name='touch'>The touch state to handle</param>
		private void HandleTouchTapEnded(TouchState touch)
		{
			// If the touch which ended is the first stored touch, remove it
			if( touch.ID == m_TouchIDOne )
			{
				m_TouchIDOne = -1;
				m_TouchStateOne = null;
			}
			// If the touch which ended is the second stored touch, remove it
			else if( touch.ID == m_TouchIDTwo )
			{
				m_TouchIDTwo = -1;
				m_TouchStateOne = null;
			}
			
			UpdateState();
		}
		
		/// <summary>
		/// Updates the state of the FSM based on how many touches are active
		/// </summary>
		private void UpdateState()
		{
			// If two touches are active, enter the zoom state
			if( m_TouchIDOne >= 0 && m_TouchIDTwo >= 0 )
			{
				if( Peek() != m_ZoomState )
				{
					NewState(m_ZoomState, m_TouchStateOne, m_TouchStateTwo);
				}
			}
			// If only one touch is active, enter the pan state
			else if( m_TouchIDOne >= 0 || m_TouchIDTwo >= 0 )
			{
				if( Peek() != m_PanState )
				{
					// Pass in the one valid touch, or return an error if netiher is valid
					if( m_TouchStateOne != null )
					{
						NewState(m_PanState, m_TouchStateOne);
					}
					else if( m_TouchStateTwo != null )
					{
						NewState(m_PanState, m_TouchStateTwo);
					}
					else
					{
						TTDebug.LogError( "TouchCameraControlFSMEngine.cs : touch states one and two shouldn't both be null!" );
					}
				}
			}
			// If no touches are active, enter the idle state
			else
			{
				if( Peek() != m_IdleState )
				{
					NewState(m_IdleState);
				}
			}
		}
	}
}