using TransTech.System.Debug;
using TransTech.FiniteStateMachine;
using UnityEngine;
using UnityExtensions;

namespace TransTech.System.Input
{
	/// <summary>
	/// The touch camera pan FSM state.
	/// </summary>
	public class TouchCameraPanFSMState : FSMState
	{
		/// <summary>
		/// ID of the touch
		/// </summary>
		private int m_TouchID;
		
		/// <summary>
		/// The position of the previous raycast
		/// </summary>
		private Vector3 m_PrevRayPos;
		/// <summary>
		/// The layer of the raycast plane
		/// </summary>
		private int m_RaycastLayer;
		/// <summary>
		/// The main camera (passed through)
		/// </summary>
		private Camera m_Camera;
		
		/// <summary>
		/// The minimum x position (left horizontal side) the camera can transform to
		/// </summary>
		private float m_MinX;
		/// <summary>
		/// The maximum x position (right horizontal side) the camera can transform to
		/// </summary>
		private float m_MaxX;
		/// <summary>
		/// The minimum z position (bottom vertical side) the camera can transform to
		/// </summary>
		private float m_MinZ;
		/// <summary>
		/// TThe maximum z position (top vertical side) the camera can transform to
		/// </summary>
		private float m_MaxZ;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="TransTech.System.Input.TouchCameraPanFSMState"/> class.
		/// </summary>
		/// <param name='cam'>The main camera</param>
		/// <param name='minX'>The minimum x position (left horizontal side) the camera can transform to</param>
		/// <param name='maxX'>The maximum x position (right horizontal side) the camera can transform to</param>
		/// <param name='minZ'>The minimum z postion (bottom vertical side) the camera can transform to</param>
		/// <param name='maxZ'>The maximum z position (top vertical side) the camera can transform to</param>
		public TouchCameraPanFSMState( Camera cam, float minX, float maxX, float minZ, float maxZ )
		{
			m_RaycastLayer = 1 << LayerMask.NameToLayer("CameraRaycastPlane");
			m_Camera = cam;
			
			m_MinX = minX;
			m_MaxX = maxX;
			m_MinZ = minZ;
			m_MaxZ = maxZ;
		}
		
		/// <summary>
		/// Override the FSM Enter function to store the touch, the touch's initial pos, and hook up the handle event
		/// </summary>
		/// <param name='args'>Expects 1 argument: a single touch state</param>
		public override void Enter (params object[] args)
		{
			// If the args contains anything other than a single touch state, throw an error
			if( args == null || args.Length != 1 || !(args[0] is TouchState) )
			{
				TTDebug.LogError( "TouchCameraPanFSMState.cs : Invalids args passed to enter" );
				return;
			}

			TouchState touch = args[0] as TouchState;
			
			if( m_TouchID == -1 )
			{
				// Raycast to set our prev pos to the touch state's start pos
				m_TouchID = touch.ID;
				var ray = m_Camera.ScreenPointToRay( touch.StartPos );
				RaycastHit hit;
				if( Physics.Raycast( ray, out hit, 100f, m_RaycastLayer ) )
				{
					m_PrevRayPos = hit.point;
				}
			}
			
			// Hook up the touch drag event
			InputTouchDragEvents.Instance.TouchDragEvent += HandleDrag;
		}
		
		/// <summary>
		/// Handles the drag event
		/// </summary>
		/// <param name='touch'>The touch state to handle</param>
		private void HandleDrag( TouchState touch )
		{
			if( m_TouchID != touch.ID )
			{
				return;
			}
			var ray = m_Camera.ScreenPointToRay( touch.CurrentPos );
			RaycastHit hit;
			if( Physics.Raycast( ray, out hit, 100f, m_RaycastLayer ) )
			{
				// Raycast to find where the touch has moved to, and apply the delta to the camera transform's position.
				var hitPos = hit.point;
				var delta = m_PrevRayPos - hitPos;
				var pos = m_Camera.transform.position;
				pos += delta;
				m_Camera.transform.position = pos;
				// Clamp the movement to the min and max x and z positons
				m_Camera.transform.SetX(Mathf.Clamp(m_Camera.transform.position.x, m_MinX, m_MaxX));
	       		m_Camera.transform.SetZ(Mathf.Clamp(m_Camera.transform.position.z, m_MinZ, m_MaxZ));
				// Set the previous raycast's positon to the hitPos + the delta
				m_PrevRayPos = hitPos + delta;
			}
			else 
			{
				TTDebug.LogError("TouchCameraPanFSMState.cs : Raycast didn't hit anything, expand size of raycase plane");
			}
		}
		
		/// <summary>
		/// Override the FSM Exit function to remove the touch ID and de-hook the drag handler
		/// </summary>
		public override void Exit ()
		{
			InputTouchDragEvents.Instance.TouchDragEvent -= HandleDrag;
			m_TouchID = -1;
		}
	}
}