using TransTech.System.Debug;
using TransTech.FiniteStateMachine;
using UnityEngine;
using UnityExtensions;

namespace TransTech.System.Input
{
	/// <summary>
	/// The touch camera zoom FSM state.
	/// </summary>
	public class TouchCameraZoomFSMState : FSMState
	{
		/// <summary>
		/// ID of the first touch
		/// </summary>
		private int m_TouchIDOne;
		/// <summary>
		/// ID of the second touch
		/// </summary>
		private int m_TouchIDTwo;
		
		/// <summary>
		/// The first touch's positon
		/// </summary>
		private Vector2 m_TouchOnePos;
		/// <summary>
		/// The second touch's position
		/// </summary>
		private Vector2 m_TouchTwoPos;
		
		/// <summary>
		/// The starting magnitude of the two touches
		/// </summary>
		private float m_StartMag;
		/// <summary>
		/// The current magnitude of the two touches
		/// </summary>
		private float m_CurrentMag;
		
		/// <summary>
		/// The inital y positon of the camera
		/// </summary>
		private float m_StartY;
		
		/// <summary>
		/// The transform of the main camera
		/// </summary>
		private Transform m_CameraTransform;
		/// <summary>
		/// The minimum y position (height) the camera can transform to</param>
		/// </summary>
		private float m_MinY;
		/// <summary>
		/// The maximum y position (height) the camera can transform to</param>
		/// </summary>
		private float m_MaxY;
		/// <summary>
		/// The modifier amount which will be multiplied against the zoom speed
		/// </summary>
		private float m_ZoomSpeed;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="TransTech.System.Input.TouchCameraZoomFSMState"/> class.
		/// </summary>
		/// <param name='cam'>The main camera's transform</param>
		/// <param name='minY'>The minimum y position (height) the camera can transform to</param>
		/// <param name='maxY'>The maximum y position (height) the camera can transform to</param>
		/// <param name='zoomSpeed'>The modifier amount which will be multiplied against the zoom speed</param>
		public TouchCameraZoomFSMState( Transform cam, float minY, float maxY, float zoomSpeed )
		{
			m_CameraTransform = cam;
			m_MinY = minY;
			m_MaxY = maxY;
			m_ZoomSpeed = zoomSpeed;
		}
		
		/// <summary>
		/// Override the FSM Enter function to store the touches, the touches' initial pos', and hook up the handle event
		/// </summary>
		/// <param name='args'>Expects 2 arguments: both touch states</param>
		public override void Enter (params object[] args)
		{
			// If the args contains anything other than two touch states, throw an error
			if( args == null || args.Length != 2 || !(args[0] is TouchState) || !(args[1] is TouchState) )
			{
				TTDebug.LogError( "TouchCameraZoomFSMState.cs : Invalids args passed to enter" );
				return;
			}
			
			TouchState touch1 = args[0] as TouchState;
			TouchState touch2 = args[1] as TouchState;
			
			m_TouchIDOne = touch1.ID;
			m_TouchIDTwo = touch2.ID;
			
			m_TouchOnePos = touch1.StartPos;
			m_TouchTwoPos = touch2.StartPos;
			
			// Store the start magnitude and y height
			m_StartMag = (touch1.StartPos - touch2.StartPos).magnitude;
			m_StartY = m_CameraTransform.position.y;
			
			// Set the current mag to the starting mag
			m_CurrentMag = m_StartMag;
			
			// Hook up the touch drag event
			InputTouchDragEvents.Instance.TouchDragEvent += HandleDrag;
		}
		
		/// <summary>
		/// Handles the drag event
		/// </summary>
		/// <param name='touch'>The touch state to handle</param>
		private void HandleDrag( TouchState touch )
		{
			bool dirty = false;
			
			if( touch.ID == m_TouchIDOne )
			{
				m_TouchOnePos = touch.CurrentPos;
				dirty = true;
			}
			else if( touch.ID == m_TouchIDTwo )
			{
				m_TouchTwoPos = touch.CurrentPos;
				dirty = true;
			}
			
			// If either touch changed position, update the current magniture, and set the camera's y pos to the difference multiplied by the zoom speed multiplier
			if( dirty )
			{
				m_CurrentMag = (m_TouchOnePos - m_TouchTwoPos).magnitude;
				
				var dif = m_CurrentMag - m_StartMag;
				
				m_CameraTransform.SetY(m_StartY + (-dif * m_ZoomSpeed));

        		m_CameraTransform.SetY(Mathf.Clamp(m_CameraTransform.position.y, m_MinY, m_MaxY));
			}
		}
		
		/// <summary>
		/// Override the FSM Exit function to remove the touch IDs and de-hook the drag handler
		/// </summary>
		public override void Exit ()
		{
			InputTouchDragEvents.Instance.TouchDragEvent -= HandleDrag;
			m_TouchIDOne = -1;
			m_TouchIDTwo = -1;
		}
	}
}