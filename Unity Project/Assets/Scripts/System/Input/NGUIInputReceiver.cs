using UnityEngine;
using TransTech.Delegates;

/// <summary>
/// Class that receives then propagates input events from NGUI.  
/// NGUI only passes them on if NGUI doesn't use the event first.
/// </summary>
public class NGUIInputReceiver : MonoSingleton<NGUIInputReceiver> 
{
    /// <summary>
    /// Event fired when a mouse click event is fired.  Passes in whether the mouse was down, and the index of the button
    /// </summary>
    public event BoolIntDelegate MouseClickEvent;
    /// <summary>
    /// Event fired when the mouse scrolls it's wheel, passes in the delta movement
    /// </summary>
    public event FloatDelegate ScrollEvent;

    /// <summary>
    /// Initializes the object (called from MonoSingleton base class)
    /// </summary>
    public override void Init()
    {
        // Set ourselves as the fallthrough object for NGUI
        UICamera.fallThrough = gameObject;
    }
    
    /// <summary>
    /// Called by NGUI when a mouse button is clicked or released
    /// </summary>
    /// <param name="isDown">Is the button down or up</param>
    private void OnPress(bool isDown)
    {
        if (MouseClickEvent != null)
        {
            MouseClickEvent(isDown, UICamera.currentTouchID);
        }
    }

    /// <summary>
    /// Called by NGUI when a mouse up event happens over the same object the mouse down event occurred on
    /// </summary>
    private void OnClick()
    {
       // Debug.Log("OnClick : " + UICamera.currentTouchID);
    }

    /// <summary>
    /// Called by NGUI when a second mouse click occurrs within a fourth of a second.
    /// </summary>
    private void OnDoubleClick()
    {
        //Debug.Log("OnDoubleClick");
    }

    /// <summary>
    /// Called by NGUI when a mouse up event happens over the same object as the mouse down event, but doesn't fire again 
    /// until a mouse down event occurrs over another object
    /// </summary>
    /// <param name="selected"></param>
    private void OnSelect(bool selected)
    {
       // Debug.Log("OnSelect : " + selected);
    }

    /// <summary>
    /// Sent by NGUI when the mouse is moving between an OnPress(true) and OnPress(false) event
    /// </summary>
    /// <param name="delta"></param>
    private void OnDrag(Vector2 delta)
    {
        //Debug.Log("OnDrag : " + delta);
    }

    /// <summary>
    /// Sent out to the collider under the mouse or touch when OnPress(false) is called over a different 
    /// collider than triggered the OnPress(true) event. The passed parameter is the game object of the 
    /// collider that received the OnPress(true) event.
    /// </summary>
    /// <param name="drag">The Gameobject the original OnPress(true) event occurred on</param>
    private void OnDrop(GameObject drag)
    {
       // Debug.Log("OnDrop : " + drag.name);
    }

    /// <summary>
    /// Sent by NGUI on submission from a UIInput object
    /// </summary>
    /// <param name="text"></param>
    private void OnInput(string text)
    {
        Debug.Log("OnInput : " + text);
    }

    /// <summary>
    /// Sent after the mouse hovers over a collider for more than the tooltipdely value
    /// </summary>
    /// <param name="show">Is it showing or hiding?</param>
    private void OnTooltip(bool show)
    {
       // Debug.Log("OnTooltip : " + show);
    }

    /// <summary>
    /// Sent by NGUI when the mouse wheel moves
    /// </summary>
    /// <param name="delta">The delta movement of the mouse wheel</param>
    private void OnScroll(float delta)
    {
        if (ScrollEvent != null)
            ScrollEvent(delta);
    }

    /// <summary>
    /// Sent by NGUI when keyboard or Controller input is used
    /// </summary>
    /// <param name="key"></param>
    private void OnKey(KeyCode key)
    {
       // Debug.Log("OnKey : " + key);
    }
	
}
