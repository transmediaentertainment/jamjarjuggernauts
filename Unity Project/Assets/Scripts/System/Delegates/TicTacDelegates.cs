using TransTech.Gameplay.Players;
using TransTech.Nodes;
using TransTech.System.Input;

namespace TransTech.Delegates.TicTac
{
    public delegate bool NodeTestDelegate(Node node);

    public delegate void PlayerDelegate(Player player);

    public delegate void TouchStateDelegate(TouchState touch);
}