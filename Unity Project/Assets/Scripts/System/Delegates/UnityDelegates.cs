using UnityEngine;

namespace TransTech.Delegates.Unity
{
    public delegate void Vector2Delegate(Vector2 vec);

    public delegate void TouchDelegate(Touch touch);
}