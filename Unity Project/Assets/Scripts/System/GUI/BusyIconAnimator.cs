using UnityEngine;
using System.Collections;

/// <summary>
/// Class to animate the network busy icon
/// </summary>
public class BusyIconAnimator : MonoBehaviour 
{
    private bool m_IsAnimating;
    private float m_CurrentRotation;
    
    public GameObject m_PlaneObject;
    private const float m_Speed = 0.08f;

    void OnSpawned()
    {
        BeginAnimating();
    }

    void OnDespawned()
    {
        StopAnimating();
    }

    public void BeginAnimating()
    {
        m_CurrentRotation = 0f;
        m_IsAnimating = true;
        StartCoroutine(Animate());
    }

    public void StopAnimating()
    {
        m_IsAnimating = false;
    }

    private IEnumerator Animate()
    {
        while (m_IsAnimating)
        {
            yield return new WaitForSeconds(m_Speed);
            m_CurrentRotation += 45f;
            transform.rotation = Quaternion.AngleAxis(m_CurrentRotation, Vector3.forward);
        }
    }
}
