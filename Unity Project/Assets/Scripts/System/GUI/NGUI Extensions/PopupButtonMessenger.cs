using TransTech.Delegates;
using UnityEngine;

/// <summary>
/// Passes the index of the button pressed on a popup to the popup.
/// </summary>
public class PopupButtonMessenger : MonoBehaviour
{
	/// <summary>
	/// The button's index (assigned after creation in the popup).
	/// </summary>
    [SerializeField]
    private int m_ButtonIndex;
	
	/// <summary>
	/// Fired when the button is pressed, passing its index through.
	/// </summary>
    public event IntDelegate ButtonPressed;
	
	/// <summary>
	/// Init button with an index.
	/// </summary>
	/// <param name='buttonIndex'>The button's index.</param>
    public void Init(int buttonIndex)
    {
        m_ButtonIndex = buttonIndex;
    }
	
	/// <summary>
	/// Called by the button when it is pressed.
	/// </summary>
    public void PopupButtonPressed()
    {
        Debug.Log("Popup pressed  : " + m_ButtonIndex);
        if (ButtonPressed != null)
            ButtonPressed(m_ButtonIndex);
    }
}