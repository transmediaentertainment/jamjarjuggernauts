using TransTech.Gameplay;
using TransTech.Gameplay.Players;
using TransTech.System.Debug;
using UnityEngine;

public enum PlayerPanelTypes
{
    Default,
    ThreeSacrifices,
    MutinyMeter
}

/// <summary>
/// Controls and interfaces with the Player Status Panel in the scene.
/// </summary>
public class PlayerStatusPanel : MonoBehaviour
{
    /// <summary>
    /// Background object of the panel
    /// </summary>
    [SerializeField]
    private UISprite m_Background;

    /// <summary>
    /// Label object used to display the Player's info/status
    /// </summary>
    [SerializeField]
    private UILabel m_Label;

    /// <summary>
    /// Slider used to display Mutiny Meter (not in all configurations)
    /// </summary>
    [SerializeField]
    private UISlider m_Slider;

    /// <summary>
    /// Colour to tint the background when the Player is the active one
    /// </summary>
    [SerializeField]
    private Color m_ActiveColor = new Color(0.8901f, 0.6f, 0.2666f);

    /// <summary>
    /// Colour to tint the backgroun when the Player is not the active one
    /// </summary>
    [SerializeField]
    private Color m_InactiveColor = new Color(0.6039f, 0.6549f, 0.8588f);

    /// <summary>
    /// Colour to tint the text when the Player is the active one
    /// </summary>
    [SerializeField]
    private Color m_ActiveTextColor = Color.white;

    /// <summary>
    /// Colour to tint the text when the Player is not the active one
    /// </summary>
    [SerializeField]
    private Color m_InactiveTextColor = Color.gray;

    [SerializeField]
    private PlayerPanelTypes m_PlayerPanelType;

    /// <summary>
    /// The height of the background panel
    /// </summary>
    public float BackgroundHeight { get { return m_Background.localSize.y; } }

    /// <summary>
    /// The width of the background panel
    /// </summary>
    public float BackgroundWidth { get { return m_Background.localSize.x; } }

    /// <summary>
    /// Called by Unity when the script is created
    /// </summary>
    private void Awake()
    {
        SetPanelActive(false);
    }

    /// <summary>
    /// Turns the panel on or off
    /// </summary>
    /// <param name="isActive">True for on, false for off</param>
    public void SetPanelActive(bool isActive)
    {
        m_Background.color = isActive ? m_ActiveColor : m_InactiveColor;
        m_Label.color = isActive ? m_ActiveTextColor : m_InactiveTextColor;
    }

    /// <summary>
    /// Sets the text to be displayed on the panel
    /// </summary>
    /// <param name="text">The text to display</param>
    public void SetPanelText(Player player)
    {
        if (m_PlayerPanelType == PlayerPanelTypes.ThreeSacrifices)
        {
            var fsm = GameController.Instance.FSM;

            // The FSM hasn't been setup by the game controller yet.  Little hack to get around that fact. - JM
            if (fsm == null)
            {
                m_Label.text = player.Name + "\nScore: " + player.CurrentScore + "\nPieces: " + player.TokensInHand + "\nSacrifices: 3";
            }
            else
            {
                var strat = fsm.SacrificeStrategy as ThreePerGameSacrificeStrategy;
                if (strat == null)
                {
                    TTDebug.LogError("PlayerStatusPanel.cs : Error Retreiving ThreePerGameSacrificeStrategy object.  Received null");
                }
                else
                {
                    var t = player.Name + "\nScore: " + player.CurrentScore + "\nPieces: " + player.TokensInHand + "\nSacrifices: ";
                    switch (player.PlayerNumber)
                    {
                        case 1:
                            t += strat.PlayerOneCount;
                            break;

                        case 2:
                            t += strat.PlayerTwoCount;
                            break;

                        case 3:
                            t += strat.PlayerThreeCount;
                            break;

                        case 4:
                            t += strat.PlayerFourCount;
                            break;

                        default:
                            TTDebug.LogError("PlayerStatusPanel.cs : Invalid player number received : " + player.PlayerNumber);
                            break;
                    }
                    m_Label.text = t;
                }
            }
        }
        else
        {
            m_Label.text = player.Name + "\nScore: " + player.CurrentScore + "\nPieces: " + player.TokensInHand;
        }
    }

    /// <summary>
    /// Sets the slider to the given value
    /// </summary>
    /// <param name="value">A 0 - 1 value for the slider</param>
    public void SetPanelSliderValue(float value)
    {
        if (m_Slider == null)
        {
            TTDebug.LogWarning("PlayerStatusPanel.cs : No Slider available on this panel");
            return;
        }
        m_Slider.value = Mathf.Clamp01(value);
        m_Slider.ForceUpdate();
    }
}