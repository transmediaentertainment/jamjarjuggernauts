using UnityEngine;

/// <summary>
/// Class to handle chosing the HD or SD atlas depending on device screen res
/// </summary>
public class NGUIAtlasManager : MonoBehaviour
{
	/// <summary>
	/// True if res requires HD res
	/// </summary>
    public static bool IsHD { get; private set; }

	/// <summary>
	/// The main reference atlas.
	/// </summary>
    public UIAtlas m_MainAtlas;
	/// <summary>
	/// The main reference regular font.
	/// </summary>
    public UIFont m_MainRegularFont;
	/// <summary>
	/// The main reference heading font.
	/// </summary>
    public UIFont m_MainHeadingFont;
	
	/// <summary>
	/// The SD atlas.
	/// </summary>
    public UIAtlas m_SDAtlas;
	/// <summary>
	/// The SD regular font.
	/// </summary>
    public UIFont m_SDRegularFont;
	/// <summary>
	/// The SD heading font.
	/// </summary>
    public UIFont m_SDHeadingFont;
	/// <summary>
	/// The HD atlas.
	/// </summary>
    public UIAtlas m_HDAtlas;
	/// <summary>
	/// The HD regular font.
	/// </summary>
    public UIFont m_HDRegularFont;
	/// <summary>
	/// The HD heading font.
	/// </summary>
    public UIFont m_HDHeadingFont;

#if UNITY_EDITOR
    public iPhoneGeneration m_TestGeneration = iPhoneGeneration.iPhone5;
#endif

    private void Awake()
    {
        Application.targetFrameRate = 60;
#if UNITY_IPHONE
		SetUpIOSAtlas();
#endif
    }

#if UNITY_IPHONE
	private void SetUpIOSAtlas()
	{
		var gen = iPhone.generation;
#if UNITY_EDITOR
		gen = m_TestGeneration;
#endif

		if(gen == iPhoneGeneration.iPad1Gen || gen == iPhoneGeneration.iPhone || gen == iPhoneGeneration.iPhone3G
			|| gen == iPhoneGeneration.iPodTouch1Gen || gen == iPhoneGeneration.iPodTouch2Gen)
			return;

		if(gen == iPhoneGeneration.iPad2Gen || gen == iPhoneGeneration.iPadMini1Gen || gen == iPhoneGeneration.iPhone3GS || gen == iPhoneGeneration.iPodTouch3Gen)
		{
			// SD
			m_MainAtlas.replacement = m_SDAtlas;
			m_MainRegularFont.replacement = m_SDRegularFont;
			m_MainHeadingFont.replacement = m_SDHeadingFont;
			IsHD = false;
		}
		else if(gen == iPhoneGeneration.iPad3Gen || gen == iPhoneGeneration.iPad4Gen || gen == iPhoneGeneration.iPhone4 || gen == iPhoneGeneration.iPhone4S ||
			gen == iPhoneGeneration.iPhone5 || gen == iPhoneGeneration.iPodTouch4Gen || gen == iPhoneGeneration.iPodTouch5Gen)
		{
			// HD
			m_MainAtlas.replacement = m_HDAtlas;
			m_MainRegularFont.replacement = m_HDRegularFont;
			m_MainHeadingFont.replacement = m_HDHeadingFont;
			IsHD = true;
		}
		else if(gen == iPhoneGeneration.iPhoneUnknown || gen == iPhoneGeneration.iPodTouchUnknown || gen == iPhoneGeneration.iPadUnknown)
		{
			m_MainAtlas.replacement = m_HDAtlas;
			m_MainRegularFont.replacement = m_HDRegularFont;
			m_MainHeadingFont.replacement = m_HDHeadingFont;
			IsHD = true;
		}
	}
#endif
}