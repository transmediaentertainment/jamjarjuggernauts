using PathologicalGames;
using TransTech.Delegates;
using TransTech.System.Debug;
using UnityEngine;

/// <summary>
/// Custom UI Popup with buttons and limited auto-sizing capabilities.
/// </summary>
public class Popup : MonoBehaviour
{
	/// <summary>
	/// The main anchor which the label and button anchor are attached to.
	/// </summary>
    public Transform m_Anchor;
	/// <summary>
	/// The prefab for the buttons which will spawn on the popup.
	/// </summary>
    public GameObject m_ButtonPrefab;
	/// <summary>
	/// The popup's text label.
	/// </summary>
    public UILabel m_Label;
	/// <summary>
	/// The anchor which the buttons are attached to.
	/// </summary>
    public Transform m_ButtonAnchor;
	/// <summary>
	/// The background sliced sprite.
	/// </summary>
    public GameObject m_Background;
	/// <summary>
	/// The buttons.
	/// </summary>
    public UIButton[] m_Buttons;
	
	/// <summary>
	/// The button's edge padding.
	/// </summary>
    private float m_ButtonEdgePadding = 20f;
	/// <summary>
	/// The minimum height of the buttons.
	/// </summary>
    private float m_MinButtonHeight = 44f;
	/// <summary>
	/// The minimum width of the buttons.
	/// </summary>
    private float m_MinButtonWidth = 60f;
	/// <summary>
	/// The actual height of the buttons.
	/// </summary>
    private float m_ButtonHeight = 0f;
	/// <summary>
	/// The actual width of the buttons.
	/// </summary>
    private float m_ButtonWidth = 0f;
	/// <summary>
	/// The height of all the buttons plus padding.
	/// </summary>
    private float m_ButtonAnchorHeight = 0f;
	/// <summary>
	/// The width of all the buttons plus padding.
	/// </summary>
    private float m_ButtonAnchorWidth = 0f;
	
	/// <summary>
	/// The popup's edge padding.
	/// </summary>
    private float m_PopupEdgePadding = 50f;
	/// <summary>
	/// The popup's interior padding.
	/// </summary>
    private float m_PopupInteriorPadding = 20f;
	/// <summary>
	/// The minimum height of the background.
	/// </summary>
    private float m_MinBackgroundHeight = 100f;
	/// <summary>
	/// The minimum width of the background.
	/// </summary>
    private float m_MinBackgroundWidth = 300f;
	/// <summary>
	/// The actual height of the background.
	/// </summary>
    private float m_BackgroundHeight = 0f;
	/// <summary>
	/// The actual height of the background.
	/// </summary>
    private float m_BackgroundWidth = 0f;
	
	/// <summary>
	/// Fired when a popup button is pressed (passing through the button's index).
	/// </summary>
    public event IntDelegate PopupButtonPressedEvent;
	
	/// <summary>
	/// The popup prefab (used when adding a popup through the editor tool).
	/// </summary>
    private static GameObject m_PopupPrefab;
	/// <summary>
	/// The center UI anchor the popup attaches to (used when adding a popup through the editor tool).
	/// </summary>
    private static Transform m_PopupAnchor;
	
	/// <summary>
	/// Checks if the popup has initted (used when adding a popup through the editor tool).
	/// </summary>
    private bool m_IsInit = false;
	
	/// <summary>
	/// Static function to create a popup.
	/// </summary>
	/// <returns>A reference to the popup (to allow cleanup).</returns>
	/// <param name='labelText'>The text label to appear in the popup.</param>
	/// <param name='buttonText'>A params array of text to appear in each button (the length of this array determines the number of buttons on the popup).</param>
    public static Popup CreatePopup(string labelText, params string[] buttonText)
    {
		// Ensure the popup prefab exists
        if (m_PopupPrefab == null)
        {
            m_PopupPrefab = Resources.Load("UI/Popups/Popup") as GameObject;
            if (m_PopupPrefab == null)
            {
                TTDebug.LogError("Popup.cs : Unable to load popup prefab from resources");
            }
        }
        
		// If it's playing and the popup anchor is not assigned, assign it
        if (Application.isPlaying && m_PopupAnchor == null)
        {
            var go = GameObject.FindWithTag("CenterAnchor");
            if (go == null)
            {
                TTDebug.LogError("popup.cs : Unable to find popupAnchor object with tag");
            }
            else
            {
                m_PopupAnchor = go.transform;
            }
        }
		
        Transform t = null;
        // If we aren't playing the game and we're in the editor, we want to add it to the scene.
        if (Application.isEditor && !Application.isPlaying)
        {
            var go = Instantiate(m_PopupPrefab) as GameObject;
            t = go.transform;
        }
        else // Otherwise we use the spawn pool.
        {
            t = PoolManager.Pools["GUI"].Spawn(m_PopupPrefab.transform);
        }
        var popup = t.gameObject.GetComponent<Popup>();

        popup.transform.parent = m_PopupAnchor;
        popup.transform.localScale = Vector3.one;
        popup.transform.localPosition = Vector3.zero;

        popup.Init(labelText, buttonText);

        return popup;
    }
	
	/// <summary>
	/// Static function to return a popup to the spawn pool (destroy it).
	/// </summary>
	/// <param name='popup'>The popup to return to the pool</param>
    public static void ReturnPopup(Popup popup)
    {
        PoolManager.Pools["GUI"].Despawn(popup.transform);
    }
	
	/// <summary>
	/// Called on the second active frame by Unity
	/// </summary>
    private void Start()
    {
        if (!m_IsInit)
        {
            for (int i = 0; i < m_Buttons.Length; i++)
            {
                var messenger = m_Buttons[i].GetComponent<PopupButtonMessenger>();
                messenger.Init(i);
                messenger.ButtonPressed += PopupButtonPressed;
            }
            m_IsInit = true;
        }
    }

    /// <summary>
    /// After creating a popup, Init fills the label and button text and autosizes the popup
    /// </summary>
    /// <param name='labelText'>The text label to appear in the popup.</param>
    /// <param name='buttonsText'>A params array of text to appear in each button (the length of this array determines the number of buttons on the popup).</param>
    public void Init(string labelText, params string[] buttonsText)
    {
        m_BackgroundHeight = m_MinBackgroundHeight;
        m_BackgroundWidth = m_MinBackgroundWidth;

        m_ButtonHeight = m_MinButtonHeight;
        m_ButtonWidth = m_MinButtonWidth;

        if (labelText == null || labelText.Length <= 0)
        {
            Debug.LogError("popup.cs : no labelText passed to Popup");
        }

        m_Label.text = labelText;

        if (buttonsText == null || buttonsText.Length <= 0)
        {
            Debug.LogError("popup.cs : no buttonText passed to Popup");
        }

        m_Buttons = new UIButton[buttonsText.Length];

        // Spawn and init buttons and calculate max widths and heights based on label text
        for (int i = 0; i < buttonsText.Length; i++)
        {
            // Spawn button objects and put UIButton components into array
            // If we aren't playing the game and we're in the editor, we want to add it to the scene.
            Transform t = null;
            if (Application.isEditor && !Application.isPlaying)
            {
                var g = Instantiate(m_ButtonPrefab) as GameObject;
                t = g.transform;
            }
            else
            {
                t = PoolManager.Pools["GUI"].Spawn(m_ButtonPrefab.transform);
            }
            m_Buttons[i] = t.gameObject.GetComponent<UIButton>();

            // Get the gameobject
            var go = m_Buttons[i].gameObject;

            // Assign transform parent and scale
            m_Buttons[i].transform.parent = m_ButtonAnchor;
            m_Buttons[i].transform.localScale = Vector3.one;

            // Assign Event callbacks to tell which button was pressed
            var messageScript = go.GetComponent<UIButtonMessage>();
            messageScript.target = go;
            messageScript.functionName = "PopupButtonPressed";

            var popupButtonMessengerScript = go.GetComponent<PopupButtonMessenger>();
            popupButtonMessengerScript.Init(i);
            popupButtonMessengerScript.ButtonPressed += PopupButtonPressed;

            // Get the label
            var buttonLabel = go.GetComponentInChildren<UILabel>();

            // Assign the text
            buttonLabel.text = buttonsText[i];

            // Get the pixel size based on text in label
            Vector2 buttonLabelSize = buttonLabel.localSize;//.bitmapFont.CalculatePrintedSize(buttonLabel.text);

            // Assign height and width to match the largest button
            var buttonLabelHeight = buttonLabelSize.y + m_ButtonEdgePadding;
            if (buttonLabelHeight > m_ButtonHeight)
            {
                m_ButtonHeight = buttonLabelHeight;
            }
            var buttonLabelWidth = buttonLabelSize.x + m_ButtonEdgePadding;
            if (buttonLabelWidth > m_ButtonWidth)
            {
                m_ButtonWidth = buttonLabelWidth;
            }
        }

        // TODO: Add a vertical button layout mode as well as the currently implemented horizontal button layout

        // Position the button anchor to center the buttons
        m_ButtonAnchorHeight = m_ButtonHeight;
        m_ButtonAnchorWidth = (m_Buttons.Length * m_ButtonWidth) + ((m_Buttons.Length - 1) * m_ButtonEdgePadding);
        m_ButtonAnchor.transform.localPosition = new Vector3(-m_ButtonAnchorWidth * 0.5f, -40f, 0f);

        // Get the pixel size based on text in label
        Vector2 labelSize = m_Label.localSize;//bitmapFont.CalculatePrintedSize(m_Label.text);

        // Assign width if label is larger the minimum
        var labelWidth = labelSize.x + m_PopupEdgePadding;
        if (labelWidth > m_BackgroundWidth)
        {
            m_BackgroundWidth = labelWidth;
        }

        // Assign width if combined button width is larger than minimum or text label width
        if (m_ButtonAnchorWidth + m_PopupEdgePadding > m_BackgroundWidth)
        {
            m_BackgroundWidth = m_ButtonAnchorWidth + m_PopupEdgePadding;
        }

        // Assign height if label and button size is larger than the minimum
        var labelHeight = labelSize.y;
        if (m_PopupEdgePadding + labelHeight + m_PopupInteriorPadding + m_ButtonAnchorHeight > m_BackgroundHeight)
        {
            m_BackgroundHeight = m_PopupEdgePadding + labelHeight + m_PopupInteriorPadding + m_ButtonAnchorHeight;
        }

        // Apply the scale to the background
	    var backgroundSprite = m_Background.GetComponent<UISprite>();
        //m_Background.transform.localScale = new Vector3(m_BackgroundWidth, m_BackgroundHeight, 1f);
        backgroundSprite.width = (int)m_BackgroundWidth;
	    backgroundSprite.height = (int) m_BackgroundHeight;

        m_Anchor.transform.localPosition = new Vector3(0f, m_BackgroundHeight * 0.5f, 0f);

        m_Label.transform.localPosition = new Vector3(0f, -m_PopupEdgePadding * 0.5f - labelHeight * 0.5f, 0f);
        m_ButtonAnchor.transform.localPosition = new Vector3(m_ButtonAnchor.transform.localPosition.x, -m_PopupEdgePadding * 0.5f - labelHeight - m_PopupInteriorPadding - m_ButtonHeight * 0.5f, m_ButtonAnchor.transform.localPosition.z);

        // Scale and position the buttons
        for (int i = 0; i < m_Buttons.Length; i++)
        {
            // Scale the width and height of the button background sprite and collider to the largest button size
            //var background = m_Buttons[i].gameObject.GetComponentInChildren<UISlicedSprite>();
            //background.transform.localScale = new Vector3(m_ButtonWidth, m_ButtonHeight, 1f);
            var background = m_Buttons[i].GetComponentInChildren<UISprite>();
            background.width = (int)m_ButtonWidth;
            background.height = (int) m_ButtonHeight;
            var boxCollider = m_Buttons[i].collider as BoxCollider;
            boxCollider.size = new Vector3(m_ButtonWidth, m_ButtonHeight, 1f);

            // Position the buttons along the bottom of the popup
            m_Buttons[i].transform.localPosition = new Vector3(i * (m_ButtonWidth + m_ButtonEdgePadding) + m_ButtonWidth * 0.5f, 0f, 0f);
        }

        m_IsInit = true;
    }
	
	/// <summary>
	/// Called by the pool manager when the popup is despawned
	/// </summary>
    private void OnDespawned()
    {
        for (var i = 0; i < m_Buttons.Length; i++)
        {
            m_Buttons[i].GetComponent<PopupButtonMessenger>().ButtonPressed -= PopupButtonPressed;
            m_Buttons[i].transform.parent = null;
            m_Buttons[i].gameObject.SetActive(false);
            PoolManager.Pools["GUI"].Despawn(m_Buttons[i].transform);
        }
        m_Buttons = null;
    }
	
	/// <summary>
	/// Fired when a popup button is pressed, passing its index through.
	/// </summary>
	/// <param name='buttonIndex'>The button's index.</param>
    private void PopupButtonPressed(int buttonIndex)
    {
        if (PopupButtonPressedEvent != null)
            PopupButtonPressedEvent(buttonIndex);
    }
}