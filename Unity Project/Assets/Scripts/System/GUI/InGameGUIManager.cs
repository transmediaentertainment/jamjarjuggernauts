using Holoville.HOTween;
using System.Collections;
using System.Collections.Generic;
using TransTech.Delegates;
using TransTech.Gameplay.Players;
using TransTech.System.Debug;
using UnityEngine;

/// <summary>
/// Class to handle and manage the in game GUI
/// </summary>
public class InGameGUIManager : MonoBehaviour
{
	/// <summary>
	/// The sidepanel menu button.
	/// </summary>
	public UIButton m_MenuButton;
	/// <summary>
	/// The sidepanel end turn button.
	/// </summary>
    public UIButton m_EndTurnButton;
	/// <summary>
	/// If true the text on end turn changes to "end game".
	/// </summary>
    private bool m_EndTurnButtonIsEndGameButton = false;
	/// <summary>
	/// The end turn button label.
	/// </summary>
    private UILabel m_EndTurnButtonLabel;
	/// <summary>
	/// The sidepanel sacrifice button.
	/// </summary>
    public UIButton m_SacrificeButton;
	/// <summary>
	/// The sidepanel cancel button.
	/// </summary>
    public UIButton m_CancelButton;
	/// <summary>
	/// The sidepanel capture button.
	/// </summary>
    public UIButton m_CaptureButton;
	/// <summary>
	/// The sidepanel king button.
	/// </summary>
    public UIButton m_KingButton;
	/// <summary>
	/// The sidepanel undo button.
	/// </summary>
    public UIButton m_UndoButton;
	
	/// <summary>
	/// The starting pos for the sidepanel buttons.
	/// </summary>
    private readonly Vector3 m_ButtonLocalPosStart = new Vector3(104f, 33f, 0f);
	/// <summary>
	/// The offset for the sidepanel buttons.
	/// </summary>
    private readonly Vector3 m_ButtonLocalOffset = new Vector3(0f, 57f, 0f);
	/// <summary>
	/// The spacing for the sidepanel buttons.
	/// </summary>
    private readonly Vector3 m_ButtonLocalSpacing = new Vector3(0f, 3f, 0f);
	/// <summary>
	/// The off screen pos for the sidepanel buttons.
	/// </summary>
    private readonly Vector3 m_ButtonOffScreenPos = new Vector3(104f, -25f, 0f);
	
	/// <summary>
	/// The anchor for the sidepanel buttons.
	/// </summary>
    public Transform m_ButtonAnchor;

    private enum GUIDepth
    {
        BackingPanel = -1,
        BackBackground,
        BackLabel,
        FrontBackground,
        FrontLabel
    }
	
	/// <summary>
	/// An array holding the player status panels.
	/// </summary>
    private PlayerStatusPanel[] m_PlayerStatusPanels = new PlayerStatusPanel[4];
	/// <summary>
	/// The anchor to parent the player status panels to
	/// </summary>
    public Transform m_PlayerStatusPanelAnchor;
	/// <summary>
	/// The start pos for the player panels.
	/// </summary>
    private Vector3 m_PlayerPanelLocalPosStart = new Vector3(104f, -115f, 0f);
	/// <summary>
	/// The local offset for the player panels.
	/// </summary>
    private Vector3 m_PlayerPanelLocalOffset = new Vector3(0f, -100f, 0f);
	/// <summary>
	/// The spacing for the player panels.
	/// </summary>
    private Vector3 m_PlayerPanelSpacing = new Vector3(0f, -3f, 0f);
	
	/// <summary>
	/// Fired when the menu button is pressed.
	/// </summary>
    public event VoidDelegate MenuButtonPressedEvent;
	/// <summary>
	/// Fired when the end turn button is pressed.
	/// </summary>
    public event VoidDelegate EndTurnButtonPressedEvent;
	/// <summary>
	/// Fired when the sacrifice button is pressed.
	/// </summary>
    public event VoidDelegate SacraficeButtonPressedEvent;
	/// <summary>
	/// Fired when the cancel button is pressed.
	/// </summary>
    public event VoidDelegate CancelButtonPressedEvent;
	/// <summary>
	/// Fired when the capture button is pressed.
	/// </summary>
    public event VoidDelegate CaptureButtonPressedEvent;
	/// <summary>
	/// Fired when the king button is pressed.
	/// </summary>
    public event VoidDelegate KingButtonPressedEvent;
	/// <summary>
	/// Fired when the undo button is pressed.
	/// </summary>
    public event VoidDelegate UndoButtonPressedEvent;
	
	/// <summary>
	/// The list of active buttons
	/// </summary>
    private List<Transform> m_ActiveButtonList = new List<Transform>(3);
	/// <summary>
	/// True the button list is dirty
	/// </summary>
    private bool m_ButtonListDirty = false;
	
	/// <summary>
	/// Sets the end turn button to end game button.
	/// </summary>
    public void SetEndTurnButtonToEndGameButton()
    {
        if (m_EndTurnButtonIsEndGameButton)
            return;

        m_EndTurnButtonIsEndGameButton = true;

        m_EndTurnButtonLabel.text = "End Game";
    }
	
	/// <summary>
	/// Sets the end game button to end turn button.
	/// </summary>
    public void SetEndGameButtonToEndTurnButton()
    {
        if (!m_EndTurnButtonIsEndGameButton)
            return;

        m_EndTurnButtonIsEndGameButton = false;

        m_EndTurnButtonLabel.text = "End Turn";
    }
	
	/// <summary>
	/// Start this instance.
	/// </summary>
    private IEnumerator Start()
    {
        var fsm = GameController.Instance.FSM;
        while (fsm == null)
        {
            yield return null;
            fsm = GameController.Instance.FSM;
        }		
        fsm.SetEndTurnButtonActive += (b) => { if (b) AddButtonToList(m_EndTurnButton.transform); else RemoveButtonFromList(m_EndTurnButton.transform); };
        fsm.SetCancelButtonActive += (b) => { if (b) AddButtonToList(m_CancelButton.transform); else RemoveButtonFromList(m_CancelButton.transform); };
        fsm.SetSacrificeButtonActive += (b) => { if (b) AddButtonToList(m_SacrificeButton.transform); else RemoveButtonFromList(m_SacrificeButton.transform); };
        fsm.SetCaptureButtonActive += (b) => { if (b) AddButtonToList(m_CaptureButton.transform); else RemoveButtonFromList(m_CaptureButton.transform); };
        fsm.SetKingButtonActive += (b) => { if (b) AddButtonToList(m_KingButton.transform); else RemoveButtonFromList(m_KingButton.transform); };
        fsm.SetUndoButtonActive += (b) => { if (b) AddButtonToList(m_UndoButton.transform); else RemoveButtonFromList(m_UndoButton.transform); };

        m_EndTurnButton.transform.parent = m_ButtonAnchor;
        m_EndTurnButton.transform.localPosition = m_ButtonOffScreenPos;
        m_SacrificeButton.transform.parent = m_ButtonAnchor;
        m_SacrificeButton.transform.localPosition = m_ButtonOffScreenPos;
        m_CancelButton.transform.parent = m_ButtonAnchor;
        m_CancelButton.transform.localPosition = m_ButtonOffScreenPos;
        m_CaptureButton.transform.parent = m_ButtonAnchor;
        m_CaptureButton.transform.localPosition = m_ButtonOffScreenPos;
        m_KingButton.transform.parent = m_ButtonAnchor;
        m_KingButton.transform.localPosition = m_ButtonOffScreenPos;
        m_UndoButton.transform.parent = m_ButtonAnchor;
        m_UndoButton.transform.localPosition = m_ButtonOffScreenPos;

        m_EndTurnButtonLabel = m_EndTurnButton.GetComponentInChildren<UILabel>();
    }
	
	/// <summary>
	/// Enables the panel buttons.
	/// </summary>
	/// <param name='val'>Value to set (true/false).</param>
	public void EnablePanelButtons(bool val)
	{
		m_MenuButton.isEnabled = val;
		m_EndTurnButton.isEnabled = val;
    	m_SacrificeButton.isEnabled = val;
    	m_CancelButton.isEnabled = val;
    	m_CaptureButton.isEnabled = val;
    	m_KingButton.isEnabled = val;
    	m_UndoButton.isEnabled = val;
	}
	
	/// <summary>
	/// Called after the normal Unity update
	/// </summary>
    private void LateUpdate()
    {
        if (m_ButtonListDirty)
        {
            int buttonCount = 0;

            if (m_ActiveButtonList.Contains(m_EndTurnButton.transform))
            {
                ActivateButton(m_EndTurnButton.transform, buttonCount);
                buttonCount++;
            }
            else
            {
                DeactivateButton(m_EndTurnButton.transform);
            }
            if (m_ActiveButtonList.Contains(m_CancelButton.transform))
            {
                ActivateButton(m_CancelButton.transform, buttonCount);
                buttonCount++;
            }
            else
            {
                DeactivateButton(m_CancelButton.transform);
            }
            if (m_ActiveButtonList.Contains(m_UndoButton.transform))
            {
                ActivateButton(m_UndoButton.transform, buttonCount);
                buttonCount++;
            }
            else
            {
                DeactivateButton(m_UndoButton.transform);
            }
            if (m_ActiveButtonList.Contains(m_SacrificeButton.transform))
            {
                ActivateButton(m_SacrificeButton.transform, buttonCount);
                buttonCount++;
            }
            else
            {
                DeactivateButton(m_SacrificeButton.transform);
            }
            if (m_ActiveButtonList.Contains(m_CaptureButton.transform))
            {
                ActivateButton(m_CaptureButton.transform, buttonCount);
                buttonCount++;
            }
            else
            {
                DeactivateButton(m_CaptureButton.transform);
            }
            if (m_ActiveButtonList.Contains(m_KingButton.transform))
            {
                ActivateButton(m_KingButton.transform, buttonCount);
                buttonCount++;
            }
            else
            {
                DeactivateButton(m_KingButton.transform);
            }

            m_ButtonListDirty = false;
        }
    }
	
	/// <summary>
	/// Activates the passed button.
	/// </summary>
	/// <param name='t'>The button.</param>
	/// <param name='currentButtonCount'>Current active button count.</param>
    private void ActivateButton(Transform t, int currentButtonCount)
    {
        t.gameObject.SetActive(true);
        t.parent = m_ButtonAnchor;

        t.gameObject.GetComponentInChildren<UILabel>().depth = (int)GUIDepth.FrontLabel;
        t.gameObject.GetComponentInChildren<UISprite>().depth = (int)GUIDepth.FrontBackground;
        t.gameObject.GetComponent<UIButton>().isEnabled = true;

        HOTween.To(t, 0.3f, new TweenParms().Prop("localPosition", m_ButtonLocalPosStart + (currentButtonCount * m_ButtonLocalOffset) + ((currentButtonCount - 1) * ((NGUIAtlasManager.IsHD ? 2 : 1) * m_ButtonLocalSpacing))).Ease(EaseType.EaseOutExpo));
    }
	
	/// <summary>
	/// Deactivates the passed button.
	/// </summary>
	/// <param name='t'>The button.</param>
    private void DeactivateButton(Transform t)
    {
        if (!t.gameObject.activeInHierarchy)
            return;

        t.gameObject.GetComponentInChildren<UILabel>().depth = (int)GUIDepth.BackLabel;
        t.gameObject.GetComponentInChildren<UISprite>().depth = (int)GUIDepth.BackBackground;
        t.gameObject.GetComponent<UIButton>().isEnabled = false;

        HOTween.To(t, 0.3f, new TweenParms().Prop("localPosition", new Vector3(t.localPosition.x - 205f, t.localPosition.y, t.localPosition.z)).Ease(EaseType.EaseInExpo).OnComplete(() => { t.gameObject.SetActive(false); t.localPosition = m_ButtonOffScreenPos; }));
    }
	
	/// <summary>
	/// Adds the passed button to the list.
	/// </summary>
	/// <param name='t'>The button.</param>
    private void AddButtonToList(Transform t)
    {
        if (m_ActiveButtonList.Contains(t))
            return;

        m_ActiveButtonList.Add(t);
        m_ButtonListDirty = true;
    }
	
	/// <summary>
	/// Removes the passed button from the list.
	/// </summary>
	/// <param name='t'>The button.</param>
    private void RemoveButtonFromList(Transform t)
    {
        if (!m_ActiveButtonList.Contains(t))
            return;

        m_ActiveButtonList.Remove(t);
        m_ButtonListDirty = true;
    }
	
	/// <summary>
	/// Registers the player events.
	/// </summary>
	/// <param name='player'>The player.</param>
	/// <param name='playerStatusPanelPrefab'>The player status panel prefab.</param>
    public void RegisterPlayerEvents(Player player, GameObject playerStatusPanelPrefab)
    {
        player.PlayerScoreChangedEvent += UpdatePlayerInfo;
        player.TokenCountChangedEvent += UpdatePlayerInfo;
        var newPlayerPanel = (GameObject)Instantiate(playerStatusPanelPrefab);
        if (m_PlayerStatusPanels[player.PlayerNumber - 1] != null)
        {
            TTDebug.LogError("InGameGUIManager.cs : Player panel already created for player : " + player.PlayerNumber);
        }
        var height = newPlayerPanel.GetComponent<PlayerStatusPanel>().BackgroundHeight;
        var buttonHeight = 57f;
        m_PlayerPanelLocalPosStart = new Vector3(104f, (-height / 2f) - buttonHeight + (2 * ((NGUIAtlasManager.IsHD ? 2 : 1) * m_PlayerPanelSpacing.y)), 0f);
        m_PlayerPanelLocalOffset = new Vector3(0f, -height, 0f);

        newPlayerPanel.transform.parent = m_PlayerStatusPanelAnchor;
        newPlayerPanel.transform.localPosition = m_PlayerPanelLocalPosStart + (player.PlayerNumber - 1) * m_PlayerPanelLocalOffset + (player.PlayerNumber - 1) * ((NGUIAtlasManager.IsHD ? 2 : 1) * m_PlayerPanelSpacing);
        newPlayerPanel.transform.localScale = Vector3.one;
        m_PlayerStatusPanels[player.PlayerNumber - 1] = newPlayerPanel.GetComponent<PlayerStatusPanel>();
        UpdatePlayerInfo(player);
    }
	
	/// <summary>
	/// Deregisters the player events.
	/// </summary>
	/// <param name='player'>The player.</param>
    public void DeregisterPlayerEvents(Player player)
    {
        player.PlayerScoreChangedEvent -= UpdatePlayerInfo;
        player.TokenCountChangedEvent -= UpdatePlayerInfo;
    }
	
	/// <summary>
	/// Sets the player's turn.
	/// </summary>
	/// <param name='player'>The player.</param>
    public void SetPlayerTurn(int player)
    {
        for (int i = 0; i < m_PlayerStatusPanels.Length; i++)
        {
            if (m_PlayerStatusPanels[i] == null)
                continue;
            if (player == i + 1)
                m_PlayerStatusPanels[i].SetPanelActive(true);
            else
                m_PlayerStatusPanels[i].SetPanelActive(false);
        }
    }
	
	/// <summary>
	/// Updates the player's name.
	/// </summary>
	/// <param name='player'>The player.</param>
    private void UpdatePlayerInfo(Player player)
    {
        m_PlayerStatusPanels[player.PlayerNumber - 1].SetPanelText(player);
    }
	
	/// <summary>
	/// Updates the player's mutiny meter.
	/// </summary>
	/// <param name='player'>The player.</param>
	/// <param name='meterValue'>The meter's value.</param>
    public void UpdatePlayerMeter(int player, float meterValue)
    {
        m_PlayerStatusPanels[player - 1].SetPanelSliderValue(meterValue);
    }
	
	/// <summary>
	/// Called when the menu button is pressed.
	/// </summary>
    public void MenuButtonPressed()
    {
        if (MenuButtonPressedEvent != null)
            MenuButtonPressedEvent();
    }
	
	/// <summary>
	/// Called when the end turn button is pressed.
	/// </summary>
    public void EndTurnButtonPressed()
    {
        if (EndTurnButtonPressedEvent != null)
            EndTurnButtonPressedEvent();
    }
	
	/// <summary>
	/// Called when the cancel button is pressed.
	/// </summary>
    public void CancelButtonPressed()
    {
        if (CancelButtonPressedEvent != null)
            CancelButtonPressedEvent();
    }
	
	/// <summary>
	/// Called when the sacrifice button is pressed.
	/// </summary>
    public void SacrificeButtonPressed()
    {
        if (SacraficeButtonPressedEvent != null)
            SacraficeButtonPressedEvent();
    }
	
	/// <summary>
	/// Called when the capture button is pressed.
	/// </summary>
    public void CaptureButtonPressed()
    {
        if (CaptureButtonPressedEvent != null)
            CaptureButtonPressedEvent();
    }
	
	/// <summary>
	/// Called when the king button is pressed.
	/// </summary>
    public void KingButtonPressed()
    {
        if (KingButtonPressedEvent != null)
            KingButtonPressedEvent();
    }
	
	/// <summary>
	/// Called when the undo button is pressed.
	/// </summary>
    public void UndoButtonPressed()
    {
        if (UndoButtonPressedEvent != null)
            UndoButtonPressedEvent();
    }
}