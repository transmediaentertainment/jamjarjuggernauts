using TransTech.Delegates;
using TransTech.System.GUI;
using UnityEngine;
using TransTech.System.IO;

/// <summary>
/// Controller object for interfacing Unity with the Main Menu system
/// </summary>
public class MainMenuController : MonoBehaviour
{
    /// <summary>
    /// The UIPanel object for the Player Number Selection screen
    /// </summary>
    public UIPanel m_PlayerNumButtonPanel;

    /// <summary>
    /// The UIPanel object for the Name Entry screen
    /// </summary>
    public UIPanel m_PlayerNameLabelPanel;
	
	/// <summary>
	/// The UIPanel object for the Map Selection screen.
	/// </summary>
	public UIPanel m_MapSelectionPanel;
	
	/// <summary>
	/// The UIPanel objects for the Tutorial Pages.
	/// </summary>
	public UIPanel m_TutorialPage1Panel;
	public UIPanel m_TutorialPage2Panel;
	public UIPanel m_TutorialPage3Panel;
	public UIPanel m_TutorialPage4Panel;
	public UIPanel m_TutorialPage5Panel;
	public UIPanel m_TutorialPage6Panel;
	public UIPanel m_TutorialPage7Panel;
	public UIPanel m_TutorialPage8Panel;
	public UIPanel m_TutorialPage9Panel;

    /// <summary>
    /// The UIInput script for Player 1's name
    /// </summary>
    public UIInput m_P1NameInput;

    /// <summary>
    /// The UIInput script for Player 2's name
    /// </summary>
    public UIInput m_P2NameInput;

    /// <summary>
    /// The UIInput script for Player 3's name
    /// </summary>
    public UIInput m_P3NameInput;

    /// <summary>
    /// The UIInput script for Player 4's name
    /// </summary>
    public UIInput m_P4NameInput;

    /// <summary>
    /// The Gameobject for the Resume Game button
    /// </summary>
    public GameObject m_ResumeGameButton;

    public UIButton m_2PlayerButton;
    public UIButton m_3PlayerButton;
    public UIButton m_4PlayerButton;
    public UIButton m_RuleDropDown;
	
	public GameObject m_MapButtonPrefab;

    #region Player Count Rules Delegates

    /// <summary>
    /// Fired when the Two Player button is pressed
    /// </summary>
    public VoidDelegate TwoPlayerButtonPressedEvent;

    /// <summary>
    /// Fired when the Three player button is pressed
    /// </summary>
    public VoidDelegate ThreePlayerButtonPressedEvent;

    /// <summary>
    /// Fired when the Four player button is pressed
    /// </summary>
    public VoidDelegate FourPlayerButtonPressedEvent;

    /// <summary>
    /// Fired when the Resume Game button is pressed;
    /// </summary>
    public VoidDelegate ResumeGameButtonPressedEvent;
	
	/// <summary>
	/// Fired wgeb the Tutoral button is pressed;
	/// </summary>
	public VoidDelegate TutorialButtonPressedEvent;

    /// <summary>
    /// Fired when the Ruleset pop up menu changes value
    /// </summary>
    public StringDelegate RuleSetChangedEvent;

    #endregion Player Count Rules Delegates

    #region Player Name Entry Delegates

    /// <summary>
    /// Fired when the Back button is pressed
    /// </summary>
    public VoidDelegate BackButtonPressedEvent;
	
	/// <summary>
	/// Fired when the Next button is pressed.
	/// </summary>
	public VoidDelegate NextButtonPressedEvent;

    /// <summary>
    /// Fired when the Player button is pressed
    /// </summary>
    public VoidDelegate PlayButtonPressedEvent;

    #endregion Player Name Entry Delegates

    /// <summary>
    /// Reference to the Main Menu FSM
    /// </summary>
    /// // Commented out to remove warning.  If this is required again uncomment
    //private MainMenuFSMEngine m_FSM;

    /// <summary>
    /// Called by Unity when the object is created
    /// </summary>
    private void Awake()
    {
        /*m_FSM = */new MainMenuFSMEngine(this, m_PlayerNumButtonPanel.transform, m_PlayerNameLabelPanel.transform, m_MapSelectionPanel.transform, m_MapButtonPrefab,
            m_P1NameInput, m_P2NameInput, m_P3NameInput, m_P4NameInput);

        if (SaveGameToFile.DoesAutosaveExist())
        {
            m_ResumeGameButton.SetActive(true);
        }
        else
        {
            m_ResumeGameButton.SetActive(false);
        }
    }

    /// <summary>
    /// Enables and disables the UI Buttons on the player count selection page.
    /// </summary>
    /// <param name="enable">True to enable, false to disable.</param>
    public void EnablePlayerCountPanelButtons(bool enable)
    {
        m_2PlayerButton.isEnabled = enable;
        m_3PlayerButton.isEnabled = enable;
        m_4PlayerButton.isEnabled = enable;
        m_ResumeGameButton.GetComponent<UIButton>().isEnabled = enable;
        m_RuleDropDown.isEnabled = enable;
    }

    /// <summary>
    /// Called by the Two Player UIButton in the menu when clicked
    /// </summary>
    private void TwoPlayerGameButtonPressed()
    {
        if (TwoPlayerButtonPressedEvent != null)
            TwoPlayerButtonPressedEvent();
    }

    /// <summary>
    /// Called by the Three Player UIButton in the menu when clicked
    /// </summary>
    private void ThreePlayerGameButtonPressed()
    {
        if (ThreePlayerButtonPressedEvent != null)
            ThreePlayerButtonPressedEvent();
    }

    /// <summary>
    /// Called by the Four Player UIButton in the menu when clicked
    /// </summary>
    private void FourPlayerGameButtonPressed()
    {
        if (FourPlayerButtonPressedEvent != null)
            FourPlayerButtonPressedEvent();
    }

    /// <summary>
    /// Called by the Resume Game UIButton in the menu when clicked
    /// </summary>
    private void ResumeGameButtonPressed()
    {
        if (ResumeGameButtonPressedEvent != null)
            ResumeGameButtonPressedEvent();
    }
	
	/// <summary>
    /// Called by the Tutorial UIButton in the menu when clicked
    /// </summary>
    private void TutorialButtonPressed()
    {
        if (TutorialButtonPressedEvent != null)
            TutorialButtonPressedEvent();
    }

    /// <summary>
    /// Called by the Play UIButton in the menu when clicked
    /// </summary>
    private void PlayButtonPressed()
    {
        if (PlayButtonPressedEvent != null)
            PlayButtonPressedEvent();
    }
	
	/// <summary>
	/// Called by the Next UIButton in the menu when clicked.
	/// </summary>
	private void NextButtonPressed()
	{
		if( NextButtonPressedEvent != null)
			NextButtonPressedEvent();
	}

    /// <summary>
    /// Called by the Back UIButton in the menu when clicked
    /// </summary>
    private void BackButtonPressed()
    {
        if (BackButtonPressedEvent != null)
            BackButtonPressedEvent();
    }

    /// <summary>
    /// Called by the Ruleset UIPopup in the menu when it changes value
    /// </summary>
    private void RuleSetChanged(string newRuleSet)
    {
        if (RuleSetChangedEvent != null)
            RuleSetChangedEvent(newRuleSet);
    }

    /// <summary>
    /// Called by the Editor Button in the menu when clicked.
    /// </summary>
    private void EditorButtonPressed()
    {
        Application.LoadLevel("EditorScene");
    }
}