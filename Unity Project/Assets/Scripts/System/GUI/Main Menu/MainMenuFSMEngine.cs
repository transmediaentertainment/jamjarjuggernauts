﻿using TransTech.FiniteStateMachine;
using TransTech.System.Debug;
using UnityEngine;
using TransTech.System.IO;

namespace TransTech.System.GUI
{
    /// <summary>
    /// FSM Engine to handle the Main Menu of the game
    /// </summary>
    public class MainMenuFSMEngine : FSM
    {
        /// <summary>
        /// FSM State for the screen where player's pick how many people will player, and the ruleset
        /// </summary>
        private PlayerCountRulesFSMState m_PlayerCountRulesState;

        /// <summary>
        /// FSM State for the screen where player's enter their names.
        /// </summary>
        private PlayerNameEntryFSMState m_PlayerNameEntryState;
		
		/// <summary>
		/// FSM State for the screen where player's select the map to play
		/// </summary>
		private MapSelectionFSMState m_MapSelectionState;
		
		/// <summary>
		/// FSM States for the Tutorial Pages.
		/// </summary>
		private MainTutorialPageFSMState m_MainTutorialPage1State;
		private MainTutorialPageFSMState m_MainTutorialPage2State;
		private MainTutorialPageFSMState m_MainTutorialPage3State;
		private MainTutorialPageFSMState m_MainTutorialPage4State;
		private MainTutorialPageFSMState m_MainTutorialPage5State;
		private MainTutorialPageFSMState m_MainTutorialPage6State;
		private MainTutorialPageFSMState m_MainTutorialPage7State;
		private MainTutorialPageFSMState m_MainTutorialPage8State;
		private MainTutorialPageFSMState m_MainTutorialPage9State;

        /// <summary>
        /// Number of players that have been selected to play
        /// </summary>
        private int m_NumPlayers = 0;

        /// <summary>
        /// The current ruleset chosen by the player
        /// </summary>
        private string m_RuleSet = "";

        /// <summary>
        /// The names that have been entered by the players.
        /// </summary>
        private string[] m_PlayerNames = new string[4];
		
		/// <summary>
		/// The name of the map.
		/// </summary>
		private string m_MapName = "";
		
		/// <summary>
		/// The Block map data
		/// </summary>
		private byte[,,] m_MapData = null;

        /// <summary>
        /// Constructor for creating and initializing the object
        /// </summary>
        /// <param name="mmc">Reference to the Main Menu Controller for interface with Unity</param>
        /// <param name="playerCountRulesPanel">The Transform of the panel for the Player Count Rules screen</param>
        /// <param name="playerNameEntryPanel">The Transform of the panel for the Player Name Entry screen</param>
        /// <param name="nameInputs">The UIInput components for each of the name entry fields.</param>
        public MainMenuFSMEngine(MainMenuController mmc, Transform playerCountRulesPanel, Transform playerNameEntryPanel, Transform mapSelectionPanel, GameObject mapButtonPrefab, params UIInput[] nameInputs)
        {
            m_PlayerCountRulesState = new PlayerCountRulesFSMState(mmc, this, playerCountRulesPanel);
            m_PlayerNameEntryState = new PlayerNameEntryFSMState(mmc, this, playerNameEntryPanel, nameInputs);
			m_MapSelectionState = new MapSelectionFSMState( mmc, this, mapSelectionPanel, mapButtonPrefab);
			
			// Init the Tutoral Pages
			m_MainTutorialPage1State = new MainTutorialPageFSMState( mmc.m_TutorialPage1Panel.transform, TutorialPages.Page1Text, "Close", "Next" );
			m_MainTutorialPage2State = new MainTutorialPageFSMState( mmc.m_TutorialPage2Panel.transform, TutorialPages.Page2Text, "Previous", "Close", "Next" );
			m_MainTutorialPage3State = new MainTutorialPageFSMState( mmc.m_TutorialPage3Panel.transform, TutorialPages.Page3Text, "Previous", "Close", "Next" );
			m_MainTutorialPage4State = new MainTutorialPageFSMState( mmc.m_TutorialPage4Panel.transform, TutorialPages.NoSacrificeLimits, "Previous", "Close", "Next" );
			m_MainTutorialPage5State = new MainTutorialPageFSMState( mmc.m_TutorialPage5Panel.transform, TutorialPages.Sacrifice3Limit, "Previous", "Close", "Next" );
			m_MainTutorialPage6State = new MainTutorialPageFSMState( mmc.m_TutorialPage6Panel.transform, TutorialPages.MutinyRandomEvery, "Previous", "Close", "Next" );
			m_MainTutorialPage7State = new MainTutorialPageFSMState( mmc.m_TutorialPage7Panel.transform, TutorialPages.MutinyRandomOnly, "Previous", "Close", "Next" );
			m_MainTutorialPage8State = new MainTutorialPageFSMState( mmc.m_TutorialPage8Panel.transform, TutorialPages.MutinyBestEvery, "Previous", "Close", "Next" );
			m_MainTutorialPage9State = new MainTutorialPageFSMState( mmc.m_TutorialPage9Panel.transform, TutorialPages.MutinyBestOnly, "Previous", "Close" );
			
			m_MainTutorialPage1State.ButtonZeroPressedEvent += () => PopState();
			m_MainTutorialPage1State.ButtonOnePressedEvent += () => PushState(m_MainTutorialPage2State);
			
			SetTutorialPageEvents(m_MainTutorialPage2State, m_MainTutorialPage3State);
			SetTutorialPageEvents(m_MainTutorialPage3State, m_MainTutorialPage4State);
			SetTutorialPageEvents(m_MainTutorialPage4State, m_MainTutorialPage5State);
			SetTutorialPageEvents(m_MainTutorialPage5State, m_MainTutorialPage6State);
			SetTutorialPageEvents(m_MainTutorialPage6State, m_MainTutorialPage7State);
			SetTutorialPageEvents(m_MainTutorialPage7State, m_MainTutorialPage8State);
			SetTutorialPageEvents(m_MainTutorialPage8State, m_MainTutorialPage9State);
			
			m_MainTutorialPage9State.ButtonZeroPressedEvent += () => PopState();
			m_MainTutorialPage9State.ButtonOnePressedEvent += () => NewState( m_PlayerCountRulesState );

            // Set the state to the Player Count Rules state
            NewState(m_PlayerCountRulesState);
        }
		
		private void SetTutorialPageEvents( MainTutorialPageFSMState currentTutorialPage, MainTutorialPageFSMState tutorialPageToPush )
		{
			currentTutorialPage.ButtonZeroPressedEvent += () => PopState();
			currentTutorialPage.ButtonOnePressedEvent += () => NewState( m_PlayerCountRulesState );
			currentTutorialPage.ButtonTwoPressedEvent += () => PushState( tutorialPageToPush );
		}

        /// <summary>
        /// Sets the number of players that will be playing the game
        /// </summary>
        /// <param name="num"></param>
        public void SetNumPlayers(int num)
        {
            if (num < 2 || num > 4)
            {
                TTDebug.LogError("MainMenuFSMEngine.cs : Invalid number of players : " + num);
                return;
            }
            m_NumPlayers = num;
        }
		
		/// <summary>
		/// Sets the block map data.
		/// </summary>
		/// <param name='blockMap'>
		/// The block map data
		/// </param>
		public void SetMapData(string levelName)
		{
			m_MapName = levelName;
			
			bool success;
	        m_MapData = LevelFileManager.LoadLevelDataFromDisk(levelName, out success);
	        if (!success)
	        {
	            TTDebug.LogError("InGameEditorController.cs : Unable to load level from disk : " + levelName);
	            return;
	        }
		}

        /// <summary>
        /// Sets the screen to the Name Entry screen
        /// </summary>
        public void MoveToNameEntry()
        {
            if (Peek() == m_PlayerCountRulesState)
                PushState(m_PlayerNameEntryState, m_NumPlayers);
        }
		
		/// <summary>
        /// Sets the screen to the Name Entry screen
        /// </summary>
        public void MoveToMapSelection()
        {
            if (Peek() == m_PlayerNameEntryState)
                PushState(m_MapSelectionState);
        }
		
		/// <summary>
		/// Moves to first tutorial panel.
		/// </summary>
		public void MoveToFirstTutorialPanel()
		{
			if (Peek() == m_PlayerCountRulesState)
				PushState(m_MainTutorialPage1State);
		}

        /// <summary>
        /// Sets the rule set to use for the game.
        /// </summary>
        /// <param name="ruleSet">The rule set description to use</param>
        public void SetRuleSet(string ruleSet)
        {
            TTDebug.Log("Setting Rule Set : " + ruleSet);
            m_RuleSet = ruleSet;
        }

        /// <summary>
        /// Sets the name of a player
        /// </summary>
        /// <param name="name">The name of the player</param>
        /// <param name="playerNumber">The Player's number (index)</param>
        public void SetPlayerName(string name, int playerNumber)
        {
            if (playerNumber < 1 || playerNumber > 4)
            {
                TTDebug.LogError("MainMenuFSMEngine.cs : Invalid Player Number : " + playerNumber);
                return;
            }
            m_PlayerNames[playerNumber - 1] = name;
        }

        /// <summary>
        /// Begins the game.
        /// </summary>
        public void BeginGame()
        {
            var go = new GameObject("Data Transfer", typeof(SceneDataTransfer));
            var transfer = go.GetComponent<SceneDataTransfer>();

            for (int i = 0; i < m_NumPlayers; i++)
            {
                // If nothing has been entered, put in the default "Player x"
                if (m_PlayerNames[i] == null || m_PlayerNames[i].Length == 0)
                {
                    m_PlayerNames[i] = "Player " + (i + 1).ToString();
                }
                transfer.SetPlayerName(i + 1, m_PlayerNames[i]);
            }
            transfer.SetRuleset(m_RuleSet);
			transfer.SetMapData(m_MapName, m_MapData);

            Application.LoadLevel("Initial Test Scene");
        }

        public void ResumeGame()
        {
            var go = new GameObject("Data Transfer", typeof(SceneDataTransfer));
            var transfer = go.GetComponent<SceneDataTransfer>();
			
			SetMapData(SaveGameToFile.LevelName());
			
			transfer.SetMapData(m_MapName, m_MapData);

            transfer.SetToResumeGame();

            Application.LoadLevel("Initial Test Scene");
        }
    }
}