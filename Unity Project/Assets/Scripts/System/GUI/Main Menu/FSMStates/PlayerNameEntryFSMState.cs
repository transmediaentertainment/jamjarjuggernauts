using TransTech.System.Debug;
using UnityEngine;

namespace TransTech.System.GUI
{
    /// <summary>
    /// FSM State to handle the Player Name Entry screen
    /// </summary>
    public class PlayerNameEntryFSMState : MainMenuPanelFSMState
    {
        /// <summary>
        /// Reference to the Main Menu Controller
        /// </summary>
        private MainMenuController m_MMC;

        /// <summary>
        /// Reference to our parent FSM
        /// </summary>
        private MainMenuFSMEngine m_FSM;

        /// <summary>
        /// References to the UIInput fields the players use to input their names
        /// </summary>
        private UIInput[] m_NameInputs;

        /// <summary>
        /// Constructor for object initialization
        /// </summary>
        /// <param name="mmc">Reference to the Main Menu Controller for interfacing with Unity</param>
        /// <param name="fsm">Reference to our parent FSM</param>
        /// <param name="panel">The Transform of the panel we are controlling</param>
        /// <param name="nameInputs">References to the UIInput fields for name entry in the scene</param>
        public PlayerNameEntryFSMState(MainMenuController mmc, MainMenuFSMEngine fsm, Transform panel, UIInput[] nameInputs)
            : base(panel)
        {
            m_MMC = mmc;
            m_FSM = fsm;
            m_NameInputs = nameInputs;
        }

        /// <summary>
        /// Called by the FSM when entering the state
        /// </summary>
        /// <param name="args">Optional arguments (int for number of players)</param>
        public override void Enter(params object[] args)
        {
            base.Enter(args);
            RegisterForEvents();
            if (args != null && args.Length == 1 && args[0] is int)
            {
                // Turn the relevant UIInput fields on or off
                NGUITools.SetActive(m_NameInputs[0].gameObject, true);
                NGUITools.SetActive(m_NameInputs[1].gameObject, true);
                switch ((int)args[0])
                {
                    case 2:
                        NGUITools.SetActive(m_NameInputs[2].gameObject, false);
                        NGUITools.SetActive(m_NameInputs[3].gameObject, false);
                        break;

                    case 3:
                        NGUITools.SetActive(m_NameInputs[2].gameObject, true);
                        NGUITools.SetActive(m_NameInputs[3].gameObject, false);
                        break;

                    case 4:
                        NGUITools.SetActive(m_NameInputs[2].gameObject, true);
                        NGUITools.SetActive(m_NameInputs[3].gameObject, true);
                        break;

                    default:
                        TTDebug.LogError("PlayerNameEntryFSMState.cs : Incorrect player number : " + (int)args[0]);
                        break;
                }
            }
            else
            {
                TTDebug.LogError("PlayerNameEntryFSMState.cs : Number of Players in game not passed in");
            }
        }

        /// <summary>
        /// Called by the FSM when exiting the state
        /// </summary>
        public override void Exit()
        {
            base.Exit();
            DeregisterForEvents();
        }

        /// <summary>
        /// Called by the FSM when the state gains focus (all states on top are popped from the stack)
        /// </summary>
        public override void GainedFocus()
        {
            base.GainedFocus();
            RegisterForEvents();
        }

        /// <summary>
        /// Called by the FSM when the State loses focus (another state is pushed onto the stack)
        /// </summary>
        public override void LostFocus()
        {
            base.LostFocus();
            DeregisterForEvents();
        }

        /// <summary>
        /// Registers internal function to their relevant Controller events.
        /// </summary>
        private void RegisterForEvents()
        {
            m_MMC.NextButtonPressedEvent += NextButtonPressed;
            m_MMC.BackButtonPressedEvent += BackButtonPressed;
        }

        /// <summary>
        /// Deregisters internal function to their relevant Controller events.
        /// </summary>
        private void DeregisterForEvents()
        {
            m_MMC.NextButtonPressedEvent -= NextButtonPressed;
            m_MMC.BackButtonPressedEvent -= BackButtonPressed;
        }

        /// <summary>
        /// Handles the Next button being pressed in the menu
        /// </summary>
        private void NextButtonPressed()
        {
            for (int i = 0; i < m_NameInputs.Length; i++)
            {
                m_FSM.SetPlayerName(m_NameInputs[i].value, i + 1);
            }

            m_FSM.MoveToMapSelection();
        }

        /// <summary>
        /// Handles the Back button being pressed in the menu
        /// </summary>
        private void BackButtonPressed()
        {
            m_FSM.PopState();
        }
    }
}