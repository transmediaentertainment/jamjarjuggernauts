using PathologicalGames;
using UnityEngine;
using TransTech.System.IO;
using TransTech.System.Debug;

namespace TransTech.System.GUI
{
    /// <summary>
    /// FSM State to handle the Main Menu Map Settings screen
    /// </summary>
    public class MapSelectionFSMState : MainMenuPanelFSMState
    {
		private GameObject m_ButtonPrefab;

        private GameObject[] m_Buttons;
        private string[] m_Levels;

        private UIGrid m_Grid;
		//private UIDraggablePanel m_DragPanel;
        private UIScrollView m_ScrollView;
		
        /// <summary>
        /// Reference to the Main Menu Controller
        /// </summary>
        private MainMenuController m_MMC;

        /// <summary>
        /// Reference to our parent FSM
        /// </summary>
        private MainMenuFSMEngine m_FSM;


        /// <summary>
        /// Constructor for object initialization
        /// </summary>
        /// <param name="mmc">Reference to the Main Menu Controller for interfacing with Unity</param>
        /// <param name="fsm">Reference to our parent FSM</param>
        /// <param name="panelTransform">The Transform of the panel we are controlling</param>
        public MapSelectionFSMState(MainMenuController mmc, MainMenuFSMEngine fsm, Transform panelTransform, GameObject buttonPrefab)
            : base(panelTransform)
        {
            m_MMC = mmc;
            m_FSM = fsm;
			m_ButtonPrefab = buttonPrefab;
            m_Grid = panelTransform.GetComponentInChildren<UIGrid>();
            m_ScrollView = panelTransform.GetComponentInChildren<UIScrollView>();
        }

        /// <summary>
        /// Called by the FSM when entering the state
        /// </summary>
        /// <param name="args">Optional arguments (int for number of players)</param>
        public override void Enter(params object[] args)
        {
            base.Enter(args);
            RegisterForEvents();
            
			m_Levels = LevelFileManager.GetLevelList();

            m_Buttons = new GameObject[m_Levels.Length];

            for (int i = 0; i < m_Levels.Length; i++)
            {
                m_Buttons[i] = PoolManager.Pools["GUI"].Spawn(m_ButtonPrefab.transform).gameObject;
                var label = m_Buttons[i].GetComponentInChildren<UILabel>();
                var splits = m_Levels[i].Split('\\', '/', '.');

                label.text = splits[splits.Length - 2];
                var messenger = m_Buttons[i].GetComponent<PopupButtonMessenger>();
                messenger.Init(i);
                messenger.ButtonPressed += HandleButtonPressed;
                m_Buttons[i].transform.parent = m_Grid.transform;
				m_Buttons[i].transform.localPosition = Vector3.zero;
                m_Buttons[i].transform.localScale = Vector3.one;
				//m_Buttons[i].GetComponentInChildren<UIDragPanelContents>().draggablePanel = m_DragPanel;
                m_Buttons[i].GetComponentInChildren<UIDragScrollView>().scrollView = m_ScrollView;
            }

            m_Grid.Reposition();
        }

        /// <summary>
        /// Called by the FSM when exiting the state
        /// </summary>
        public override void Exit()
        {
            base.Exit();
            DeregisterForEvents();
			
			for (int i = 0; i < m_Buttons.Length; i++)
            {
                var messenger = m_Buttons[i].GetComponent<PopupButtonMessenger>();
                messenger.ButtonPressed -= HandleButtonPressed;
                m_Buttons[i].transform.parent = null;
                PoolManager.Pools["GUI"].Despawn(m_Buttons[i].transform);
                m_Buttons[i] = null;
            }
        }

        /// <summary>
        /// Called by the FSM when the state gains focus (all states on top are popped from the stack)
        /// </summary>
        public override void GainedFocus()
        {
            base.GainedFocus();
            RegisterForEvents();
        }

        /// <summary>
        /// Called by the FSM when the State loses focus (another state is pushed onto the stack)
        /// </summary>
        public override void LostFocus()
        {
            base.LostFocus();
            DeregisterForEvents();
        }

        /// <summary>
        /// Registers internal function to their relevant Controller events.
        /// </summary>
        private void RegisterForEvents()
        {
            m_MMC.PlayButtonPressedEvent += PlayButtonPressed;
            m_MMC.BackButtonPressedEvent += BackButtonPressed;
        }

        /// <summary>
        /// Deregisters internal function to their relevant Controller events.
        /// </summary>
        private void DeregisterForEvents()
        {
            m_MMC.PlayButtonPressedEvent -= PlayButtonPressed;
            m_MMC.BackButtonPressedEvent -= BackButtonPressed;
        }

        /// <summary>
        /// Handles the Play button being pressed in the menu
        /// </summary>
        private void PlayButtonPressed()
        {
            m_FSM.BeginGame();
        }

        /// <summary>
        /// Handles the Back button being pressed in the menu
        /// </summary>
        private void BackButtonPressed()
        {
            m_FSM.PopState();
        }
		
		private void HandleButtonPressed(int buttonNumber)
        {
            var splits = m_Levels[buttonNumber].Split('\\', '/', '.');
            TTDebug.Log("Will load level " + splits[splits.Length - 2]);
			
			m_FSM.SetMapData(splits[splits.Length - 2]);
        }
    }
}