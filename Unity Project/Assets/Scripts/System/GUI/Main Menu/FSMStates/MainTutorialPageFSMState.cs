using UnityEngine;
using TransTech.Delegates;
using TransTech.System.GUI;

namespace TransTech.System.GUI
{
	/// <summary>
	/// FSM State to handle the Tutorial screens
	/// </summary>
	public class MainTutorialPageFSMState : MainMenuPanelFSMState
	{
		/// <summary>
		/// Occurs when button zero is pressed
		/// </summary>
		public event VoidDelegate ButtonZeroPressedEvent;
		/// <summary>
		/// Occurs when button one is pressed
		/// </summary>
		public event VoidDelegate ButtonOnePressedEvent;
		/// <summary>
		/// Occurs when button two is pressed
		/// </summary>
		public event VoidDelegate ButtonTwoPressedEvent;
		
		/// <summary>
		/// The transform of the parent panel to attach the popup to
		/// </summary>
		private Transform m_ParentPanelTransform;
		/// <summary>
		/// The Popup to display
		/// </summary>
		private Popup m_Popup;
		
		/// <summary>
		/// The page text to display on the popup
		/// </summary>
		private string m_PageText;
		/// <summary>
		/// An array of string to hold the buttons' text
		/// </summary>
		private string[] m_Buttons;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="TransTech.System.GUI.MainTutorialPageFSMState"/> class.
		/// </summary>
		/// <param name='panelTransform'>The parent panel's transform.</param>
		/// <param name='pageText'>the page text to display on the popup.</param>
		/// <param name='buttons'>An array of string to hold the buttons' text.</param>
		public MainTutorialPageFSMState(Transform panelTransform, string pageText, params string[] buttons ) : base(panelTransform)
		{
			m_ParentPanelTransform = panelTransform;
			m_PageText = pageText;
			m_Buttons = buttons;
		}
		
		/// <summary>
		/// Called by the FSM when entering the state.
		/// </summary>
		/// <param name='args'>Additional arguments for the state (none)</param>
		public override void Enter (params object[] args)
		{
			// If there isn't a popup, create one and attach it to the parent panel
			if( m_Popup == null )
			{
				m_Popup = Popup.CreatePopup( m_PageText, m_Buttons );
				m_Popup.transform.parent = m_ParentPanelTransform;
				m_Popup.transform.localPosition = Vector3.zero;
			}
			base.Enter (args);
			m_Popup.PopupButtonPressedEvent += PopupButtonPressed;
		}
		
		/// <summary>
		///  Called by the FSM when the state loses focus (from other state Push) 
		/// </summary>
		public override void LostFocus ()
		{
			m_Popup.PopupButtonPressedEvent -= PopupButtonPressed;
			base.LostFocus ();
		}
		
		/// <summary>
		///  Called by the FSM when the state gains focus (from Pop) 
		/// </summary>
		public override void GainedFocus ()
		{
			base.GainedFocus ();
			m_Popup.PopupButtonPressedEvent += PopupButtonPressed;
		}
		
		/// <summary>
		/// Callback from the popup which fires when a button is pressed
		/// </summary>
		/// <param name='buttonNumber'>The index of the pressed Button</param>
		private void PopupButtonPressed( int buttonNumber )
		{
			// Previous button
            if (buttonNumber == 0)
            {
                if (ButtonZeroPressedEvent != null)
                    ButtonZeroPressedEvent();
            }

            // Close button
            else if (buttonNumber == 1)
            {
                if (ButtonOnePressedEvent != null)
                    ButtonOnePressedEvent();
            }

            // Next button
            else if (buttonNumber == 2)
            {
                if (ButtonTwoPressedEvent != null)
                    ButtonTwoPressedEvent();
            }
		}
		
		/// <summary>
		///  Called by the FSM when exiting the state 
		/// </summary>
		public override void Exit ()
		{
			m_Popup.PopupButtonPressedEvent -= PopupButtonPressed;
			base.Exit ();
		}
	}
}