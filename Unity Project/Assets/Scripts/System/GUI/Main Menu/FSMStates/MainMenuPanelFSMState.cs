using Holoville.HOTween;
using TransTech.FiniteStateMachine;
using UnityEngine;

namespace TransTech.System.GUI
{
    /// <summary>
    /// Base abstract class for all the Main Menu FSM States to inherit from.
    /// Handles all of the animation on and off screen.
    /// </summary>
    public abstract class MainMenuPanelFSMState : FSMState
    {
        /// <summary>
        /// Transform of the panel to animate
        /// </summary>
        private Transform m_PanelTransform;

        /// <summary>
        /// Local position for Panels to sit when off the right of the screen
        /// </summary>
        private readonly Vector3 m_OffScreenPanelPosRight = new Vector3(1200f, 0f, 0f);

        /// <summary>
        /// Local position for Panels to sit when off the left of the screen
        /// </summary>
        private readonly Vector3 m_OffScreenPanelPosLeft = new Vector3(-1200f, 0f, 0f);

        /// <summary>
        /// Local position for Panels to sit when on screen
        /// </summary>
        private readonly Vector3 m_OnScreenPanelPos = Vector3.zero;

        /// <summary>
        /// The amount of time to take for transitions
        /// </summary>
        private const float m_MoveTime = 0.3f;

        /// <summary>
        /// Constructor for setting up the Panel State
        /// </summary>
        /// <param name="panelTransform">The Transform of the panel to animate</param>
        public MainMenuPanelFSMState(Transform panelTransform)
        {
            m_PanelTransform = panelTransform;
            m_PanelTransform.localPosition = m_OffScreenPanelPosRight;
        }

        /// <summary>
        /// Called by the FSM when entering the state
        /// </summary>
        /// <param name="args">Additional arguments for the state (none)</param>
        public override void Enter(params object[] args)
        {
            AnimateToCenter(m_MoveTime);
        }

        /// <summary>
        /// Called by the FSM when exiting the state
        /// </summary>
        public override void Exit()
        {
            AnimateToRight(0f);
        }

        /// <summary>
        /// Called by the FSM when the state gains focus (from Pop)
        /// </summary>
        public override void GainedFocus()
        {
            AnimateToCenter(m_MoveTime);
        }

        /// <summary>
        /// Called by the FSM when the state loses focus (from other state Push)
        /// </summary>
        public override void LostFocus()
        {
            AnimateToLeft(0f);
        }

        /// <summary>
        /// Animates the Panel to the On Screen position from it's current position
        /// </summary>
        /// <param name="delay">Time to wait before performing the transition</param>
        private void AnimateToCenter(float delay)
        {
            HOTween.To(m_PanelTransform, m_MoveTime, new TweenParms().Prop("localPosition", m_OnScreenPanelPos)
                    .Ease(EaseType.EaseOutExpo).Delay(delay));
        }

        /// <summary>
        /// Animates the Panel to the Right Off Screen position from it's current position
        /// </summary>
        /// <param name="delay">Time to wait before performing the transition</param>
        private void AnimateToRight(float delay)
        {
            HOTween.To(m_PanelTransform, m_MoveTime, new TweenParms().Prop("localPosition", m_OffScreenPanelPosRight)
                    .Ease(EaseType.EaseInExpo).Delay(delay));
        }

        /// <summary>
        /// Animates the Panel to the Left Off Screen position from it's current position
        /// </summary>
        /// <param name="delay">Time to wait before performing the transition</param>
        private void AnimateToLeft(float delay)
        {
            HOTween.To(m_PanelTransform, m_MoveTime, new TweenParms().Prop("localPosition", m_OffScreenPanelPosLeft)
                    .Ease(EaseType.EaseInExpo).Delay(delay));
        }
    }
}