using UnityEngine;
using TransTech.System.IO;
using TransTech.System.Debug;

namespace TransTech.System.GUI
{
    /// <summary>
    /// FSM State to handle the Main Menu Player Count and Rules settings screen
    /// </summary>
    public class PlayerCountRulesFSMState : MainMenuPanelFSMState
    {
        /// <summary>
        /// Reference to the Main Menu Controller
        /// </summary>
        private MainMenuController m_MMC;

        /// <summary>
        /// Reference to our parent FSM
        /// </summary>
        private MainMenuFSMEngine m_FSM;

        /// <summary>
        /// Reference to the Popup shown when player will overwrite save game.
        /// </summary>
        private Popup m_ResumePopup;
        /// <summary>
        /// Stores the number of players chosen before showing the popup
        /// </summary>
        private int m_NumPlayersChosen;

        /// <summary>
        /// Constructor for object initialization
        /// </summary>
        /// <param name="mmc">Reference to the Main Menu Controller for interfacing with Unity</param>
        /// <param name="fsm">Reference to our parent FSM</param>
        /// <param name="panelTransform">The Transform of the panel we are controlling</param>
        public PlayerCountRulesFSMState(MainMenuController mmc, MainMenuFSMEngine fsm, Transform panelTransform)
            : base(panelTransform)
        {
            m_MMC = mmc;
            m_FSM = fsm;
        }

        /// <summary>
        /// Called by the FSM when entering the state
        /// </summary>
        /// <param name="args">Optional arguments (none)</param>
        public override void Enter(params object[] args)
        {
            base.Enter(args);
            RegisterForEvents();
        }

        /// <summary>
        /// Called by the FSM when exiting the state
        /// </summary>
        public override void Exit()
        {
            base.Exit();
            DeregisterForEvents();
        }

        /// <summary>
        /// Called by the FSM when the state gains focus (all states on top are popped from the stack)
        /// </summary>
        public override void GainedFocus()
        {
            base.GainedFocus();
            RegisterForEvents();
        }

        /// <summary>
        /// Called by the FSM when the State loses focus (another state is pushed onto the stack)
        /// </summary>
        public override void LostFocus()
        {
            base.LostFocus();
            DeregisterForEvents();
        }

        /// <summary>
        /// Registers internal function to their relevant Controller events.
        /// </summary>
        private void RegisterForEvents()
        {
            m_MMC.RuleSetChangedEvent += RuleSetChanged;
            m_MMC.TwoPlayerButtonPressedEvent += TwoPlayerButtonPressed;
            m_MMC.ThreePlayerButtonPressedEvent += ThreePlayerButtonPressed;
            m_MMC.FourPlayerButtonPressedEvent += FourPlayerButtonPressed;
            m_MMC.ResumeGameButtonPressedEvent += ResumeGameButtonPressed;
			m_MMC.TutorialButtonPressedEvent += TutorialButtonPressed;
        }

        /// <summary>
        /// Deregisters internal function to their relevant Controller events.
        /// </summary>
        private void DeregisterForEvents()
        {
            m_MMC.RuleSetChangedEvent -= RuleSetChanged;
            m_MMC.TwoPlayerButtonPressedEvent -= TwoPlayerButtonPressed;
            m_MMC.ThreePlayerButtonPressedEvent -= ThreePlayerButtonPressed;
            m_MMC.FourPlayerButtonPressedEvent -= FourPlayerButtonPressed;
            m_MMC.ResumeGameButtonPressedEvent -= ResumeGameButtonPressed;
			m_MMC.TutorialButtonPressedEvent -= TutorialButtonPressed;
        }

        /// <summary>
        /// Handles when the rule set is changed in the menu
        /// </summary>
        /// <param name="newRuleSet"></param>
        private void RuleSetChanged(string newRuleSet)
        {
            m_FSM.SetRuleSet(newRuleSet);
        }

        /// <summary>
        /// Handles the Two Player button being pressed in the menu
        /// </summary>
        private void TwoPlayerButtonPressed()
        {
            if (SaveGameToFile.DoesAutosaveExist())
            {
                m_ResumePopup = Popup.CreatePopup("There is an existing game in progress.\nIf you begin a new game, it will be over written.", "Cancel", "Continue");
                m_ResumePopup.PopupButtonPressedEvent += HandleResumePopupButtonPressed;
                m_NumPlayersChosen = 2;
                m_MMC.EnablePlayerCountPanelButtons(false);
                // De register so the background items don't do anything.
                DeregisterForEvents();
            }
            else
            {
                m_FSM.SetNumPlayers(2);
                m_FSM.MoveToNameEntry();
            }
        }

        /// <summary>
        /// Handles the Three Player button being pressed in the menu
        /// </summary>
        private void ThreePlayerButtonPressed()
        {
            if (SaveGameToFile.DoesAutosaveExist())
            {
                m_ResumePopup = Popup.CreatePopup("There is an existing game in progress.\nIf you begin a new game, it will be over written.", "Cancel", "Continue");
                m_ResumePopup.PopupButtonPressedEvent += HandleResumePopupButtonPressed;
                m_NumPlayersChosen = 3;
                m_MMC.EnablePlayerCountPanelButtons(false);
                // De register so the background items don't do anything.
                DeregisterForEvents();
            }
            else
            {
                m_FSM.SetNumPlayers(3);
                m_FSM.MoveToNameEntry();
            }
        }

        /// <summary>
        /// Handles the Four Player button being pressed in the menu
        /// </summary>
        private void FourPlayerButtonPressed()
        {
            if (SaveGameToFile.DoesAutosaveExist())
            {
                m_ResumePopup = Popup.CreatePopup("There is an existing game in progress.\nIf you begin a new game, it will be over written.", "Cancel", "Continue");
                m_ResumePopup.PopupButtonPressedEvent += HandleResumePopupButtonPressed;
                m_NumPlayersChosen = 4;
                m_MMC.EnablePlayerCountPanelButtons(false);
                // De register so the background items don't do anything.
                DeregisterForEvents();
            }
            else
            {
                m_FSM.SetNumPlayers(4);
                m_FSM.MoveToNameEntry();
            }
        }

        /// <summary>
        /// Handles button presses in the popup
        /// </summary>
        /// <param name="buttonNumber">The index of the button pressed</param>
        private void HandleResumePopupButtonPressed(int buttonNumber)
        {
            switch (buttonNumber)
            {
                case 0:
                    // Cancel
                    m_ResumePopup.PopupButtonPressedEvent -= HandleResumePopupButtonPressed;
                    Popup.ReturnPopup(m_ResumePopup);
                    m_MMC.EnablePlayerCountPanelButtons(true);
                    RegisterForEvents();
                    break;
                case 1:
                    // Continue
                    m_ResumePopup.PopupButtonPressedEvent -= HandleResumePopupButtonPressed;
                    Popup.ReturnPopup(m_ResumePopup);
                    m_MMC.EnablePlayerCountPanelButtons(true);
                    m_FSM.SetNumPlayers(m_NumPlayersChosen);
                    m_FSM.MoveToNameEntry();
                    break;
                default:
                    TTDebug.LogError("PlayerCountRulesFSMState.cs : Invalid button number returned from resume game popup");
                    break;
            }
        }

        /// <summary>
        /// Handles the Resume Game button being pressed in the menu
        /// </summary>
        private void ResumeGameButtonPressed()
        {
            m_FSM.ResumeGame();
        }
		
		private void TutorialButtonPressed()
		{
			m_FSM.MoveToFirstTutorialPanel();
		}
    }
}