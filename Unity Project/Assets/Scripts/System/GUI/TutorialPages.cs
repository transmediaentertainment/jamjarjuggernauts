namespace TransTech.System.GUI
{
	/// <summary>
	/// A static class containing const strings of each tutorial page.
	/// </summary>
    public static class TutorialPages
    {
        public const string Page1Text = @"Basic Rules

										The game needs 2-4 players to play.
										The game takes place on a board with tiles in a grid.
										Some tiles are empty and players can place their pieces.
										Some tiles are unusable, like tiles an enemy has played on.
										Each player receives 3 pieces to begin, and then 2 pieces on each turn.
										Players take their turns one at a time.";

        public const string Page2Text = @"On Your Turn

										During a turn a player can:
										- Place pieces from their hand onto empty tiles on the board
										- Sacrifice 3 pieces (that are in a row) to:
										-- Fortify a tile on the board they already hold
										-- Attack and take over a non-fortified tile owned by an opposing team
										A player can end their turn at any point by pressing the end turn button.
										You do not have to play all of the pieces in your hand on your turn.";

        public const string Page3Text = @"Scoring & End Game

										Scoring is determined by connecting 3 or more pieces in a row:
										- 3 in a row is worth 1 point.
										- 4 in a row is worth 2 points.
										- 5 in a row is worth 3 points, etc.
										The score is calculated each time the board changes.
										When pieces are taken in an attack, the piece taken can cause a player to lose points.
										The score at the end of the game is the score that counts.
										The game is over when a turn is ended and the board is full.";
		
		public const string NoSacrificeLimits = @"No Sacrifice Limits
												
												You may make as many sacrifices as you wish.
												Your pieces will not mutiny, no matter how many sacrifices you make.";
		
		public const string Sacrifice3Limit = @"3 Sacrifices Per Game
												
											 	You may only make 3 sacrifices throughout the match.
											 	Your pieces will not mutiny, even if you make all 3 sacrifices.";
		
		public const string MutinyRandomEvery = @"Mutiny Meter - Random Defection, Every Turn

												You may make as many sacrifices as you wish.
												Each turn you make a sacrifice attack, your mutiny meter rises.
												The higher the meter, the greater the chance that one of your pieces will mutiny.
												Mutinying pieces will defect to a random team.
												They may mutiny even on a turn where you do not make a sacrifice.";

		public const string MutinyRandomOnly = @"Mutiny Meter - Random Defection, Turn Only

												You may make as many sacrifices as you wish.
												Each turn you make a sacrifice attack, your mutiny meter rises.
												The higher the meter, the greater the chance that one of your pieces will mutiny.
												Mutinying pieces will defect to a random team.
												They may only mutiny on a turn where you make a sacrifice.";

		public const string MutinyBestEvery = @"Mutiny Meter - Best Team Defection, Every Turn

												You may make as many sacrifices as you wish.
												Each turn you make a sacrifice attack, your mutiny meter rises.
												The higher the meter, the greater the chance that one of your pieces will mutiny.
												Mutinying pieces will defect to the enemy team with the lowest mutiny meter.
												They may mutiny even on a turn where you do not make a sacrifice.";

		public const string MutinyBestOnly = @"Mutiny Meter - Best Team Defection, Turn Only

												You may make as many sacrifices as you wish.
												Each turn you make a sacrifice attack, your mutiny meter rises.
												The higher the meter, the greater the chance that one of your pieces will mutiny.
												Mutinying pieces will defect to the enemy team with the lowest mutiny meter.
												They may only mutiny on a turn where you make a sacrifice.";
	}
}
