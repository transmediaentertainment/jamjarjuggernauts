/// <summary>
/// The Game Session interface.
/// </summary>
public interface IGameSession
{
	/// <summary>
	/// Gets or sets the name of player one.
	/// </summary>
	/// <value>
	/// The name of player one.
	/// </value>
    string PlayerOneName { get; set; }
	
	/// <summary>
	/// Gets or sets the name of player two.
	/// </summary>
	/// <value>
	/// The name of player two.
	/// </value>
    string PlayerTwoName { get; set; }
	
	/// <summary>
	/// Gets or sets the name of player three.
	/// </summary>
	/// <value>
	/// The name of player three.
	/// </value>
    string PlayerThreeName { get; set; }
	
	/// <summary>
	/// Gets or sets the name of player four.
	/// </summary>
	/// <value>
	/// The name of player four.
	/// </value>
    string PlayerFourName { get; set; }

	/// <summary>
	/// Gets or sets the player count.
	/// </summary>
	/// <value>
	/// The player count.
	/// </value>
    int PlayerCount { get; set; }
	
	/// <summary>
	/// Gets or sets the ruleset.
	/// </summary>
	/// <value>
	/// The ruleset.
	/// </value>
    string Ruleset { get; set; }
	
	/// <summary>
	/// Gets or sets player one's score.
	/// </summary>
	/// <value>
	/// Player one's score.
	/// </value>
    int PlayerOneScore { get; set; }
	
	/// <summary>
	/// Gets or sets player two's score.
	/// </summary>
	/// <value>
	/// Player two's score.
	/// </value>
    int PlayerTwoScore { get; set; }
	
	/// <summary>
	/// Gets or sets player three's score.
	/// </summary>
	/// <value>
	/// Player three's score.
	/// </value>
    int PlayerThreeScore { get; set; }
	
	/// <summary>
	/// Gets or sets player four's score.
	/// </summary>
	/// <value>
	/// Player four's score.
	/// </value>
    int PlayerFourScore { get; set; }
	
	/// <summary>
	/// Gets or sets a value indicating whether the game was completed.
	/// </summary>
	/// <value>
	/// True if the game was completed
	/// </value>
    bool GameCompleted { get; set; }
	
	/// <summary>
	/// Gets or sets the game resume count.
	/// </summary>
	/// <value>
	/// The game resume count.
	/// </value>
	int GameResumeCount { get; set; }
	
	/// <summary>
	/// Gets or sets the number of player one sacrifices.
	/// </summary>
	/// <value>
	/// The number of player one sacrifices.
	/// </value>
	int PlayerOneSacrifices { get; set; }
	/// <summary>
	/// Gets or sets the number of player two sacrifices.
	/// </summary>
	/// <value>
	/// The number of player two sacrifices.
	/// </value>
	int PlayerTwoSacrifices { get; set; }
	/// <summary>
	/// Gets or sets the number of player three sacrifices.
	/// </summary>
	/// <value>
	/// The number of player three sacrifices.
	/// </value>
	int PlayerThreeSacrifices { get; set; }
	/// <summary>
	/// Gets or sets the number of player four sacrifices.
	/// </summary>
	/// <value>
	/// The number of player four sacrifices.
	/// </value>
	int PlayerFourSacrifices { get; set; }
	
	/// <summary>
	/// Gets or sets the number of player one fortifies.
	/// </summary>
	/// <value>
	/// The number of player one fortifies.
	/// </value>
	int PlayerOneFortifies { get; set; }
	/// <summary>
	/// Gets or sets the number of player two fortifies.
	/// </summary>
	/// <value>
	/// The number of player two fortifies.
	/// </value>
	int PlayerTwoFortifies { get; set; }
	/// <summary>
	/// Gets or sets the number of player three fortifies.
	/// </summary>
	/// <value>
	/// The number of player three fortifies.
	/// </value>
	int PlayerThreeFortifies { get; set; }
	/// <summary>
	/// Gets or sets the number of player four fortifies.
	/// </summary>
	/// <value>
	/// The number of player four fortifies.
	/// </value>
	int PlayerFourFortifies { get; set; }
}