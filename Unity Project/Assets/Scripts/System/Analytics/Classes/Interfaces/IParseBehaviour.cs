/// <summary>
/// The parse behaviour interface.
/// </summary>
public interface IParseBehaviour
{
	/// <summary>
	/// News the parse behavior.
	/// </summary>
    void New();
	/// <summary>
	/// Fetches the specified object id.
	/// </summary>
	/// <param name='id'>The object id to fetch</param>
    void Fetch(string id);
	/// <summary>
	/// Gets the instance's object id.
	/// </summary>
	/// <returns>The oject id</returns>
    string GetID();
	/// <summary>
	/// Tells the instance to update on the server.
	/// </summary>
    void UpdateOnServer();
	/// <summary>
	/// Tells the instance to destroy after updating.
	/// </summary>
    void DestroyWhenUpdated();
}