using TransTech.System.Debug;
using UnityEngine;
using Parse;

/// <summary>
/// Uni parse's parse behaviour implementation.
/// </summary>
public abstract class ParseBehaviour : MonoBehaviour, IParseBehaviour
{
	/// <summary>
	/// Flag to tell object to destroy after updating.
	/// </summary>
    private bool m_ShouldDestroy = false;
	/// <summary>
	/// The parse instance.
	/// </summary>
    protected ParseObject m_Instance;
	
	/// <summary>
	/// New this instance.
	/// </summary>
    public abstract void New();
	/// <summary>
	/// Fetch the specified object id.
	/// </summary>
	/// <param name='id'>The object id to fetch</param>
    public abstract void Fetch(string id);
	/// <summary>
	/// Gets the instance's object id.
	/// </summary>
	/// <returns>The oject id</returns>
    public string GetID()
    {
        if (m_Instance == null)
        {
            TTDebug.LogError("ParseBehaviour.cs : No instance created yet.");
            return null;
        }
        return m_Instance.ObjectId;
    }
	
	/// <summary>
	/// Tells the instance to update on the server.
	/// </summary>
    public virtual void UpdateOnServer()
    {
        m_Instance.SaveAsync().ContinueWith((t) =>
        {
            if (m_ShouldDestroy)
            {
                Destroy(gameObject);
            }
        });
    }
	
	/// <summary>
	/// Tells the instance to destroy after updating.
	/// </summary>
    public void DestroyWhenUpdated()
    {
        m_ShouldDestroy = true;
    }
}