using System.Collections;
using Parse;

/// <summary>
/// Parse's game session implementation.
/// </summary>
public class ParseGameSession : ParseBehaviour, IGameSession
{
   /// <summary>
	/// Gets or sets the name of player one.
	/// </summary>
	/// <value>
	/// The name of player one.
	/// </value>
    public string PlayerOneName { get; set; }
	
	/// <summary>
	/// Gets or sets the name of player two.
	/// </summary>
	/// <value>
	/// The name of player two.
	/// </value>
    public string PlayerTwoName { get; set; }
	
	/// <summary>
	/// Gets or sets the name of player three.
	/// </summary>
	/// <value>
	/// The name of player three.
	/// </value>
    public string PlayerThreeName { get; set; }
	
	/// <summary>
	/// Gets or sets the name of player four.
	/// </summary>
	/// <value>
	/// The name of player four.
	/// </value>
    public string PlayerFourName { get; set; }

	/// <summary>
	/// Gets or sets the player count.
	/// </summary>
	/// <value>
	/// The player count.
	/// </value>
    public int PlayerCount { get; set; }
	
	/// <summary>
	/// Gets or sets the ruleset.
	/// </summary>
	/// <value>
	/// The ruleset.
	/// </value>
    public string Ruleset { get; set; }
	
	/// <summary>
	/// Gets or sets player one's score.
	/// </summary>
	/// <value>
	/// Player one's score.
	/// </value>
    public int PlayerOneScore { get; set; }
	
	/// <summary>
	/// Gets or sets player two's score.
	/// </summary>
	/// <value>
	/// Player two's score.
	/// </value>
    public int PlayerTwoScore { get; set; }
	
	/// <summary>
	/// Gets or sets player three's score.
	/// </summary>
	/// <value>
	/// Player three's score.
	/// </value>
    public int PlayerThreeScore { get; set; }
	
	/// <summary>
	/// Gets or sets player four's score.
	/// </summary>
	/// <value>
	/// Player four's score.
	/// </value>
    public int PlayerFourScore { get; set; }
	
	/// <summary>
	/// Gets or sets a value indicating whether the game was completed.
	/// </summary>
	/// <value>
	/// True if the game was completed
	/// </value>
    public bool GameCompleted { get; set; }
	
	/// <summary>
	/// Gets or sets the game resume count.
	/// </summary>
	/// <value>
	/// The game resume count.
	/// </value>
	public int GameResumeCount { get; set; }
	
	/// <summary>
	/// Gets or sets the number of player one sacrifices.
	/// </summary>
	/// <value>
	/// The number of player one sacrifices.
	/// </value>
	public int PlayerOneSacrifices { get; set; }
	/// <summary>
	/// Gets or sets the number of player two sacrifices.
	/// </summary>
	/// <value>
	/// The number of player two sacrifices.
	/// </value>
	public int PlayerTwoSacrifices { get; set; }
	/// <summary>
	/// Gets or sets the number of player three sacrifices.
	/// </summary>
	/// <value>
	/// The number of player three sacrifices.
	/// </value>
	public int PlayerThreeSacrifices { get; set; }
	/// <summary>
	/// Gets or sets the number of player four sacrifices.
	/// </summary>
	/// <value>
	/// The number of player four sacrifices.
	/// </value>
	public int PlayerFourSacrifices { get; set; }
	
	/// <summary>
	/// Gets or sets the number of player one fortifies.
	/// </summary>
	/// <value>
	/// The number of player one fortifies.
	/// </value>
	public int PlayerOneFortifies { get; set; }
	/// <summary>
	/// Gets or sets the number of player two fortifies.
	/// </summary>
	/// <value>
	/// The number of player two fortifies.
	/// </value>
	public int PlayerTwoFortifies { get; set; }
	/// <summary>
	/// Gets or sets the number of player three fortifies.
	/// </summary>
	/// <value>
	/// The number of player three fortifies.
	/// </value>
	public int PlayerThreeFortifies { get; set; }
	/// <summary>
	/// Gets or sets the number of player four fortifies.
	/// </summary>
	/// <value>
	/// The number of player four fortifies.
	/// </value>
	public int PlayerFourFortifies { get; set; }
	
	/// <summary>
	/// News the Uni Parse game session
	/// </summary>
    public override void New()
    {
        m_Instance = new ParseObject("GameSession");
        m_Instance.SaveAsync();
    }
	
	/// <summary>
	/// Fetch the specified object id.
	/// </summary>
	/// <param name='id'>The object id to fetch</param>
    public override void Fetch(string id)
    {
        ParseObject.GetQuery("GameSession").GetAsync(id).ContinueWith((t) => {
            if (t.IsFaulted)
            {
                // TODO : Handle Error

            }
            else
            {
                m_Instance = t.Result;

                GameResumeCount += m_Instance.Get<int>("gameResumeCount");

                PlayerOneName = m_Instance.Get<string>("p1Name");
                PlayerTwoName = m_Instance.Get<string>("p2Name");
                PlayerThreeName = m_Instance.Get<string>("p3Name");
                PlayerFourName = m_Instance.Get<string>("p4Name");

                PlayerCount = m_Instance.Get<int>("playerCount");

                Ruleset = m_Instance.Get<string>("ruleset");

                PlayerOneScore = m_Instance.Get<int>("p1Score");
                PlayerTwoScore = m_Instance.Get<int>("p2Score");
                PlayerThreeScore = m_Instance.Get<int>("p3Score");
                PlayerFourScore = m_Instance.Get<int>("p4Score");

                PlayerOneSacrifices += m_Instance.Get<int>("p1Sacrifices");
                PlayerTwoSacrifices += m_Instance.Get<int>("p2Sacrifices");
                PlayerThreeSacrifices += m_Instance.Get<int>("p3Sacrifices");
                PlayerFourSacrifices += m_Instance.Get<int>("p4Sacrifices");

                PlayerOneFortifies += m_Instance.Get<int>("p1Fortifies");
                PlayerTwoFortifies += m_Instance.Get<int>("p2Fortifies");
                PlayerThreeFortifies += m_Instance.Get<int>("p3Fortifies");
                PlayerFourFortifies += m_Instance.Get<int>("p4Fortifies");

                UpdateOnServer();
            }
        });
    }
	
	/// <summary>
	/// Updates the vars on the server.
	/// </summary>
    public override void UpdateOnServer()
    {
        m_Instance["p1Name"] = PlayerOneName;
        m_Instance["p2Name"] = PlayerTwoName;
        m_Instance["p3Name"] = PlayerThreeName;
        m_Instance["p4Name"] = PlayerFourName;

        m_Instance["playerCount"] = PlayerCount;

        m_Instance["ruleset"] = Ruleset;

        m_Instance["p1Score"] = PlayerOneScore;
        m_Instance["p2Score"] = PlayerTwoScore;
        m_Instance["p3Score"] = PlayerThreeScore;
        m_Instance["p4Score"] = PlayerFourScore;

        m_Instance["gameCompleted"] = GameCompleted;
		
		m_Instance["gameResumeCount"] = GameResumeCount;
		
		m_Instance["p1Sacrifices"] = PlayerOneSacrifices;
		m_Instance["p2Sacrifices"] = PlayerTwoSacrifices;
		m_Instance["p3Sacrifices"] = PlayerThreeSacrifices;
		m_Instance["p4Sacrifices"] = PlayerFourSacrifices;
		
		m_Instance["p1Fortifies"] = PlayerOneFortifies;
		m_Instance["p2Fortifies"] = PlayerTwoFortifies;
		m_Instance["p3Fortifies"] = PlayerThreeFortifies;
		m_Instance["p4Fortifies"] = PlayerFourFortifies;

        base.UpdateOnServer();
    }
}