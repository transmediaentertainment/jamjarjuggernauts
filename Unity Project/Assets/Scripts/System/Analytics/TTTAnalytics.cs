using TransTech.System.Debug;

/// <summary>
/// Tic Tac Total analytics singleton system
/// </summary>
public class TTTAnalytics : MonoSingleton<TTTAnalytics>
{
	/// <summary>
	/// Init the TTTAnalytics
	/// </summary>
    public override void Init()
    {
        base.Init();
        DontDestroyOnLoad(gameObject);
    }
	
	/// <summary>
	/// The current game session.
	/// </summary>
    private IGameSession m_CurrentGameSession;
	/// <summary>
	/// Gets the current game session.
	/// </summary>
	/// <value>The current game session.</value>
	public IGameSession CurrentGameSession { get { return m_CurrentGameSession; } }
	
	/// <summary>
	/// Called when the game begins
	/// </summary>
	/// <param name='p1Name'>Player 1's name.</param>
	/// <param name='p2Name'>Player 2's name.</param>
	/// <param name='p3Name'>Player 3's name.</param>
	/// <param name='p4Name'>Player 4's name.</param>
	/// <param name='playerCount'>The player count.</param>
	/// <param name='ruleset'>The current ruleset.</param>
    public void GameBegan(string p1Name, string p2Name, string p3Name, string p4Name, int playerCount, string ruleset)
    {
        m_CurrentGameSession = gameObject.AddComponent<ParseGameSession>();
        ((IParseBehaviour)m_CurrentGameSession).New();
        m_CurrentGameSession.PlayerOneName = p1Name;
        m_CurrentGameSession.PlayerTwoName = p2Name;
        m_CurrentGameSession.PlayerThreeName = p3Name;
        m_CurrentGameSession.PlayerFourName = p4Name;
        m_CurrentGameSession.PlayerCount = playerCount;
        m_CurrentGameSession.Ruleset = ruleset;
        ((IParseBehaviour)m_CurrentGameSession).UpdateOnServer();
    }
	
	/// <summary>
	/// Called when the game resumes.
	/// </summary>
	/// <param name='gameSessionID'>The Game session ID.</param>
    public void GameResumed(string gameSessionID)
    {
        m_CurrentGameSession = gameObject.AddComponent<ParseGameSession>();
        ((IParseBehaviour)m_CurrentGameSession).Fetch(gameSessionID);
    }
	
	/// <summary>
	/// Called when the the ends.
	/// </summary>
	/// <param name='p1Score'>Player 1's score.</param>
	/// <param name='p2Score'>Player 2's score.</param>
	/// <param name='p3Score'>Player 3's score.</param>
	/// <param name='p4Score'>Player 4's score.</param>
    public void GameEnded(int p1Score, int p2Score, int p3Score, int p4Score)
    {
        if (m_CurrentGameSession == null)
        {
            TTDebug.LogError("TTTAnalytics.cs : Attempting to End a Game Session without starting one.");
            return;
        }

        m_CurrentGameSession.PlayerOneScore = p1Score;
        m_CurrentGameSession.PlayerTwoScore = p2Score;
        m_CurrentGameSession.PlayerThreeScore = p3Score;
        m_CurrentGameSession.PlayerFourScore = p4Score;
        m_CurrentGameSession.GameCompleted = true;
        ((IParseBehaviour)m_CurrentGameSession).UpdateOnServer();
        ((IParseBehaviour)m_CurrentGameSession).DestroyWhenUpdated();
    }
	
	/// <summary>
	/// Gets the game session object ID.
	/// </summary>
	/// <returns>The game session object ID.</returns>
    public string GetGameSessionObjectID()
    {
        if (m_CurrentGameSession == null)
        {
            TTDebug.LogError("TTTAnalytics.cs : Attempting to get ID from game session without starting one.");
            return "";
        }

        return ((IParseBehaviour)m_CurrentGameSession).GetID();
    }
}