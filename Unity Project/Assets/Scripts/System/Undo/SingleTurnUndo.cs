using System.Collections.Generic;
using TransTech.Delegates;
using TransTech.Gameplay.Players;
using TransTech.Gameplay.States;
using TransTech.Nodes;
using TransTech.Patterns.Memento;
using UnityEngine;

namespace TransTech.System.Undo
{
    /// <summary>
    /// Manages the Undo stack for a single player turn
    /// for all player states and node states
    /// </summary>
    public class SingleTurnUndo
    {
        /// <summary>
        /// Memento Caretaker to handle Player One's states
        /// </summary>
        private MementoCaretaker<PlayerMemento> m_PlayerOneCaretaker = new MementoCaretaker<PlayerMemento>();
        /// <summary>
        /// Current state of Player One
        /// </summary>
        private PlayerMemento m_PlayerOneCurrentState;
        /// <summary>
        /// Memento Caretaker to handle Player Two's states
        /// </summary>
        private MementoCaretaker<PlayerMemento> m_PlayerTwoCaretaker = new MementoCaretaker<PlayerMemento>();
        /// <summary>
        /// Current state of Player Two
        /// </summary>
        private PlayerMemento m_PlayerTwoCurrentState;
        /// <summary>
        /// Memento Caretaker to handle Player Three's states
        /// </summary>
        private MementoCaretaker<PlayerMemento> m_PlayerThreeCaretaker = new MementoCaretaker<PlayerMemento>();
        /// <summary>
        /// Current state of Player Three
        /// </summary>
        private PlayerMemento m_PlayerThreeCurrentState;
        /// <summary>
        /// Memento Caretaker to handle Player Four's states
        /// </summary>
        private MementoCaretaker<PlayerMemento> m_PlayerFourCaretaker = new MementoCaretaker<PlayerMemento>();
        /// <summary>
        /// Current state of Player Four
        /// </summary>
        private PlayerMemento m_PlayerFourCurrentState;

        /// <summary>
        /// List of indices that were sacrifice attack moves
        /// </summary>
        private List<int> m_SacrificeAttackIndices = new List<int>();

        /// <summary>
        /// List of indices that were sacrifice fortify moves
        /// </summary>
        private List<int> m_SacrificeFortifyIndices = new List<int>();

        /// <summary>
        /// Dictionary of Caretakers for each node on the board, 
        /// indexed by their UniqueID number
        /// </summary>
        private Dictionary<uint, MementoCaretaker<NodeMemento>> m_NodeCaretakers;
        /// <summary>
        /// Dictionary of the current state of each node on the board,
        /// indexed by their UniqueID Number
        /// </summary>
        private Dictionary<uint, NodeMemento> m_NodeCurrentStates;

        /// <summary>
        /// Local reference the the current NodeMesh
        /// </summary>
        private NodeMesh m_NodeMesh;
        /// <summary>
        /// Local reference to the current GUI Manager
        /// </summary>
        private InGameGUIManager m_GUIManager;

        /// <summary>
        /// Is the Undo System on?
        /// </summary>
        private bool m_IsOn = false;

        public event VoidDelegate UndoneSacrificeAttackEvent;
        public event VoidDelegate UndoneSacrificeFortifyEvent;

        /// <summary>
        /// Initializes the object
        /// </summary>
        /// <param name="nodeMesh">The Node Mesh in the current game</param>
        /// <param name="fsm">The FSM running the game</param>
        /// <param name="guiManager">The GUI Manager in the current scene</param>
        public void Init(GameFSMEngine fsm, InGameGUIManager guiManager, NodeMesh nodeMesh)
        {
            m_GUIManager = guiManager;
            m_NodeMesh = nodeMesh;
            m_NodeCaretakers = new Dictionary<uint, MementoCaretaker<NodeMemento>>();
            m_NodeCurrentStates = new Dictionary<uint, NodeMemento>();
            var nodes = nodeMesh.GetNodes();
            foreach (var kvp in nodes)
            {
                m_NodeCaretakers.Add(kvp.Key, new MementoCaretaker<NodeMemento>());
                m_NodeCurrentStates.Add(kvp.Key, null);
            }
            TurnOnUndo();

            // At the start of a sacrifice, we turn off Undo and allow another state to handle it
            fsm.SacrificeStateBeganEvent += TurnOffUndo;
            // At the end of a sacrifice, we store that state and turn ourselves back on
            fsm.SacrificeAttackCompletedEvent += () => { StoreState(true, false); TurnOnUndo(); };
            fsm.SacrificeFortifyCompletedEvent += () => { StoreState(true, true); TurnOnUndo(); };
            // If the sacrifice is cancelled, we undo back to the state before the sacrifice was started, and turn ourselves back on.
            fsm.PlayerCancelledAttackOrFortifyStateEvent += () => { RestoreState(false); TurnOnUndo(); };
        }

        /// <summary>
        /// Called at the start of each turn, to store the state of the game.
        /// </summary>
        /// <param name="player">The number of the current player</param>
        public void BeginTurn(int player)
        {
            TransTech.System.Debug.TTDebug.Log("Undo Begin Turn");
            // Clear everything
            m_PlayerOneCaretaker.Clear();
            m_PlayerOneCurrentState = null;
            m_PlayerTwoCaretaker.Clear();
            m_PlayerTwoCurrentState = null;
            m_PlayerThreeCaretaker.Clear();
            m_PlayerThreeCurrentState = null;
            m_PlayerFourCaretaker.Clear();
            m_PlayerFourCurrentState = null;

            // Clear all the node caretakers
            foreach (var kvp in m_NodeCaretakers)
            {
                kvp.Value.Clear();
                m_NodeCurrentStates[kvp.Key] = null;
            }

            // Grab initial States
            StoreState(false);
        }

        /// <summary>
        /// Collects and stores the state of everything in the game.
        /// </summary>
        public void StoreState(bool wasSacrifice, bool wasFortify = false)
        {
            TransTech.System.Debug.TTDebug.Log("Storing state");
            var gc = GameController.Instance;
            // We push the current state onto the history stack, then 
            // get each object to create a new memento, which we store 
            // as the new current state.

            // Assume player one and two are valid
            if(m_PlayerOneCurrentState != null)
                m_PlayerOneCaretaker.PushMemento(m_PlayerOneCurrentState);
            m_PlayerOneCurrentState = gc.PlayerOne.CreateMemento();

            if(m_PlayerTwoCurrentState != null)
                m_PlayerTwoCaretaker.PushMemento(m_PlayerTwoCurrentState);
            m_PlayerTwoCurrentState = gc.PlayerTwo.CreateMemento();

            if (gc.PlayerThree != null)
            {
                if(m_PlayerThreeCurrentState != null)
                    m_PlayerThreeCaretaker.PushMemento(m_PlayerThreeCurrentState);
                m_PlayerThreeCurrentState = gc.PlayerThree.CreateMemento();
            }

            if (gc.PlayerFour != null)
            {
                if(m_PlayerFourCurrentState != null)
                    m_PlayerFourCaretaker.PushMemento(m_PlayerFourCurrentState);
                m_PlayerFourCurrentState = gc.PlayerFour.CreateMemento();
            }

            var nodes = m_NodeMesh.GetNodes();
            foreach (var kvp in nodes)
            {
                var ct = m_NodeCaretakers[kvp.Key];
                if(m_NodeCurrentStates[kvp.Key] != null)
                    ct.PushMemento(m_NodeCurrentStates[kvp.Key]);
                m_NodeCurrentStates[kvp.Key] = kvp.Value.CreateMemento();
            }

            if (wasSacrifice)
            {
                if (wasFortify)
                    m_SacrificeFortifyIndices.Add(m_PlayerOneCaretaker.Count - 1);
                else
                    m_SacrificeAttackIndices.Add(m_PlayerOneCaretaker.Count - 1);
            }
        }

        /// <summary>
        /// Performs an Undo of a single state across all stored objects.
        /// </summary>
        public void Undo()
        {
            RestoreState(true);
        }

        private void RestoreState(bool isUndo)
        {
            // Early exit if there isn't anything available
            if (m_PlayerOneCaretaker.Count == 0 && isUndo)
                return;
            else if (m_PlayerOneCurrentState == null && !isUndo)
                return;


            // We remove our event listener so we don't store the undone state again.
            TurnOffUndo();

            var gc = GameController.Instance;

            // We pop the last state from the history stack, 
            // store it as the current state, then set the memento
            // in each of the stored objects as the new current state.

            var undoCount = m_PlayerOneCaretaker.Count;

            if(isUndo)
                m_PlayerOneCurrentState = m_PlayerOneCaretaker.PopMemento();
            gc.PlayerOne.SetMemento(m_PlayerOneCurrentState);
            if(isUndo)
                m_PlayerTwoCurrentState = m_PlayerTwoCaretaker.PopMemento();
            gc.PlayerTwo.SetMemento(m_PlayerTwoCurrentState);
            if (gc.PlayerThree != null)
            {
                if(isUndo)
                    m_PlayerThreeCurrentState = m_PlayerThreeCaretaker.PopMemento();
                gc.PlayerThree.SetMemento(m_PlayerThreeCurrentState);
            }
            if (gc.PlayerFour != null)
            {
                if(isUndo)
                    m_PlayerFourCurrentState = m_PlayerFourCaretaker.PopMemento();
                gc.PlayerFour.SetMemento(m_PlayerFourCurrentState);
            }

            var nodes = m_NodeMesh.GetNodes();
            foreach (var kvp in nodes)
            {
                if(isUndo)
                    m_NodeCurrentStates[kvp.Key] = m_NodeCaretakers[kvp.Key].PopMemento();
                kvp.Value.SetMemento(m_NodeCurrentStates[kvp.Key]);
            }
            
            // Check if it was a sacrifice undo
            if (m_SacrificeAttackIndices.Contains(undoCount - 1))
            {
                TransTech.System.Debug.TTDebug.Log("Undone Attack Sacrifice!");
                if (UndoneSacrificeAttackEvent != null)
                    UndoneSacrificeAttackEvent();
                m_SacrificeAttackIndices.Remove(undoCount - 1);
            }
            else if (m_SacrificeFortifyIndices.Contains(undoCount - 1))
            {
                TransTech.System.Debug.TTDebug.Log("Undone Fortify Sacrifice!");
                if (UndoneSacrificeFortifyEvent != null)
                    UndoneSacrificeFortifyEvent();
                m_SacrificeFortifyIndices.Remove(undoCount - 1);
            }

            // Begin listening to board change events again.
            TurnOnUndo();
        }

        /// <summary>
        /// Wrapper method so we can subscribe to events properly.
        /// </summary>
        private void FalseStoreState()
        {
            StoreState(false);
        }

        /// <summary>
        /// Registers the Undo system for the relevant events.
        /// </summary>
        public void TurnOnUndo()
        {
            if (!m_IsOn)
            {
                TransTech.System.Debug.TTDebug.Log("Turning on Undo");
                m_NodeMesh.BoardChangedEvent += FalseStoreState;
                m_GUIManager.UndoButtonPressedEvent += Undo;
                m_IsOn = true;
            }
        }

        /// <summary>
        /// Deregisters the Undo system for the relevant events.
        /// </summary>
        public void TurnOffUndo()
        {
            if (m_IsOn)
            {
                TransTech.System.Debug.TTDebug.Log("Turning off undo");
                m_NodeMesh.BoardChangedEvent -= FalseStoreState;
                m_GUIManager.UndoButtonPressedEvent -= Undo;
                m_IsOn = false;
            }
        }
    }
}