﻿using TransTech.Delegates;
using System;
using UnityEngine;
using System.Collections;

// This should be moved to the TransTech namespace in Unity 4
/// <summary>
/// Provides events for Unity Update and Fixed Update events
/// </summary>
public class SystemEventCenter : MonoSingleton<SystemEventCenter>
{
	/// <summary>
	/// Fired before all update events.
	/// </summary>
    public event FloatDelegate EarlyUpdateEvent;
	
	/// <summary>
	/// Fired after all early update events and before all late update events.
	/// </summary>
    public event FloatDelegate UpdateEvent;
	
	/// <summary>
	/// Fired after all update events.
	/// </summary>
    public event FloatDelegate LateUpdateEvent;
	
	/// <summary>
	/// Fired before all fixed update events.
	/// </summary>
    public event FloatDelegate EarlyFixedUpdateEvent;
	
	/// <summary>
	/// Fired after all early fixed update events and before all late fixed update events.
	/// </summary>
    public event FloatDelegate FixedUpdateEvent;
	
	/// <summary>
	/// Fired after all fixed update events.
	/// </summary>
    public event FloatDelegate LateFixedUpdateEvent;
	
	/// <summary>
	/// The delta time.
	/// </summary>
    private float m_DeltaTime;
	/// <summary>
	/// The fixed delta time.
	/// </summary>
    private float m_FixedDeltaTime;

	/// <summary>
	/// Called when Unity updates, calls early update then regular update
	/// </summary>
    private void Update()
    {
        m_DeltaTime = Time.deltaTime;
        if (EarlyUpdateEvent != null)
            EarlyUpdateEvent(m_DeltaTime);

        if (UpdateEvent != null)
            UpdateEvent(m_DeltaTime);
    }
	
	/// <summary>
	/// Called when Unity late updates, calls late update
	/// </summary>
    private void LateUpdate()
    {
        if (LateUpdateEvent != null)
            LateUpdateEvent(m_DeltaTime);
    }
	
	/// <summary>
	/// Called when Unity fixed updates, calls early fixed update then regular fixed update then later fixed update
	/// </summary>
    private void FixedUpdate()
    {
        m_FixedDeltaTime = Time.fixedDeltaTime;

        if (EarlyFixedUpdateEvent != null)
            EarlyFixedUpdateEvent(m_FixedDeltaTime);

        if (FixedUpdateEvent != null)
            FixedUpdateEvent(m_FixedDeltaTime);

        if (LateFixedUpdateEvent != null)
            LateFixedUpdateEvent(m_FixedDeltaTime);
    }

    public void RunCoroutine(Func<IEnumerator> routine)
    {
        StartCoroutine(routine());
    }

    public void RunCoroutine<T>(Func<T, IEnumerator> routine, T arg)
    {
        StartCoroutine(routine(arg));
    }

    public void RunCoroutine<T1, T2>(Func<T1, T2, IEnumerator> routine, T1 arg1, T2 arg2)
    {
        StartCoroutine(routine(arg1, arg2));
    }

    public void RunCoroutine<T1, T2, T3>(Func<T1, T2, T3, IEnumerator> routine, T1 arg1, T2 arg2, T3 arg3)
    {
        StartCoroutine(routine(arg1, arg2, arg3));
    }
}