using TransTech.System.Input;
using UnityEngine;
using UnityExtensions;
using TransTech.System.Debug;

[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour
{
	/// <summary>
	/// The mouse scroll speed.
	/// </summary>
    [SerializeField]
    private float m_ScrollSpeed = 1.0f;
	
	/// <summary>
	/// The left-most horizontal pos the camera can move to
	/// </summary>
    [SerializeField]
    private float m_MinX;
	
	/// <summary>
	/// The right-most horizontal pos the camera can move to
	/// </summary>
    [SerializeField]
    private float m_MaxX;
	
	/// <summary>
	/// The botton-most vertical pos the camera can move to
	/// </summary>
    [SerializeField]
    private float m_MinZ;
	
	/// <summary>
	/// The top-most vertical pos the camera can move to
	/// </summary>
    [SerializeField]
    private float m_MaxZ;
	
	/// <summary>
	/// The minumum height the camera can move to
	/// </summary>
    [SerializeField]
    private float m_MinHeight = 3.0f;
	
	/// <summary>
	/// The maximum height the camera can move to
	/// </summary>
    [SerializeField]
    private float m_MaxHeight = 12.0f;
	
	/// <summary>
	/// The center position of the board.
	/// </summary>
    [SerializeField]
    private Vector3 m_CenterBoardPos;
	
	/// <summary>
	/// The width of the panel background.
	/// </summary>
    private const int m_PanelBackgroundWidth = 208;
	
	/// <summary>
	/// The raycast plane.
	/// </summary>
	[SerializeField]
	private GameObject m_RaycastPlane;
	
	/// <summary>
	/// The raycast layer.
	/// </summary>
	private int m_RaycastLayer;
	
	/// <summary>
	/// The previous raycast position.
	/// </summary>
	private Vector3 m_PrevRayPos;
	
#if UNITY_IPHONE || UNITY_ANDROID
	/// <summary>
	/// The Touch Camera Control FSM engine, for iOS or Android.
	/// </summary>
	/*private TouchCameraControlFSMEngine m_FSM;*/
#endif
	
	/// <summary>
	/// Awake this instance.
	/// </summary>
    private void Awake()
    {
		m_RaycastLayer = 1 << LayerMask.NameToLayer("CameraRaycastPlane");
		
        CameraManager.Instance.RegisterCamera(camera);

        InputMouseClickEvents.Instance.RightMouseDown += HandleRightMouseDown;
		InputMouseClickEvents.Instance.RightMouseHeld += HandleRightMouseHeld;
        InputMouseClickEvents.Instance.RightMouseUp += HandleRightMouseUp;

        InputMouseMovementEvents.Instance.ScrollWheelMovedEvent += HandleMouseScroll;
    }
	
	/// <summary>
	/// Init the camera controller.
	/// </summary>
	/// <param name='centerBoardPos'>The center position of the board.</param>
	/// <param name='scrollSpeed'>The mouse scroll speed.</param>
	/// <param name='minX'>The left-most horizontal pos the camera can move to</param>
	/// <param name='maxX'>The right-most horizontal pos the camera can move to</param>
	/// <param name='minZ'>The bottom-most vertical pos the camera can move to</param>
	/// <param name='maxZ'>The top-most vertical pos the camera can move to</param>
	/// <param name='minHeight'>The minimum height the camera cam move to</param>
	/// <param name='maxHeight'>The maximum height the camera cam move to</param>
    public void Init(Vector3 centerBoardPos, float scrollSpeed, float minX, float maxX, float minZ, float maxZ, float minHeight, float maxHeight)
    {
        m_CenterBoardPos = centerBoardPos;
        m_ScrollSpeed = scrollSpeed;
        UpdateMinMax(minX, maxX, minZ, maxZ, minHeight, maxHeight);

        transform.position = m_CenterBoardPos + new Vector3(0f, (m_MaxHeight - m_MinHeight) / 2f + m_MinHeight, 0f);

        var ratio = 768f / Screen.height;
        var pixSize = m_PanelBackgroundWidth / ratio;
        Debug.Log("Pix Size : " + pixSize);
        var newPercent = pixSize / Screen.width;
        camera.rect = new Rect(newPercent, 0f, 1f - newPercent, 1f);
		
#if UNITY_IPHONE || UNITY_ANDROID
		/*m_FSM = */new TouchCameraControlFSMEngine( camera, minX, maxX, minHeight, maxHeight, minZ, maxZ, 0.02f );	
#endif
    }
	
	/// <summary>
	/// Updates the min and max of X, Z and height (Y).
	/// </summary>
	/// <param name='minX'>The left-most horizontal pos the camera can move to</param>
	/// <param name='maxX'>The right-most horizontal pos the camera can move to</param>
	/// <param name='minZ'>The bottom-most vertical pos the camera can move to</param>
	/// <param name='maxZ'>The top-most vertical pos the camera can move to</param>
	/// <param name='minHeight'>The minimum height the camera cam move to</param>
	/// <param name='maxHeight'>The maximum height the camera cam move to</param>
    public void UpdateMinMax(float minX, float maxX, float minZ, float maxZ, float minHeight, float maxHeight)
    {
        m_MinX = minX;
        m_MaxX = maxX;
        m_MinZ = minZ;
        m_MaxZ = maxZ;
        m_MinHeight = minHeight;
        m_MaxHeight = maxHeight;
    }
	
	/// <summary>
	/// Handles the right mouse down.
	/// </summary>
    private void HandleRightMouseDown()
    {
        Screen.showCursor = false;
		
		var ray = camera.ScreenPointToRay( Input.mousePosition );
		RaycastHit hit;
		if( Physics.Raycast( ray, out hit, 100f, m_RaycastLayer ) )
		{
			m_PrevRayPos = hit.point;
		}
		else 
		{
			TTDebug.LogError("CameraController.cs : Raycast didn't hit anything, expand size of raycase plane");
		}
    }
	
	/// <summary>
	/// Handles the right mouse held.
	/// </summary>
	private void HandleRightMouseHeld()
	{
		RaycastCamera( Input.mousePosition );
	}
	
	/// <summary>
	/// Handles the right mouse up.
	/// </summary>
    private void HandleRightMouseUp()
    {
        Screen.showCursor = true;
    }
	
	/// <summary>
	/// Raycasts the camera to determine screen movement and keep it smooth and 1-1.
	/// </summary>
	/// <param name='screenPos'>The screen position.</param>
	private void RaycastCamera( Vector3 screenPos )
	{
		var ray = camera.ScreenPointToRay( screenPos );
		RaycastHit hit;
		if( Physics.Raycast( ray, out hit, 100f, m_RaycastLayer ) )
		{
			var hitPos = hit.point;
			var delta = m_PrevRayPos - hitPos;
			var pos = transform.position;
			pos += delta;
			transform.position = pos;
			transform.SetX(Mathf.Clamp(transform.position.x, m_MinX, m_MaxX));
       		transform.SetZ(Mathf.Clamp(transform.position.z, m_MinZ, m_MaxZ));
			m_PrevRayPos = hitPos + delta;
		}
		else 
		{
			TTDebug.LogError("CameraController.cs : Raycast didn't hit anything, expand size of raycase plane");
		}
	}
	
	/// <summary>
	/// Handles the mouse scroll.
	/// </summary>
	/// <param name='scroll'>The amount of scroll wheel movement.</param>
    private void HandleMouseScroll(float scroll)
    {
        transform.MoveY(scroll * m_ScrollSpeed);

        transform.SetY(Mathf.Clamp(transform.position.y, m_MinHeight, m_MaxHeight));
    }
}