using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoSingleton<CameraManager>
{
    /// <summary>
    /// The list of cameras.
    /// </summary>
    private List<Camera> m_Cameras;
	
	/// <summary>
	/// Init the camera manager
	/// </summary>
    public override void Init()
    {
        var cameras = FindObjectsOfType(typeof(Camera)) as Camera[];
        m_Cameras = new List<Camera>();
        foreach (var camera1 in cameras)
        {
            var uicamera = camera1.GetComponent<UICamera>();
            if (uicamera == null)
                m_Cameras.Add(camera1);
        }
    }
	
	/// <summary>
	/// Gets the cameras used for raycasting.
	/// </summary>
	/// <returns>The cameras used for raycasting.</returns>
	/// <param name='rayScreenPos'>Raycast screen position.</param>
    public Camera[] GetCamerasForRaycasting(Vector3 rayScreenPos)
    {
        var cams = new List<Camera>();

        for (int i = 0; i < m_Cameras.Count; i++)
        {
            if (m_Cameras[i].pixelRect.Contains(rayScreenPos))
                cams.Add(m_Cameras[i]);
        }
        return cams.ToArray();
    }
	
	/// <summary>
	/// Gets the board camera.
	/// </summary>
	/// <returns>The board camera.</returns>
    public Camera GetBoardCamera()
    {
        foreach (var cam in m_Cameras)
        {
            if (cam.GetComponent<CameraController>() != null)
                return cam;
        }
        return null;
    }
	
	/// <summary>
	/// Registers a camera.
	/// </summary>
	/// <param name='cam'>The camera to register</param>
    public void RegisterCamera(Camera cam)
    {
        if (m_Cameras.Contains(cam))
            return;
        m_Cameras.Add(cam);
    }
	
	/// <summary>
	/// Deregisters a camera.
	/// </summary>
	/// <param name='cam'>The camera to deregister</param>
    public void DeregisterCamera(Camera cam)
    {
        if (m_Cameras.Contains(cam))
        {
            m_Cameras.Remove(cam);
        }
    }
}