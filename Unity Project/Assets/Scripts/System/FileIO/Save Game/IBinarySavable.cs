using System.IO;

namespace TransTech.System.IO
{
	/// <summary>
	/// Interface to conform class to Saving and Laoding from binary
	/// </summary>
    public interface IBinarySavable
    {
        bool SaveWithBinaryWriter(BinaryWriter bw);
        bool LoadFromBinaryReader(BinaryReader br);
    }
}
