using TransTech.Gameplay.Players;
using System.IO;
using UnityEngine;
using System;
using TransTech.System.Debug;

namespace TransTech.System.IO
{
	/// <summary>
	/// Class which handles saving autosave games to file
	/// </summary>
    public static class SaveGameToFile
    {
		/// <summary>
		/// The file path diectory.
		/// </summary>
        private static readonly string m_FilePath = Application.persistentDataPath + "/Autosave.dat";
		
		/// <summary>
		/// Checks if the auto save exists.
		/// </summary>
		/// <returns>True if the autosave exist.</returns>
        public static bool DoesAutosaveExist()
        {
            return File.Exists(m_FilePath);
        }
		
		/// <summary>
		/// Destroys the autosave file.
		/// </summary>
        public static void DestroyAutosaveFile()
        {
            File.Delete(m_FilePath);
        }
		
		/// <summary>
		/// Gets the number of players.
		/// </summary>
		/// <returns>The number of players</returns>
        public static int NumPlayers()
        {
            if (!File.Exists(m_FilePath))
                return -1;
            using (FileStream stream = new FileStream(m_FilePath, FileMode.Open))
            {
                using (BinaryReader br = new BinaryReader(stream))
                {
                    // First int is number of players
                    int numPlayers = br.ReadInt32();
					br.Close();
                    return numPlayers;
                }
            }
        }
		
		/// <summary>
		/// Gets the level name.
		/// </summary>
		/// <returns>The level name</returns>
		public static string LevelName()
		{
			if (!File.Exists(m_FilePath))
                return "";
            using (FileStream stream = new FileStream(m_FilePath, FileMode.Open))
            {
                using (BinaryReader br = new BinaryReader(stream))
                {
                    // First int is number of players
                    br.ReadInt32();
					
					string levelName = br.ReadString();
					
					br.Close();
					
					return levelName;
                }
            }
		}
		
		/// <summary>
		/// Saves the game.
		/// </summary>
		/// <returns>True on success</returns>
		/// <param name='p1'>Player 1</param>
		/// <param name='p2'>Player 2</param>
		/// <param name='p3'>Player 3</param>
		/// <param name='p4'>Player 4</param>
		/// <param name='nodeMesh'>The piece layout nodeMesh</param>
		/// <param name='playerTurn'>The index of which player's turn the game is up to</param>
        public static bool SaveGame(Player p1, Player p2, Player p3, Player p4, TicTacNodeMeshViewController nodeMesh, int playerTurn)
        {
			bool result = true;
			
            using(FileStream fs = new FileStream(m_FilePath, FileMode.OpenOrCreate))
            {
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                    TTDebug.Log("Saving to file : " + m_FilePath);
                    try
                    {
                        if (p4 != null)
                            bw.Write(4);
                        else if (p3 != null)
                            bw.Write(3);
                        else
                            bw.Write(2);
						
						// Save the map name
						bw.Write(GameController.Instance.CurrentMapName);
						
						var id = TTTAnalytics.Instance.GetGameSessionObjectID();
						if(id == null)
							bw.Write("");
						else
							bw.Write(id);

                        // Save the rule set
                        bw.Write(GameController.Instance.CurrentRuleSet);
						
                        // Save the current player who's turn it is
                        bw.Write(playerTurn);
						
                        if (!p1.CreateMemento().SaveWithBinaryWriter(bw))
                            result = false;
                        if (!p2.CreateMemento().SaveWithBinaryWriter(bw))
                            result = false;
                        if (p3 != null)
                            if (!p3.CreateMemento().SaveWithBinaryWriter(bw))
                                result = false;
                        if (p4 != null)
                            if (!p4.CreateMemento().SaveWithBinaryWriter(bw))
                                result = false;
						TTDebug.Log("5");

                        bw.Write(nodeMesh.XSize);
                        bw.Write(nodeMesh.YSize);

                        var nodes = nodeMesh.CurrentNodeMesh.GetNodes();
                        foreach (var kvp in nodes)
                        {
                            if (!kvp.Value.CreateMemento().SaveWithBinaryWriter(bw))
                                result = false;
                        }
                    }
                    catch (Exception e)
                    {
                        TTDebug.LogError("SaveGameToFile.cs : Error writing to file : " + e.Message);
                        result = false;
                    }
                    finally
                    {
                        bw.Close();
                    }
                }
            }
            return result;
        }
		
		/// <summary>
		/// Loads the game.
		/// </summary>
		/// <returns>True on success</returns>
		/// <param name='p1'>Player 1</param>
		/// <param name='p2'>Player 2</param>
		/// <param name='p3'>Player 3</param>
		/// <param name='p4'>Player 4</param>
		/// <param name='nodeMesh'>The piece layout nodeMesh</param>
		/// <param name='playerTurn'>The index of which player's turn the game is up to</param>
        public static bool LoadGame(Player p1, Player p2, Player p3, Player p4, TicTacNodeMeshViewController nodeMesh, out int playerTurn)
        {
            using (FileStream stream = new FileStream(m_FilePath, FileMode.Open))
            {
                using (BinaryReader br = new BinaryReader(stream))
                {
					bool shouldCreateNewAnalytics = false;
                    // First int is number of players
                    int numPlayers = br.ReadInt32();
					
					// then the map name
					string mapName = br.ReadString();
					GameController.Instance.SetMapName(mapName);
					
					// then the game session analytics object ID
					string id = br.ReadString();
					TTDebug.Log("Loading Server ID : |" + id + "| ID Length : " + id.Length);
					if(id != "")
					{
						TTTAnalytics.Instance.GameResumed(id);
						TTTAnalytics.Instance.CurrentGameSession.GameResumeCount += 1;
					}
					else 
					{
						TTDebug.LogWarning("SaveGameToFile.cs : No Server ID for analytics object saved correctly.  Creating new server object.");
						shouldCreateNewAnalytics = true;
					}
                    // then the rule set
                    string ruleset = br.ReadString();
                    GameController.Instance.SetRuleSet(ruleset);
					
                    // Then the current player's turn
                    int currentPlayerTurn = br.ReadInt32();
                    playerTurn = currentPlayerTurn;

                    if(!LoadPlayerMemento(p1, br))
                        return false;

                    if (!LoadPlayerMemento(p2, br))
                        return false;

                    if (numPlayers >= 3 && p3 != null)
                    {
                        if (!LoadPlayerMemento(p3, br))
                            return false;
                    }

                    if (numPlayers >= 4 && p4 != null)
                    {
                        if (!LoadPlayerMemento(p4, br))
                            return false;
                    }

                    // TODO : When the Node Mesh View controller is expanded to have different sized boards,
                    // propogate these values to the object correctly.  We need to pull them out though so
                    // the reader moves throught the file correctly
                    // The variables are commented out to remove warnings.  Un comment when required.
                    /*var xSize = */br.ReadInt32();
                    /*var ySize = */br.ReadInt32();
					
					// We need to init the node mesh before using it
					nodeMesh.Init();

                    var nodes = nodeMesh.CurrentNodeMesh.GetNodes();
                    foreach (var kvp in nodes)
                    {
                        var memento = kvp.Value.CreateMemento();
                        if(!memento.LoadFromBinaryReader(br))
                            return false;
                        kvp.Value.SetMemento(memento);
                    }
					
					if(shouldCreateNewAnalytics)
					{
						TTTAnalytics.Instance.GameBegan(p1.Name, p2.Name, p3 == null ? "" : p3.Name, p4 == null ? "" : p4.Name, numPlayers, ruleset);
						TTTAnalytics.Instance.CurrentGameSession.GameResumeCount += 1;
						// Force refresh of the Game Resume Count.
						((IParseBehaviour)TTTAnalytics.Instance.CurrentGameSession).UpdateOnServer();
					}
                }
            }
            return true;
        }
		
		/// <summary>
		/// Loads the player memento.
		/// </summary>
		/// <returns>The player memento.</returns>
		/// <param name='p'>The player</param>
		/// <param name='br'>The binary reader</param>
        private static bool LoadPlayerMemento(Player p, BinaryReader br)
        {
            var memento = p.CreateMemento();
            if (!memento.LoadFromBinaryReader(br))
                return false;
            else
                p.SetMemento(memento);
            return true;
        }
    }
}
