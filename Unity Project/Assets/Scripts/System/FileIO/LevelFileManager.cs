using System.Collections;
using System.IO;
using UnityEngine;
using System;
using TransTech.System.Debug;
using TransTech.Delegates;
using Parse;

namespace TransTech.System.IO
{	
	/// <summary>
	/// Manager class which handles level file saving and loading
	/// </summary>
	public static class LevelFileManager
	{
		/// <summary>
		/// The file path directory where the level files are stored.
		/// </summary>
        private static readonly string m_LevelDirectory = Application.persistentDataPath + "/Levels/";
		
		/// <summary>
		/// Checks that the file path directory exists, and creates it if it does not.
		/// </summary>
        private static void DirCheck()
        {
            TTDebug.Log("Data Path : " + m_LevelDirectory);
            if (!Directory.Exists(m_LevelDirectory))
            {
                Directory.CreateDirectory(m_LevelDirectory);
            }
        }
		
		/// <summary>
		/// Gets the level list.
		/// </summary>
		/// <returns>The level list.</returns>
        public static string[] GetLevelList()
		{
            DirCheck();
			return Directory.GetFiles( m_LevelDirectory, "*.lvl" );
		}
		
		/// <summary>
		/// Saves the level data to disk.
		/// </summary>
		/// <returns>Whether or not the save was successful</returns>
		/// <param name='levelName'>The name of the level to save</param>
		/// <param name='levelByteData'>The 3D byte array of actual level data</param>
        public static bool SaveLevelDataToDisk(string levelName, byte[, ,] levelByteData)
		{
            DirCheck();
			bool result = true;
			
			if( levelName == null || levelName.Length == 0 || levelName == "" )
			{
				TTDebug.LogError("LevelFileManager.cs : Empty or invalid levelName passed to SaveLevelDataToDisk");
				return false;
			}
			
			using(FileStream fs = new FileStream(m_LevelDirectory + levelName + ".lvl", FileMode.OpenOrCreate))
            {
                using(BinaryWriter bw = new BinaryWriter(fs))
                {
					try
					{
						// Write string levelName
						bw.Write(levelName);
						
						// Write int x direction length
						bw.Write(levelByteData.GetLength(0));
						
						// Write int y direction length
						bw.Write(levelByteData.GetLength(1));
						
						// Write int depth
						bw.Write(levelByteData.GetLength(2));
						
						for( int x = 0; x < levelByteData.GetLength(0); x++ )
						{
							for( int y = 0; y < levelByteData.GetLength(1); y++ )
							{
								for( int depth = 0; depth < levelByteData.GetLength(2); depth++ )
								{
									// Write byte data from the array
									bw.Write(levelByteData[x,y,depth]);
								}
							}
						}
					}
					catch(Exception e)
                    {
                        TTDebug.LogError("SaveGameToFile.cs : Error writing to file : " + e.Message);
                        result = false;
                    }
                    finally
                    {
                        bw.Close();
                    }
				}
			}
			
			return result;
		}
		
		/// <summary>
		/// Loads the level data from disk.
		/// </summary>
		/// <returns>The level data</returns>
		/// <param name='levelName'>The name of the level to load</param>
		/// <param name='success'>Output param: Whether the load was successful</param>
        public static byte[, ,] LoadLevelDataFromDisk(string levelName, out bool success)
		{
            DirCheck();
			byte[,,] levelByteData = null;
			success = true;
			
			using (var stream = new FileStream(m_LevelDirectory + levelName + ".lvl", FileMode.Open))
            {
                using (var br = new BinaryReader(stream))
                {
					try
					{
						// Read string levelName
						br.ReadString();
						
						// Read int x direction length
						int X = br.ReadInt32();
						
						// Read int y direction length
						int Y = br.ReadInt32();
						
						// Read int depth
						int Depth = br.ReadInt32();
						
						levelByteData = new byte[X,Y,Depth];
						
						for( int x = 0; x < X; x++ )
						{
							for( int y = 0; y < Y; y++ )
							{
								for( int depth = 0; depth < Depth; depth++ )
								{
									// Read byte data into the array
									levelByteData[x,y,depth] = br.ReadByte();
								}
							}
						}
					}
					catch(Exception e)
                    {
                        TTDebug.LogError("SaveGameToFile.cs : Error writing to file : " + e.Message);
                        success = false;
                    }
					finally
					{
						br.Close();
					}
                }
            }
			
			return levelByteData;
		}
		
		/// <summary>
		/// Saves the file to the server.
		/// </summary>
		/// <returns>Whether or not the save was successful</returns>
		/// <param name='fileName'>The name of the file to save</param>
		/// <param name='onComplete'>Bool String delegate returning success and the url of the saved file</param>
        public static bool SaveFileToServer(string fileName, BoolStringDelegate onComplete)
        {
            DirCheck();
            if (!File.Exists(m_LevelDirectory + fileName))
            {
                TTDebug.Log("LevelFileManager.cs : File not found on disk : " + m_LevelDirectory + fileName);
                return false;
            }

            // Check for existing server data.
            var splits = fileName.Split('.');
            if (File.Exists(m_LevelDirectory + splits[splits.Length - 2] + ".server"))
            {
                // Get the existing server object
                string objectID = null;
                using (var stream = new FileStream(m_LevelDirectory + splits[splits.Length - 2] + ".server", FileMode.Open))
                {
                    using (var br = new BinaryReader(stream))
                    {
                        try
                        {
                            objectID = br.ReadString();
                        }
                        catch (Exception e)
                        {
                            TTDebug.LogError("LevelFileManager.cs : Error reading server file : " + splits[splits.Length - 2] + " : " + e.Message);
                        }
                        finally
                        {
                            br.Close();
                        }
                    }
                }
                if(objectID == null)
                    TTDebug.LogError("LevelFileManager.cs : Unable to read existing object ID from .server file : " + splits[splits.Length - 2]);
                else
                {
                    TTDebug.Log("Getting PI from server : " + objectID);
                }
                //var levelFile = new ParseClass("/classes/LevelFile");
                ParseQuery<ParseObject> query = ParseObject.GetQuery("LevelName");
                query.GetAsync(objectID).ContinueWith((t) =>
                    {
                        if (t.IsFaulted)
                        {
                            // TODO : Handle Error
                            if (onComplete != null)
                                onComplete(false, "Save File To Server Existing : " + t.Exception.InnerExceptions[0].ToString());
                        }
                        else
                        {
                            var bytes = File.ReadAllBytes(m_LevelDirectory + fileName);
                            var file = new ParseFile(fileName, bytes);
                            file.SaveAsync().ContinueWith((t2) =>
                                {
                                    if (t2.IsFaulted)
                                    {
                                        if (onComplete != null)
                                            onComplete(false, "Save File To Server Existing : " + t2.Exception.InnerExceptions[0].ToString());
                                    }
                                    else
                                    {
                                        t.Result["name"] = fileName;
                                        t.Result["fileData"] = file;
                                        t.Result.SaveAsync().ContinueWith((t3) =>
                                            {
                                                if (t3.IsFaulted)
                                                {
                                                    // TODO : Handle fault
                                                    if (onComplete != null)
                                                        onComplete(false, "Save File To Server Existing : " + t3.Exception.InnerExceptions[0].ToString());
                                                }
                                                else
                                                {
                                                    if (onComplete != null)
                                                        onComplete(true, "Save File To Server Existing");
                                                }
                                            });
                                    }
                                });
                        }
                    });
            }
            else
            {
                //var parseFiles = ParseClass.files.New();
                var bytes = File.ReadAllBytes(m_LevelDirectory + fileName);
                ParseFile file = new ParseFile(fileName, bytes);
                file.SaveAsync().ContinueWith((t) => 
                {
                    if (t.IsFaulted)
                    {
                        // TODO : Handle the fault
                    }
                    else
                    {
                        var lvlFile = new ParseObject("LevelFile");
                        lvlFile["name"] = fileName;
                        lvlFile["fileData"] = file;
                        lvlFile.SaveAsync().ContinueWith((t2) => 
                            {
                                if (t2.IsFaulted)
                                {
                                    // TODO : Handle fault
                                    if (onComplete != null)
                                        onComplete(false, "Save Level to Server");
                                }
                                else
                                {
                                    SaveServerInfoForLevelFile(fileName, lvlFile, file);
                                    if (onComplete != null)
                                        onComplete(true, "Save Level To Server");
                                }
                            });
                    }
                });
                // NOTE : This is all specific to the desktop version.  
                // TODO : re-write so there is an ios/Android compatable version.
                /*parseFiles.UploadBinaryDataToFiles(fileName, bytes,
                                                   delegate(Request request)
                                                       {
                                                           if (request.response.status == 201)
                                                           {
                                                               var levelFile =
                                                                   (new ParseClass("/classes/LevelFile")).New();
                                                               var json =
                                                                   (Hashtable) JSON.JsonDecode(request.response.Bytes);
                                                               levelFile.Set("fileData",
                                                                             new ParseFile((string) json["name"],
                                                                                           (string) json["url"]));
                                                               levelFile.Set("name", fileName);
                                                               // This is our readable file name
                                                               levelFile.Create();
                                                               levelFile.CurrentTaskCompletedEvent += onComplete;
                                                               levelFile.CurrentTaskCompletedEvent +=
                                                                   (b, s) =>
                                                                       {
                                                                           if (b)
                                                                           {
                                                                               SaveServerInfoForLevelFile(fileName,
                                                                                                          levelFile);
                                                                           }
                                                                           // We are logging errors elsewhere, so won't bother here.
                                                                       };
                                                           }
                                                       });*/
            }
            return true;
        }

	   /* private static string m_UpdatingFileName;
	    private static ParseInstance m_UpdatingParseInstance;
	    private static BoolStringDelegate m_UpdatingOnCompleteFunction;

        // We need to keep this as a separate function and not a Lambda function, as we need to reference it inside the function to remove it from the event to stop cyclical calling of it.
        private static void UpdateServerReferenceDelegate(bool b, string s)
        {
            TTDebug.Log("LevelFileManager.cs : UpdateServer Delegate");
            if (b)
            {
                // Upload the new file
                //var parseFiles = ParseClass.files.New();
                
                var bytes = File.ReadAllBytes(m_LevelDirectory + m_UpdatingFileName);
                var file = new Parse.ParseFile(m_UpdatingFileName, bytes);
                file.SaveAsync().ContinueWith((t) =>
                {
                    if (t.IsFaulted)
                    {
                        // TODO : Handle error
                    }
                    else
                    {

                    }
                });
                parseFiles.UploadBinaryDataToFiles(m_UpdatingFileName, bytes,
                                                   delegate(Request request)
                                                       {
                                                           if (request.response.status == 201)
                                                           {
                                                               var json =
                                                                   (Hashtable)
                                                                   JSON.JsonDecode(request.response.Bytes);
                                                               m_UpdatingParseInstance.Set("fileData",
                                                                                           new ParseFile(
                                                                                               (string) json["name"],
                                                                                               (string) json["url"]));
                                                               // Don't need to update the name
                                                               m_UpdatingParseInstance.Update();
                                                               m_UpdatingParseInstance.CurrentTaskCompletedEvent +=
                                                                   m_UpdatingOnCompleteFunction;
                                                               m_UpdatingParseInstance.CurrentTaskCompletedEvent +=
                                                                   (b2, s2) =>
                                                                       {
                                                                           if (b2)
                                                                           {
                                                                               SaveServerInfoForLevelFile(
                                                                                   m_UpdatingFileName,
                                                                                   m_UpdatingParseInstance);
                                                                           }
                                                                           // We are logging errors elsewhere, so won't bother here.
                                                                       };
                                                           }
                                                       });
            }
        }
		*/
		/// <summary>
		/// Saves the server info for level file.
		/// </summary>
		/// <param name='fileName'>Level file name.</param>
		/// <param name='pi'>The parse instance.</param>
        private static void SaveServerInfoForLevelFile(string fileName, ParseObject lvlInfo, ParseFile file/*, ParseInstance pi*/)
        {
            DirCheck();
            if(!File.Exists(m_LevelDirectory + fileName))
            {
                TTDebug.LogError("LevelFileManager.cs : Attempting to store server data for a file that doesn't exist locally : " + fileName);
                return;
            }

            var levelName = fileName.Split('.');
            using (var stream = new FileStream(m_LevelDirectory + levelName[0] + ".server", FileMode.OpenOrCreate))
            {
                using (var bw = new BinaryWriter(stream))
                {
                    try
                    {
                        bw.Write(lvlInfo.ObjectId);
                        //var pf = pi.GetParseFile("fileData");
                        bw.Write(file.Name);
                        bw.Write(file.Url.ToString());
                    }
                    catch (Exception e)
                    {
                        TTDebug.LogError("LevelFileManager.cs : Error writing server data to disk : " + e.Message);
                    }
                    finally
                    {
                        bw.Close();
                    }
                }
            }

        }
		
		/// <summary>
		/// Gets the level file list from server.
		/// </summary>
		/// <param name='onComplete'>Bool delegate On complete</param>
        public static void GetLevelFileListFromServer(Action<bool, IEnumerable> onComplete)
        {
            var query = ParseObject.GetQuery("LevelFile").OrderBy("name");
            query.FindAsync().ContinueWith((t) =>
                {
                    if (t.IsFaulted)
                    {
                        if (onComplete != null)
                        {
                            onComplete(false, null);
                        }
                    }
                    else
                    {
                        IEnumerable results = t.Result;
                        if (onComplete != null)
                            onComplete(true, results);
                    }
                });
        }
		
		/// <summary>
		/// Downloads the file from server.
		/// </summary>
		/// <param name='pi'>The parse instance</param>
		/// <param name='onComplete'>Bool delegate On complete</param>
        public static void DownloadFileFromServer(ParseObject po , BoolStringDelegate onComplete)
        {
            SystemEventCenter.Instance.RunCoroutine<ParseObject, BoolStringDelegate>(DownloadFileFromServerRoutine, po, onComplete);
            /*DirCheck();

            Parse.ParseFile pf = (Parse.ParseFile)po["fileData"];
            var www = new WWW(pf.Url.ToString());
            var pf = pi.GetParseFile("fileData");
            var r = new Request("GET", pf.URL);
            r.Send(delegate(Request request)
            {
                TTDebug.Log("Response status : " + request.response.status);
                if (request.response.status == 200)
                {
                    var fileName = pi.Get<string>("name");
                    TTDebug.Log("Writing file to : " + m_LevelDirectory + fileName);
                    File.WriteAllBytes(m_LevelDirectory + fileName, request.response.Bytes);
                    onComplete(true, fileName);
                    // Make the .server file
                    SaveServerInfoForLevelFile(fileName, pi);
                }
                else
                {
                    onComplete(false, request.response.message);
                }
            });*/

        }

        private static IEnumerator DownloadFileFromServerRoutine(ParseObject po, BoolStringDelegate onComplete)
        {
            DirCheck();
            ParseFile pf = (ParseFile)po["fileData"];
            var www = new WWW(pf.Url.ToString());
            yield return www;
            if (!string.IsNullOrEmpty(www.error))
            {
                if(onComplete != null)
                    onComplete(false, "Download File From Server : " + www.error);
                yield break;
            }
            var fileName = po.Get<string>("name");
            File.WriteAllBytes(m_LevelDirectory + fileName, www.bytes);
            SaveServerInfoForLevelFile(fileName, po, pf);
            if (onComplete != null)
                onComplete(true, "Download File From Server");
        }
	}
}