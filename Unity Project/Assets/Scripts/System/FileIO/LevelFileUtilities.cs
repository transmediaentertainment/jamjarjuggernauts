using DopplerInteractive.TidyTileMapper.Utilities;

namespace TransTech.System.IO
{
    /// <summary>
    /// Utility class for converting Level data into different formats
    /// </summary>
	public static class LevelFileUtilities
	{
        /// <summary>
        /// Extracts a 3 dimensional bytes array from a BlockMap object
        /// </summary>
        /// <param name="map">The Block Map to process</param>
        /// <param name="xSize">The X dimension of the Block Map</param>
        /// <param name="ySize">The Y dimension of the Block Map</param>
        /// <param name="depth">The Depth (Z Dimension) of the Block Map</param>
        /// <returns>A 3 Dimensional Byte array of the Block Map</returns>
		public static byte[,,] GetByteArrayFromMap( BlockMap map, int xSize, int ySize, int depth )
		{
			var levelData = new byte[xSize,ySize,depth];
			
			for( int x = 0; x < xSize; x++ )
			{
				for( int y = 0; y < ySize; y++ )
				{
					for( int d = 0; d < depth; d++ )
					{
						var b = BlockUtilities.GetBlockAt( map, x, y, d );
						var tag = b.GetComponent<EditorBlockTag>();
						levelData[x,y,d] = (byte)tag.BlockType;
					}
				}
			}
			
			return levelData;
		}
		
        /// <summary>
        /// Returns a BlockMap populated by the data from a 3 Dimensional Byte Array
        /// </summary>
        /// <param name="levelData">The 3 Dimensional Byte array to convert</param>
        /// <param name="mapName">The name of the map the byte array belongs to</param>
        /// <returns>A Block map populated from the 3D Byte array</returns>
		public static BlockMap GetMapFromByteArray( byte[,,] levelData, string mapName )
		{
			var map = NodeTTMBridge.Instance.CreateEmptyMap( mapName, 1, 1 );
			
			for( int x = 0; x < levelData.GetLength(0); x++ )
			{
				for( int y = 0; y < levelData.GetLength(1); y++ )
				{
					for( int d = 0; d < levelData.GetLength(2); d++ )
					{
						NodeTTMBridge.Instance.SetBlockInLayer( x, y, d, (Blocks)levelData[x,y,d] );
					}
				}
			}
			return map;
		}
	} 
}