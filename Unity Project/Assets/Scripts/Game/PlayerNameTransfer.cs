using System.Collections;
using UnityEngine;

/// <summary>
/// Simple script used to maintain data between scenes
/// </summary>
public class SceneDataTransfer : MonoBehaviour
{
    /// <summary>
    /// The name for Player One
    /// </summary>
    private string m_Player1Name;

    /// <summary>
    /// The name for Player Two
    /// </summary>
    private string m_Player2Name;

    /// <summary>
    /// The name for Player Three
    /// </summary>
    private string m_Player3Name;

    /// <summary>
    /// The name for Player Four
    /// </summary>
    private string m_Player4Name;

    /// <summary>
    /// The ruleset to use in the game
    /// </summary>
    private string m_RulesetChosen;
	
	/// <summary>
	/// The name of the map
	/// </summary>
	private string m_MapName;
	
	/// <summary>
	/// The block map data
	/// </summary>
	private byte[,,] m_MapData;

    /// <summary>
    /// Are we resuming the autosaved game
    /// </summary>
    private bool m_IsResuming = false;

    /// <summary>
    /// Called by Unity when the Object is created
    /// </summary>
    private void Awake()
    {
        // Make sure we will live through a scene change
        DontDestroyOnLoad(gameObject);
    }

    /// <summary>
    /// Called by Unity when a level is loaded
    /// </summary>
    /// <param name="level">The index of the level loaded</param>
    private void OnLevelWasLoaded(int level)
    {
		GameController.Instance.SetMapName(m_MapName);
		GameController.Instance.SetMapData(m_MapData);
		
		var nodeMeshController = FindObjectOfType( typeof(TicTacNodeMeshViewController) ) as TicTacNodeMeshViewController;
		if( nodeMeshController != null )
		{
			nodeMeshController.SetBlockMap( m_MapData );
		}
		
        if (m_IsResuming)
        {
            StartCoroutine(WaitAndResume());
        }
        else
        {
            // If the name filed isn't null and the length is greater than zero,
            // Create a player for the name field
            if (m_Player1Name != null && m_Player1Name.Length > 0)
            {
                GameController.Instance.AddPlayer(m_Player1Name);
            }
            if (m_Player2Name != null && m_Player2Name.Length > 0)
            {
                GameController.Instance.AddPlayer(m_Player2Name);
            }
            if (m_Player3Name != null && m_Player3Name.Length > 0)
            {
                GameController.Instance.AddPlayer(m_Player3Name);
            }
            if (m_Player4Name != null && m_Player4Name.Length > 0)
            {
                GameController.Instance.AddPlayer(m_Player4Name);
            }

            GameController.Instance.SetRuleSet(m_RulesetChosen);
				
            GameController.Instance.BeginGame();

            // Clean ourselves up.
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Coroutine to wait a frame then resume the saved game
    /// </summary>
    /// <returns>Used by Unity for Coroutine.</returns>
    private IEnumerator WaitAndResume()
    {
        yield return null;
        GameController.Instance.ResumeGame();

        // Clean ourselves up.
        Destroy(gameObject);
    }

    /// <summary>
    /// Sets the name of a given player
    /// </summary>
    /// <param name="playerNumber">The number of the player to set the name for</param>
    /// <param name="playerName">The Player's name</param>
    public void SetPlayerName(int playerNumber, string playerName)
    {
        switch (playerNumber)
        {
            case 1:
                m_Player1Name = playerName;
                break;

            case 2:
                m_Player2Name = playerName;
                break;

            case 3:
                m_Player3Name = playerName;
                break;

            case 4:
                m_Player4Name = playerName;
                break;
        }
    }

    /// <summary>
    /// Sets the rule set to use in the game
    /// </summary>
    /// <param name="ruleSet">The ruleset description</param>
    public void SetRuleset(string ruleSet)
    {
        TransTech.System.Debug.TTDebug.Log("Ruleset : " + ruleSet);
        m_RulesetChosen = ruleSet;
    }

    /// <summary>
    /// Sets the block map data.
    /// </summary>
    /// <param name="mapName">The name of the map</param>
    /// <param name='mapData'>The block map data.</param>
    public void SetMapData(string mapName, byte[,,] mapData)
	{
		m_MapName = mapName;
		m_MapData = mapData;
	}

    /// <summary>
    /// Sets the transfer to use the autosaved game on level loaded.
    /// </summary>
    public void SetToResumeGame()
    {
        m_IsResuming = true;
    }
}