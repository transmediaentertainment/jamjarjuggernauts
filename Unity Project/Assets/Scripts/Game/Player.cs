using TransTech.Delegates.TicTac;

namespace TransTech.Gameplay.Players
{
    /// <summary>
    /// Class handling all the details of a Player in the game
    /// </summary>
    public partial class Player
    {
        /// <summary>
        /// The number of the player (1 - 4)
        /// </summary>
        public int PlayerNumber { get; private set; }
        /// <summary>
        /// Number of tokens currently in the player's hand
        /// </summary>
        public int TokensInHand 
        { 
            get { return m_TokensInHand;  }
            private set 
            { 
                var prev = m_TokensInHand;
                m_TokensInHand = value;
                if (prev != m_TokensInHand)
                    if (TokenCountChangedEvent != null)
                        TokenCountChangedEvent(this);
            } 
        }
        private int m_TokensInHand;
        /// <summary>
        /// Event fired when the number of tokens the player has changes
        /// </summary>
        public event PlayerDelegate TokenCountChangedEvent;
        /// <summary>
        /// Name of the player
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// Current score of the player
        /// </summary>
        public int CurrentScore { get; private set; }
        /// <summary>
        /// Event fired when the Player's score changes
        /// </summary>
        public event PlayerDelegate PlayerScoreChangedEvent;
		/// <summary>
		/// Number of turns the player has completed.
		/// </summary>
		public int TurnsCompleted { get; private set; }

        /// <summary>
        /// Constructor for Initialization
        /// </summary>
        /// <param name="playerNumber">The Player's number for the game</param>
        /// <param name="name">The name of the player</param>
        public Player(int playerNumber, string name)
        {
            PlayerNumber = playerNumber;
            TokensInHand = 0;
            Name = name;
            CurrentScore = 0;
			TurnsCompleted = 0;
        }

        /// <summary>
        /// Adds tokens to the player's hand
        /// </summary>
        /// <param name="newTokens">Number of tokens to add</param>
        public void AddTokensToHand(int newTokens)
        {
            TokensInHand += newTokens;
        }

        /// <summary>
        /// Uses a token up if one is available
        /// </summary>
        /// <returns>Was a token used or not?</returns>
        public bool UseToken()
        {
            // If we have tokens, decrement and return true
            if (TokensInHand > 0)
            {
                TokensInHand--;
                return true;
            }
            // Else return false
            return false;
        }

        /// <summary>
        /// Sets the score for the player
        /// </summary>
        /// <param name="score">The new score value for the player</param>
        public void SetScore(int score)
        {
            CurrentScore = score;
            if (PlayerScoreChangedEvent != null)
                PlayerScoreChangedEvent(this);
        }
		
        /// <summary>
        /// Called when a turn is completed
        /// </summary>
		public void CompletedTurn()
		{
			TurnsCompleted += 1;
		}
    }
}