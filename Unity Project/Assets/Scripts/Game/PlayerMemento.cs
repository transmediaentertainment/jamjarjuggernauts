using System;
using System.IO;
using TransTech.Patterns.Memento;
using TransTech.System.Debug;
using TransTech.System.IO;

namespace TransTech.Gameplay.Players
{
    /// <summary>
    /// Abstract public class for the Player Memento (See memento pattern)
    /// </summary>
    public abstract class PlayerMemento : MementoObject, IBinarySavable
    {
        /// <summary>
        /// Saves the memento to a binary file
        /// </summary>
        /// <param name="bw">The BinaryWriter object to write the memento to</param>
        /// <returns>Ture if successful, false if errors</returns>
        public abstract bool SaveWithBinaryWriter(BinaryWriter bw);

        /// <summary>
        /// Loads data from a binary file and replaces existing Memento data with it.
        /// </summary>
        /// <param name="br">The BinaryReader object to read from</param>
        /// <returns>True if successful, false if errors</returns>
        public abstract bool LoadFromBinaryReader(BinaryReader br);
    }

    /// <summary>
    /// Player class's Memento implementation.
    /// </summary>
    public partial class Player : IMementoOriginator<PlayerMemento>
    {
        /// <summary>
        /// Private Implementation of the Player Memento class (See Memento Pattern)
        /// It is private so only the Player class can create/manipulate it.
        /// </summary>
        private class PlayerMementoImplementation : PlayerMemento
        {
            /// <summary>
            /// The number of the Player
            /// </summary>
            public int PlayerNumber { get; private set; }
            /// <summary>
            /// Name of the Player
            /// </summary>
            public string Name { get; private set; }
            /// <summary>
            /// The Current score of the player
            /// </summary>
            public int CurrentScore { get; private set; }
            /// <summary>
            /// Number of tokens in the Player's hand
            /// </summary>
            public int TokensInHand { get; private set; }
			/// <summary>
			/// Number of turns the player has completed
			/// </summary>
			public int TurnsCompleted { get; private set; }

            /// <summary>
            /// Constructor for Initialization
            /// </summary>
            /// <param name="player">The Player to capture the state of.</param>
            public PlayerMementoImplementation(Player player)
            {
                PlayerNumber = player.PlayerNumber;
                Name = player.Name;
                CurrentScore = player.CurrentScore;
                TokensInHand = player.TokensInHand;
				TurnsCompleted = player.TurnsCompleted;
            }

            /// <summary>
            /// Saves the memento to a binary file
            /// </summary>
            /// <param name="bw">The BinaryWriter object to write the memento to</param>
            /// <returns>Ture if successful, false if errors</returns>
            public override bool SaveWithBinaryWriter(BinaryWriter bw)
            {
                try
                {
                    bw.Write(PlayerNumber);
                    bw.Write(Name);
                    bw.Write(CurrentScore);
                    bw.Write(TokensInHand);
					bw.Write(TurnsCompleted);
                }
                catch (Exception e)
                {
                    TTDebug.LogError("PlayerMemento.cs : Unable to save to Binary Writer correctly : " + e.Message);
                    return false;
                }
                return true;
            }

            /// <summary>
            /// Loads data from a binary file and replaces existing Memento data with it.
            /// </summary>
            /// <param name="br">The BinaryReader object to read from</param>
            /// <returns>True if successful, false if errors</returns>
            public override bool LoadFromBinaryReader(BinaryReader br)
            {
                try
                {
                    PlayerNumber = br.ReadInt32();
                    Name = br.ReadString();
                    CurrentScore = br.ReadInt32();
                    TokensInHand = br.ReadInt32();
					TurnsCompleted = br.ReadInt32();
                }
                catch (Exception e)
                {
                    TTDebug.LogError("PlayerMemento.cs : Unable to load from Binary Reader correctly : " + e.Message);
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Creates and returns a Memento of the Player
        /// </summary>
        /// <returns>PlayerMemento of the player</returns>
        public PlayerMemento CreateMemento()
        {
            return new PlayerMementoImplementation(this);
        }

        /// <summary>
        /// Restores the Player to the state in the provided memento
        /// </summary>
        /// <param name="memento">The memento object to restore state from</param>
        public void SetMemento(PlayerMemento memento)
        {
            var imp = (PlayerMementoImplementation)memento;
            PlayerNumber = imp.PlayerNumber;
            Name = imp.Name;
            CurrentScore = imp.CurrentScore;
            TokensInHand = imp.TokensInHand;
			TurnsCompleted = imp.TurnsCompleted;
        }
    }
}