using System.Collections;
using System.Collections.Generic;
using TransTech.Delegates.TicTac;
using TransTech.Gameplay;
using TransTech.Gameplay.Players;
using TransTech.Gameplay.States;
using TransTech.Nodes;
using TransTech.System.Debug;
using TransTech.System.IO;
using TransTech.System.Undo;
using UnityEngine;

/// <summary>
/// Sits in the scene and is the main controller for the game.
/// </summary>
public class GameController : MonoSingleton<GameController>
{
    // These skins will eventually be associated with Player Accounts and will be removed

    #region Skin Fields

    /// <summary>
    /// Skin for player one
    /// </summary>
    public PlayerSkin m_PlayerOneSkin;

    /// <summary>
    ///  Skin for player two
    /// </summary>
    public PlayerSkin m_PlayerTwoSkin;

    /// <summary>
    /// Skin for player three
    /// </summary>
    public PlayerSkin m_PlayerThreeSkin;

    /// <summary>
    /// Skin for player four
    /// </summary>
    public PlayerSkin m_PlayerFourSkin;

    /// <summary>
    /// Skin for the board
    /// </summary>
    public BoardSkin m_BoardSkin;

    #endregion Skin Fields

    /// <summary>
    /// Reference to the Node Mesh View Controller in the scene
    /// </summary>
    public TicTacNodeMeshViewController m_NodeMeshViewController;

    /// <summary>
    /// Reference to the GUI Manager in the scene
    /// </summary>
    public InGameGUIManager m_GUIManager;

    /// <summary>
    /// Finite State Machine handling the game logic and states.
    /// </summary>
    private GameFSMEngine m_FSM;

    /// <summary>
    /// External access to the Game's Finite State Machine
    /// </summary>
    public GameFSMEngine FSM { get { return m_FSM; } }

    /// <summary>
    /// Gets the current delegate used for testing raycasts against nodes.
    /// </summary>
    public NodeTestDelegate CurrentNodeTest { get { return m_FSM.CurrentNodeStrategy; } }

    /// <summary>
    /// Reference to the Player object for player one
    /// </summary>
    public Player PlayerOne { get; private set; }

    /// <summary>
    /// Reference to the Player object for player two
    /// </summary>
    public Player PlayerTwo { get; private set; }

    /// <summary>
    /// Reference to the Player object for player three
    /// </summary>
    public Player PlayerThree { get; private set; }

    /// <summary>
    /// Reference to the Player object for player four
    /// </summary>
    public Player PlayerFour { get; private set; }

    /// <summary>
    /// Object that handles undoing a single turn back to it's start
    /// </summary>
    private SingleTurnUndo m_SingleTurnUndo;

    /// <summary>
    /// Object used to calculate the score on a board line
    /// </summary>
    private LineScoreCalculator m_ScoreCalc = new LineScoreCalculator();

    /// <summary>
    /// Set to true when the score should be recalculated
    /// </summary>
    private bool m_ScoreDirty = false;

    /// <summary>
    /// Event fired when the undo system undoes an attack sacrifice.  Current player is passed in.
    /// </summary>
    private event PlayerDelegate SacrificeAttackUndoneEvent;

    /// <summary>
    /// Event fired when the undo system undoes a fortify sacrifice.  Current player is passed in.
    /// </summary>
    private event PlayerDelegate SacrificeFortifyUndoneEvent;

    /// <summary>
    /// The chosen rulset for the game.
    /// </summary>
    private string m_RuleSet;
	public string CurrentRuleSet { get { return m_RuleSet; } }
	
	/// <summary>
	/// The name of the map.
	/// </summary>
	private string m_MapName;
	public string CurrentMapName { get { return m_MapName; } }
	
	/// <summary>
	/// The map data.
	/// </summary>
	private byte[,,] m_MapData;
	public byte[,,] CurrentMapData { get {return m_MapData; } }
    
    /// <summary>
    /// Sets the rulset to use for the game
    /// </summary>
    /// <param name="ruleSet">Description of the rule set</param>
    public void SetRuleSet(string ruleSet)
    {
        m_RuleSet = ruleSet;
    }

    /// <summary>
    /// Sets the name of the map for the game.
    /// </summary>
    /// <param name="mapName"></param>
	public void SetMapName(string mapName)
	{
		m_MapName = mapName;
	}
	
	/// <summary>
	/// Sets the map data for the game.
	/// </summary>
	/// <param name='mapData'>
	/// The 3D byte array of map data.
	/// </param>
	public void SetMapData(byte[,,] mapData)
	{
		m_MapData = mapData;
	}

    /// <summary>
    /// Initializes the object (override of MonoSingleton)
    /// </summary>
    public override void Init()
    {
        // Set all the skins up
        SkinManager.Instance.SetPlayerSkin(m_PlayerOneSkin, NodeType.Player1);
        SkinManager.Instance.SetPlayerSkin(m_PlayerTwoSkin, NodeType.Player2);
        SkinManager.Instance.SetPlayerSkin(m_PlayerThreeSkin, NodeType.Player3);
        SkinManager.Instance.SetPlayerSkin(m_PlayerFourSkin, NodeType.Player4);
        SkinManager.Instance.SetBoardSkin(m_BoardSkin);

        InputController.Instance.RegisterLeftClickForRaycast();
#if UNITY_IPHONE || UNITY_ANDROID

        // Register for touches
		// We only need this on touch devices.
		InputController.Instance.RegisterTapBeganForRaycast();
		InputController.Instance.RegisterTapEndedForRaycast();
#endif

        // Register the GameTile layer to receive raycast events.
        InputController.Instance.AddRaycastCollisionLayer("GameTile");
    }

    /// <summary>
    /// Called by Unity the frame after the object is created
    /// </summary>
    private IEnumerator Start()
    {
        yield return null;

        // Set our score as dirty when the board changes so we refresh the score when needed.
        var nodeMesh = m_NodeMeshViewController.CurrentNodeMesh;
        nodeMesh.BoardChangedEvent += () => m_ScoreDirty = true;

        // Create and init the undo system
        m_SingleTurnUndo = new SingleTurnUndo();
        m_SingleTurnUndo.Init(m_FSM, m_GUIManager, nodeMesh);
        m_SingleTurnUndo.UndoneSacrificeAttackEvent += HandleSacrificeAttackUndone;
        m_SingleTurnUndo.UndoneSacrificeFortifyEvent += HandelSacrificeFortifyUndone;
        m_FSM.PlayerTurnStarted += m_SingleTurnUndo.BeginTurn;

        // Grab the main board camera and center it to the generated board
        var cam = CameraManager.Instance.GetBoardCamera();
        if (cam != null)
        {
            var cont = cam.GetComponent<CameraController>();
            cont.Init(new Vector3(m_NodeMeshViewController.XWorldSize / 2f - 0.6f, 0f, m_NodeMeshViewController.YWorldSize / 2f - 0.6f), 1.0f, -0.5f, m_NodeMeshViewController.XWorldSize - 0.5f,
            -0.5f, m_NodeMeshViewController.YWorldSize - 0.5f, 3f, 12f);
        }

        // Register for events to notify of the end of game.
        m_NodeMeshViewController.EmptySpacesAvailableEvent += m_GUIManager.SetEndGameButtonToEndTurnButton;
        m_NodeMeshViewController.NoEmptySpacesAvailableEvent += m_GUIManager.SetEndTurnButtonToEndGameButton;
    }

    /// <summary>
    /// Adds a new player to the game
    /// </summary>
    /// <param name="playerName">Name of the new player</param>
    public void AddPlayer(string playerName)
    {
        if (PlayerOne == null)
            PlayerOne = new Player(1, playerName);
        else if (PlayerTwo == null)
            PlayerTwo = new Player(2, playerName);
        else if (PlayerThree == null)
            PlayerThree = new Player(3, playerName);
        else if (PlayerFour == null)
            PlayerFour = new Player(4, playerName);
    }

    /// <summary>
    /// Selects a random player from the game, that isn't the given player
    /// </summary>
    /// <param name="excludedPlayer">The player to exclude from the selection</param>
    /// <returns></returns>
    public Player GetRandomPlayer(Player excludedPlayer)
    {
        int playerCount = 2;
        if (PlayerThree != null)
            playerCount = 3;
        if (PlayerFour != null)
            playerCount = 4;

        // Early exit for 2 players
        if (playerCount == 2 && excludedPlayer != null)
        {
            if (excludedPlayer == PlayerOne)
                return PlayerTwo;
            
            return PlayerOne;
        }

        int rand = Random.Range(1, playerCount + 1);
        if (excludedPlayer != null)
            while (rand == excludedPlayer.PlayerNumber)
                rand = Random.Range(1, playerCount + 1);

        switch (rand)
        {
            case 1:
                return PlayerOne;
            case 2:
                return PlayerTwo;
            case 3:
                return PlayerThree;
            case 4:
                return PlayerFour;
            default:
                TTDebug.LogError("GameController.cs : Player Number was out of range : " + rand);
                return null;
        }
    }

    /// <summary>
    /// Begins the game.  Should be called after all players have been added.
    /// </summary>
    public void BeginGame()
    {
        // Report to the Analytics system
        if (PlayerFour != null)
        {
            TTTAnalytics.Instance.GameBegan(PlayerOne.Name, PlayerTwo.Name, PlayerThree.Name, PlayerFour.Name, 4, m_RuleSet);
        }
        else if (PlayerThree != null)
        {
            TTTAnalytics.Instance.GameBegan(PlayerOne.Name, PlayerTwo.Name, PlayerThree.Name, "", 3, m_RuleSet);
        }
        else
        {
            TTTAnalytics.Instance.GameBegan(PlayerOne.Name, PlayerTwo.Name, "", "", 2, m_RuleSet);
        }

        ISacrificeStrategy sacrificeStrat = GetSacrificeStrategy();

        SacrificeAttackUndoneEvent += sacrificeStrat.HandleSacrificeAttackUndone;
        SacrificeFortifyUndoneEvent += sacrificeStrat.HandleSacrificeFortifyUndone;

        // Register players for events
        // We assume player one and two are setup.
        m_GUIManager.RegisterPlayerEvents(PlayerOne, sacrificeStrat.PlayerStatusPanelPrefab);
        m_GUIManager.RegisterPlayerEvents(PlayerTwo, sacrificeStrat.PlayerStatusPanelPrefab);
        if (PlayerThree != null)
            m_GUIManager.RegisterPlayerEvents(PlayerThree, sacrificeStrat.PlayerStatusPanelPrefab);
        if (PlayerFour != null)
            m_GUIManager.RegisterPlayerEvents(PlayerFour, sacrificeStrat.PlayerStatusPanelPrefab);

        // Create our Game FSM
        if (PlayerFour != null)
        {
            m_FSM = new GameFSMEngine(sacrificeStrat, m_NodeMeshViewController, m_GUIManager, PlayerOne, PlayerTwo, PlayerThree, PlayerFour);
        }
        else if (PlayerThree != null)
        {
            m_FSM = new GameFSMEngine(sacrificeStrat, m_NodeMeshViewController, m_GUIManager, PlayerOne, PlayerTwo, PlayerThree);
        }
        else
        {
            m_FSM = new GameFSMEngine(sacrificeStrat, m_NodeMeshViewController, m_GUIManager, PlayerOne, PlayerTwo);
        }

        // Hook up the FSM and the GUI manager
        m_FSM.PlayerTurnStarted += m_GUIManager.SetPlayerTurn;
    }

    /// <summary>
    /// Resumes a saved game.  Call this instead of BeginGame() when resuming.
    /// </summary>
    public void ResumeGame()
    {
        int numPlayers = SaveGameToFile.NumPlayers();

        // Clean up any players that might have been added.
        PlayerOne = null;
        PlayerTwo = null;
        PlayerThree = null;
        PlayerFour = null;

        // Create basic empty players.  They will be populated by their Mementos in the save game
        for (int i = 0; i < numPlayers; i++)
        {
            AddPlayer("Empty");
        }

        int currentPlayer = 0;
        SaveGameToFile.LoadGame(PlayerOne, PlayerTwo, PlayerThree, PlayerFour, m_NodeMeshViewController, out currentPlayer);

        // The ruleset gets set from the Load Game function, so this is safe to call here.
        ISacrificeStrategy sacrificeStrat = GetSacrificeStrategy();

        SacrificeAttackUndoneEvent += sacrificeStrat.HandleSacrificeAttackUndone;
        SacrificeFortifyUndoneEvent += sacrificeStrat.HandleSacrificeFortifyUndone;

        // Register players for events
        // We assume player one and two are setup.
        m_GUIManager.RegisterPlayerEvents(PlayerOne, sacrificeStrat.PlayerStatusPanelPrefab);
        m_GUIManager.RegisterPlayerEvents(PlayerTwo, sacrificeStrat.PlayerStatusPanelPrefab);
        if (PlayerThree != null)
            m_GUIManager.RegisterPlayerEvents(PlayerThree, sacrificeStrat.PlayerStatusPanelPrefab);
        if (PlayerFour != null)
            m_GUIManager.RegisterPlayerEvents(PlayerFour, sacrificeStrat.PlayerStatusPanelPrefab);

        // Create our Game FSM
        if (PlayerFour != null)
        {
            m_FSM = new GameFSMEngine(sacrificeStrat, m_NodeMeshViewController, m_GUIManager, currentPlayer, PlayerOne, PlayerTwo, PlayerThree, PlayerFour);
        }
        else if (PlayerThree != null)
        {
            m_FSM = new GameFSMEngine(sacrificeStrat, m_NodeMeshViewController, m_GUIManager, currentPlayer, PlayerOne, PlayerTwo, PlayerThree);
        }
        else
        {
            m_FSM = new GameFSMEngine(sacrificeStrat, m_NodeMeshViewController, m_GUIManager, currentPlayer, PlayerOne, PlayerTwo);
        }

        // Hook up the FSM and the GUI manager
        m_FSM.PlayerTurnStarted += m_GUIManager.SetPlayerTurn;

        //m_FSM.SetCurrentPlayer(currentPlayer);

        UpdateScore();
    }

    /// <summary>
    /// Creates the Sacrifice strategy object based on the string in the m_RuleSet field.
    /// </summary>
    /// <returns>The generated Sacrifice Strategy Object</returns>
    private ISacrificeStrategy GetSacrificeStrategy()
    {
        ISacrificeStrategy sacrificeStrat;

        switch (m_RuleSet)
        {
            case "No Sacrifice Limits":
                sacrificeStrat = new DefaultSacrificeStrategy();
                break;

            case "3 Sacrifices Per Game":
                sacrificeStrat = new ThreePerGameSacrificeStrategy();
                break;

            case "MM - Random Defection, Every Turn":
                sacrificeStrat = new MutinyMeterSacrificeStrategy(true, true, m_NodeMeshViewController, m_GUIManager, m_RuleSet);
                break;

            case "MM - Random Defection, Sacr. Turn Only":
                sacrificeStrat = new MutinyMeterSacrificeStrategy(false, true, m_NodeMeshViewController, m_GUIManager, m_RuleSet);
                break;

            case "MM - Best Team Defection, Every Turn":
                sacrificeStrat = new MutinyMeterSacrificeStrategy(true, false, m_NodeMeshViewController, m_GUIManager, m_RuleSet);
                break;

            case "MM - Best Team Defection, Sacr. Turn Only":
                sacrificeStrat = new MutinyMeterSacrificeStrategy(false, false, m_NodeMeshViewController, m_GUIManager, m_RuleSet);
                break;

            default:
                TTDebug.LogWarning("Unrecognised Rule Set : " + m_RuleSet + ". Setting to default");
                sacrificeStrat = new DefaultSacrificeStrategy();
                break;
        }

        return sacrificeStrat;
    }

    /// <summary>
    /// Called by Unity every frame after all Update functions have been called
    /// </summary>
    private void LateUpdate()
    {
        // Update the score if the board has changed
        if (m_ScoreDirty)
            UpdateScore();
    }

    /// <summary>
    /// Calculates and Updates the score for the game
    /// </summary>
    private void UpdateScore()
    {
        // Collect and store the left most, top most and right most nodes
        var nodeMesh = m_NodeMeshViewController.CurrentNodeMesh;
        var nodes = nodeMesh.GetNodes();

        var leftNodes = new List<Node>();
        var topNodes = new List<Node>();
        var rightNodes = new List<Node>();

        foreach (var kvp in nodes)
        {
            var node = kvp.Value;
            if (node.Coordinate.XPos == 0)
                leftNodes.Add(node);
            if (node.Coordinate.XPos == m_NodeMeshViewController.XSize - 1)
                rightNodes.Add(node);
            if (node.Coordinate.ZPos == m_NodeMeshViewController.YSize - 1)
                topNodes.Add(node);
        }

        m_ScoreCalc.ResetScores();

        // From leftNodes, iterate over all the right links
        // From LeftNodes, iterate over all the down right links
        foreach (var node in leftNodes)
        {
            m_ScoreCalc.CalculateLinearLineScore(node, NodeDirections.Right, m_NodeMeshViewController);
            m_ScoreCalc.CalculateLinearLineScore(node, NodeDirections.DownRight, m_NodeMeshViewController);
        }

        // From topNodes, iterate over all the down right links
        // From topNodes, iterate over all the down links
        // From topNodes, iterate over all the down left links
        for (int i = 0; i < topNodes.Count; i++)
        {
            // We need to make sure we don't double up in the corners for down right and down left
            if (i != 0 && i != m_NodeMeshViewController.XSize - 1)
            {
                m_ScoreCalc.CalculateLinearLineScore(topNodes[i], NodeDirections.DownRight, m_NodeMeshViewController);
                m_ScoreCalc.CalculateLinearLineScore(topNodes[i], NodeDirections.DownLeft, m_NodeMeshViewController);
            }
            m_ScoreCalc.CalculateLinearLineScore(topNodes[i], NodeDirections.Down, m_NodeMeshViewController);
        }

        // From right nodes, iterate over all the down left links
        foreach (var node in rightNodes)
            m_ScoreCalc.CalculateLinearLineScore(node, NodeDirections.DownLeft, m_NodeMeshViewController);

        // Set the calculated scores
        PlayerOne.SetScore(m_ScoreCalc.Player1Score);
        PlayerTwo.SetScore(m_ScoreCalc.Player2Score);

        if (PlayerThree != null)
            PlayerThree.SetScore(m_ScoreCalc.Player3Score);
        if (PlayerFour != null)
            PlayerFour.SetScore(m_ScoreCalc.Player4Score);

        m_ScoreDirty = false;
    }

    /// <summary>
    /// Gets registered to the Undo System's Sacrifice Attack Undone event, and chains the event passing in the current player.
    /// </summary>
    private void HandleSacrificeAttackUndone()
    {
        if (SacrificeAttackUndoneEvent != null)
        {
            switch (m_FSM.CurrentPlayerTurn)
            {
                case 1:
                    SacrificeAttackUndoneEvent(PlayerOne);
					TTTAnalytics.Instance.CurrentGameSession.PlayerOneSacrifices -= 1;
                    break;

                case 2:
                    SacrificeAttackUndoneEvent(PlayerTwo);
					TTTAnalytics.Instance.CurrentGameSession.PlayerTwoSacrifices -= 1;
                    break;

                case 3:
                    SacrificeAttackUndoneEvent(PlayerThree);
					TTTAnalytics.Instance.CurrentGameSession.PlayerThreeSacrifices -= 1;
                    break;

                case 4:
                    SacrificeAttackUndoneEvent(PlayerFour);
					TTTAnalytics.Instance.CurrentGameSession.PlayerFourSacrifices -= 1;
                    break;
            }
        }
    }

    /// <summary>
    /// Gets registered to the Undo System's Sacrifice Fortify Undone event, and chains the event passing in the current player.
    /// </summary>
    private void HandelSacrificeFortifyUndone()
    {
        if (SacrificeFortifyUndoneEvent != null)
        {
            switch (m_FSM.CurrentPlayerTurn)
            {
                case 1:
                    SacrificeFortifyUndoneEvent(PlayerOne);
					TTTAnalytics.Instance.CurrentGameSession.PlayerOneFortifies -= 1;
                    break;

                case 2:
                    SacrificeFortifyUndoneEvent(PlayerTwo);
					TTTAnalytics.Instance.CurrentGameSession.PlayerTwoFortifies -= 1;
                    break;

                case 3:
                    SacrificeFortifyUndoneEvent(PlayerThree);
					TTTAnalytics.Instance.CurrentGameSession.PlayerThreeFortifies -= 1;
                    break;

                case 4:
                    SacrificeFortifyUndoneEvent(PlayerFour);
					TTTAnalytics.Instance.CurrentGameSession.PlayerFourFortifies -= 1;
                    break;
            }
        }
    }
}