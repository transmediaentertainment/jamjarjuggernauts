using System.Collections.Generic;
using TransTech.Nodes;
using TransTech.System.Debug;

namespace TransTech.Gameplay
{
    /// <summary>
    /// Calculates the score for a line
    /// </summary>
    public class LineScoreCalculator
    {
        #region Score Strategies

        /// <summary>
        /// Delegate signature for a ScoreStrategy function
        /// </summary>
        /// <param name="connections">The number of connections to calculate a score for</param>
        /// <returns>The score for the given number of connections</returns>
        private delegate int ScoreStrategy(int connections);

        /// <summary>
        /// Linear scoring strategy.  
        /// </summary>
        /// <param name="connections">Number of connections to calculate for</param>
        /// <returns>Score for the given connections</returns>
        private int LinearScore(int connections)
        {
            return connections - 2;
        }

        /// <summary>
        /// Exponential scoring strategy
        /// </summary>
        /// <param name="connections">Number of connections to calculate for</param>
        /// <returns>Score for the given connections</returns>
        private int ExponentialScore(int connections)
        {
            var score = connections - 2;
            return score * score;
        }

        /// <summary>
        /// Traingular scoring strategy : See http://en.wikipedia.org/wiki/Triangular_number
        /// </summary>
        /// <param name="connections">Number of connections to calculate for</param>
        /// <returns>Score for the given connections</returns>
        private int TriangularScore(int connections)
        {
            var num = connections - 2;
            return (num * (num + 1)) / 2;
        }

        #endregion Score Strategies

        /// <summary>
        /// Score for Player 1
        /// </summary>
        public int Player1Score { get; private set; }
        /// <summary>
        /// Score for Player 2
        /// </summary>
        public int Player2Score { get; private set; }
        /// <summary>
        /// Score for Player 3
        /// </summary>
        public int Player3Score { get; private set; }
        /// <summary>
        /// Score for Player 4
        /// </summary>
        public int Player4Score { get; private set; }

        /// <summary>
        /// Resets all the Player scores to 0
        /// </summary>
        public void ResetScores()
        {
            Player1Score = 0;
            Player2Score = 0;
            Player3Score = 0;
            Player4Score = 0;
        }

        /// <summary>
        /// Calculates and tallies the score for a line of nodes, starting at the given node, and going in the given direction,
        /// using the Linear scoring method
        /// </summary>
        /// <param name="startNode">The node to begin calculating from</param>
        /// <param name="dir">The direction to travel in</param>
        /// <param name="nodeMeshViewController">Reference to the node mesh view controller</param>
        public void CalculateLinearLineScore(Node startNode, NodeDirections dir, TicTacNodeMeshViewController nodeMeshViewController)
        {
            CalculateLineScore(startNode, dir, LinearScore, nodeMeshViewController);
        }

        /// <summary>
        /// Calculates and tallies the score for a line of nodes, starting at the given node, and going in the given direction,
        /// using the Exponential scoring method
        /// </summary>
        /// <param name="startNode">The node to begin calculating from</param>
        /// <param name="dir">The direction to travel in</param>
        /// <param name="nodeMeshViewController">Reference to the node mesh view controller</param>
        public void CalculateExponentialLineScore(Node startNode, NodeDirections dir, TicTacNodeMeshViewController nodeMeshViewController)
        {
            CalculateLineScore(startNode, dir, ExponentialScore, nodeMeshViewController);
        }

        /// <summary>
        /// Calculates and tallies the score for a line of nodes, starting at the given node, and going in the given direction,
        /// using the Triangular scoring method
        /// </summary>
        /// <param name="startNode">The node to begin calculating from</param>
        /// <param name="dir">The direction to travel in</param>
        /// <param name="nodeMeshViewController">Reference to the node mesh view controller</param>
        public void CalculateTriangularLineScore(Node startNode, NodeDirections dir, TicTacNodeMeshViewController nodeMeshViewController)
        {
            CalculateLineScore(startNode, dir, TriangularScore, nodeMeshViewController);
        }

        /// <summary>
        /// Calculates and tallies the score for a line of nodes, starting at the given node, and going in the given direction,
        /// using the provided score strategy
        /// </summary>
        /// <param name="startNode">The node to begin calculating from</param>
        /// <param name="dir">The direction to travel in</param>
        /// <param name="scoreStrategy">The score stratgey to use</param>
        /// <param name="nodeMeshViewController">Reference to the node mesh view controller</param>
        private void CalculateLineScore(Node startNode, NodeDirections dir, ScoreStrategy scoreStrategy, TicTacNodeMeshViewController nodeMeshViewController)
        {
            var currentNode = startNode;
            var currentNodeConnection = currentNode.NodeType;
            int connectionLength = 1;
            var nextNode = currentNode[(int)dir];

            var currentRun = new List<Node>();
            currentRun.Add(currentNode);

            while (nextNode != null)
            {
                // If we can score from it
                if (IsScorableNode((NodeType)nextNode.NodeType))
                {
                    // If it is the same as the last one
                    //if(nextNode.NodeType == currentNodeConnection)
                    if (IsSameOrKing((NodeType)nextNode.NodeType, (NodeType)currentNodeConnection))
                    {
                        connectionLength++;
                        currentRun.Add(nextNode);
                    }
                    else // Otherwise we reset the run, and apply score
                    {
                        AddScoreForConnection(connectionLength, (NodeType)currentNodeConnection, scoreStrategy);
                        if (currentRun.Count > 1)
                        {
                            nodeMeshViewController.SizeConnections(currentRun.ToArray(), dir);
                        }
                        currentRun.Clear();
                        currentRun.Add(nextNode);
                        connectionLength = 1;
                    }
                }
                else // Apply any current score and reset the run
                {
                    AddScoreForConnection(connectionLength, (NodeType)currentNodeConnection, scoreStrategy);
                    if (currentRun.Count > 1)
                    {
                        nodeMeshViewController.SizeConnections(currentRun.ToArray(), dir);
                    }
                    currentRun.Clear();
                    currentRun.Add(nextNode);

                    connectionLength = 1;
                }
                currentNode = nextNode;
                nextNode = currentNode[(int)dir];
                currentNodeConnection = currentNode.NodeType;
            }

            // Apply any score left after all have been checked
            AddScoreForConnection(connectionLength, (NodeType)currentNodeConnection, scoreStrategy);
            if (currentRun.Count > 1)
            {
                nodeMeshViewController.SizeConnections(currentRun.ToArray(), dir);
            }
            currentRun.Clear();
        }

        /// <summary>
        /// Adds the score to the corresponding player's tally for a connection run
        /// </summary>
        /// <param name="connectionLength">Length of the connection</param>
        /// <param name="currentNodeConnection">Current type of node making the connection</param>
        /// <param name="scoreStrategy">The scoring strategy to use</param>
        private void AddScoreForConnection(int connectionLength, NodeType currentNodeConnection, ScoreStrategy scoreStrategy)
        {
            // We only score if 3 or more connection are made
            if (connectionLength >= 3)
            {
                switch (currentNodeConnection)
                {
                    case NodeType.Player1:
                        Player1Score += scoreStrategy(connectionLength);
                        break;

                    case NodeType.Player1King:
                        Player1Score += scoreStrategy(connectionLength);
                        break;

                    case NodeType.Player2:
                        Player2Score += scoreStrategy(connectionLength);
                        break;

                    case NodeType.Player2King:
                        Player2Score += scoreStrategy(connectionLength);
                        break;

                    case NodeType.Player3:
                        Player3Score += scoreStrategy(connectionLength);
                        break;

                    case NodeType.Player3King:
                        Player3Score += scoreStrategy(connectionLength);
                        break;

                    case NodeType.Player4:
                        Player4Score += scoreStrategy(connectionLength);
                        break;

                    case NodeType.Player4King:
                        Player4Score += scoreStrategy(connectionLength);
                        break;

                    default:
                        TTDebug.LogError("LineScoreCalculator.cs : Invalid node type in score : " + currentNodeConnection.ToString());
                        break;
                }
            }
        }

        /// <summary>
        /// Is the given node a type that can generate any score?
        /// </summary>
        /// <param name="nodeType">The type of node to check</param>
        /// <returns>True is node is scorable, false if not</returns>
        private bool IsScorableNode(NodeType nodeType)
        {
            return nodeType != NodeType.Empty && nodeType != NodeType.None && nodeType != NodeType.Unusable;
        }

        /// <summary>
        /// Do the two nodes belong to the same player?
        /// </summary>
        /// <param name="node1">First node to compare</param>
        /// <param name="node2">Second node to compare</param>
        /// <returns>True if both belong to the same player (king or regular piece), false if not.</returns>
        private bool IsSameOrKing(NodeType node1, NodeType node2)
        {
            if (node1 == node2)
                return true;

            // Checks for king vs regular node matches
            switch (node1)
            {
                case NodeType.Player1:
                    return node2 == NodeType.Player1King;
                case NodeType.Player1King:
                    return node2 == NodeType.Player1;
                case NodeType.Player2:
                    return node2 == NodeType.Player2King;
                case NodeType.Player2King:
                    return node2 == NodeType.Player2;
                case NodeType.Player3:
                    return node2 == NodeType.Player3King;
                case NodeType.Player3King:
                    return node2 == NodeType.Player3;
                case NodeType.Player4:
                    return node2 == NodeType.Player4King;
                case NodeType.Player4King:
                    return node2 == NodeType.Player4;
            }
            return false;
        }
    }
}