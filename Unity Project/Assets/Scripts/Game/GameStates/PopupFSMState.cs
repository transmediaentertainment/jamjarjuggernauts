using TransTech.FiniteStateMachine;
using TransTech.System.Debug;

namespace TransTech.Gameplay.States
{	
    /// <summary>
    /// Abstract class for an FSM State that shows a popup
    /// </summary>
	public abstract class PopupFSMState : FSMState
	{		
		/// <summary>
		/// The GUI manager
		/// </summary>
		protected InGameGUIManager m_GUIManager;
		/// <summary>
		/// The Popup
		/// </summary>
		protected Popup m_Popup;
		
		/// <summary>
		/// The text for the popup's label
		/// </summary>
		protected string m_LabelText;
		/// <summary>
		/// The text for the popup's buttons
		/// </summary>
		protected string[] m_ButtonText;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="TransTech.Gameplay.States.PopupFSMState"/> class.
		/// </summary>
		/// <param name='guiManager'>The GUI manager.</param>
		/// <param name='labelText'>The text for the popup's label.</param>
		/// <param name='buttonText'>The text for the popup's buttons.</param>
		protected PopupFSMState( InGameGUIManager guiManager, string labelText, params string[] buttonText )
		{
			m_GUIManager = guiManager;
			m_LabelText = labelText;
			m_ButtonText = buttonText;
		}
		
		/// <summary>
		/// Called by the FSM when entering the state
		/// </summary>
		/// <param name='args'>Additional arguments for the state (none)</param>
		public override void Enter (params object[] args)
		{
			m_Popup = Popup.CreatePopup( m_LabelText, m_ButtonText );
			
			TTDebug.Log("Attempting to disable panel buttons");
			
			m_GUIManager.EnablePanelButtons(false);
		}
		
		/// <summary>
		/// Called by the FSM when exiting the state
		/// </summary>
		public override void Exit ()
		{
			Popup.ReturnPopup( m_Popup );
			
			m_GUIManager.EnablePanelButtons(true);
		}
	}
}