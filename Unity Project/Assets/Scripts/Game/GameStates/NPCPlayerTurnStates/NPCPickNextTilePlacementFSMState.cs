﻿using TransTech.FiniteStateMachine;
using TransTech.Gameplay.Players;
using TransTech.Nodes;
using System.Collections.Generic;
using UnityEngine;

namespace TransTech.Gameplay.States
{
	class NPCPickNextTilePlacementFSMState : FSMState
	{
        private struct Coord
        {
            public int XPos { get; set; }
            public int YPos { get; set; }

            public Coord(int x, int y) : this()
            {
                XPos = x;
                YPos = y;
            }
        }

        private class NodeLine
        {
            public int StartPos { get; private set; }
            public int EndPos { get; private set; }
            public List<int> OwnedPositions;
            public bool IsInit { get; private set; }
            public int Length { get { return Mathf.Abs(EndPos - StartPos) + 1; } }

            public NodeLine()
            {
                OwnedPositions = new List<int>();
                Reset();
            }

            public void Init(int start, int end)
            {
                StartPos = start;
                EndPos = end;
                IsInit = true;
            }

            public void Reset()
            {
                StartPos = -1;
                EndPos = -1;
                OwnedPositions.Clear();
                IsInit = false;
            }

            public void IncrementEnd()
            {
                EndPos++;
            }
        }

        //private List<Coord> m_EmptySpots = new List<Coord>(); 
	    private TicTacNodeMeshViewController m_NodeMesh;

	    //private LineScoreCalculator m_ScoreCalc = new LineScoreCalculator();
	    private Player m_Player;

	    private int[,] m_AttackH;
	    private int[,] m_AttVert;
	    private int[,] m_AttRightUp;
	    private int[,] m_AttLeftUp;
	    private int[,] m_DefenseH;

	    private const float m_AttPotentialWeight = 1f;
	    private const float m_AttRealWeight = 2f;

        public NPCPickNextTilePlacementFSMState(TicTacNodeMeshViewController nodeMeshViewController, Player player)
        {
            m_NodeMesh = nodeMeshViewController;
            m_Player = player;
            var mesh = m_NodeMesh.CurrentNodeMesh as NodeMesh2DGrid;
            m_AttackH = new int[mesh.XSize, mesh.YSize];
            m_DefenseH = new int[mesh.XSize, mesh.YSize];
        }

        private void ResetH()
        {
            for(int x = 0; x < m_AttackH.GetLength(0); x++)
            {
                for(int y = 0; y < m_AttackH.GetLength(1); y++)
                {
                    m_AttackH[x, y] = 0;
                    m_DefenseH[x, y] = 0;
                }
            }
        }

        public override void Enter(params object[] args)
        {
            base.Enter(args);

            var mesh = m_NodeMesh.CurrentNodeMesh as NodeMesh2DGrid;

            NodeLine line = new NodeLine();
            // All horizontal scores
            for(int y = 0; y < mesh.YSize; y++)
            {
                for(int x = 0; x < mesh.XSize; x++)
                {
                    var n = mesh.GetNodeAtPosition(x, y);
                    if (n.NodeType == (int) NodeType.Empty ||
                        IsSameOrKing((NodeType) m_Player.PlayerNumber, (NodeType) n.NodeType))
                    {
                        if (!line.IsInit)
                        {
                            line.Init(x, x);
                        }
                        else
                        {
                            line.IncrementEnd();
                        }
                        if (n.NodeType != (int) NodeType.Empty)
                        {
                            line.OwnedPositions.Add(x);
                        }
                    }
                }
            
                    
                // This is not empty, nor ours, so reset counters for line
                for(int x2 = line.StartPos; x2 <= line.EndPos; x2++)
                {
                    // Potential score
                    var p = 0;
                    // Just doing linear score for now
                    p = line.Length - 2;
                    for(int i = 0; i < line.OwnedPositions.Count; i++)
                    {
                        p += line.Length - (Mathf.Abs(line.OwnedPositions[i] - x2));
                    }

                    // Real score change
                    //var r = 0;
                    var oldScore = 0;
                    var oldLineCount = 0;
                    var newScore = 0;
                    var newLineCount = 0;
                    for(int i = line.StartPos; i <= line.EndPos; i++)
                    {
                        if(i == x2 || line.OwnedPositions.Contains(i))
                        {
                            newLineCount++;
                        }
                        else
                        {
                            // End of line.  Get score
                            // Just doing linear score for now
                            newScore += Mathf.Clamp(newLineCount - 2, 0, int.MaxValue);
                        }
                        if(line.OwnedPositions.Contains(i))
                        {
                            oldLineCount++;
                        }
                        else
                        {
                            oldScore += Mathf.Clamp(oldLineCount - 1, 0, int.MaxValue);
                        }
                    }

                   // r = newScore - oldScore;
                   // m_AttHoriz[x]

                }
                line.Reset();
            }
        }

        /// <summary>
        /// Do the two nodes belong to the same player?
        /// </summary>
        /// <param name="node1">First node to compare</param>
        /// <param name="node2">Second node to compare</param>
        /// <returns>True if both belong to the same player (king or regular piece), false if not.</returns>
        private bool IsSameOrKing(NodeType node1, NodeType node2)
        {
            if (node1 == node2)
                return true;

            // Checks for king vs regular node matches
            switch (node1)
            {
                case NodeType.Player1:
                    return node2 == NodeType.Player1King;
                case NodeType.Player1King:
                    return node2 == NodeType.Player1;
                case NodeType.Player2:
                    return node2 == NodeType.Player2King;
                case NodeType.Player2King:
                    return node2 == NodeType.Player2;
                case NodeType.Player3:
                    return node2 == NodeType.Player3King;
                case NodeType.Player3King:
                    return node2 == NodeType.Player3;
                case NodeType.Player4:
                    return node2 == NodeType.Player4King;
                case NodeType.Player4King:
                    return node2 == NodeType.Player4;
            }
            return false;
        }

       
	}
}
