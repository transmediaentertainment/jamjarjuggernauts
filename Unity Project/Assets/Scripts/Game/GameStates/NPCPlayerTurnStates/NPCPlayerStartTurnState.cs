using Holoville.HOTween;
using TransTech.Delegates;
using TransTech.FiniteStateMachine;
using TransTech.Gameplay.States;
using UnityEngine;
using System.Collections;

public class NPCPlayerStartTurnState : PopupFSMState
{
    public event VoidDelegate NPCPlayerTurnWillBeginEvent;

    private float m_Timer;
    private const float m_WaitTime = 1.5f;

    public NPCPlayerStartTurnState(InGameGUIManager guiManager, string playerName)
        : base(guiManager, "Player " + playerName + " now starting their turn", null)
    {
    }

    public override void Enter(params object[] args)
    {
        base.Enter(args);

        m_Timer = 0f;
    }

    public override void Update(float deltaTime)
    {
        base.Update(deltaTime);

        m_Timer += deltaTime;
        if(m_Timer >= m_WaitTime)
        {
            if (NPCPlayerTurnWillBeginEvent != null)
                NPCPlayerTurnWillBeginEvent();
        }
    }

    public override void Exit()
    {
        base.Exit();
        HOTween.To(m_Popup.transform, 0.3f, new TweenParms().Prop("localPosition", new Vector3(-750f, 0f, 0f)).Ease(EaseType.EaseInExpo).OnComplete(() => Popup.ReturnPopup(m_Popup)));
    }
}
