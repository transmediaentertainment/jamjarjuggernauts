using TransTech.Delegates;
using TransTech.System.Debug;

namespace TransTech.Gameplay.States
{
    /// <summary>
    /// Game State for the end of the game, displaying the results of the game
    /// </summary>
    public class GameEndedFSMState : PopupFSMState
    {
        /// <summary>
        /// Event fired when the player clicks the Retry button
        /// </summary>
        public event VoidDelegate GameEndedRetryEvent;
        /// <summary>
        /// Event fires when the player clicks the Main Menu button
        /// </summary>
        public event VoidDelegate GameEndedMenuEvent;

        /// <summary>
        /// Constructor for initialization
        /// </summary>
        /// <param name="guiManager">Reference to the scene's GUI Manager</param>
        public GameEndedFSMState(InGameGUIManager guiManager) :
            base(guiManager, "", "Rematch?", "Main Menu")
        {
        }

        /// <summary>
        /// Called by the FSM on Entering the state
        /// </summary>
        /// <param name="args">Arguments to pass into the state (string)</param>
        public override void Enter(params object[] args)
        {
            // The only argument that should be passed through is the string to display on the popup
            if (args == null || args.Length != 1 || !(args[0] is string))
            {
                TTDebug.LogError("gameEndedFSMState.cs : string was not passed through for label");
            }
            // We set the label text before calling the base class enter function, so it can initialize the
            // popup with the correct information
            m_LabelText = (string)args[0];

            base.Enter(args);
            m_Popup.PopupButtonPressedEvent += PopupButtonPressed;
            var gc = GameController.Instance;
            if (gc.PlayerFour != null)
            {
                TTTAnalytics.Instance.GameEnded(gc.PlayerOne.CurrentScore, gc.PlayerTwo.CurrentScore, gc.PlayerThree.CurrentScore, gc.PlayerFour.CurrentScore);
            }
            else if (gc.PlayerThree != null)
            {
                TTTAnalytics.Instance.GameEnded(gc.PlayerOne.CurrentScore, gc.PlayerTwo.CurrentScore, gc.PlayerThree.CurrentScore, 0);
            }
            else
            {
                TTTAnalytics.Instance.GameEnded(gc.PlayerOne.CurrentScore, gc.PlayerTwo.CurrentScore, 0, 0);
            }
        }

        /// <summary>
        /// Handles the buttons being pressed in our popup window
        /// </summary>
        /// <param name="buttonNumber">The index of the button pressed</param>
        private void PopupButtonPressed(int buttonNumber)
        {
            if (buttonNumber == 0)
            {
                if (GameEndedRetryEvent != null)
                    GameEndedRetryEvent();
            }
            else if (buttonNumber == 1)
            {
                if (GameEndedMenuEvent != null)
                    GameEndedMenuEvent();
            }
        }

        /// <summary>
        /// Called by the FSM on exiting the state
        /// </summary>
        public override void Exit()
        {
            base.Exit();
            m_Popup.PopupButtonPressedEvent -= PopupButtonPressed;
        }
    }
}