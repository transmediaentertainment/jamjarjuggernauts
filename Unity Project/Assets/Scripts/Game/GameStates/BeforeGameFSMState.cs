using TransTech.Delegates;

namespace TransTech.Gameplay.States
{
    /// <summary>
    /// Game state for before the game has actually commenced
    /// </summary>
    public class BeforeGameFSMState : PopupFSMState
    {
        /// <summary>
        /// Event fired when the player presses the begin game button
        /// </summary>
        public event VoidDelegate GameWillBeginEvent;

        /// <summary>
        /// Constructor for initialization
        /// </summary>
        /// <param name="guiManager">Reference to the scene's GUI Manager</param>
        public BeforeGameFSMState(InGameGUIManager guiManager) :
            base(guiManager, "Welcome to Tic Tac Total Domination!", "Tap to Start!")
        {
        }

        /// <summary>
        /// Called by the FSM on Entering the state
        /// </summary>
        /// <param name="args">Arguments to pass into the state (none)</param>
        public override void Enter(params object[] args)
        {
            base.Enter(args);
            m_Popup.PopupButtonPressedEvent += PopupButtonPressed;
        }

        /// <summary>
        /// Handles the buttons being pressed in our popup window
        /// </summary>
        /// <param name="buttonNumber">The index of the button pressed</param>
        private void PopupButtonPressed(int buttonNumber)
        {
            if (buttonNumber == 0)
            {
                if (GameWillBeginEvent != null)
                    GameWillBeginEvent();
            }
        }

        /// <summary>
        /// Called by the FSM on exiting the state
        /// </summary>
        public override void Exit()
        {
            base.Exit();
            m_Popup.PopupButtonPressedEvent -= PopupButtonPressed;
        }
    }
}