using TransTech.Delegates;
using TransTech.FiniteStateMachine;
using TransTech.Gameplay.Players;

namespace TransTech.Gameplay.States
{
    public class NPCPlayerTurnFSMStateEngine : PlayerTurn, IPlayerTurnEvents
    {
        /// <summary>
        /// Event fired when the player's turn ends
        /// </summary>
        public event VoidDelegate PlayerTurnEnded;

        /// <summary>
        /// Event fired when the player begin's the sacrificing state
        /// </summary>
        public event VoidDelegate PlayerBeganSacrificingEvent;

        /// <summary>
        /// Event fired when the player Fortify's a position
        /// </summary>
        public event VoidDelegate PlayerEndedFortifyingEvent;

        /// <summary>
        /// Event fired when the player Captures/Attacks a position
        /// </summary>
        public event VoidDelegate PlayerEndedCapturingEvent;

        /// <summary>
        /// Event fired when the cancel button is pressed during ether the attack/capture or fortify/king state
        /// </summary>
        public event VoidDelegate PlayerCancelledAttackOrFortifyStateEvent;

        /// <summary>
        /// Event fired when the end turn button changes active state
        /// </summary>
        public event BoolDelegate SetEndTurnButtonActiveEvent;
        /// <summary>
        /// Event fired when the cancel button changes active state
        /// </summary>
        public event BoolDelegate SetCancelButtonActiveEvent;
        /// <summary>
        /// Event fired when the sacrifice button changes active state
        /// </summary>
        public event BoolDelegate SetSacrificeButtonActiveEvent;
        /// <summary>
        /// Event fired when the capture(attack) button changes active state
        /// </summary>
        public event BoolDelegate SetCaptureButtonActiveEvent;
        /// <summary>
        /// Event fired when the king(fortify) button changes active state
        /// </summary>
        public event BoolDelegate SetKingButtonActiveEvent;
        /// <summary>
        /// Event fired when the Undo button changes active state
        /// </summary>
        public event BoolDelegate SetUndoButtonActiveEvent;

        public NPCPlayerTurnFSMStateEngine(Player player, ISacrificeStrategy sacrificeStrategy, TicTacNodeMeshViewController nodeMeshViewController, InGameGUIManager inGameGUIManager, bool isResuming) 
            : base(player, sacrificeStrategy, nodeMeshViewController, inGameGUIManager, isResuming)
        {
            m_IdleState = new IdleFSMState();
            m_StartTurnState = new NPCPlayerStartTurnState(m_GUIManager, CurrentPlayer.Name);

            NewState(m_IdleState);
        }

        private IdleFSMState m_IdleState;
        private NPCPlayerStartTurnState m_StartTurnState;

        /// <summary>
        /// Activates or deactives the buttons in the side panel based on bool value
        /// </summary>
        /// <param name="endTurn">Bool for End Turn button</param>
        /// <param name="cancel">Bool for cancel button</param>
        /// <param name="sacrifice">Bool for Sacrifice button</param>
        /// <param name="capture">Bool for Capture(attack) button</param>
        /// <param name="king">Bool for king(fortify) button</param>
        /// <param name="undo">Bool for Undo button</param>
        protected void SetButtonsActive(bool endTurn, bool cancel, bool sacrifice, bool capture, bool king, bool undo)
        {
            if (SetEndTurnButtonActiveEvent != null)
                SetEndTurnButtonActiveEvent(endTurn);
            if (SetCancelButtonActiveEvent != null)
                SetCancelButtonActiveEvent(cancel);
            if (SetSacrificeButtonActiveEvent != null)
                SetSacrificeButtonActiveEvent(sacrifice);
            if (SetCaptureButtonActiveEvent != null)
                SetCaptureButtonActiveEvent(capture);
            if (SetKingButtonActiveEvent != null)
                SetKingButtonActiveEvent(king);
            if (SetUndoButtonActiveEvent != null)
                SetUndoButtonActiveEvent(undo);
        }

        public override void Enter(params object[] args)
        {
            base.Enter(args);

            // We don't need to hook up to GUI event handlers.  We aren't doing anything with those buttons.
            NewState(m_StartTurnState);
            SetButtonsActive(false, false, false, false, false, false);

        }
    }
}