using TransTech.Delegates.TicTac;
using TransTech.FiniteStateMachine;
using TransTech.Gameplay.Players;
using TransTech.Nodes;
using TransTech.System.Debug;

namespace TransTech.Gameplay.States
{
    /// <summary>
    /// FSM State for when the player is placing pieces on the board.
    /// </summary>
    public class RegularPlacementFSMState : FSMState, IPlayerTurnState
    {
        #region IPlayerTurnState implementation

        /// <summary>
        /// The hit strategy to use while in this state.
        /// </summary>
        public NodeTestDelegate NodeHitStrategy
        {
            get
            {
                return RaycastOnNode;
            }
        }

        #endregion IPlayerTurnState implementation

        /// <summary>
        /// The Player that is placing pieces
        /// </summary>
        public Player CurrentPlayer { get; private set; }

        /// <summary>
        /// Constructor for initialization
        /// </summary>
        /// <param name="player">The Player that is placing pieces during this state</param>
        public RegularPlacementFSMState(Player player)
        {
            CurrentPlayer = player;
        }

        /// <summary>
        /// Called by the FSM Engine when entering this state
        /// </summary>
        /// <param name="args"></param>
        public override void Enter(params object[] args)
        {
            InputController.Instance.InvalidateCurrentTouches();
        }

        /// <summary>
        /// Handles a raycast hitting a node
        /// </summary>
        /// <param name="node">The Node that was hit by the raycast</param>
        /// <returns></returns>
        private bool RaycastOnNode(Node node)
        {
            // If the node is emtpy, and the player has a token to place, set the node type.
            if (node.NodeType == (int)NodeType.Empty)
            {
                if (CurrentPlayer.UseToken())
                {
                    node.NodeType = CurrentPlayer.PlayerNumber;
                    return true;
                }
                return false;
            }
            return false;
        }
    }
}