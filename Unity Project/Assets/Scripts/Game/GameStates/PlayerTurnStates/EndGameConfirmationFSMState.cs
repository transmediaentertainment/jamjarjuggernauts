using Holoville.HOTween;
using TransTech.Delegates;
using TransTech.Delegates.TicTac;
using TransTech.Nodes;
using TransTech.System.Debug;
using UnityEngine;

namespace TransTech.Gameplay.States
{
    /// <summary>
    /// FSM State to pop up a window and handle confirmation of the end of a game.
    /// </summary>
    public class EndGameConfirmationFSMState : PopupFSMState, IPlayerTurnState
    {
        #region IPlayerTurnState implementation

        /// <summary>
        /// Function for testing a Node being hit by a raycast.
        /// </summary>
        public NodeTestDelegate NodeHitStrategy
        {
            get
            {
                return HitStrat;
            }
        }

        #endregion IPlayerTurnState implementation

        /// <summary>
        /// Delegate function to handle Node Raycasts (just returns false)
        /// </summary>
        /// <param name="node">The node hit by a raycast</param>
        /// <returns>False</returns>
        private bool HitStrat(Node node)
        {
            return false;
        }

        /// <summary>
        /// Event fired when the Yes button is pressed
        /// </summary>
        public event VoidDelegate YesButtonPressed;

        /// <summary>
        /// Event fired when the No button is pressed
        /// </summary>
        public event VoidDelegate NoButtonPressed;

        /// <summary>
        /// Construtor to set up the state
        /// </summary>
        /// <param name="guiManager">Reference to the GUIManager in the scene</param>
        public EndGameConfirmationFSMState(InGameGUIManager guiManager)
            : base(guiManager, "Are you sure you want to end the game?", "Yes", "No")
        {
        }

        /// <summary>
        /// Called by the FSM System on entering the state
        /// </summary>
        /// <param name="args">Arguments passed by the FSM Engine (none)</param>
        public override void Enter(params object[] args)
        {
            base.Enter(args);
            m_Popup.PopupButtonPressedEvent += HandleButtonPressed;
            m_Popup.transform.localPosition = new Vector3(750f, 0f, 0f);

            HOTween.To(m_Popup.transform, 0.3f, new TweenParms().Prop("localPosition", Vector3.zero).Ease(EaseType.EaseOutExpo));
        }

        /// <summary>
        /// Delegate for handling when a button in the popup is pressed
        /// </summary>
        /// <param name="buttonNum">Number (index) of the button pressed</param>
        private void HandleButtonPressed(int buttonNum)
        {
            if (buttonNum == 0)
            {
                // Yes
                if (YesButtonPressed != null)
                    YesButtonPressed();
            }
            else if (buttonNum == 1)
            {
                // No
                if (NoButtonPressed != null)
                    NoButtonPressed();
            }
            else
            {
                TTDebug.LogError("EndGameConfirmationFSMState.cs : Invalid button number passed in : " + buttonNum);
            }
        }

        /// <summary>
        /// Called by the FSM Engine when exiting the state
        /// </summary>
        public override void Exit()
        {
            m_Popup.PopupButtonPressedEvent -= HandleButtonPressed;
			
			m_GUIManager.EnablePanelButtons(true);

            HOTween.To(m_Popup.transform, 0.3f, new TweenParms().Prop("localPosition", new Vector3(-750f, 0f, 0f)).Ease(EaseType.EaseInExpo).OnComplete(() => { Popup.ReturnPopup(m_Popup); }));
        }
    }
}