using TransTech.Delegates.TicTac;

namespace TransTech.Gameplay.States
{
    /// <summary>
    /// Interface for each of the Player Turn FSM internal states.
    /// </summary>
    public interface IPlayerTurnState
    {
        /// <summary>
        /// The Hit Strategy delegate used to raycast for the current state.
        /// </summary>
        NodeTestDelegate NodeHitStrategy { get; }
    }
}