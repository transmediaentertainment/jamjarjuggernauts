using System.Collections.Generic;
using TransTech.Delegates;
using TransTech.Delegates.TicTac;
using TransTech.FiniteStateMachine;
using TransTech.Gameplay.Players;
using TransTech.Nodes;

namespace TransTech.Gameplay.States
{
    /// <summary>
    /// The FSM State used by the Game FSM for when the player is selecting pieces to sacrifice
    /// </summary>
    public class SacraficeFSMState : FSMState, IPlayerTurnState
    {
        /// <summary>
        /// The player currently having their turn.
        /// </summary>
        public Player CurrentPlayer { get; private set; }

        #region IPlayerTurnState implementation

        /// <summary>
        /// Function for testing a Node being hit by a raycast.
        /// </summary>
        public NodeTestDelegate NodeHitStrategy
        {
            get
            {
                return RaycastOnNode;
            }
        }

        #endregion IPlayerTurnState implementation

        /// <summary>
        /// Currently selected sacrifices
        /// </summary>
        private List<Node> m_Sacrafices = new List<Node>();

        /// <summary>
        /// Current nodes that can be selected by the current player
        /// </summary>
        private List<Node> m_PossibleSacrafices = new List<Node>();

        /// <summary>
        /// Current direction of the selection (for 2 or more nodes selected)
        /// </summary>
        private NodeDirections m_SacraficeDirection = NodeDirections.None;

        /// <summary>
        /// Are we ready to sacrifice? (Are 3 adjacent nodes of the same type selected?)
        /// </summary>
        public bool ReadyToSacrafice { get; private set; }

        /// <summary>
        /// Event fired when Sacrifice is valid and ready
        /// </summary>
        public event VoidDelegate SacraficeIsReadyEvent;

        /// <summary>
        /// Reference to the Node Mesh View Controller
        /// </summary>
        private TicTacNodeMeshViewController m_NodeMeshViewController;

        /// <summary>
        /// Reference to the GUI Manager in the scene.
        /// </summary>
        private InGameGUIManager m_GUIManager;

        /// <summary>
        /// Constructor for setting up the object
        /// </summary>
        /// <param name="player">The Player this sacrifice state is relevant to</param>
        /// <param name="nodeMeshViewController">Reference to the node mesh view controller</param>
        public SacraficeFSMState(Player player, TicTacNodeMeshViewController nodeMeshViewController, InGameGUIManager guiManager)
        {
            CurrentPlayer = player;
            m_NodeMeshViewController = nodeMeshViewController;
            m_GUIManager = guiManager;
        }

        /// <summary>
        /// Called by the FSM system when the state is entered
        /// </summary>
        /// <param name="args">Arguments passed into the Sacrifice state (none)</param>
        public override void Enter(params object[] args)
        {
            // Clear all our lists.
            m_Sacrafices.Clear();
            m_PossibleSacrafices.Clear();
            m_SacraficeDirection = NodeDirections.None;
            ReadyToSacrafice = false;
            m_GUIManager.UndoButtonPressedEvent += HandleUndoButtonPressed;

            UpdateHightlighing();
        }

        /// <summary>
        /// Handles the Undo button being preesed
        /// </summary>
        private void HandleUndoButtonPressed()
        {
            // If we haven't selected any sacrifices yet, just return
            if (m_Sacrafices.Count == 0)
                return;

            // Undo the last selected node from the sacrifice list
            var undoneNode = m_Sacrafices[m_Sacrafices.Count - 1];
            m_Sacrafices.RemoveAt(m_Sacrafices.Count - 1);
            // Set it as unselected
            m_NodeMeshViewController.SetNodeUnselected(undoneNode);
            // If we are back to one node, we no longer have a direction
            if (m_Sacrafices.Count == 1)
                m_SacraficeDirection = NodeDirections.None;

            UpdateHightlighing();
        }

        /// <summary>
        /// Updates the highlighting/dimming of nodes on the board based on current sacrifice selections.
        /// </summary>
        private void UpdateHightlighing()
        {
            var nodeMesh = m_NodeMeshViewController.CurrentNodeMesh;
            var nodes = nodeMesh.GetNodes();
            // If we haven't selected any nodes to sacrifice yet, just highlight all pieces that are in 
            // a row of 3 or more.  Dim ones that can't make a row of 3.
            if (m_Sacrafices.Count == 0)
            {
                var leftNodes = new List<Node>();
                var topNodes = new List<Node>();
                var rightNodes = new List<Node>();

                // Collect all of the left, top and right nodes so we can scan the board
                foreach (var kvp in nodes)
                {
                    var node = kvp.Value;
                    if (node.Coordinate.XPos == 0)
                        leftNodes.Add(node);
                    if (node.Coordinate.XPos == m_NodeMeshViewController.XSize - 1)
                        rightNodes.Add(node);
                    if (node.Coordinate.ZPos == m_NodeMeshViewController.YSize - 1)
                        topNodes.Add(node);
                }

                var highlightList = new List<Node>();

                // Parse each of the nodes from the left in the right and down right directions
                foreach (var node in leftNodes)
                {
                    SelectHighlightPieces(node, NodeDirections.Right, highlightList);
                    SelectHighlightPieces(node, NodeDirections.DownRight, highlightList);
                }

                // Parse each of the nodes from the top in the down, down right, and down left directions,
                // skipping the corners as they are covered in the left and right nodes lists.
                for (int i = 0; i < topNodes.Count; i++)
                {
                    if (i != 0 && i != m_NodeMeshViewController.XSize - 1)
                    {
                        SelectHighlightPieces(topNodes[i], NodeDirections.DownRight, highlightList);
                        SelectHighlightPieces(topNodes[i], NodeDirections.DownLeft, highlightList);
                    }
                    SelectHighlightPieces(topNodes[i], NodeDirections.Down, highlightList);
                }

                // Parse each of the nodes from the right in the down left direction
                foreach (var node in rightNodes)
                    SelectHighlightPieces(node, NodeDirections.DownLeft, highlightList);

                // Set the nodes either darkened or highlighted.
                foreach (var kvp in nodes)
                {
                    if (highlightList.Contains(kvp.Value))
                    {
                        m_NodeMeshViewController.SetNodeHighlighted(kvp.Value);
                        m_PossibleSacrafices.Add(kvp.Value);
                    }
                    else
                    {
                        m_NodeMeshViewController.SetNodeDimmed(kvp.Value);
                    }
                }
            }
            // If we have selected on piece to sacrifice, highlight all adjacent nodes that can
            // make up a row of 3 or more.
            else if (m_Sacrafices.Count == 1)
            {
                m_PossibleSacrafices.Clear();

                var s = m_Sacrafices[0];
                // Loop through each of our selected node's connections
                for (int i = 0; i < 8; i++)
                {
                    var dir = (NodeDirections)i;
                    var flip = FlipDirection(dir);
                    var n = s[i];

                    if (n != null && n.NodeType == CurrentPlayer.PlayerNumber)
                    {
                        var add = false;
                        var ni = n[i];
                        var f = s[(int)flip];
                        if (ni != null && ni.NodeType == CurrentPlayer.PlayerNumber)
                        {
                            add = true;
                        }
                        else if (f != null && f.NodeType == CurrentPlayer.PlayerNumber)
                        {
                            add = true;
                        }
                        if (add && !m_PossibleSacrafices.Contains(n))
                        {
                            m_PossibleSacrafices.Add(n);
                        }
                    }
                }

                foreach (var kvp in nodes)
                {
                    if (m_PossibleSacrafices.Contains(kvp.Value) || m_Sacrafices.Contains(kvp.Value))
                    {
                        m_NodeMeshViewController.SetNodeHighlighted(kvp.Value);
                    }
                    else
                    {
                        m_NodeMeshViewController.SetNodeDimmed(kvp.Value);
                    }
                }
            }
            // If we have 2 selected sacrifices, determine the direction they are in and highlight only in that direction.
            else if (m_Sacrafices.Count < 3)
            {
                // If the direction hasn't been calculated.
                if (m_SacraficeDirection == NodeDirections.None)
                {
                    // Determine the direction from the first to the second selected node.
                    var sac1 = m_Sacrafices[0];
                    var sac2 = m_Sacrafices[1];
                    for (int i = 0; i < 8; i++)
                    {
                        if (sac1[i] == sac2)
                        {
                            m_SacraficeDirection = (NodeDirections)i;
                            break;
                        }
                    }
                }

                m_PossibleSacrafices.Clear();

                // For each of the scarifices
                foreach (var sacrafice in m_Sacrafices)
                {
                    // If the adjacent node is not in the sacrifice list already
                    if (!m_Sacrafices.Contains(sacrafice[(int)m_SacraficeDirection]))
                    {
                        var n = sacrafice[(int)m_SacraficeDirection];

                        // If the node in the sacrifice direction is the same player's node
                        if (n != null && n.NodeType == CurrentPlayer.PlayerNumber)
                        {
                            // Add to possibilities and highlight
                            m_PossibleSacrafices.Add(n);
                            m_NodeMeshViewController.SetNodeHighlighted(n);
                        }
                    }

                    // If the adjacent noe in the opposite direction isn't in the sacrifice list already
                    if (!m_Sacrafices.Contains(sacrafice[(int)FlipDirection(m_SacraficeDirection)]))
                    {
                        var n = sacrafice[(int)FlipDirection(m_SacraficeDirection)];

                        // If the node in the opposite direction is the same player's node
                        if (n != null && n.NodeType == CurrentPlayer.PlayerNumber)
                        {
                            // Add to the possibilities and highlight
                            m_PossibleSacrafices.Add(n);
                            m_NodeMeshViewController.SetNodeHighlighted(n);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Iterates over the nodes in the given direction from the start node, and
        /// adds ones that should be highlighted to the given list.
        /// </summary>
        /// <param name="startNode">The node to begin iterating from</param>
        /// <param name="dir">The direction to iterate in</param>
        /// <param name="highlightList">List to add Nodes to highlight to</param>
        private void SelectHighlightPieces(Node startNode, NodeDirections dir, List<Node> highlightList)
        {
            var currentNode = startNode;

            // Current run of connecting nodes (same player type)
            var currentRun = new List<Node>();

            // While there are still nodes in the direction we are moving
            while (currentNode != null)
            {
                // If it's the Current Player
                if (currentNode.NodeType == CurrentPlayer.PlayerNumber)
                {
                    // Add it to the run
                    currentRun.Add(currentNode);
                }
                else
                {
                    // Do we have a run of 3 or more?
                    if (currentRun.Count >= 3)
                    {
                        // If so, Add all of the items in the run to the highlight list.
                        foreach (var n in currentRun)
                        {
                            if (!highlightList.Contains(n))
                                highlightList.Add(n);
                        }
                    }

                    // Start a new run
                    currentRun.Clear();
                }

                // Grab the next node in the direction we are traveling.
                currentNode = currentNode[(int)dir];
            }

            // Catch the final run once we have no nodes left.
            if (currentRun.Count >= 3)
            {
                foreach (var n in currentRun)
                {
                    if (!highlightList.Contains(n))
                        highlightList.Add(n);
                }
            }
        }

        /// <summary>
        /// Handles Raycasting (IPlayerTurnState) against nodes.
        /// </summary>
        /// <param name="node">The node that the raycast hit.</param>
        /// <returns>Whether the node was acted upon or not.</returns>
        private bool RaycastOnNode(Node node)
        {
            // If it is possible to sacrafice this node
            if (m_PossibleSacrafices.Contains(node))
            {
                // Add it to our list of sacrafices, set it as selected.
                m_Sacrafices.Add(node);
                m_NodeMeshViewController.SetNodeSelected(node);
				m_PossibleSacrafices.Remove(node);
            }
            else
            {
                // We don't do anything
                return false;
            }

            // Set all the current possibles to dimmed.
            foreach (var n in m_PossibleSacrafices)
            {
                if (n != node)
                {
                    m_NodeMeshViewController.SetNodeDimmed(n);
                }
            }

            UpdateHightlighing();

            // Test if there are 3
            if (m_Sacrafices.Count >= 3)
            {
                // Set as ready to sacrafice
                ReadyToSacrafice = true;
                if (SacraficeIsReadyEvent != null)
                    SacraficeIsReadyEvent();
            }
            return true;
        }

        /// <summary>
        /// Sacrafices the currently selected pieces
        /// </summary>
        public void SacraficeCurrent()
        {
            foreach (var sacrafice in m_Sacrafices)
            {
                m_NodeMeshViewController.SetNodeUnselected(sacrafice);
                m_NodeMeshViewController.SetNodeRegular(sacrafice);
                sacrafice.NodeType = (int)NodeType.Empty;
            }
        }

        /// <summary>
        /// Exits the state, handles cleanup
        /// </summary>
        public override void Exit()
        {
            m_PossibleSacrafices.Clear();
            foreach (var n in m_Sacrafices)
            {
                m_NodeMeshViewController.SetNodeUnselected(n);
            }
            m_Sacrafices.Clear();
            m_NodeMeshViewController.SetBoardRegular();
            m_GUIManager.UndoButtonPressedEvent -= HandleUndoButtonPressed;
        }

        /// <summary>
        /// Helper function to return the opposite direction to the one provided.
        /// </summary>
        /// <param name="dir">Direction to flip</param>
        /// <returns>The flipped direction</returns>
        private NodeDirections FlipDirection(NodeDirections dir)
        {
            switch (dir)
            {
                case NodeDirections.Down:
                    return NodeDirections.Up;
                case NodeDirections.DownLeft:
                    return NodeDirections.UpRight;
                case NodeDirections.DownRight:
                    return NodeDirections.UpLeft;
                case NodeDirections.Left:
                    return NodeDirections.Right;
                case NodeDirections.Right:
                    return NodeDirections.Left;
                case NodeDirections.Up:
                    return NodeDirections.Down;
                case NodeDirections.UpLeft:
                    return NodeDirections.DownRight;
                case NodeDirections.UpRight:
                    return NodeDirections.DownLeft;
            }
            return NodeDirections.None;
        }
    }
}