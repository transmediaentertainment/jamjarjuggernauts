using Holoville.HOTween;
using TransTech.Delegates;
using TransTech.Delegates.TicTac;
using TransTech.Nodes;
using UnityEngine;

namespace TransTech.Gameplay.States
{
    /// <summary>
    /// FSM State for confirming if the player wants to sacrifice their pieces or not.
    /// </summary>
    public class SacrificeConfirmationFSMState : PopupFSMState, IPlayerTurnState
    {
        /// <summary>
        /// The hit strategy to use while in this state.
        /// </summary>
        public NodeTestDelegate NodeHitStrategy
        {
            get { return n => false; }
        }

        /// <summary>
        /// Constructor for Initialization
        /// </summary>
        /// <param name="guiManager">Reference to the scene's GUI Manager</param>
        public SacrificeConfirmationFSMState(InGameGUIManager guiManager)
            : base(guiManager, "Are you sure you want to\nSacrafice these pieces?", "Yes", "No")
        {
        }

        /// <summary>
        /// Event fired when the Popup's Yes button is pressed
        /// </summary>
        public event VoidDelegate YesButtonPressed;
        /// <summary>
        /// Event fired when the Popup's No button is pressed
        /// </summary>
        public event VoidDelegate NoButtonPressed;

        /// <summary>
        /// Called by the FSM Engine when entering the state.
        /// </summary>
        /// <param name="args">Arguments passed by the FSM Engine (none)</param>
        public override void Enter(params object[] args)
        {
            base.Enter(args);

            m_Popup.PopupButtonPressedEvent += HandleButtonPress;
            m_Popup.transform.localPosition = new Vector3(750f, 0f, 0f);

            HOTween.To(m_Popup.transform, 0.3f,
                       new TweenParms().Prop("localPosition", Vector3.zero).Ease(EaseType.EaseOutExpo));
        }

        /// <summary>
        /// Handles when one of the popup's buttons is pressed
        /// </summary>
        /// <param name="buttonNum"></param>
        private void HandleButtonPress(int buttonNum)
        {
            if (buttonNum == 0)
            {
                if (YesButtonPressed != null)
                    YesButtonPressed();
            }
            else if (buttonNum == 1)
            {
                if (NoButtonPressed != null)
                    NoButtonPressed();
            }
        }

        /// <summary>
        /// Called by the FSM Engine when the state is exited
        /// </summary>
        public override void Exit()
        {
            // We don't called base.Exit() as we don't want to return the popup until the animation is finished.
            m_Popup.PopupButtonPressedEvent -= HandleButtonPress;

            m_GUIManager.EnablePanelButtons(true);

            // We return the popup to it's object pool once the animation is completed
            HOTween.To(m_Popup.transform, 0.3f,
                       new TweenParms().Prop("localPosition", new Vector3(-750f, 0f, 0f)).Ease(EaseType.EaseInExpo).
                           OnComplete(() => Popup.ReturnPopup(m_Popup)));
        }
    }
}