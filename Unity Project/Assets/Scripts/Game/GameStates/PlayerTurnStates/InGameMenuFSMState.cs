using TransTech.Delegates;
using TransTech.System.Debug;

namespace TransTech.Gameplay.States
{
    /// <summary>
    /// State for the In Game popup manu
    /// </summary>
    public class InGameMenuFSMState : PopupFSMState
    {
        /// <summary>
        /// Event fired when the Continue button is pressed
        /// </summary>
        public event VoidDelegate InGameMenuContinueEvent;

        /// <summary>
        /// Event fired when the Retry button is pressed
        /// </summary>
        public event VoidDelegate InGameMenuRetryEvent;

        /// <summary>
        /// Event fired when the Main Menu button is pressed
        /// </summary>
        public event VoidDelegate InGameMenuMainMenuEvent;

        /// <summary>
        /// Event fired when the Tutorial button is pressed
        /// </summary>
        public event VoidDelegate InGameMenuTutorialEvent;

        /// <summary>
        /// Constructor for setting up the Popup
        /// </summary>
        /// <param name="guiManager">Reference to the GUIManager in the scene</param>
        public InGameMenuFSMState(InGameGUIManager guiManager) :
            base(guiManager, "Pause Menu", "Continue", "Restart\nMatch", "Main Menu", "Tutorial")
        {
			TTDebug.Log("Constructing InGameMenuFSMState");
        }

        /// <summary>
        /// Called by the FSM Engine when entering the state
        /// </summary>
        /// <param name="args">Arguments passed into the state (None)</param>
        public override void Enter(params object[] args)
        {
			TTDebug.Log("Entering InGameMenuFSMState");
			
            base.Enter(args);
            m_Popup.PopupButtonPressedEvent += PopupButtonPressed;
        }

        /// <summary>
        /// Called by the FSM Engine when the state loses focus
        /// </summary>
        public override void LostFocus()
        {
            base.LostFocus();
            m_Popup.PopupButtonPressedEvent -= PopupButtonPressed;
        }

        /// <summary>
        /// Called by the FSM Engine when the state regains focus
        /// </summary>
        public override void GainedFocus()
        {
            base.GainedFocus();
            m_Popup.PopupButtonPressedEvent += PopupButtonPressed;
        }

        /// <summary>
        /// Delegate to handle events fired when the popup buttons are pressed
        /// </summary>
        /// <param name="buttonNumber">Number (index) of the button pressed</param>
        private void PopupButtonPressed(int buttonNumber)
        {
            // Continue Button
            if (buttonNumber == 0)
            {
                if (InGameMenuContinueEvent != null)
                    InGameMenuContinueEvent();
            }

            // Retry button
            else if (buttonNumber == 1)
            {
                if (InGameMenuRetryEvent != null)
                    InGameMenuRetryEvent();
            }

            // Main Menu button
            else if (buttonNumber == 2)
            {
                if (InGameMenuMainMenuEvent != null)
                    InGameMenuMainMenuEvent();
            }
            // Tutorial button
            else if (buttonNumber == 3)
            {
                if (InGameMenuTutorialEvent != null)
                    InGameMenuTutorialEvent();
            }
			else
			{
				TTDebug.LogError("InGmaeMneuFSMState.cs : Menu not returning valid number: " + buttonNumber);
			}
        }

        /// <summary>
        /// Called by the FSM Engine when exiting the state.
        /// </summary>
        public override void Exit()
        {
            m_Popup.PopupButtonPressedEvent -= PopupButtonPressed;
            base.Exit();
        }
    }
}