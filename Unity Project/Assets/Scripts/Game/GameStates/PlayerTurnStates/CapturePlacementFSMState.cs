using TransTech.Delegates;
using TransTech.Delegates.TicTac;
using TransTech.FiniteStateMachine;
using TransTech.Gameplay.Players;
using TransTech.Nodes;
using TransTech.System.Debug;

namespace TransTech.Gameplay.States
{
    /// <summary>
    /// Gameplay state for when the player has sacrificed pieces and has
    /// chosen to capture/attack an opponent's piece
    /// </summary>
    public class CapturePlacementFSMState : FSMState, IPlayerTurnState
    {
        #region IPlayerTurnState implementation

        /// <summary>
        /// Function for testing a Node being hit by a raycast.
        /// </summary>
        public NodeTestDelegate NodeHitStrategy
        {
            get
            {
                return RaycastOnNode;
            }
        }

        #endregion IPlayerTurnState implementation

        /// <summary>
        /// The player currently having their turn.
        /// </summary>
        public Player CurrentPlayer { get; private set; }

        /// <summary>
        /// Event fired when a board piece is captured
        /// </summary>
        public event VoidDelegate BoardPieceCapturedEvent;

        /// <summary>
        /// Local reference to the node mesh view controller
        /// </summary>
        private TicTacNodeMeshViewController m_NodeMeshViewController;

        /// <summary>
        /// Constructor for setting up the object
        /// </summary>
        /// <param name="player">The player this state is relevant to</param>
        /// <param name="nodeMeshViewController">Reference to the node mesh view controller</param>
        public CapturePlacementFSMState(Player player, TicTacNodeMeshViewController nodeMeshViewController)
        {
            CurrentPlayer = player;
            m_NodeMeshViewController = nodeMeshViewController;
        }

        /// <summary>
        /// Called by the FSM system when the state is entered
        /// </summary>
        /// <param name="args">Arguments passed into the state on entering (none)</param>
        public override void Enter(params object[] args)
        {
            // Make sure we invalidate any current touches on entering so
            // we don't select a node immediately
            InputController.Instance.InvalidateCurrentTouches();
            var nodes = m_NodeMeshViewController.CurrentNodeMesh.GetNodes();
            int playerNodeTypes = 0; ;
            switch (CurrentPlayer.PlayerNumber)
            {
                case 1:
                    playerNodeTypes = (int)NodeType.Player1;
                    break;

                case 2:
                    playerNodeTypes = (int)NodeType.Player2;
                    break;

                case 3:
                    playerNodeTypes = (int)NodeType.Player3;
                    break;

                case 4:
                    playerNodeTypes = (int)NodeType.Player4;
                    break;

                default:
                    TTDebug.LogError("CapturePlacementFSMState.cs : Invalid player number : " + CurrentPlayer.PlayerNumber.ToString());
                    break;
            }

            // For each node on the board
            foreach (var kvp in nodes)
            {
                // If the node is a king node, the current player's regular node, or and empty node, dim it
                if (kvp.Value.NodeType == playerNodeTypes || kvp.Value.NodeType == (int)NodeType.Empty || kvp.Value.NodeType == (int)NodeType.Unusable ||
                    kvp.Value.NodeType == (int)NodeType.Player1King || kvp.Value.NodeType == (int)NodeType.Player2King || kvp.Value.NodeType == (int)NodeType.Player3King ||
                    kvp.Value.NodeType == (int)NodeType.Player4King)
                {
                    m_NodeMeshViewController.SetNodeDimmed(kvp.Value);
                }
                else // Highlight the node
                {
                    m_NodeMeshViewController.SetNodeHighlighted(kvp.Value);
                }
            }
        }

        /// <summary>
        /// Called by the FSM system when exiting the state
        /// </summary>
        public override void Exit()
        {
            // Make sure to set the visuals back to normal
            m_NodeMeshViewController.SetBoardRegular();
        }

        /// <summary>
        /// Handles Raycasting (IPlayerTurnState) against nodes.
        /// </summary>
        /// <param name="node">The node that the raycast hit.</param>
        /// <returns>Whether the node was acted upon or not.</returns>
        private bool RaycastOnNode(Node node)
        {
            // If the node belongs to one of the other player's and isn't a king/fortified node.
            if (node.NodeType != CurrentPlayer.PlayerNumber && node.NodeType != (int)NodeType.Empty
                && node.NodeType != (int)NodeType.Unusable && node.NodeType != (int)NodeType.None
                && node.NodeType != (int)NodeType.Player1King && node.NodeType != (int)NodeType.Player2King
                && node.NodeType != (int)NodeType.Player3King && node.NodeType != (int)NodeType.Player4King)
            {
                // Set it to the current player's piece
                m_NodeMeshViewController.SetNodeRegular(node);
                node.NodeType = CurrentPlayer.PlayerNumber;

                // Fire the event to let others know a piece was captured
                if (BoardPieceCapturedEvent != null)
                    BoardPieceCapturedEvent();
                return true;
            }
            return false;
        }
    }
}