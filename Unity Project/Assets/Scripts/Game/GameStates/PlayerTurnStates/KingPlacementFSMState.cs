using TransTech.Delegates;
using TransTech.Delegates.TicTac;
using TransTech.FiniteStateMachine;
using TransTech.Gameplay.Players;
using TransTech.Nodes;
using TransTech.System.Debug;

namespace TransTech.Gameplay.States
{
    /// <summary>
    /// FSM State for when the Player is placing a King/Fortify piece.
    /// </summary>
    public class KingPlacementFSMState : FSMState, IPlayerTurnState
    {
        /// <summary>
        /// Reference to the current Player object
        /// </summary>
        public Player CurrentPlayer { get; private set; }

        /// <summary>
        /// Reference to the scene's NodeMeshViewController
        /// </summary>
        private TicTacNodeMeshViewController m_NodeMeshViewController;

        /// <summary>
        /// The hit strategy to use while in this state.
        /// </summary>
        public NodeTestDelegate NodeHitStrategy
        {
            get { return RaycastOnNode; }
        }

        /// <summary>
        /// Constructor for Initialization
        /// </summary>
        /// <param name="player">The Player this state will be operating for</param>
        /// <param name="nodeMeshViewController">Reference to the scene's Node Mesh View Controller</param>
        public KingPlacementFSMState(Player player, TicTacNodeMeshViewController nodeMeshViewController)
        {
            CurrentPlayer = player;
            m_NodeMeshViewController = nodeMeshViewController;
        }

        /// <summary>
        /// Event fired when the king/fortify piece is placed
        /// </summary>
        public event VoidDelegate KingPiecePlacedEvent;

        /// <summary>
        /// Called by the FSM Engine when entering the state.
        /// </summary>
        /// <param name="args">Arguments passed by the FSM Engine (none)</param>
        public override void Enter(params object[] args)
        {
            InputController.Instance.InvalidateCurrentTouches();
            var nodes = m_NodeMeshViewController.CurrentNodeMesh.GetNodes();
            int nodeType = 0;
            switch (CurrentPlayer.PlayerNumber)
            {
                case 1:
                    nodeType = (int)NodeType.Player1;
                    break;

                case 2:
                    nodeType = (int)NodeType.Player2;
                    break;

                case 3:
                    nodeType = (int)NodeType.Player3;
                    break;

                case 4:
                    nodeType = (int)NodeType.Player4;
                    break;
            }

            // Loop through the nodes, and set all valid nodes to highlighted, and all invalid nodes to dimmed
            foreach (var kvp in nodes)
            {
                if (kvp.Value.NodeType == nodeType)
                {
                    m_NodeMeshViewController.SetNodeHighlighted(kvp.Value);
                }
                else
                {
                    m_NodeMeshViewController.SetNodeDimmed(kvp.Value);
                }
            }
        }

        /// <summary>
        /// Handles a raycast hitting a node.
        /// </summary>
        /// <param name="node">The Node that was hit by the raycast</param>
        /// <returns>Whether the node was 'hit' or not</returns>
        private bool RaycastOnNode(Node node)
        {
            bool success = false;
            // If the nodeType is out current player's node, Set the node to regular highlighting, and set it to 
            // the king state for the current player
            if (node.NodeType == CurrentPlayer.PlayerNumber)
            {
                switch (CurrentPlayer.PlayerNumber)
                {
                    case 1:
                        m_NodeMeshViewController.SetNodeRegular(node);
                        node.NodeType = (int)NodeType.Player1King;
                        success = true;
                        break;

                    case 2:
                        m_NodeMeshViewController.SetNodeRegular(node);
                        node.NodeType = (int)NodeType.Player2King;
                        success = true;
                        break;

                    case 3:
                        m_NodeMeshViewController.SetNodeRegular(node);
                        node.NodeType = (int)NodeType.Player3King;
                        success = true;
                        break;

                    case 4:
                        m_NodeMeshViewController.SetNodeRegular(node);
                        node.NodeType = (int)NodeType.Player4King;
                        success = true;
                        break;

                    default:
                        TTDebug.LogError("KingPlacementFSMState.cs : Invalid Player number : " + CurrentPlayer.PlayerNumber);
                        break;
                }
            }
            // If we successfully hit a valid node
            if (success)
            {
                // Set the board back to regular highlighting
                m_NodeMeshViewController.SetBoardRegular();
                // Fire our piece placed event
                if (KingPiecePlacedEvent != null)
                    KingPiecePlacedEvent();
            }
            return success;
        }

        /// <summary>
        /// Called by the FSM Engine when exiting this state.
        /// </summary>
        public override void Exit()
        {
            // Return the board to regular highlighting state
            m_NodeMeshViewController.SetBoardRegular();
        }
    }
}