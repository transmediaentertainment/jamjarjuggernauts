using Holoville.HOTween;
using TransTech.Delegates;
using TransTech.Delegates.TicTac;
using TransTech.Nodes;
using UnityEngine;

namespace TransTech.Gameplay.States
{
    /// <summary>
    /// FSM State for the start of a player's turn
    /// </summary>
    public class PlayerTurnStartFSMState : PopupFSMState, IPlayerTurnState
    {
        #region IPlayerTurnState implementation
        /// <summary>
        /// The hit strategy to use while in this state.
        /// </summary>
        public NodeTestDelegate NodeHitStrategy
        {
            get { return n => false; }
        }

        #endregion IPlayerTurnState implementation

        /// <summary>
        /// Event fired when our popup button is pressed
        /// </summary>
        public event VoidDelegate PlayerTurnWillBeginEvent;

        /// <summary>
        /// Constructor for Initialization
        /// </summary>
        /// <param name="guiManager">Reference to the scene's GUI Manager</param>
        /// <param name="playerName">Name of the player who's turn is beginning</param>
        public PlayerTurnStartFSMState(InGameGUIManager guiManager, string playerName) :
            base(guiManager, "Pass over to " + playerName, "Tap to Play!")
        {
        }

        /// <summary>
        /// Called by the FSM Engine on entering the state
        /// </summary>
        /// <param name="args"></param>
        public override void Enter(params object[] args)
        {
            base.Enter(args);
            m_Popup.PopupButtonPressedEvent += PopupButtonPressed;

            m_Popup.transform.localPosition = new Vector3(750f, 0f, 0f);
            // Animate the button from the off screen right position to the center
            HOTween.To(m_Popup.transform, 0.3f, new TweenParms().Prop("localPosition", Vector3.zero).Ease(EaseType.EaseOutExpo));
        }

        /// <summary>
        /// Handles a button being pressed in the popup
        /// </summary>
        /// <param name="buttonNumber">Index of the button pressed</param>
        private void PopupButtonPressed(int buttonNumber)
        {
            if (buttonNumber == 0)
            {
                if (PlayerTurnWillBeginEvent != null)
                    PlayerTurnWillBeginEvent();
            }
        }

        /// <summary>
        /// Called by the FSM Engine on exiting the state
        /// </summary>
        public override void Exit()
        {
            // We don't call the popup's base exit function as we don't want to return the popup until the animation is done.
            m_Popup.PopupButtonPressedEvent -= PopupButtonPressed;
			
			m_GUIManager.EnablePanelButtons(true);
            // Animate the popup from the center of the screen to the offscreen right position.  On Completion, return the popup to it's pool
            HOTween.To(m_Popup.transform, 0.3f, new TweenParms().Prop("localPosition", new Vector3(-750f, 0f, 0f)).Ease(EaseType.EaseInExpo).OnComplete(() => Popup.ReturnPopup(m_Popup)));
        }
    }
}