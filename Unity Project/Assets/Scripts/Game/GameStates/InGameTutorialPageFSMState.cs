using TransTech.Delegates;

namespace TransTech.Gameplay.States
{
    /// <summary>
    /// FSM State to pop up an In Game tutorial page
    /// </summary>
	public class InGameTutorialPageFSMState : PopupFSMState
    {
        /// <summary>
        /// Event fired when the zero indexed button is pressed
        /// </summary>
        public event VoidDelegate ButtonZeroPressedEvent;
        /// <summary>
        /// Event fired when the one indexed button is pressed
        /// </summary>
        public event VoidDelegate ButtonOnePressedEvent;
        /// <summary>
        /// Event fired when the two indexed button is pressed
        /// </summary>
		public event VoidDelegate ButtonTwoPressedEvent;
		
        /// <summary>
        /// Constructor for initialization
        /// </summary>
        /// <param name="guiManager">Reference to the scene's GUI Manager</param>
        /// <param name="pageText">The text to display on the popup</param>
        /// <param name="buttons">The buttons to add to the popup</param>
		public InGameTutorialPageFSMState( InGameGUIManager guiManager, string pageText, params string[] buttons ) : base(guiManager, pageText, buttons){}

        /// <summary>
        /// Called by the FSM System on entering the state
        /// </summary>
        /// <param name="args">Arguments passed by the FSM Engine (none)</param>
		public override void Enter (params object[] args)
		{
			base.Enter (args);
			m_Popup.PopupButtonPressedEvent += PopupButtonPressed;
		}
		
        /// <summary>
        /// Called by the FSM when the state loses foces
        /// </summary>
		public override void LostFocus ()
		{
			base.LostFocus ();
			m_Popup.PopupButtonPressedEvent -= PopupButtonPressed;
			m_Popup.gameObject.SetActive(false);
		}
		
        /// <summary>
        /// Called by the FSM when the state regains focus
        /// </summary>
		public override void GainedFocus ()
		{
			base.GainedFocus ();
			m_Popup.PopupButtonPressedEvent += PopupButtonPressed;
			m_Popup.gameObject.SetActive(true);
		}
		
        /// <summary>
        /// Handles a button in the popup being pressed
        /// </summary>
        /// <param name="buttonNumber">Index of the button that was pressed</param>
		private void PopupButtonPressed( int buttonNumber )
		{
            if (buttonNumber == 0)
            {
                if (ButtonZeroPressedEvent != null)
                    ButtonZeroPressedEvent();
            }

            else if (buttonNumber == 1)
            {
                if (ButtonOnePressedEvent != null)
                    ButtonOnePressedEvent();
            }
            else if (buttonNumber == 2)
            {
                if (ButtonTwoPressedEvent != null)
                    ButtonTwoPressedEvent();
            }
        }

        /// <summary>
        /// Called by the FSM Engine when exiting the state
        /// </summary>
        public override void Exit()
        {
            m_Popup.PopupButtonPressedEvent -= PopupButtonPressed;
            base.Exit();
        }
    }
}