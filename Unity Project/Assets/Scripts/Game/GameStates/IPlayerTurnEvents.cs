using TransTech.Delegates;
using UnityEngine;
using System.Collections;

namespace TransTech.Gameplay.States
{
    public interface IPlayerTurnEvents
    {
        /// <summary>
        /// Event fired when the player's turn ends
        /// </summary>
        event VoidDelegate PlayerTurnEnded;

        /// <summary>
        /// Event fired when the player begin's the sacrificing state
        /// </summary>
        event VoidDelegate PlayerBeganSacrificingEvent;

        /// <summary>
        /// Event fired when the player Fortify's a position
        /// </summary>
        event VoidDelegate PlayerEndedFortifyingEvent;

        /// <summary>
        /// Event fired when the player Captures/Attacks a position
        /// </summary>
        event VoidDelegate PlayerEndedCapturingEvent;

        /// <summary>
        /// Event fired when the cancel button is pressed during ether the attack/capture or fortify/king state
        /// </summary>
        event VoidDelegate PlayerCancelledAttackOrFortifyStateEvent;

        /// <summary>
        /// Event fired when the end turn button changes active state
        /// </summary>
        event BoolDelegate SetEndTurnButtonActiveEvent;
        /// <summary>
        /// Event fired when the cancel button changes active state
        /// </summary>
        event BoolDelegate SetCancelButtonActiveEvent;
        /// <summary>
        /// Event fired when the sacrifice button changes active state
        /// </summary>
        event BoolDelegate SetSacrificeButtonActiveEvent;
        /// <summary>
        /// Event fired when the capture(attack) button changes active state
        /// </summary>
        event BoolDelegate SetCaptureButtonActiveEvent;
        /// <summary>
        /// Event fired when the king(fortify) button changes active state
        /// </summary>
        event BoolDelegate SetKingButtonActiveEvent;
        /// <summary>
        /// Event fired when the Undo button changes active state
        /// </summary>
        event BoolDelegate SetUndoButtonActiveEvent;
    }
}
