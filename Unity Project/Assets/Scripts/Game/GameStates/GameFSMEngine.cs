using System.Collections.Generic;
using TransTech.Delegates;
using TransTech.Delegates.TicTac;
using TransTech.FiniteStateMachine;
using TransTech.Gameplay.Players;
using TransTech.Nodes;
using TransTech.System.Debug;
using TransTech.System.IO;
using UnityEngine;
using TransTech.System.GUI;

namespace TransTech.Gameplay.States
{
    /// <summary>
    /// The FSM Engine for the Main Game
    /// </summary>
	public class GameFSMEngine : FSM
	{
        /// <summary>
        /// State for before the game begins
        /// </summary>
		private BeforeGameFSMState m_BeforeGameState;
        /// <summary>
        /// Player State FSM for Player One
        /// </summary>
		private PlayerTurnFSMStateEngine m_Player1State;
        /// <summary>
        /// Player State FSM for Player Two
        /// </summary>
		private PlayerTurnFSMStateEngine m_Player2State;
        /// <summary>
        /// Player State FSM for Player Three
        /// </summary>
		private PlayerTurnFSMStateEngine m_Player3State;
        /// <summary>
        /// Player State FSM for Player Four
        /// </summary>
		private PlayerTurnFSMStateEngine m_Player4State;
        /// <summary>
        /// State for after the game has ended, and displaying scores
        /// </summary>
		private GameEndedFSMState m_GameEndedState;
        /// <summary>
        /// State for the in game Menu
        /// </summary>
		private InGameMenuFSMState m_InGameMenuState;
        /// <summary>
        /// State for the 1st page of the Tutorial
        /// </summary>
		private InGameTutorialPageFSMState m_TutorialPage1State;
        /// <summary>
        /// State for the 2nd page of the Tutorial
        /// </summary>
        private InGameTutorialPageFSMState m_TutorialPage2State;
        /// <summary>
        /// State for the 3rd page of the Tutorial
        /// </summary>
        private InGameTutorialPageFSMState m_TutorialPage3State;
        /// <summary>
        /// State for the 4th page of the Tutorial
        /// </summary>
        private InGameTutorialPageFSMState m_TutorialPage4State;
		
        /// <summary>
        /// Number of players in the game.
        /// </summary>
		private int m_NumPlayers;
        /// <summary>
        /// Index of the player currently having their turn
        /// </summary>
		public int CurrentPlayerTurn { get; private set; }
		
        /// <summary>
        /// Reference to the node mesh view controller
        /// </summary>
		private TicTacNodeMeshViewController m_NodeMeshViewController;
        /// <summary>
        /// Reference to the GUI Manager in the scene
        /// </summary>
		private InGameGUIManager m_InGameGUIManager;

        /// <summary>
        /// Event fired when a player begins their turn
        /// </summary>
        public event IntDelegate PlayerTurnStarted;

        /// <summary>
        /// Event fired when the end turn button is turned on or off
        /// </summary>
        public event BoolDelegate SetEndTurnButtonActive;

        /// <summary>
        /// Event fired when the Cancel button is turned on or off
        /// </summary>
        public event BoolDelegate SetCancelButtonActive;

        /// <summary>
        /// Event fired when the Sacrifice button is turned on or off
        /// </summary>
        public event BoolDelegate SetSacrificeButtonActive;

        /// <summary>
        /// Event fired when the Capture(Attack) button is turned on or off
        /// </summary>
        public event BoolDelegate SetCaptureButtonActive;

        /// <summary>
        /// Event fired when the King(Fortify) button is turned on or off
        /// </summary>
        public event BoolDelegate SetKingButtonActive;

        /// <summary>
        /// Event fired when the Undo button is turned on or off
        /// </summary>
        public event BoolDelegate SetUndoButtonActive;

        /// <summary>
        /// Event fired with the sacrifice state is entered
        /// </summary>
        public event VoidDelegate SacrificeStateBeganEvent;

        /// <summary>
        /// Event fired when the sacrifice state is exited and an attack occurred 
        /// </summary>
        public event VoidDelegate SacrificeAttackCompletedEvent;

        /// <summary>
        /// Event fired when the sacrifice state is exited and a fortify occurred
        /// </summary>
        public event VoidDelegate SacrificeFortifyCompletedEvent;

        /// <summary>
        /// Event fired when the sacrifice state was exited without an attack or fortify occurring
        /// </summary>
        public event VoidDelegate PlayerCancelledAttackOrFortifyStateEvent;

        /// <summary>
        /// The current Strategy object to use when performing a sacrifice (for different rule sets)
        /// </summary>
        public ISacrificeStrategy SacrificeStrategy { get { return m_SacrificeStrategy; } }
        private ISacrificeStrategy m_SacrificeStrategy;

        /// <summary>
        /// Returns the Strategy function to use for raycasting against nodes.
        /// </summary>
        public NodeTestDelegate CurrentNodeStrategy
        {
            get
            {
                // If we are in a Player's turn state, return the strategy from the state
                var state = Peek();
                if (state is PlayerTurnFSMStateEngine)
                {
                    return ((PlayerTurnFSMStateEngine)state).NodeHitStrategy;
                }
                // Otherwise, the raycast should always fail.
                return (n) => false;
            }
        }

        /// <summary>
        /// Constructor for when resuming a game.
        /// </summary>
        /// <param name="sacrificeStrategy">The sacrifice strategy for the current ruleset</param>
        /// <param name="nodeMeshViewController">Reference to the scene's Node Mesh View Controller</param>
        /// <param name="inGameGUIManager">Reference to the scene's GUI Manager</param>
        /// <param name="startingPlayer">Index of the player that should start on resuming the game</param>
        /// <param name="players">Array of the Player objects in the game</param>
        public GameFSMEngine(ISacrificeStrategy sacrificeStrategy, TicTacNodeMeshViewController nodeMeshViewController, InGameGUIManager inGameGUIManager, int startingPlayer, params Player[] players)
        {
            m_SacrificeStrategy = sacrificeStrategy;

            m_NodeMeshViewController = nodeMeshViewController;
            m_InGameGUIManager = inGameGUIManager;
            if (players == null)
            {
                TTDebug.LogError("GameFSMEngine.cs : No players were passed in");
                return;
            }
            if (players.Length < 2)
            {
                TTDebug.LogError("GameFSMEngine.cs : You need at least 2 players for a game!");
                return;
            }
            m_NumPlayers = players.Length;
            m_ResumeStartingPlayer = startingPlayer;

            // The equality operation puts only the first player into resume mode.  All other players need to go back into regular mode so they get pieces on their next turn.
            m_Player1State = new PlayerTurnFSMStateEngine(players[0], m_SacrificeStrategy, m_NodeMeshViewController, m_InGameGUIManager, startingPlayer == 1);
            m_Player1State.PlayerTurnEnded += HandlePlayerTurnEnd;
            SetUpPlayerEvents(m_Player1State);
            m_Player2State = new PlayerTurnFSMStateEngine(players[1], m_SacrificeStrategy, m_NodeMeshViewController, m_InGameGUIManager, startingPlayer == 2);
            m_Player2State.PlayerTurnEnded += HandlePlayerTurnEnd;
            SetUpPlayerEvents(m_Player2State);
            if (players.Length > 2)
            {
                m_Player3State = new PlayerTurnFSMStateEngine(players[2], m_SacrificeStrategy, m_NodeMeshViewController, m_InGameGUIManager, startingPlayer == 3);
                m_Player3State.PlayerTurnEnded += HandlePlayerTurnEnd;
                SetUpPlayerEvents(m_Player3State);
            }
            if (players.Length > 3)
            {
                m_Player4State = new PlayerTurnFSMStateEngine(players[3], m_SacrificeStrategy, m_NodeMeshViewController, m_InGameGUIManager, startingPlayer == 4);
                m_Player4State.PlayerTurnEnded += HandlePlayerTurnEnd;
                SetUpPlayerEvents(m_Player4State);
            }
            m_GameEndedState = new GameEndedFSMState(m_InGameGUIManager);

            m_GameEndedState.GameEndedRetryEvent += RetryLevel;
            m_GameEndedState.GameEndedMenuEvent += () => Application.LoadLevel("MainMenu");

            SystemEventCenter.Instance.UpdateEvent += SetResumeState;

            m_InGameMenuState = new InGameMenuFSMState(m_InGameGUIManager);
            m_InGameGUIManager.MenuButtonPressedEvent += () => PushState(m_InGameMenuState);

            SetUpTutorial();
			
			m_InGameMenuState.InGameMenuContinueEvent += () => PopState();
            m_InGameMenuState.InGameMenuRetryEvent += RetryLevel;
            m_InGameMenuState.InGameMenuMainMenuEvent += () => Application.LoadLevel("MainMenu");
            m_InGameMenuState.InGameMenuTutorialEvent += () => PushState(m_TutorialPage1State);
        }

        /// <summary>
        /// Constructor for beginning a new game
        /// </summary>
        /// <param name="sacrificeStrategy">The sacrifice strategy for the current ruleset</param>
        /// <param name="nodeMeshViewController">Reference to the scene's Node Mesh View Controller</param>
        /// <param name="inGameGUIManager">Reference to the scene's GUI Manager</param>
        /// <param name="players">The Player objects for the players in the game</param>
        public GameFSMEngine(ISacrificeStrategy sacrificeStrategy, TicTacNodeMeshViewController nodeMeshViewController, InGameGUIManager inGameGUIManager, params Player[] players)
        {
            m_SacrificeStrategy = sacrificeStrategy;

            m_NodeMeshViewController = nodeMeshViewController;
            m_InGameGUIManager = inGameGUIManager;
            if (players == null)
            {
                TTDebug.LogError("GameFSMEngine.cs : No players were passed in");
                return;
            }
            if (players.Length < 2)
            {
                TTDebug.LogError("GameFSMEngine.cs : You need at least 2 players for a game!");
                return;
            }
            m_NumPlayers = players.Length;

            m_BeforeGameState = new BeforeGameFSMState(m_InGameGUIManager);
            m_BeforeGameState.GameWillBeginEvent += () => SetCurrentPlayer(1);
            m_Player1State = new PlayerTurnFSMStateEngine(players[0], m_SacrificeStrategy, m_NodeMeshViewController, m_InGameGUIManager, false);
            m_Player1State.PlayerTurnEnded += HandlePlayerTurnEnd;
            SetUpPlayerEvents(m_Player1State);
            m_Player2State = new PlayerTurnFSMStateEngine(players[1], m_SacrificeStrategy, m_NodeMeshViewController, m_InGameGUIManager, false);
            m_Player2State.PlayerTurnEnded += HandlePlayerTurnEnd;
            SetUpPlayerEvents(m_Player2State);
            if (players.Length > 2)
            {
                m_Player3State = new PlayerTurnFSMStateEngine(players[2], m_SacrificeStrategy, m_NodeMeshViewController, m_InGameGUIManager, false);
                m_Player3State.PlayerTurnEnded += HandlePlayerTurnEnd;
                SetUpPlayerEvents(m_Player3State);
            }
            if (players.Length > 3)
            {
                m_Player4State = new PlayerTurnFSMStateEngine(players[3], m_SacrificeStrategy, m_NodeMeshViewController, m_InGameGUIManager, false);
                m_Player4State.PlayerTurnEnded += HandlePlayerTurnEnd;
                SetUpPlayerEvents(m_Player4State);
            }
            m_GameEndedState = new GameEndedFSMState(m_InGameGUIManager);

            m_GameEndedState.GameEndedRetryEvent += RetryLevel;
            m_GameEndedState.GameEndedMenuEvent += () => Application.LoadLevel("MainMenu");

            SystemEventCenter.Instance.UpdateEvent += SetStartState;

            m_InGameMenuState = new InGameMenuFSMState(m_InGameGUIManager);
            m_InGameGUIManager.MenuButtonPressedEvent += () => PushState(m_InGameMenuState);

            SetUpTutorial();
			
			m_InGameMenuState.InGameMenuContinueEvent += () => PopState();
            m_InGameMenuState.InGameMenuRetryEvent += RetryLevel;
            m_InGameMenuState.InGameMenuMainMenuEvent += () => Application.LoadLevel("MainMenu");
            m_InGameMenuState.InGameMenuTutorialEvent += () => PushState(m_TutorialPage1State);
        }
		
        /// <summary>
        /// Performs setup of the Tutorial based on current rule set
        /// </summary>
		private void SetUpTutorial()
		{
			string rulesetString = "";
			
            // Grab the correct string for the current ruleset
			switch( GameController.Instance.CurrentRuleSet )
			{
			case "No Sacrifice Limits":
                rulesetString = TutorialPages.NoSacrificeLimits;
                break;
            case "3 Sacrifices Per Game":
               rulesetString = TutorialPages.Sacrifice3Limit;
                break;
            case "MM - Random Defection, Every Turn":
                rulesetString = TutorialPages.MutinyRandomEvery;
                break;
            case "MM - Random Defection, Sacr. Turn Only":
                rulesetString = TutorialPages.MutinyRandomOnly;
                break;
            case "MM - Best Team Defection, Every Turn":
                rulesetString = TutorialPages.MutinyBestEvery;
                break;
            case "MM - Best Team Defection, Sacr. Turn Only":
                rulesetString = TutorialPages.MutinyBestOnly;
                break;
            default:
                TTDebug.LogWarning("GameFSMEngine.cs : Current ruleset string does not match any established ruleset");
                break;
			}
			
			m_TutorialPage1State = new InGameTutorialPageFSMState( m_InGameGUIManager, TutorialPages.Page1Text, "Close", "Next" );
            m_TutorialPage2State = new InGameTutorialPageFSMState(m_InGameGUIManager, TutorialPages.Page2Text, "Previous", "Close", "Next");
            m_TutorialPage3State = new InGameTutorialPageFSMState(m_InGameGUIManager, TutorialPages.Page3Text, "Previous", "Close", "Next");
            m_TutorialPage4State = new InGameTutorialPageFSMState(m_InGameGUIManager, rulesetString, "Previous", "Close");
			
			// m_InGameGUIManager.EnablePanelButtons(false); is called when popping to ensure the panel buttons
			// do not get activated when closing a tutorial page while the menu is still open
            m_TutorialPage1State.ButtonZeroPressedEvent += () => { PopState(); m_InGameGUIManager.EnablePanelButtons(false); };
            m_TutorialPage1State.ButtonOnePressedEvent += () => PushState(m_TutorialPage2State);

            m_TutorialPage2State.ButtonZeroPressedEvent += () => { PopState(); m_InGameGUIManager.EnablePanelButtons(false); };
            m_TutorialPage2State.ButtonOnePressedEvent += () => { PopState(); PopState(); m_InGameGUIManager.EnablePanelButtons(false); };
            m_TutorialPage2State.ButtonTwoPressedEvent += () => PushState(m_TutorialPage3State);

            m_TutorialPage3State.ButtonZeroPressedEvent += () => { PopState(); m_InGameGUIManager.EnablePanelButtons(false); };
            m_TutorialPage3State.ButtonOnePressedEvent += () => { PopState(); PopState(); PopState(); m_InGameGUIManager.EnablePanelButtons(false); };
            m_TutorialPage3State.ButtonTwoPressedEvent += () => PushState(m_TutorialPage4State);

            m_TutorialPage4State.ButtonZeroPressedEvent += () => { PopState(); m_InGameGUIManager.EnablePanelButtons(false); };
            m_TutorialPage4State.ButtonOnePressedEvent += () => { PopState(); PopState(); PopState(); PopState(); m_InGameGUIManager.EnablePanelButtons(false); };
		}

        /// <summary>
        /// Restarts the game with the current rulset and players
        /// </summary>
        private void RetryLevel()
        {
            // Create and setup our scene data transfer object
            var go = new GameObject("Data Transfer");
            var dataTransfer = go.AddComponent<SceneDataTransfer>();

            dataTransfer.SetPlayerName(1, m_Player1State.CurrentPlayer.Name);
            dataTransfer.SetPlayerName(2, m_Player2State.CurrentPlayer.Name);
            if (m_Player3State != null)
                dataTransfer.SetPlayerName(3, m_Player3State.CurrentPlayer.Name);
            if (m_Player4State != null)
                dataTransfer.SetPlayerName(4, m_Player4State.CurrentPlayer.Name);
            dataTransfer.SetRuleset(GameController.Instance.CurrentRuleSet);
			
			dataTransfer.SetMapData( GameController.Instance.CurrentMapName, GameController.Instance.CurrentMapData );

            // We have to remove here, otherwise it stuffs up, and calls the function over and over again
            m_GameEndedState.GameEndedRetryEvent -= RetryLevel;
            
            Application.LoadLevel(Application.loadedLevel);
        }

        /// <summary>
        /// Counter used to count frames when waiting for initialization
        /// </summary>
        private int m_FrameCounter = 0;
        /// <summary>
        /// Index of the player that is resuming
        /// </summary>
        private int m_ResumeStartingPlayer = -1;

        /// <summary>
        /// Sets the Resuming starting player after a couple of frames.  This is hooked into the 
        /// SystemEventCenter Update function when resuming a game.
        /// </summary>
        /// <param name="deltaTime">The delta time since the last frame.</param>
        private void SetResumeState(float deltaTime)
        {
            // If we have waited two frames, set the resuming player's state and deregister from updates
            m_FrameCounter++;
            if (m_FrameCounter >= 2)
            {
                SetCurrentPlayer(m_ResumeStartingPlayer);
                SystemEventCenter.Instance.UpdateEvent -= SetResumeState;
            }
        }

        /// <summary>
        /// Sets the before game state after a couple of frames when starting a new game
        /// </summary>
        /// <param name="deltaTime"></param>
        private void SetStartState(float deltaTime)
        {
            // If we have waited two frames, set the before game state and deregister from updates
            m_FrameCounter++;
            if (m_FrameCounter >= 2)
            {
                NewState(m_BeforeGameState);
                SystemEventCenter.Instance.UpdateEvent -= SetStartState;
            }
        }

        /// <summary>
        /// Hooks up all the events for the given Player FSM State
        /// </summary>
        /// <param name="playerState"></param>
        private void SetUpPlayerEvents(PlayerTurnFSMStateEngine playerState)
        {
            playerState.SetEndTurnButtonActiveEvent += (b) => { if (SetEndTurnButtonActive != null) SetEndTurnButtonActive(b); };
            playerState.SetCancelButtonActiveEvent += (b) => { if (SetCancelButtonActive != null) SetCancelButtonActive(b); };
            playerState.SetSacrificeButtonActiveEvent += (b) => { if (SetSacrificeButtonActive != null) SetSacrificeButtonActive(b); };
            playerState.SetCaptureButtonActiveEvent += (b) => { if (SetCaptureButtonActive != null) SetCaptureButtonActive(b); };
            playerState.SetKingButtonActiveEvent += (b) => { if (SetKingButtonActive != null) SetKingButtonActive(b); };
            playerState.SetUndoButtonActiveEvent += (b) => { if (SetUndoButtonActive != null) SetUndoButtonActive(b); };
            playerState.PlayerCancelledAttackOrFortifyStateEvent += () => { if (PlayerCancelledAttackOrFortifyStateEvent != null) PlayerCancelledAttackOrFortifyStateEvent(); };
            playerState.PlayerBeganSacrificingEvent += () => { if (SacrificeStateBeganEvent != null) SacrificeStateBeganEvent(); };
            playerState.PlayerEndedCapturingEvent += () => { m_SacrificeStrategy.HandleSacrificeAttackCompleted(playerState.CurrentPlayer); if (SacrificeAttackCompletedEvent != null) SacrificeAttackCompletedEvent(); };
            playerState.PlayerEndedFortifyingEvent += () => { m_SacrificeStrategy.HandleSacrificeFortifyCompleted(playerState.CurrentPlayer); if (SacrificeFortifyCompletedEvent != null) SacrificeFortifyCompletedEvent(); };
        }

        /// <summary>
        /// Disables all buttons
        /// </summary>
        private void DisableButtons()
        {
            if (SetEndTurnButtonActive != null)
                SetEndTurnButtonActive(false);
            if (SetCancelButtonActive != null)
                SetCancelButtonActive(false);
            if (SetSacrificeButtonActive != null)
                SetSacrificeButtonActive(false);
            if (SetCaptureButtonActive != null)
                SetCaptureButtonActive(false);
            if (SetKingButtonActive != null)
                SetKingButtonActive(false);
            if (SetUndoButtonActive != null)
                SetUndoButtonActive(false);
        }

        /// <summary>
        /// Sets the Current player to the given player index
        /// </summary>
        /// <param name="playerNumber">Number/Index of the player</param>
        public void SetCurrentPlayer(int playerNumber)
        {
            DisableButtons();

            CurrentPlayerTurn = playerNumber;
            switch (playerNumber)
            {
                case 1:
                    NewState(m_Player1State);
                    if (PlayerTurnStarted != null)
                        PlayerTurnStarted(1);
                    break;

                case 2:
                    NewState(m_Player2State);
                    if (PlayerTurnStarted != null)
                        PlayerTurnStarted(2);
                    break;

                case 3:
                    NewState(m_Player3State);
                    if (PlayerTurnStarted != null)
                        PlayerTurnStarted(3);
                    break;

                case 4:
                    NewState(m_Player4State);
                    if (PlayerTurnStarted != null)
                        PlayerTurnStarted(4);
                    break;

                default:
                    TTDebug.LogError("GameFSMEngine.cs : Invalid player number passed in : " + playerNumber);
                    return;
            }

            // We save the game here as the player will have their updated pieces count.
            if (m_Player4State != null)
                SaveGameToFile.SaveGame(m_Player1State.CurrentPlayer, m_Player2State.CurrentPlayer, m_Player3State.CurrentPlayer, m_Player4State.CurrentPlayer, m_NodeMeshViewController, playerNumber);
            else if (m_Player3State != null)
                SaveGameToFile.SaveGame(m_Player1State.CurrentPlayer, m_Player2State.CurrentPlayer, m_Player3State.CurrentPlayer, null, m_NodeMeshViewController, playerNumber);
            else
                SaveGameToFile.SaveGame(m_Player1State.CurrentPlayer, m_Player2State.CurrentPlayer, null, null, m_NodeMeshViewController, playerNumber);
        }
		
        /// <summary>
        /// Counter for counting turns between Analytics submissions
        /// </summary>
		private int m_TurnCounter;
        /// <summary>
        /// Threshold for submitting Analytics data
        /// </summary>
		private const int AnalyticsTurnThreshold = 5;

        /// <summary>
        /// Handles the end of a Player's turn
        /// </summary>
        private void HandlePlayerTurnEnd()
        {
            // Check for empty pieces
            if (!m_NodeMeshViewController.HasEmptyNodes())
            {
                // Game has ended, so determine the winner!

                // Put the players in a list to sort
                var playerList = new List<Player>();

                // Add the players that are not null
                if (m_Player1State != null)
                    playerList.Add(m_Player1State.CurrentPlayer);
                if (m_Player2State != null)
                    playerList.Add(m_Player2State.CurrentPlayer);
                if (m_Player3State != null)
                    playerList.Add(m_Player3State.CurrentPlayer);
                if (m_Player4State != null)
                    playerList.Add(m_Player4State.CurrentPlayer);

                // Sort the players high to low based on scores
                playerList.Sort(SortPlayerScores);

                string winString = "";

                for (int i = 0; i < playerList.Count; i++)
                {
                    var placeNum = i;
                    bool showEqual = false;

                    // Determine ties - nested so that if third or fourth player ties with first, it will still work properly
                    if (i > 0 && playerList[i].CurrentScore == playerList[i - 1].CurrentScore)
                    {
                        placeNum = i - 1;

                        showEqual = true;

                        if (i > 1 && playerList[i].CurrentScore == playerList[i - 2].CurrentScore)
                        {
                            placeNum = i - 2;

                            if (i > 2 && playerList[i].CurrentScore == playerList[i - 3].CurrentScore)
                            {
                                placeNum = i - 3;
                            }
                        }
                    }

                    // Determine if we should display "Equal Place" on earlier players
                    if (i == 0 && playerList.Count > 1 && playerList[i].CurrentScore == playerList[i + 1].CurrentScore)
                    {
                        showEqual = true;
                    }
                    else if (i == 1 && playerList.Count > 2 && playerList[i].CurrentScore == playerList[i + 1].CurrentScore)
                    {
                        showEqual = true;
                    }
                    else if (i == 2 && playerList.Count > 3 && playerList[i].CurrentScore == playerList[i + 1].CurrentScore)
                    {
                        showEqual = true;
                    }

                    if (showEqual)
                    {
                        winString += "Equal ";
                    }

                    switch (placeNum)
                    {
                        case 0:
                            winString += "First Place: ";
                            break;

                        case 1:
                            winString += "Second Place: ";
                            break;

                        case 2:
                            winString += "Third Place: ";
                            break;

                        case 3:
                            winString += "Fourth Place: ";
                            break;
                    }

                    winString += playerList[i].Name + " with " + playerList[i].CurrentScore + " points!\n";
                }

                NewState(m_GameEndedState, winString);

                // Delete the data file
                SaveGameToFile.DestroyAutosaveFile();
            }
            else
            {
                // Fire the turn ended function in our sacrifice strategy
                switch (CurrentPlayerTurn)
                {
                    case 1:
                        m_SacrificeStrategy.HandleTurnEnded(m_Player1State.CurrentPlayer);
                        break;

                    case 2:
                        m_SacrificeStrategy.HandleTurnEnded(m_Player2State.CurrentPlayer);
                        break;

                    case 3:
                        m_SacrificeStrategy.HandleTurnEnded(m_Player3State.CurrentPlayer);
                        break;

                    case 4:
                        m_SacrificeStrategy.HandleTurnEnded(m_Player4State.CurrentPlayer);
                        break;
                }
                var nextPlayer = CurrentPlayerTurn + 1;
                if (nextPlayer > m_NumPlayers)
                    nextPlayer = 1;
                SetCurrentPlayer(nextPlayer);
				
                // If we have hit our threshold, submit data to analytics
				m_TurnCounter += 1;
				if(m_TurnCounter >= AnalyticsTurnThreshold)
				{
					m_TurnCounter = 0;
					TTTAnalytics.Instance.CurrentGameSession.PlayerOneScore = m_Player1State.CurrentPlayer.CurrentScore;
					TTTAnalytics.Instance.CurrentGameSession.PlayerTwoScore = m_Player2State.CurrentPlayer.CurrentScore;
					if(m_Player3State != null)
						TTTAnalytics.Instance.CurrentGameSession.PlayerThreeScore = m_Player3State.CurrentPlayer.CurrentScore;
					if(m_Player4State != null)
						TTTAnalytics.Instance.CurrentGameSession.PlayerFourScore = m_Player4State.CurrentPlayer.CurrentScore;
					((IParseBehaviour)TTTAnalytics.Instance.CurrentGameSession).UpdateOnServer();
				}
            }
        }

        /// <summary>
        /// Sort players based on score, high to low.  Used as sorting perdicate.
        /// </summary>
        /// <param name="p1">First player</param>
        /// <param name="p2">Second Player</param>
        /// <returns>Result of comparison</returns>
        private int SortPlayerScores(Player p1, Player p2)
        {
            if (p1.CurrentScore > p2.CurrentScore)
            {
                return -1;
            }
            if (p1.CurrentScore < p2.CurrentScore)
            {
                return 1;
            }
            // They must be equal
            return 0;
        }
    }
}