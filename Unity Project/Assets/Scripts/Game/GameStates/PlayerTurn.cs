﻿using TransTech.Delegates;
using TransTech.Delegates.TicTac;
using TransTech.FiniteStateMachine;
using TransTech.Gameplay.Players;
using TransTech.Nodes;

namespace TransTech.Gameplay.States
{
    public class PlayerTurn : HFSMState
	{
        /// <summary>
        /// Reference to the Player this state handles the turn cycle of
        /// </summary>
        public Player CurrentPlayer { get; private set; }

        /// <summary>
        /// Number (index) of the player in the game.
        /// </summary>
        public int PlayerNumber { get { return CurrentPlayer != null ? CurrentPlayer.PlayerNumber : -1; } }

        /// <summary>
        /// The Sacrifice Strategy object to use for the current ruleset
        /// </summary>
        protected ISacrificeStrategy m_SacrificeStratgey;

        /// <summary>
        /// Reference to the GUI Manager in the scene
        /// </summary>
        protected InGameGUIManager m_GUIManager;

        /// <summary>
        /// Reference to the Node Mesh View Controller in the scene.
        /// </summary>
        protected TicTacNodeMeshViewController m_NodeMeshViewController;

	    public virtual NodeTestDelegate NodeHitStrategy
	    {
	        get { return DefaultNodeTest; }
	    }

        private bool DefaultNodeTest(Node n)
        {
            return false;
        }

        /// <summary>
        /// Is this turn resuming from a save game (don't apply any pieces)
        /// </summary>
        protected bool m_IsResuming;
        /// <summary>
        /// Is this the player's first turn?
        /// </summary>
        private bool m_FirstTurn;

        public PlayerTurn(Player player, ISacrificeStrategy sacrificeStrategy, TicTacNodeMeshViewController nodeMeshViewController, InGameGUIManager inGameGUIManager, bool isResuming)
        {
            CurrentPlayer = player;
            m_NodeMeshViewController = nodeMeshViewController;
            m_GUIManager = inGameGUIManager;
            m_FirstTurn = player.TurnsCompleted == 0;
            m_IsResuming = isResuming;
            m_SacrificeStratgey = sacrificeStrategy;
        }

        public override void Enter(params object[] args)
        {
            base.Enter(args);

            if (m_IsResuming)
            {
                // Don't add any tokens on first turn if resuming a game.
                m_IsResuming = false;
            }
            else
            {
                CurrentPlayer.AddTokensToHand(m_FirstTurn ? 3 : 2);
            }
            m_FirstTurn = false;
        }
	}
}
