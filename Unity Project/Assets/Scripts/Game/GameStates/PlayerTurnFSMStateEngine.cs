using TransTech.Delegates;
using TransTech.Delegates.TicTac;
using TransTech.FiniteStateMachine;
using TransTech.Gameplay.Players;

namespace TransTech.Gameplay.States
{
    /// <summary>
    /// Hierarchical FSM Engine / State for a Player's turn.
    /// Handles the state cycle for a player's turn
    /// </summary>
    public class PlayerTurnFSMStateEngine : PlayerTurn, IPlayerTurnEvents
    {
        /// <summary>
        /// Event fired when the player's turn ends
        /// </summary>
        public event VoidDelegate PlayerTurnEnded;

        /// <summary>
        /// Event fired when the player begin's the sacrificing state
        /// </summary>
        public event VoidDelegate PlayerBeganSacrificingEvent;

        /// <summary>
        /// Event fired when the player Fortify's a position
        /// </summary>
        public event VoidDelegate PlayerEndedFortifyingEvent;

        /// <summary>
        /// Event fired when the player Captures/Attacks a position
        /// </summary>
        public event VoidDelegate PlayerEndedCapturingEvent;

        /// <summary>
        /// Event fired when the cancel button is pressed during ether the attack/capture or fortify/king state
        /// </summary>
        public event VoidDelegate PlayerCancelledAttackOrFortifyStateEvent;

        /// <summary>
        /// Event fired when the end turn button changes active state
        /// </summary>
        public event BoolDelegate SetEndTurnButtonActiveEvent;
        /// <summary>
        /// Event fired when the cancel button changes active state
        /// </summary>
        public event BoolDelegate SetCancelButtonActiveEvent;
        /// <summary>
        /// Event fired when the sacrifice button changes active state
        /// </summary>
        public event BoolDelegate SetSacrificeButtonActiveEvent;
        /// <summary>
        /// Event fired when the capture(attack) button changes active state
        /// </summary>
        public event BoolDelegate SetCaptureButtonActiveEvent;
        /// <summary>
        /// Event fired when the king(fortify) button changes active state
        /// </summary>
        public event BoolDelegate SetKingButtonActiveEvent;
        /// <summary>
        /// Event fired when the Undo button changes active state
        /// </summary>
        public event BoolDelegate SetUndoButtonActiveEvent;

        /// <summary>
        /// State for the start of a player's turn
        /// </summary>
        private PlayerTurnStartFSMState m_PlayerTurnStartState;

        /// <summary>
        /// State for the placement of pieces from the player's 'hand'
        /// </summary>
        private RegularPlacementFSMState m_RegularPlacementState;

        /// <summary>
        /// State for when the player is sacrificing pieces
        /// </summary>
        private SacraficeFSMState m_SacraficeState;

        /// <summary>
        /// State for a popup to confirm whether the player will sacrifice pieces or not
        /// </summary>
        private SacrificeConfirmationFSMState m_SacrificeConfirmationState;

        /// <summary>
        /// State for when the player is 'kinging' or 'fortifying' pieces.
        /// </summary>
        private KingPlacementFSMState m_KingPlacementState;

        /// <summary>
        /// State for when the player capturing an opponents piece
        /// </summary>
        private CapturePlacementFSMState m_CapturePlacementState;

        /// <summary>
        /// State for a popup to confirm the end of a game.
        /// </summary>
        private EndGameConfirmationFSMState m_EndGameConfirmationState;

        /// <summary>
        /// Idle state for when it is not this player's turn
        /// </summary>
        private IdleFSMState m_IdleState;

       
        /// <summary>
        /// Enumeration of capture/king buttons (for easier to read code)
        /// </summary>
        private enum LastButtonPressed
        {
            None,
            Capture,
            King
        }

        /// <summary>
        /// The last button pressed (out of capture(attack) or king(fortify))
        /// </summary>
        private LastButtonPressed m_LastButtonPressed = LastButtonPressed.None;

        /// <summary>
        /// Returns the Node Hit Strategy for the current Player State
        /// </summary>
        public override NodeTestDelegate NodeHitStrategy
        {
            get
            {
                var state = (IPlayerTurnState)Peek();
                return state.NodeHitStrategy;
            }
        }

        /// <summary>
        /// Is this the player's first turn?
        /// </summary>
        private bool m_FirstTurn;
        
        /// <summary>
        /// Constructor for initializing the Player FSM engine
        /// </summary>
        /// <param name="player">The Player belonging to the FSM</param>
        /// <param name="sacrificeStrategy">The sacrifice strategy to use for the ruleset</param>
        /// <param name="nodeMeshViewController">Reference to the Node Mesh View Controller in the scene</param>
        /// <param name="inGameGUIManager">Reference to the GUI Manager in the scene</param>
        /// <param name="isResuming">Is this player resuming their first turn from a save game?</param>
        public PlayerTurnFSMStateEngine(Player player, ISacrificeStrategy sacrificeStrategy, TicTacNodeMeshViewController nodeMeshViewController, InGameGUIManager inGameGUIManager, bool isResuming) 
            : base(player, sacrificeStrategy, nodeMeshViewController, inGameGUIManager, isResuming)
        {
            // Create and initialize all of our internal states
            m_PlayerTurnStartState = new PlayerTurnStartFSMState(m_GUIManager, CurrentPlayer.Name);
            m_RegularPlacementState = new RegularPlacementFSMState(player);
            m_SacraficeState = new SacraficeFSMState(player, m_NodeMeshViewController, m_GUIManager);
            m_SacrificeConfirmationState = new SacrificeConfirmationFSMState(m_GUIManager);
            m_SacrificeConfirmationState.YesButtonPressed += HandleSacraficeYesButtonPressed;
            m_SacrificeConfirmationState.NoButtonPressed += HandleSacrificeNoButtonPressed;
            m_KingPlacementState = new KingPlacementFSMState(player, m_NodeMeshViewController);
            m_CapturePlacementState = new CapturePlacementFSMState(player, m_NodeMeshViewController);
            m_EndGameConfirmationState = new EndGameConfirmationFSMState(m_GUIManager);
            m_EndGameConfirmationState.YesButtonPressed += HandleEndGameYesButtonPressed;
            m_EndGameConfirmationState.NoButtonPressed += HandleEndGameNoButtonPressed;
            m_IdleState = new IdleFSMState();

            // Set ourselves as Idle
            NewState(m_IdleState);
        }

        /// <summary>
        /// Activates or deactives the buttons in the side panel based on bool value
        /// </summary>
        /// <param name="endTurn">Bool for End Turn button</param>
        /// <param name="cancel">Bool for cancel button</param>
        /// <param name="sacrifice">Bool for Sacrifice button</param>
        /// <param name="capture">Bool for Capture(attack) button</param>
        /// <param name="king">Bool for king(fortify) button</param>
        /// <param name="undo">Bool for Undo button</param>
        protected void SetButtonsActive(bool endTurn, bool cancel, bool sacrifice, bool capture, bool king, bool undo)
        {
            if (SetEndTurnButtonActiveEvent != null)
                SetEndTurnButtonActiveEvent(endTurn);
            if (SetCancelButtonActiveEvent != null)
                SetCancelButtonActiveEvent(cancel);
            if (SetSacrificeButtonActiveEvent != null)
                SetSacrificeButtonActiveEvent(sacrifice);
            if (SetCaptureButtonActiveEvent != null)
                SetCaptureButtonActiveEvent(capture);
            if (SetKingButtonActiveEvent != null)
                SetKingButtonActiveEvent(king);
            if (SetUndoButtonActiveEvent != null)
                SetUndoButtonActiveEvent(undo);
        }

        /// <summary>
        /// Called by our parent FSM when entering the state
        /// </summary>
        /// <param name="args"></param>
        public override void Enter(params object[] args)
        {
            base.Enter(args);
            
            // Hook up all our event handlers
            m_GUIManager.EndTurnButtonPressedEvent += HandleEndTurnButtonPressed;
            m_GUIManager.SacraficeButtonPressedEvent += HandleSacraficeButtonPressed;
            m_GUIManager.CaptureButtonPressedEvent += HandleCaptureButtonPressed;
            m_GUIManager.KingButtonPressedEvent += HandleKingButtonPressed;
            m_GUIManager.CancelButtonPressedEvent += HandleCancelButtonPressed;
            // Set our internal state to the start state
            NewState(m_PlayerTurnStartState);
            m_PlayerTurnStartState.PlayerTurnWillBeginEvent += () => { NewState(m_RegularPlacementState); SetButtonsActive(true, false, true, false, false, true); };
            m_SacrificeStratgey.HandleTurnStarted(CurrentPlayer);
        }

        /// <summary>
        /// Handles the Confirmation of sacrifice button being pressed
        /// </summary>
        private void HandleSacraficeYesButtonPressed()
        {
            // If the player chose capture (attack)
            if (m_LastButtonPressed == LastButtonPressed.Capture)
            {
                m_SacraficeState.SacraficeCurrent();
                m_CapturePlacementState.BoardPieceCapturedEvent += PieceCaptured;
                NewState(m_CapturePlacementState);

                SetButtonsActive(false, true, false, false, false, false);
            }
            // If the player chose King (fortify)
            else if (m_LastButtonPressed == LastButtonPressed.King)
            {
                m_SacraficeState.SacraficeCurrent();
                m_KingPlacementState.KingPiecePlacedEvent += HandleKingPiecePlaced;
                NewState(m_KingPlacementState);

                SetButtonsActive(false, true, false, false, false, false);
            }
            m_LastButtonPressed = LastButtonPressed.None;
        }

        /// <summary>
        /// Handles the Dismissal of sacrifice button being pressed
        /// </summary>
        private void HandleSacrificeNoButtonPressed()
        {
            m_LastButtonPressed = LastButtonPressed.None;
            NewState(m_RegularPlacementState);

            SetButtonsActive(true, false, true, false, false, true);
        }

        /// <summary>
        /// Handles the end of turn button being pressed
        /// </summary>
        private void HandleEndTurnButtonPressed()
        {
            // If there are empty nodes, we complete our turn as usual
            if (m_NodeMeshViewController.HasEmptyNodes())
            {
				CurrentPlayer.CompletedTurn();
                if (PlayerTurnEnded != null)
                    PlayerTurnEnded();
            }
            else // Otherwise it is the end of the game!
            {
                NewState(m_EndGameConfirmationState);
                SetButtonsActive(false, false, false, false, false, false);
            }
        }

        /// <summary>
        /// Handles the Sacrifice button in the left panel being pressed
        /// </summary>
        private void HandleSacraficeButtonPressed()
        {
            if (Peek() == m_RegularPlacementState)
            {
                // Check the strategy to see if we are allowed to sacrifice
                if (m_SacrificeStratgey.CanPlayerSacrifice(CurrentPlayer))
                {
                    NewState(m_SacraficeState, m_NodeMeshViewController.CurrentNodeMesh);
                    SetButtonsActive(false, true, false, true, true, true);
                    if (PlayerBeganSacrificingEvent != null)
                        PlayerBeganSacrificingEvent();
                }
            }
        }

        /// <summary>
        /// Handles the Capture(attack) button in the left panel being pressed
        /// </summary>
        private void HandleCaptureButtonPressed()
        {
            if (Peek() == m_SacraficeState && m_SacraficeState.ReadyToSacrafice)
            {
                m_LastButtonPressed = LastButtonPressed.Capture;
                PushState(m_SacrificeConfirmationState);
                SetButtonsActive(false, false, false, false, false, false);
            }
        }

        /// <summary>
        /// Handles the end game confirmation button being pressed
        /// </summary>
        private void HandleEndGameYesButtonPressed()
        {
            if (Peek() == m_EndGameConfirmationState)
            {
                if (PlayerTurnEnded != null)
                    PlayerTurnEnded();
            }
        }

        /// <summary>
        /// Handles the end game dismissal button being pressed
        /// </summary>
        private void HandleEndGameNoButtonPressed()
        {
            if (Peek() == m_EndGameConfirmationState)
            {
                NewState(m_RegularPlacementState);
                SetButtonsActive(true, false, true, false, false, true);
            }
        }

        /// <summary>
        /// Handles a piece being captured
        /// </summary>
        private void PieceCaptured()
        {
            if (Peek() == m_CapturePlacementState)
            {
                // Increment the current player's sacrifice count for analytics
				switch(CurrentPlayer.PlayerNumber)
				{
				case 1:
					TTTAnalytics.Instance.CurrentGameSession.PlayerOneSacrifices += 1;
					break;
				case 2:
					TTTAnalytics.Instance.CurrentGameSession.PlayerTwoSacrifices += 1;
					break;
				case 3:
					TTTAnalytics.Instance.CurrentGameSession.PlayerThreeSacrifices += 1;
					break;
				case 4:
					TTTAnalytics.Instance.CurrentGameSession.PlayerFourSacrifices += 1;
					break;
				}
                m_CapturePlacementState.BoardPieceCapturedEvent -= PieceCaptured;
                NewState(m_RegularPlacementState);

                SetButtonsActive(true, false, true, false, false, true);
                if (PlayerEndedCapturingEvent != null)
                    PlayerEndedCapturingEvent();
            }
        }

        /// <summary>
        /// Handles the King (fortify) button being pressed
        /// </summary>
        private void HandleKingButtonPressed()
        {
            if (Peek() == m_SacraficeState && m_SacraficeState.ReadyToSacrafice)
            {
                m_LastButtonPressed = LastButtonPressed.King;
                PushState(m_SacrificeConfirmationState);
                SetButtonsActive(false, false, false, false, false, false);
            }
        }

        /// <summary>
        /// Handles a King (fortify) piece being placed
        /// </summary>
        private void HandleKingPiecePlaced()
        {
            if (Peek() == m_KingPlacementState)
            {
                // Increment the current player's fortify count for analytics
				switch(CurrentPlayer.PlayerNumber)
				{
				case 1:
					TTTAnalytics.Instance.CurrentGameSession.PlayerOneFortifies += 1;
					break;
				case 2:
					TTTAnalytics.Instance.CurrentGameSession.PlayerTwoFortifies += 1;
					break;
				case 3:
					TTTAnalytics.Instance.CurrentGameSession.PlayerThreeFortifies += 1;
					break;
				case 4:
					TTTAnalytics.Instance.CurrentGameSession.PlayerFourFortifies += 1;
					break;
				}
                m_KingPlacementState.KingPiecePlacedEvent -= HandleKingPiecePlaced;
                NewState(m_RegularPlacementState);

                SetButtonsActive(true, false, true, false, false, true);
                if (PlayerEndedFortifyingEvent != null)
                    PlayerEndedFortifyingEvent();
            }
        }

        /// <summary>
        /// Handle the Cancel button being pressed
        /// </summary>
        private void HandleCancelButtonPressed()
        {
            // If we're in the sacrifice state
            if (Peek() == m_SacraficeState)
            {
                // Just go back to the regular state
                NewState(m_RegularPlacementState);
                SetButtonsActive(true, false, true, false, false, true);
            }
            // Else if we're in the capture or king states
            else if (Peek() == m_CapturePlacementState || Peek() == m_KingPlacementState)
            {
                NewState(m_RegularPlacementState);
                SetButtonsActive(true, false, true, false, false, true);
                // We need to fire the cancelled event
                if (PlayerCancelledAttackOrFortifyStateEvent != null)
                    PlayerCancelledAttackOrFortifyStateEvent();
            }
        }

        /// <summary>
        /// Called by the parent FSM on exiting the state
        /// </summary>
        public override void Exit()
        {
            // Unhook our event handlers
            m_GUIManager.EndTurnButtonPressedEvent -= HandleEndTurnButtonPressed;
            m_GUIManager.SacraficeButtonPressedEvent -= HandleSacraficeButtonPressed;
            m_GUIManager.CaptureButtonPressedEvent -= HandleCaptureButtonPressed;
            m_GUIManager.KingButtonPressedEvent -= HandleKingButtonPressed;
            m_GUIManager.CancelButtonPressedEvent -= HandleCancelButtonPressed;
            // Go back to an idle state
            NewState(m_IdleState);
        }
    }
}