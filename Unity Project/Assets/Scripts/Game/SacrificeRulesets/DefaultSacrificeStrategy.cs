using TransTech.Gameplay.Players;
using TransTech.System.Debug;
using UnityEngine;

namespace TransTech.Gameplay
{
    /// <summary>
    /// Default strategy, where the player can always sacrifice
    /// </summary>
    public class DefaultSacrificeStrategy : ISacrificeStrategy
    {
        /// <summary>
        /// Basic description of the Strategy
        /// </summary>
        public string StrategyDescription { get { return "No Sacrifice Limits"; } }

        /// <summary>
        /// Reference to the Player Status Panel prefab used for this strategy
        /// </summary>
        private GameObject m_PlayerStatusPanelPrefab;

        /// <summary>
        /// Reference to the Player Status Panel prefab used for this strategy
        /// </summary>
        public GameObject PlayerStatusPanelPrefab
        {
            get
            {
                // If we haven't loaded the prefab yet
                if (m_PlayerStatusPanelPrefab == null)
                {
                    // Load the prefab from resources
                    m_PlayerStatusPanelPrefab = Resources.Load("UI/PlayerPanels/PlayerInfoPanel") as GameObject;
                    if (m_PlayerStatusPanelPrefab == null)
                        TTDebug.LogError("DefaultSecrificeStrategy.cs : Unable to load Player Panel Prefab.  Check Resources folder to make sure it hasn't been moved.");
                }
                return m_PlayerStatusPanelPrefab;
            }
        }

        /// <summary>
        /// Can the Player sacrifice?
        /// </summary>
        /// <param name="p">The player to check</param>
        /// <returns>True for this strategy (players can always sacrifice)</returns>
        public bool CanPlayerSacrifice(Player p)
        {
            return true;
        }

        /// <summary>
        /// Handles the end of turn event (Does nothing for this strategy)
        /// </summary>
        /// <param name="p">The Player who's turn ended</param>
        public void HandleTurnEnded(Player p)
        {
        }

        /// <summary>
        /// Handles A sacrifice being completed (Does nothing for this strategy)
        /// </summary>
        /// <param name="p">The Player that performed the Sacrifice</param>
        public void HandleSacrificeAttackCompleted(Player p)
        {
        }

        /// <summary>
        /// Handles a Sacrifice being undone (Does nothing for this strategy)
        /// </summary>
        /// <param name="p">The player that undid their sacrifice</param>
        public void HandleSacrificeAttackUndone(Player p)
        {
        }

        /// <summary>
        /// Handles a Player starting their turn
        /// </summary>
        /// <param name="p">The Player that started their turn</param>
        public void HandleTurnStarted(Player p)
        {
        }

        /// <summary>
        /// Handles a sacrifice and fortify being completed (Does nothing for this strategy)
        /// </summary>
        /// <param name="p">The player completing the Fortify</param>
        public void HandleSacrificeFortifyCompleted(Player p)
        {
        }

        /// <summary>
        /// Handles a sacrifice and fortify being undone (Does nothing for this strategy)
        /// </summary>
        /// <param name="p">The player undoing the Fortify</param>
        public void HandleSacrificeFortifyUndone(Player p)
        {
        }
    }
}