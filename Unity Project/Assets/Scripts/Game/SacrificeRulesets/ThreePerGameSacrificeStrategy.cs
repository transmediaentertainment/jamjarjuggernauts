using TransTech.Gameplay.Players;
using TransTech.System.Debug;
using UnityEngine;

namespace TransTech.Gameplay
{
    /// <summary>
    /// Sacrifice Strategy rule set for limiting players to 3 sacrifices per game.
    /// </summary>
    public class ThreePerGameSacrificeStrategy : ISacrificeStrategy
    {
        /// <summary>
        /// Basic description of the Strategy
        /// </summary>
        public string StrategyDescription { get { return "3 Sacrifices Per Game"; } }

        /// <summary>
        /// Reference to the Player Status Panel prefab used for this strategy
        /// </summary>
        private GameObject m_PlayerStatusPanelPrefab;

        /// <summary>
        /// Reference to the Player Status Panel prefab used for this strategy
        /// </summary>
        public GameObject PlayerStatusPanelPrefab
        {
            get
            {
                // If we haven't loaded the prefab yet
                if (m_PlayerStatusPanelPrefab == null)
                {
                    // Load the prefab from resources
                    m_PlayerStatusPanelPrefab = Resources.Load("UI/PlayerPanels/PlayerInfoPanel3SacrificesMode") as GameObject;
                    if (m_PlayerStatusPanelPrefab == null)
                        TTDebug.LogError("ThreePerGameSacrificeStrategy.cs : Unable to load Player Panel Prefab.  Check Resources folder to make sure it hasn't been moved.");
                }
                return m_PlayerStatusPanelPrefab;
            }
        }

        /// <summary>
        /// Number of Sacrifices Player One has left
        /// </summary>
        public int PlayerOneCount { get; private set; }

        /// <summary>
        /// Number of Sacrifices Player Two has left
        /// </summary>
        public int PlayerTwoCount { get; private set; }

        /// <summary>
        /// Number of Sacrifices Player Three has left
        /// </summary>
        public int PlayerThreeCount { get; private set; }

        /// <summary>
        /// Number of Sacrifices Player Four has left
        /// </summary>
        public int PlayerFourCount { get; private set; }

        /// <summary>
        /// Constructor for setting up initial values
        /// </summary>
        public ThreePerGameSacrificeStrategy()
        {
            PlayerOneCount = 3;
            PlayerTwoCount = 3;
            PlayerThreeCount = 3;
            PlayerFourCount = 3;
        }

        /// <summary>
        /// Determines whether a player is allowed to sacrifice this turn or not.
        /// </summary>
        /// <param name="p">The Player to test</param>
        /// <returns>Returns true if the given player has any sacrifices left, otherwise false.</returns>
        public bool CanPlayerSacrifice(Player p)
        {
            switch (p.PlayerNumber)
            {
                case 1:
                    return PlayerOneCount > 0;
                case 2:
                    return PlayerTwoCount > 0;
                case 3:
                    return PlayerThreeCount > 0;
                case 4:
                    return PlayerFourCount > 0;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Handles a Player starting their turn
        /// </summary>
        /// <param name="p">The Player that started their turn</param>
        public void HandleTurnStarted(Player p)
        {
        }

        /// <summary>
        /// Handles a Player ending their turn
        /// </summary>
        /// <param name="p">The player that ended their turn</param>
        public void HandleTurnEnded(Player p)
        {
        }

        /// <summary>
        /// Removes one from the given Player's count on successful sacrifice
        /// </summary>
        /// <param name="p">The Player to remove the count from.</param>
        public void HandleSacrificeAttackCompleted(Player p)
        {
            UpdatePlayerCount(p, true);
        }

        /// <summary>
        /// Returns one to the count of the given player on an Undo Sacrifice event
        /// </summary>
        /// <param name="p">The Player to return the sacrifice to</param>
        public void HandleSacrificeAttackUndone(Player p)
        {
            UpdatePlayerCount(p, false);
        }

        /// <summary>
        /// Updates the player count by increasing or decreasing the count.
        /// </summary>
        /// <param name="p">The player to alter the count for</param>
        /// <param name="decrease">Should we decrease or increase</param>
        private void UpdatePlayerCount(Player p, bool decrease)
        {
            switch (p.PlayerNumber)
            {
                case 1:
                    PlayerOneCount += decrease ? -1 : 1;
                    break;

                case 2:
                    PlayerTwoCount += decrease ? -1 : 1;
                    break;

                case 3:
                    PlayerThreeCount += decrease ? -1 : 1;
                    break;

                case 4:
                    PlayerFourCount += decrease ? -1 : 1;
                    break;
            }
            LogCounts();
        }

        /// <summary>
        /// Prints the count for each player to the Debug Log.  Will be removed once UI has been setup.
        /// </summary>
        private void LogCounts()
        {
            TransTech.System.Debug.TTDebug.Log("P1 : " + PlayerOneCount + "\nP2 : " + PlayerTwoCount + "\nP3 : " + PlayerThreeCount + "\nP4 : " + PlayerFourCount);
        }

        /// <summary>
        /// Handles a sacrifice and fortify being completed
        /// </summary>
        /// <param name="p">The player completing the Fortify</param>
        public void HandleSacrificeFortifyCompleted(Player p)
        {
            UpdatePlayerCount(p, true);
        }

        /// <summary>
        /// Handles a sacrifice and fortify being undone
        /// </summary>
        /// <param name="p">The player undoing the Fortify</param>
        public void HandleSacrificeFortifyUndone(Player p)
        {
            UpdatePlayerCount(p, false);
        }
    }
}