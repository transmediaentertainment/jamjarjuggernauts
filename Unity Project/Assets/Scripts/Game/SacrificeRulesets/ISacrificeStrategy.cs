using TransTech.Gameplay.Players;
using UnityEngine;

namespace TransTech.Gameplay
{
    /// <summary>
    /// Interface for defining sacrifice strategy rulesets
    /// </summary>
    public interface ISacrificeStrategy
    {
        /// <summary>
        /// Basic Description of the strategy
        /// </summary>
        string StrategyDescription { get; }
        /// <summary>
        /// Prefab for the Player Status Panel used by this strategy
        /// </summary>
        GameObject PlayerStatusPanelPrefab { get; }
        /// <summary>
        /// Can the provided player perform a sacrifice?
        /// </summary>
        /// <param name="p">The player to check</param>
        /// <returns>True if they can sacrifice, false if they can't</returns>
        bool CanPlayerSacrifice(Player p);
        /// <summary>
        /// Called at the start of a turn for the given player
        /// </summary>
        /// <param name="p">The player to prep for the turn</param>
        void HandleTurnStarted(Player p);
        /// <summary>
        /// Called at the end of a turn for the given player
        /// </summary>
        /// <param name="p">The player to prep for the end of turn</param>
        void HandleTurnEnded(Player p);
        /// <summary>
        /// Handles a Sacrifice and Fortify action being completed
        /// </summary>
        /// <param name="p">The player that finished the fortify</param>
        void HandleSacrificeFortifyCompleted(Player p);
        /// <summary>
        /// Handles a sacrifice and fortify action being undone
        /// </summary>
        /// <param name="p">The player that undid the fortify</param>
        void HandleSacrificeFortifyUndone(Player p);
        /// <summary>
        /// Handles a sacrifice and attack action being completed
        /// </summary>
        /// <param name="p">The player that finished the attack</param>
        void HandleSacrificeAttackCompleted(Player p);
        /// <summary>
        /// Handles a sacrifice and attack action being undone
        /// </summary>
        /// <param name="p">The player that undid the attack</param>
        void HandleSacrificeAttackUndone(Player p);
    }
}