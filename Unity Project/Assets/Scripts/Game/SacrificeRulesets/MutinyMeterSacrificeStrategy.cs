using System.Collections.Generic;
using TransTech.Gameplay.Players;
using TransTech.System.Debug;
using UnityEngine;

namespace TransTech.Gameplay
{
    /// <summary>
    /// Strategy for the Mutiny Meter sacrficing ruleset.  Handles a couple of different configurations.
    /// </summary>
    public class MutinyMeterSacrificeStrategy : ISacrificeStrategy
    {
        /// <summary>
        /// Basic description of the Strategy
        /// </summary>
        public string StrategyDescription { get; private set; }

        /// <summary>
        /// Reference to the Player Status Panel prefab used for this strategy
        /// </summary>
        private GameObject m_PlayerStatusPanelPrefab;

        /// <summary>
        /// Reference to the Player Status Panel prefab used for this strategy
        /// </summary>
        public GameObject PlayerStatusPanelPrefab
        {
            get
            {
                // If we haven't loaded the prefab yet
                if (m_PlayerStatusPanelPrefab == null)
                {
                    // Load the prefab from resources
                    m_PlayerStatusPanelPrefab = Resources.Load("UI/PlayerPanels/PlayerInfoPanelMutinyMode") as GameObject;
                    if (m_PlayerStatusPanelPrefab == null)
                        TTDebug.LogError("MutinyMeterSacrificeStrategy.cs : Unable to load Mutiny Meter Player Panel Prefab.  Check Resources folder to make sure it hasn't been moved.");
                }
                return m_PlayerStatusPanelPrefab;
            }
        }

        /// <summary>
        /// Player 1's Mutiny Meter value
        /// </summary>
        public float PlayerOneMeter { get; private set; }
        /// <summary>
        /// Player 2's Mutiny Meter value
        /// </summary>
        public float PlayerTwoMeter { get; private set; }
        /// <summary>
        /// Player 3's Mutiny Meter value
        /// </summary>
        public float PlayerThreeMeter { get; private set; }
        /// <summary>
        /// Player 4's Mutiny Meter value
        /// </summary>
        public float PlayerFourMeter { get; private set; }

        /// <summary>
        /// List of Meter values over time for the current player
        /// </summary>
        private readonly List<float> m_CurrentPlayerMeterHistory = new List<float>();
        /// <summary>
        /// Current player's sacrifice count.
        /// </summary>
        private int m_CurrentPlayerSacrificeCount;

        /// <summary>
        /// Amount to increment the meter when an attack is performed
        /// </summary>
        private const float MeterAttackIncrement = 0.2f;
        /// <summary>
        /// Amount to decrement the meter when a fortify is performed
        /// </summary>
        private const float MeterFortifyDecrement = 0.2f;
        /// <summary>
        /// Amount to decay the meter at the end of a Player's turn
        /// </summary>
        private const float MeterDeacy = 0.05f;

        /// <summary>
        /// Minimum chance of a mutiny happening
        /// </summary>
        private const float MinRandomChance = 0.05f;
        /// <summary>
        /// Maximum chance of a mutiny happening
        /// </summary>
        private const float MaxRandomChance = 0.85f;
        /// <summary>
        /// The end of the meter area that the chance is clamped to 0% (0 -> this value)
        /// </summary>
        private const float ZeroZoneEnd = 0.1f;
        /// <summary>
        /// The start of the meter area that the chance it clamped to the Max random chance (this value -> 1)
        /// </summary>
        private const float MaxZoneStart = 0.9f;

        /// <summary>
        /// Reference to the Node Mesh View Controller
        /// </summary>
        private TicTacNodeMeshViewController m_NodeMeshViewController;
        /// <summary>
        /// Reference to the GUI Manager
        /// </summary>
        private InGameGUIManager m_GUIManager;

        /// <summary>
        /// Do we test for Mutiny every turn, or just on the sacrifice turn
        /// </summary>
        private bool m_MutinyEveryTurn;

        /// <summary>
        /// Do we pick a random team to Mutiny to, or the team with the lowest Meter
        /// </summary>
        private bool m_PickRandomTeam;

        /// <summary>
        /// Constructor for Initialization
        /// </summary>
        /// <param name="mutinyEveryTurn">Should there be a possibility of a mutiny at the end of every turn, or just the turn a sacrifice occurs on</param>
        /// <param name="pickRandomTeam">Should a piece mutiny to a random team, or should it select the 'happiest' (lowest meter) team</param>
        /// <param name="nodeMeshViewController">Reference to the scene' Node Mesh View Controller</param>
        /// <param name="guiManager">Reference to the scene's GUI Manager</param>
        /// <param name="description">Basic description of the Strategy</param>
        public MutinyMeterSacrificeStrategy(bool mutinyEveryTurn, bool pickRandomTeam, TicTacNodeMeshViewController nodeMeshViewController, InGameGUIManager guiManager, string description)
        {
            m_MutinyEveryTurn = mutinyEveryTurn;
            m_PickRandomTeam = pickRandomTeam;
            m_NodeMeshViewController = nodeMeshViewController;
            m_GUIManager = guiManager;
            StrategyDescription = description;

            PlayerOneMeter = -1f;
            PlayerTwoMeter = -1f;
            PlayerThreeMeter = -1f;
            PlayerFourMeter = -1f;
        }

        /// <summary>
        /// Initializes the strategy
        /// </summary>
        public void Init()
        {
            for (int i = 0; i < 5; i++)
            {
                UpdateMeter(i);
            }
        }

        /// <summary>
        /// Can the Player sacrifice?
        /// </summary>
        /// <param name="p">The player to check</param>
        /// <returns>True for this strategy (players can always sacrifice)</returns>
        public bool CanPlayerSacrifice(Player p)
        {
            return true;
        }

        /// <summary>
        /// Handles a Player starting their turn
        /// </summary>
        /// <param name="p">The Player that started their turn</param>
        public void HandleTurnStarted(Player p)
        {
            // This will tell us when a player is active, because we set them to -1 on init
            switch (p.PlayerNumber)
            {
                case 1:
                    if (PlayerOneMeter < 0f)
                        PlayerOneMeter = 0f;
                    break;

                case 2:
                    if (PlayerTwoMeter < 0f)
                        PlayerTwoMeter = 0f;
                    break;

                case 3:
                    if (PlayerThreeMeter < 0f)
                        PlayerThreeMeter = 0f;
                    break;

                case 4:
                    if (PlayerFourMeter < 0f)
                        PlayerFourMeter = 0f;
                    break;
            }
            m_CurrentPlayerMeterHistory.Clear();
            m_CurrentPlayerSacrificeCount = 0;
        }

        /// <summary>
        /// Updates the meter for the given player index
        /// </summary>
        /// <param name="p">The player to update</param>
        private void UpdateMeter(int p)
        {
            float val;
            switch (p)
            {
                case 1:
                    val = PlayerOneMeter;
                    break;

                case 2:
                    val = PlayerTwoMeter;
                    break;

                case 3:
                    val = PlayerThreeMeter;
                    break;

                case 4:
                    val = PlayerFourMeter;
                    break;

                default:
                    TTDebug.LogError("MutinyMeterSacrificeStrategy.cs : Invalid player number : " + p);
                    return;
            }
            m_GUIManager.UpdatePlayerMeter(p, val);
        }

        /// <summary>
        /// Decays the meter of the given player
        /// </summary>
        /// <param name="p">Index of the player to decay</param>
        private void DecayMeter(Player p)
        {
            // Decay
            switch (p.PlayerNumber)
            {
                case 1:
                    PlayerOneMeter = Mathf.Clamp01(PlayerOneMeter - MeterDeacy);
                    break;

                case 2:
                    PlayerTwoMeter = Mathf.Clamp01(PlayerTwoMeter - MeterDeacy);
                    break;

                case 3:
                    PlayerThreeMeter = Mathf.Clamp01(PlayerThreeMeter - MeterDeacy);
                    break;

                case 4:
                    PlayerFourMeter = Mathf.Clamp01(PlayerFourMeter - MeterDeacy);
                    break;

                default:
                    TTDebug.LogError("MutinyMeterSacrificeStrategy.cs : Invalid player number : " + p.PlayerNumber);
                    return;
            }
            UpdateMeter(p.PlayerNumber);
        }

        /// <summary>
        /// Performs a test to see if a piece should mutiny
        /// </summary>
        /// <param name="p">The player to test</param>
        private void MutinyTest(Player p)
        {
            float currentMeter;
            switch (p.PlayerNumber)
            {
                case 1:
                    currentMeter = PlayerOneMeter;
                    break;

                case 2:
                    currentMeter = PlayerTwoMeter;
                    break;

                case 3:
                    currentMeter = PlayerThreeMeter;
                    break;

                case 4:
                    currentMeter = PlayerFourMeter;
                    break;

                default:
                    TTDebug.LogError("MutinyMeterSacrificeStrategy.cs : Invalid player number : " + p.PlayerNumber);
                    return;
            }

            float percent;

            // The zero zone is a dead zone.  If the meter is in there, it doesn't attract any penalties
            if (currentMeter <= ZeroZoneEnd)
            {
                return;
            }

            // The Max Zone is an area where the maximum amount clamps.  So just use that.
            else if (currentMeter >= MaxZoneStart)
            {
                percent = MaxRandomChance;
            }
            else // Calculate the percentage
            {
                const float range = MaxZoneStart - ZeroZoneEnd;
                const float percentRange = MaxRandomChance - MinRandomChance;
                percent = ((currentMeter - ZeroZoneEnd) / range) * percentRange;
            }

            var random = Random.Range(0f, 1f);
            TTDebug.Log("Percent : " + percent + " random : " + random);
            if (random <= percent)
            {
                // Defect!
                var nodeMesh = m_NodeMeshViewController.CurrentNodeMesh;
                var nodes = nodeMesh.GetNodesOfType(p.PlayerNumber);
                if (nodes.Count == 0)
                {
                    // The player doesn't have any nodes to defect!
                    return;
                }
                var keys = nodes.Keys;

                var rand = Random.Range(0, keys.Count);
                var enumerator = keys.GetEnumerator();
                enumerator.MoveNext();
                int i = 0;
                while (i < rand)
                {
                    enumerator.MoveNext();
                    i++;
                }
                TTDebug.Log("Rand : " + rand + " Count : " + keys.Count + " i : " + i);
                var node = nodes[enumerator.Current];

                // RULE SWAP
                if (m_PickRandomTeam)
                {
                    // First implementation will be random tile from random team

                    var defectTeam = GameController.Instance.GetRandomPlayer(p);
                    node.NodeType = defectTeam.PlayerNumber;
                    TTDebug.Log("DEFECT!!!!");
                }
                else // Pick lowest team
                {
                    // Find lowest
                    int lowest = 0;
                    float minValue = float.MaxValue;

                    // The Meter number gets set to -1f by default, and set to 0f on first turn start, so we test for a negative number
                    // to determine if the player is in the game or not.
                    // This doesn't randomise if two are equal lowest, so the lower numbered players have an advantage - JM
                    if (PlayerOneMeter >= 0f && PlayerOneMeter < minValue)
                    {
                        minValue = PlayerOneMeter;
                        lowest = 1;
                    }
                    if (PlayerTwoMeter >= 0f && PlayerTwoMeter < minValue)
                    {
                        minValue = PlayerTwoMeter;
                        lowest = 2;
                    }
                    if (PlayerThreeMeter >= 0f && PlayerThreeMeter < minValue)
                    {
                        minValue = PlayerThreeMeter;
                        lowest = 3;
                    }
                    if (PlayerFourMeter >= 0f && PlayerFourMeter < minValue)
                    {
                        minValue = PlayerFourMeter;
                        lowest = 4;
                    }
                    node.NodeType = lowest;
                    TTDebug.Log("DEFECT!!!!");
                }
            }
        }

        /// <summary>
        /// Handles the end of turn event 
        /// </summary>
        /// <param name="p">The Player who's turn ended</param>
        public void HandleTurnEnded(Player p)
        {
            // First implementation, only at the end of the turn a sacrifice occurred
            if (m_MutinyEveryTurn)
            {
                MutinyTest(p);
                DecayMeter(p);
            }
            else
            {
                if (m_CurrentPlayerSacrificeCount > 0)
                {
                    MutinyTest(p);
                }
                else
                {
                    DecayMeter(p);
                }
            }

            TTDebug.Log("P1: " + PlayerOneMeter + "\nP2: " + PlayerTwoMeter + "\nP3: " + PlayerThreeMeter + "\nP4: " + PlayerFourMeter);
        }

        /// <summary>
        /// Handles A sacrifice being completed
        /// </summary>
        /// <param name="p">The Player that performed the Sacrifice</param>
        public void HandleSacrificeAttackCompleted(Player p)
        {
            // Increment the sacrifice count, add to the meter history, and update the meter
            switch (p.PlayerNumber)
            {
                case 1:
                    m_CurrentPlayerSacrificeCount++;
                    m_CurrentPlayerMeterHistory.Add(PlayerOneMeter);
                    PlayerOneMeter = Mathf.Clamp01(PlayerOneMeter + MeterAttackIncrement);
                    break;

                case 2:
                    m_CurrentPlayerSacrificeCount++;
                    m_CurrentPlayerMeterHistory.Add(PlayerTwoMeter);
                    PlayerTwoMeter = Mathf.Clamp01(PlayerTwoMeter + MeterAttackIncrement);
                    break;

                case 3:
                    m_CurrentPlayerSacrificeCount++;
                    m_CurrentPlayerMeterHistory.Add(PlayerThreeMeter);
                    PlayerThreeMeter = Mathf.Clamp01(PlayerThreeMeter + MeterAttackIncrement);
                    break;

                case 4:
                    m_CurrentPlayerSacrificeCount++;
                    m_CurrentPlayerMeterHistory.Add(PlayerFourMeter);
                    PlayerFourMeter = Mathf.Clamp01(PlayerFourMeter + MeterAttackIncrement);
                    break;

                default:
                    TTDebug.LogError("MutinyMeterSacrificeStrategy.cs : Invalid player number : " + p.PlayerNumber);
                    return; ;
            }
            UpdateMeter(p.PlayerNumber);
        }

        /// <summary>
        /// Handles a Sacrifice being undone
        /// </summary>
        /// <param name="p">The player that undid their sacrifice</param>
        public void HandleSacrificeAttackUndone(Player p)
        {
            Undo(p);
        }

        /// <summary>
        /// Handles a sacrifice and fortify being completed
        /// </summary>
        /// <param name="p">The player completing the Fortify</param>
        public void HandleSacrificeFortifyCompleted(Player p)
        {
            // Increment the sacrifice count, add to the meter history, and update the meter
            switch (p.PlayerNumber)
            {
                case 1:
                    m_CurrentPlayerMeterHistory.Add(PlayerOneMeter);
                    PlayerOneMeter = Mathf.Clamp01(PlayerOneMeter - MeterFortifyDecrement);
                    m_CurrentPlayerSacrificeCount++;
                    break;

                case 2:
                    m_CurrentPlayerMeterHistory.Add(PlayerTwoMeter);
                    PlayerTwoMeter = Mathf.Clamp01(PlayerTwoMeter - MeterFortifyDecrement);
                    m_CurrentPlayerSacrificeCount++;
                    break;

                case 3:
                    m_CurrentPlayerMeterHistory.Add(PlayerThreeMeter);
                    PlayerThreeMeter = Mathf.Clamp01(PlayerThreeMeter - MeterFortifyDecrement);
                    m_CurrentPlayerSacrificeCount++;
                    break;

                case 4:
                    m_CurrentPlayerMeterHistory.Add(PlayerFourMeter);
                    PlayerFourMeter = Mathf.Clamp01(PlayerFourMeter - MeterFortifyDecrement);
                    m_CurrentPlayerSacrificeCount++;
                    break;

                default:
                    TTDebug.LogError("MutinyMeterSacrificeStrategy.cs : Invalid player number : " + p.PlayerNumber);
                    return;
            }
            UpdateMeter(p.PlayerNumber);
        }

        /// <summary>
        /// Handles a sacrifice and fortify being undone
        /// </summary>
        /// <param name="p">The player undoing the Fortify</param>
        public void HandleSacrificeFortifyUndone(Player p)
        {
            Undo(p);
        }

        /// <summary>
        /// Performs one step of Undo during a sacrifice
        /// </summary>
        /// <param name="p">The Player performing the Undo</param>
        private void Undo(Player p)
        {
            // Restore the player's meter, remove from the history list, and decrement the sacrifice count
            switch (p.PlayerNumber)
            {
                case 1:
                    PlayerOneMeter = m_CurrentPlayerMeterHistory[m_CurrentPlayerMeterHistory.Count - 1];
                    m_CurrentPlayerMeterHistory.RemoveAt(m_CurrentPlayerMeterHistory.Count - 1);
                    m_CurrentPlayerSacrificeCount--;
                    break;

                case 2:
                    PlayerTwoMeter = m_CurrentPlayerMeterHistory[m_CurrentPlayerMeterHistory.Count - 1];
                    m_CurrentPlayerMeterHistory.RemoveAt(m_CurrentPlayerMeterHistory.Count - 1);
                    m_CurrentPlayerSacrificeCount--;
                    break;

                case 3:
                    PlayerThreeMeter = m_CurrentPlayerMeterHistory[m_CurrentPlayerMeterHistory.Count - 1];
                    m_CurrentPlayerMeterHistory.RemoveAt(m_CurrentPlayerMeterHistory.Count - 1);
                    m_CurrentPlayerSacrificeCount--;
                    break;

                case 4:
                    PlayerFourMeter = m_CurrentPlayerMeterHistory[m_CurrentPlayerMeterHistory.Count - 1];
                    m_CurrentPlayerMeterHistory.RemoveAt(m_CurrentPlayerMeterHistory.Count - 1);
                    m_CurrentPlayerSacrificeCount--;
                    break;

                default:
                    TTDebug.LogError("MutinyMeterSacrificeStrategy.cs : Invalid player number : " + p.PlayerNumber);
                    return;
            }
            UpdateMeter(p.PlayerNumber);
        }
    }
}