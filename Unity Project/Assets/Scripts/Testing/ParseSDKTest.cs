﻿using UnityEngine;
using System.Collections;
using Parse;
using System.Collections.Generic;

public class ParseSDKTest : MonoBehaviour {

	// Use this for initialization
	void Start () 
    {
        Debug.Log("Calling Async");
        ParseCloud.CallFunctionAsync<string>("hello", new Dictionary<string, object>()).ContinueWith((t) => 
            {
                Debug.Log("Returned!");
                Debug.Log("success : " + t.IsCompleted + " : " + t.IsCanceled + " : " + t.IsFaulted);
                Debug.Log("Result : " + t.Result);
            });
        
	}
	
}
