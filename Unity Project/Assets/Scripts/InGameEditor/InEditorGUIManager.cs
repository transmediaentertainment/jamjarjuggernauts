using Holoville.HOTween;
using System.Collections;
using System.Collections.Generic;
using TransTech.Delegates;
using TransTech.System.Debug;
using UnityEngine;
using TransTech.IGE.States;

/// <summary>
/// Layer Selection possibilities
/// </summary>
public enum Layers
{
	TerrainLayer,
	DetailsLayer,
	UpperLayer,
		
	COUNT
};
	
/// <summary>
///  Block Selection States.  Stored as bytes for more compact storage
/// </summary>
public enum Blocks : byte
{
    EmptyInvisibleEditorBlock,
    EmptySelectableEditorBlock,
	GrassBlock,
	RockBlock,
	WaterBlock,
		
	COUNT
};

/// <summary>
/// GUI Manager for the In Game Editor
/// </summary>
public class InEditorGUIManager : MonoBehaviour
{
    /// <summary>
    /// Reference to the Map Button Prefab
    /// </summary>
    public GameObject m_MapButtonPrefab;
    /// <summary>
    /// Reference to the busy Icon prefab.
    /// </summary>
    public GameObject m_BusyIconPrefab;

    #region Main Panel Buttons

    /// <summary>
    /// Menu button (for opening the in editor menu)
    /// </summary>
    public UIButton m_MenuButton;
    /// <summary>
    /// Reference to the Undo button in the left panel.
    /// </summary>
    public UIButton m_UndoButton;
    /// <summary>
    /// Reference to the Change Size button in the left panel.
    /// </summary>
    public UIButton m_ChangeSizeButton;
    /// <summary>
    /// Reference to the Layer 1 button in the left panel.
    /// </summary>
    public UIButton m_Layer1Button;
    /// <summary>
    /// Reference to the Layer 2 button in the left panel.
    /// </summary>
    public UIButton m_Layer2Button;
    /// <summary>
    /// Reference to the Layer 3 button in the left panel.
    /// </summary>
    public UIButton m_Layer3Button;
	/// <summary>
	/// Array of the above buttons (used for convenience)
	/// </summary>
	private UIButton[] m_LayerButtonsArray;

    #endregion // Main Panel Buttons

    #region Block Buttons

    /// <summary>
    /// Reference to the Grass button in the left panel
    /// </summary>
    public UIButton m_BlockGrassButton;
    /// <summary>
    /// Reference to the Rock button in the left panel
    /// </summary>
	public UIButton m_BlockRockButton;
    /// <summary>
    /// /// <summary>
    /// Reference to the Water button in the left panel.
    /// </summary>
    /// </summary>
	public UIButton m_BlockWaterButton;
    /// <summary>
    /// Reference to the Empty button in the left panel.
    /// </summary>
    public UIButton m_BlockEmptyButton;
	/// <summary>
	/// Aray of the above block buttons (for convenience)
	/// </summary>
	private UIButton[] m_BlockButtonsArray;

    #endregion // Block Buttons

    #region Size Buttons

    /// <summary>
    /// Reference to the Up Plus button in the Resize panel
    /// </summary>
    public UIButton m_SizeUpPlusButton;
    /// <summary>
    /// Reference to the Up Minus button in the Resize panel
    /// </summary>
	public UIButton m_SizeUpMinusButton;
    /// <summary>
    /// Reference to the Down Plus button in the Resize panel
    /// </summary>
	public UIButton m_SizeDownPlusButton;
    /// <summary>
    /// Reference to the Down Minus button in the Resize panel
    /// </summary>
	public UIButton m_SizeDownMinusButton;
    /// <summary>
    /// Reference to the Right Plus button in the Resize panel
    /// </summary>
	public UIButton m_SizeRightPlusButton;
    /// <summary>
    /// Reference to the Right Minus button in the Resize panel
    /// </summary>
	public UIButton m_SizeRightMinusButton;
    /// <summary>
    /// Reference to the Left Plus button in the Resize panel
    /// </summary>
	public UIButton m_SizeLeftPlusButton;
    /// <summary>
    /// Reference to the Left Minus button in the Resize panel
    /// </summary>
	public UIButton m_SizeLeftMinusButton;

    #endregion // Size Buttons

    #region Menu Panels

    /// <summary>
    /// Panel for the in editor menu
    /// </summary>
    public UIPanel m_MenuPanel;
    /// <summary>
    /// Panel for the Load Menu
    /// </summary>
    public UIPanel m_LoadPanel;
    /// <summary>
    /// Panel for the Save Menu
    /// </summary>
    public UIPanel m_SavePanel;
    /// <summary>
    /// Panel for the initial menu on entering the editor (Not used Currently)
    /// </summary>
    public UIPanel m_EntryMenuPanel;
    /// <summary>
    /// Panel for the Unsaved warning popup
    /// </summary>
    public UIPanel m_UnsavedPanel;
    /// <summary>
    /// Panel for the Overwrite confirmation popup
    /// </summary>
    public UIPanel m_OverwritePanel;
    /// <summary>
    /// Panel for the list of files to download from the server
    /// </summary>
    public UIPanel m_DownloadPanel;

    #endregion // Menu Panels

    /// <summary>
    ///  Block Button Pos and Anchor
    /// </summary>
    public Transform m_BlockButtonAnchor;
	
    /// <summary>
    /// Popup window for changing the map size
    /// </summary>
	public Popup m_ChangeMapSizePopup;

    #region Main Panel Button Events

    /// <summary>
    /// Event fired when the Menu Button is pressed.
    /// </summary>
    public event VoidDelegate MenuButtonPressedEvent;
    /// <summary>
    /// Event fired when the Undo button is pressed.
    /// </summary>
    public event VoidDelegate UndoButtonPressedEvent;
    /// <summary>
    /// Event fired when the change size button is pressed
    /// </summary>
	public event VoidDelegate ChangeSizeButtonPressedEvent;
    /// <summary>
    /// Event fired when the Layer 1 button is pressed
    /// </summary>
	public event VoidDelegate Layer1ButtonPressedEvent;
    /// <summary>
    /// Event fired when the Layer 2 button is pressed
    /// </summary>
	public event VoidDelegate Layer2ButtonPressedEvent;
    /// <summary>
    /// Event fired when the Layer 3 button is pressed
    /// </summary>
	public event VoidDelegate Layer3ButtonPressedEvent;

    #endregion // Main Panel Button Events

    #region Block Button Events

    /// <summary>
    /// Event fired when the Grass button is pressed
    /// </summary>
    public event VoidDelegate BlockGrassButtonPressedEvent;
    /// <summary>
    /// Event fired when the Rock button is pressed
    /// </summary>
	public event VoidDelegate BlockRockButtonPressedEvent;
    /// <summary>
    /// Event fired when the Water button is pressed
    /// </summary>
	public event VoidDelegate BlockWaterButtonPressedEvent;
    /// <summary>
    /// Event fired when the Empty button is pressed
    /// </summary>
    public event VoidDelegate BlockEmptyButtonPressedEvent;

    #endregion // Block Button Events

    #region Size Button Events

    /// <summary>
    /// Event fired when the Up Plus button is pressed
    /// </summary>
    public event VoidDelegate SizeUpPlusButtonPressedEvent;
    /// <summary>
    /// Event fired when the Up Minus button is pressed
    /// </summary>
	public event VoidDelegate SizeUpMinusButtonPressedEvent;
    /// <summary>
    /// Event fired when the Down Plus button is pressed
    /// </summary>
	public event VoidDelegate SizeDownPlusButtonPressedEvent;
    /// <summary>
    /// Event fired when the Down Minus button is pressed
    /// </summary>
	public event VoidDelegate SizeDownMinusButtonPressedEvent;
    /// <summary>
    /// Event fired when the Right Plus button is pressed
    /// </summary>
	public event VoidDelegate SizeRightPlusButtonPressedEvent;
    /// <summary>
    /// Event fired when the Right Minus button is pressed
    /// </summary>
	public event VoidDelegate SizeRightMinusButtonPressedEvent;
    /// <summary>
    /// Event fired when the Left Plus button is pressed
    /// </summary>
	public event VoidDelegate SizeLeftPlusButtonPressedEvent;
    /// <summary>
    /// Event fired when the Left Minus button is pressed
    /// </summary>
	public event VoidDelegate SizeLeftMinusButtonPressedEvent;

    #endregion // Size Button Events

    #region Menu Button Events

    /// <summary>
    /// Event fired when the Back To Editor button is pressed
    /// </summary>
    public event VoidDelegate BackToEditorButtonPressedEvent;
    /// <summary>
    /// Event fired when the Tutorial button is pressed
    /// </summary>
    public event VoidDelegate TutorialButtonPressedEvent;
    /// <summary>
    /// Event fired when the Main Menu button is pressed
    /// </summary>
    public event VoidDelegate MainMenuButtonPressedEvent;
    /// <summary>
    /// Event fired when the New Map button is pressed
    /// </summary>
    public event VoidDelegate NewMapButtonPressedEvent;
    /// <summary>
    /// Event fired when the Load Map button is pressed
    /// </summary>
    public event VoidDelegate LoadMapButtonPressedEvent;
    /// <summary>
    /// Event fired when the Cancel Load Map button is pressed
    /// </summary>
	public event VoidDelegate CancelLoadMapButtonPressedEvent;
    /// <summary>
    /// Event fired when the Open Save Menu button is pressed
    /// </summary>
    public event VoidDelegate OpenSaveMenuButtonPressedEvent;
    /// <summary>
    /// Event fired when the Save Map button is pressed
    /// </summary>
    public event VoidDelegate SaveMapButtonPressedEvent;
    /// <summary>
    /// Event fired when the Cancel Save Map button is pressed
    /// </summary>
	public event VoidDelegate CancelSaveMapButtonPressedEvent;
    /// <summary>
    /// Event fired when the UIInput for map name entry fires it's submit event (on pressing enter)
    /// </summary>
    public event StringDelegate OnSubmitSaveMapEvent;
    /// <summary>
    /// Event fired when the OK button on the successful save popup is pressed (Popup not implemented yet)
    /// </summary>
	public event VoidDelegate SaveSuccessfulButtonPressedEvent;
    /// <summary>
    /// Event fired when the Download Maps button is pressed
    /// </summary>
    public event VoidDelegate DownloadMapsButtonPressedEvent;
    /// <summary>
    /// Event fired when the Cancel button on the Download panel is pressed
    /// </summary>
    public event VoidDelegate CancelDownloadMapsButtonPressedEvent;

    #endregion // Menu button events

    /// <summary>
    /// Default colour for buttons (set in editor)
    /// </summary>
    public Color m_DefaultButtonColor;
    /// <summary>
    /// Colour for button when being hovered over (set in editor)
    /// </summary>
	public Color m_HoverButtonColor;
    /// <summary>
    /// Colour for a button that has been selected (set in editor)
    /// </summary>
	public Color m_SelectedDefaultButtonColor;
    /// <summary>
    /// Colour for a selected button being overed over (set in editor)
    /// </summary>
	public Color m_SelectedHoverButtonColor;

    /// <summary>
    /// Reference to the InGameEditorController running the scene
    /// </summary>
    public InGameEditorController m_InGameEditorController;
    /// <summary>
    /// Reference to the UI FSM engine
    /// </summary>
    private InGameEditorUIFSMEngine m_FSM;
	
    /// <summary>
    /// Called by Unity the frame after the object is created
    /// </summary>
	public void Start()
	{
		m_LayerButtonsArray = new UIButton[(int)Layers.COUNT] { m_Layer1Button, m_Layer2Button, m_Layer3Button };
		m_BlockButtonsArray = new UIButton[/*(int)Blocks.COUNT*/4] { m_BlockGrassButton, m_BlockRockButton, m_BlockWaterButton, m_BlockEmptyButton };
		
		Layer1ButtonPressed();
        BlockGrassButtonPressed();

        m_FSM = new InGameEditorUIFSMEngine(m_InGameEditorController, this, m_MenuPanel, m_SavePanel, m_LoadPanel, m_UnsavedPanel, m_OverwritePanel, m_DownloadPanel, m_MapButtonPrefab, m_BusyIconPrefab );
        MenuButtonPressedEvent += m_FSM.ShowMenu;
	}

    #region Main Panel Button Press Functions

    /// <summary>
    /// Called by the UIButton when the Menu Button is pressed
    /// </summary>
    private void MenuButtonPressed()
    {
        if (MenuButtonPressedEvent != null)
            MenuButtonPressedEvent();
    }

    /// <summary>
    /// Called by the UIButton when the Undo button is pressed
    /// </summary>
    private void UndoButtonPressed()
    {
        if (UndoButtonPressedEvent != null)
            UndoButtonPressedEvent();
    }
	
    /// <summary>
    /// Called by the UIButton when the Change Size button is pressed
    /// </summary>
	private void ChangeSizeButtonPressed()
	{
		if(ChangeSizeButtonPressedEvent != null)
			ChangeSizeButtonPressedEvent();
		
        // Toggle the popup
		if( !m_ChangeMapSizePopup.gameObject.activeSelf )
		{
			m_ChangeMapSizePopup.gameObject.SetActive(true);
			SetButtonColor(m_SelectedDefaultButtonColor, m_SelectedHoverButtonColor, m_ChangeSizeButton);
		}
		else
		{
            m_ChangeMapSizePopup.gameObject.SetActive(false);
			SetButtonColor(m_DefaultButtonColor, m_HoverButtonColor, m_ChangeSizeButton);
		}
	}
	
    /// <summary>
    /// Called by the UIButton when the Layer 1 button is pressed
    /// </summary>
	private void Layer1ButtonPressed()
	{
		if(Layer1ButtonPressedEvent != null)
			Layer1ButtonPressedEvent();
		
		SetButtonColor(m_DefaultButtonColor, m_HoverButtonColor, m_LayerButtonsArray);
		SetButtonColor(m_SelectedDefaultButtonColor, m_SelectedHoverButtonColor, m_Layer1Button);
	}

    /// <summary>
    /// Called by the UIButton when the Layer 2 button is pressed
    /// </summary>
	private void Layer2ButtonPressed()
	{
		if(Layer2ButtonPressedEvent != null)
			Layer2ButtonPressedEvent();
		
		SetButtonColor(m_DefaultButtonColor, m_HoverButtonColor, m_LayerButtonsArray);
		SetButtonColor(m_SelectedDefaultButtonColor, m_SelectedHoverButtonColor, m_Layer2Button);
	}

    /// <summary>
    /// Called by the UIButton when the Layer 3 button is pressed
    /// </summary>
	private void Layer3ButtonPressed()
	{
		if(Layer3ButtonPressedEvent != null)
			Layer3ButtonPressedEvent();
		
		SetButtonColor(m_DefaultButtonColor, m_HoverButtonColor, m_LayerButtonsArray);
		SetButtonColor(m_SelectedDefaultButtonColor, m_SelectedHoverButtonColor, m_Layer3Button);
	}

    #endregion // Main Panel Button Press Functions

    #region Block Button Press Functions

    /// <summary>
    /// Called by the UIButton when the Grass button is pressed
    /// </summary>
    private void BlockGrassButtonPressed()
	{
		if(BlockGrassButtonPressedEvent != null)
			BlockGrassButtonPressedEvent();
		
		SetButtonColor(m_DefaultButtonColor, m_HoverButtonColor, m_BlockButtonsArray);
		SetButtonColor(m_SelectedDefaultButtonColor, m_SelectedHoverButtonColor, m_BlockGrassButton);
	}

    /// <summary>
    /// Called by the UIButton when the Rock button is pressed
    /// </summary>
	private void BlockRockButtonPressed()
	{
		if(BlockRockButtonPressedEvent != null)
			BlockRockButtonPressedEvent();
		
		SetButtonColor(m_DefaultButtonColor, m_HoverButtonColor, m_BlockButtonsArray);
		SetButtonColor(m_SelectedDefaultButtonColor, m_SelectedHoverButtonColor, m_BlockRockButton);
	}

    /// <summary>
    /// Called by the UIButton when the Water button is pressed
    /// </summary>
	private void BlockWaterButtonPressed()
	{
		if(BlockWaterButtonPressedEvent != null)
			BlockWaterButtonPressedEvent();
		
		SetButtonColor(m_DefaultButtonColor, m_HoverButtonColor, m_BlockButtonsArray);
		SetButtonColor(m_SelectedDefaultButtonColor, m_SelectedHoverButtonColor, m_BlockWaterButton);
	}

    /// <summary>
    /// Called by the UIButton when the Empty button is pressed
    /// </summary>
    private void BlockEmptyButtonPressed()
    {
        if (BlockEmptyButtonPressedEvent != null)
            BlockEmptyButtonPressedEvent();

        SetButtonColor(m_DefaultButtonColor, m_HoverButtonColor, m_BlockButtonsArray);
        SetButtonColor(m_SelectedDefaultButtonColor, m_SelectedHoverButtonColor, m_BlockEmptyButton);
    }

    #endregion // Block Button Press Functions

    #region Size Change Button Press Functions

    /// <summary>
    /// Called by the UIButton when the Size Up Plus button is pressed
    /// </summary>
    private void SizeUpPlusButtonPressed()
	{
		if(SizeUpPlusButtonPressedEvent != null)
			SizeUpPlusButtonPressedEvent();
	}

    /// <summary>
    /// Called by the UIButton when the Size Up Minus button is pressed
    /// </summary>
    private void SizeUpMinusButtonPressed()
	{
		if(SizeUpMinusButtonPressedEvent != null)
			SizeUpMinusButtonPressedEvent();
	}

    /// <summary>
    /// Called by the UIButton when the Size Down Plus button is pressed
    /// </summary>
    private void SizeDownPlusButtonPressed()
	{
		if(SizeDownPlusButtonPressedEvent != null)
			SizeDownPlusButtonPressedEvent();
	}

    /// <summary>
    /// Called by the UIButton when the Size Down Minus button is pressed
    /// </summary>
    private void SizeDownMinusButtonPressed()
	{
		if(SizeDownMinusButtonPressedEvent != null)
			SizeDownMinusButtonPressedEvent();
	}

    /// <summary>
    /// Called by the UIButton when the Size Right Plus button is pressed
    /// </summary>
    private void SizeRightPlusButtonPressed()
	{
		if(SizeRightPlusButtonPressedEvent != null)
			SizeRightPlusButtonPressedEvent();
	}

    /// <summary>
    /// Called by the UIButton when the Size Right Minus button is pressed
    /// </summary>
    private void SizeRightMinusButtonPressed()
	{
		if(SizeRightMinusButtonPressedEvent != null)
			SizeRightMinusButtonPressedEvent();
	}

    /// <summary>
    /// Called by the UIButton when the Size Left Plus button is pressed
    /// </summary>
    private void SizeLeftPlusButtonPressed()
	{
		if(SizeLeftPlusButtonPressedEvent != null)
			SizeLeftPlusButtonPressedEvent();
	}

    /// <summary>
    /// Called by the UIButton when the Size Left Minus button is pressed
    /// </summary>
    private void SizeLeftMinusButtonPressed()
	{
		if(SizeLeftMinusButtonPressedEvent != null)
			SizeLeftMinusButtonPressedEvent();
	}

    #endregion // Size Change Button Press Functions

    /// <summary>
    /// Sets the default and hover colours on the provided buttons
    /// </summary>
    /// <param name="defaultColor">The colour to set the default colour field</param>
    /// <param name="hoverColor">The Colour to set the hover colour field</param>
    /// <param name="buttons">The buttons to change the colours on</param>
    public void SetButtonColor( Color defaultColor, Color hoverColor, params UIButton[] buttons )
	{
		foreach( UIButton button in buttons )
		{
			button.defaultColor = defaultColor;
			button.hover = hoverColor;
			
			button.UpdateColor(true, false);
		}
	}

    #region Menu Button Press Functions

    /// <summary>
    /// Called by the UIButton when the Back To Editor button is pressed
    /// </summary>
    private void BackToEditorButtonPressed()
    {
        if (BackToEditorButtonPressedEvent != null)
            BackToEditorButtonPressedEvent();
    }

    /// <summary>
    /// Called by the UIButton when the Tutorial button is pressed
    /// </summary>
    private void TutorialButtonPressed()
    {
        if (TutorialButtonPressedEvent != null)
            TutorialButtonPressedEvent();
    }

    /// <summary>
    /// Called by the UIButton when the Main Menu button is pressed
    /// </summary>
    private void MainMenuButtonPressed()
    {
        if (MainMenuButtonPressedEvent != null)
            MainMenuButtonPressedEvent();
    }

    /// <summary>
    /// Called by the UIButton when the New Map button is pressed
    /// </summary>
    private void NewMapButtonPressed()
    {
        if (NewMapButtonPressedEvent != null)
            NewMapButtonPressedEvent();
    }

    /// <summary>
    /// Called by the UIButton when the Load Map button is pressed
    /// </summary>
    private void LoadMapButtonPressed()
    {
        if (LoadMapButtonPressedEvent != null)
            LoadMapButtonPressedEvent();
    }

    /// <summary>
    /// Called by the UIButton when the Cancel Load Map button is pressed
    /// </summary>
	private void CancelLoadMapButtonPressed()
    {
        if (CancelLoadMapButtonPressedEvent != null)
            CancelLoadMapButtonPressedEvent();
    }

    /// <summary>
    /// Called by the UIButton when the Open Save Menu button is pressed
    /// </summary>
    private void OpenSaveMenuButtonPressed()
    {
        if (OpenSaveMenuButtonPressedEvent != null)
            OpenSaveMenuButtonPressedEvent();
    }

    /// <summary>
    /// Called by the UIButton when the Save Map button is pressed
    /// </summary>
    private void SaveMapButtonPressed()
    {
        if (SaveMapButtonPressedEvent != null)
            SaveMapButtonPressedEvent();
    }

    /// <summary>
    /// Called by the UIButton when the Cancel Save Map button is pressed
    /// </summary>
	private void CancelSaveMapButtonPressed()
    {
        if (CancelSaveMapButtonPressedEvent != null)
            CancelSaveMapButtonPressedEvent();
    }

    /// <summary>
    /// Called by the UIInput on the Save Map panel when the user submits it's data
    /// </summary>
    /// <param name="levelName">The name to save the level as</param>
    private void OnSubmitSaveMap(string levelName)
    {
        if (OnSubmitSaveMapEvent != null)
            OnSubmitSaveMapEvent(levelName);
    }

    /// <summary>
    /// Called by the UIButton when the Ok button is pressed on the successful save popup (not implemented yet)
    /// </summary>
	private void SaveSuccessfulButtonPressed()
    {
        if (SaveSuccessfulButtonPressedEvent != null)
            SaveSuccessfulButtonPressedEvent();
    }

    /// <summary>
    /// Called by the UIButton when the Download Maps button is pressed
    /// </summary>
    private void DownloadMapsButtonPressed()
    {
        if (DownloadMapsButtonPressedEvent != null)
            DownloadMapsButtonPressedEvent();
    }

    /// <summary>
    /// Called by the UIButton when the Cancel Download Maps button is pressed
    /// </summary>
    private void CancelDownloadMapButtonPressed()
    {
        if (CancelDownloadMapsButtonPressedEvent != null)
            CancelDownloadMapsButtonPressedEvent();
    }

    #endregion // Menu Button Press Functions
}
