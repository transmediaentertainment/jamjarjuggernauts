using UnityEngine;
using System.Collections;
using DopplerInteractive.TidyTileMapper.Utilities;
using TransTech.System.Debug;
using UnityExtensions;
using TransTech.System.IO;

/// <summary>
/// Controller Behaviour for the In Game Level Editor
/// </summary>
public partial class InGameEditorController : MonoBehaviour
{
    /// <summary>
    /// Maximum tile size for maps
    /// </summary>
    private const int m_MaxMapSize = 16;
    /// <summary>
    /// Minimum tile size for maps
    /// </summary>
    private const int m_MinMapSize = 4;

    /// <summary>
    /// Current size of the map in the X direction
    /// </summary>
    public int XSize { get; private set; }
    /// <summary>
    /// Current size of the map in the Y direction (z in world coordinates)
    /// </summary>
    public int YSize { get; private set; }

    /// <summary>
    /// The currently selected block type for placing in the scene.
    /// </summary>
    public Blocks CurrentBlockPlacementType { get; private set; }

    /// <summary>
    /// The current layer the user is editing
    /// </summary>
    private Layers m_CurrentLayer;
    /// <summary>
    /// The current layer the user is editing
    /// </summary>
    public Layers CurrentLayer {
        get { return m_CurrentLayer; }
        private set
        {
            if (m_CurrentLayer != value)
            {
                m_CurrentLayer = value;
                // Make sure we update the boxes that can be raycast against if the layer changes.
                UpdateHitBoxes();
            }
        }
    }

    /// <summary>
    /// Reference to the GUIManager in the scene. (Set this in the Editor)
    /// </summary>
    public InEditorGUIManager m_GUIManager;

    /// <summary>
    /// The current BlockMap object (See Tidy Tile Mapper) we are editing
    /// </summary>
    private BlockMap m_Map;

    /// <summary>
    /// Reference to the Camera Controller in the scene (Set this in the Editor)
    /// </summary>
    public CameraController m_CameraController;

    /// <summary>
    /// The center of the board in world coordinates
    /// </summary>
    private Vector3 m_BoardCenter;
    /// <summary>
    /// Minimum x position for the camera.
    /// </summary>
    private float m_MinX;
    /// <summary>
    /// Maximum X position for the camera
    /// </summary>
    private float m_MaxX;
    /// <summary>
    /// Minimum Z position for the camera
    /// </summary>
    private float m_MinZ;
    /// <summary>
    /// Maximum Z position for the camera
    /// </summary>
    private float m_MaxZ;
    /// <summary>
    /// Minimum Height (y position) for the camera
    /// </summary>
    private const float m_MinHeight = 3f;
    /// <summary>
    /// Maximum Height (y position) for the camera
    /// </summary>
    private const float m_MaxHeight = 12f;
    /// <summary>
    /// Are there any unsaved changes?
    /// </summary>
    public bool IsDirty { get; private set; }

    /// <summary>
    /// Called by Unity when the object is created
    /// </summary>
    void Awake()
    {
        // Hook up to button events;
        m_GUIManager.Layer1ButtonPressedEvent += () => CurrentLayer = Layers.TerrainLayer;
        m_GUIManager.Layer2ButtonPressedEvent += () => CurrentLayer = Layers.DetailsLayer;
        m_GUIManager.Layer3ButtonPressedEvent += () => CurrentLayer = Layers.UpperLayer;

        m_GUIManager.BlockGrassButtonPressedEvent += () => CurrentBlockPlacementType = Blocks.GrassBlock;
        m_GUIManager.BlockRockButtonPressedEvent += () => CurrentBlockPlacementType = Blocks.RockBlock;
        m_GUIManager.BlockWaterButtonPressedEvent += () => CurrentBlockPlacementType = Blocks.WaterBlock;
        m_GUIManager.BlockEmptyButtonPressedEvent += () => CurrentBlockPlacementType = Blocks.EmptySelectableEditorBlock;

        m_GUIManager.SizeUpPlusButtonPressedEvent += ExtendForward;
        m_GUIManager.SizeUpMinusButtonPressedEvent += RetractForward;
        m_GUIManager.SizeRightPlusButtonPressedEvent += ExtendRight;
        m_GUIManager.SizeRightMinusButtonPressedEvent += RetractRight;
        m_GUIManager.SizeDownPlusButtonPressedEvent += ExtendBack;
        m_GUIManager.SizeDownMinusButtonPressedEvent += RetractBack;
        m_GUIManager.SizeLeftPlusButtonPressedEvent += ExtendLeft;
        m_GUIManager.SizeLeftMinusButtonPressedEvent += RetractLeft;

        m_GUIManager.UndoButtonPressedEvent += Undo;

        // Register to raycast on mouse down
        InputMouseClickEvents.Instance.LeftMouseDown += RaycastScene;

        // Create a new default map
        NewMap("Default Map", 4, 4);

        // Calculate the Camera's restrictions, and initialise the camera controller
        m_BoardCenter = m_Map.transform.position;
        m_BoardCenter.x += (XSize / 2f) - 0.5f;
        m_BoardCenter.z += (YSize / 2f) - 0.5f;
        var mapPos = m_Map.transform.position;
        m_MinX = mapPos.x;
        m_MaxX = mapPos.x + XSize - 1;
        m_MinZ = mapPos.z;
        m_MaxZ = mapPos.z + YSize - 1;
        m_CameraController.Init(m_BoardCenter, 1f, m_MinX, m_MaxX, m_MinZ, m_MaxZ, m_MinHeight, m_MaxHeight);

        IsDirty = true;
    }

    /// <summary>
    /// Creates a new empty map, discarding the existing one.
    /// </summary>
    /// <param name="mapName">Name of the new map</param>
    /// <param name="xSize">Size in blocks on the X axis</param>
    /// <param name="ySize">Size in blocks on the Y axis (Z axis in world)</param>
    public void NewMap(string mapName, int xSize, int ySize)
    {
        // Destroy any existing map.
        if (m_Map != null)
        {
            Destroy(m_Map.gameObject);
            m_Map = null;
        }
        // Create and initialise our variables.
        m_Map = NodeTTMBridge.Instance.CreateEmptyMap(mapName, xSize, ySize);
        CurrentLayer = Layers.TerrainLayer;
        XSize = xSize;
        YSize = ySize;
        UpdateHitBoxes();
        IsDirty = true;
    }

    /// <summary>
    /// Saves the current Block Map out to disk.
    /// </summary>
    /// <param name="levelName">The name to save the level under.</param>
    public void SaveMapToDisk(string levelName)
    {
        // We get the map converted to a byte array for smaller storage.
        var bytes = LevelFileUtilities.GetByteArrayFromMap(m_Map, XSize, YSize, (int)Layers.COUNT);
        // If we successfully save to the local disk
        if(LevelFileManager.SaveLevelDataToDisk(levelName, bytes))
        {
            // Save the file to the server.
            LevelFileManager.SaveFileToServer(levelName + ".lvl", (b, s) =>
                                                             {
                                                                 if(b)
                                                                 {
                                                                     TTDebug.Log("InGameEditorController.cs : Saved map to server : " + levelName );
                                                                 }
                                                                 else
                                                                 {
                                                                     // TODO : We need to handle the error properly.
                                                                     TTDebug.LogError("InGameEditorController.cs : Error saving map to server : " + s);
                                                                 }
                                                             });
        }
        IsDirty = false;
    }

    /// <summary>
    /// Loads a Map from the local disk
    /// </summary>
    /// <param name="levelName">The name of the level to load</param>
    public void LoadMapFromDisk(string levelName)
    {
        bool success;
        // Attempt to load byte data from disk
        var bytes = LevelFileManager.LoadLevelDataFromDisk(levelName, out success);
        // Early exit if we failed to load it.
        if (!success)
        {
            TTDebug.LogError("InGameEditorController.cs : Unable to load level from disk : " + levelName);
            return;
        }

        // Destroy the map if there is an existing one
        if (m_Map != null)
        {
            Destroy(m_Map.gameObject);
            m_Map = null;
        }
        // Convert the loaded bytes to a Block Map
        m_Map = LevelFileUtilities.GetMapFromByteArray(bytes, levelName);
        // Set the size of the level
        XSize = bytes.GetLength(0);
        YSize = bytes.GetLength(1);

        // Update the camera limits
        var mapPos = m_Map.transform.position;
        m_MinX = mapPos.x;
        m_MaxX = mapPos.x + XSize - 1;
        m_MinZ = mapPos.z;
        m_MaxZ = mapPos.z + YSize - 1;
        m_CameraController.UpdateMinMax(m_MinX, m_MaxX, m_MinZ, m_MaxZ, m_MinHeight, m_MaxHeight);
    }

    /// <summary>
    /// Adds an extra column of empty blocks to the right side of the map
    /// </summary>
    private void ExtendRight()
    {
        // Early exit if we are at the size limit
        if(XSize >= m_MaxMapSize)
            return;

        // Save state (for undo) before changing anything
        SaveEditorState();
        XSize += 1;
        int x = XSize - 1;
        // Add a new column in that direction
        for (int y = 0; y < YSize; y++)
        {
            for (int depth = 0; depth < (int)Layers.COUNT; depth++)
            {
                SetEmptyBlock(x, y, depth);
            }
        }
        // Update the camera restrictions
        m_MaxX += 1;
        m_CameraController.UpdateMinMax(m_MinX, m_MaxX, m_MinZ, m_MaxZ, m_MinHeight, m_MaxHeight);
        IsDirty = true;
    }

    /// <summary>
    /// Removes the right move column from the map
    /// </summary>
    private void RetractRight()
    {
        // Early exit if we are at the size limit
        if(XSize <= m_MinMapSize)
            return;

        // Save state (for undo) before changing anything
        SaveEditorState();
        XSize -= 1;
        int x = XSize;
        // Just loop over the one column and remove the blocks
        for (int y = 0; y < YSize; y++)
        {
            for (int depth = 0; depth < (int)Layers.COUNT; depth++)
            {
                BlockUtilities.RemoveBlockFromMap(m_Map, x, y, depth, false, false);
            }
        }
        // Update the camera restrictions
        m_MaxX -= 1;
        m_CameraController.UpdateMinMax(m_MinX, m_MaxX, m_MinZ, m_MaxZ, m_MinHeight, m_MaxHeight);
        IsDirty = true;
    }

    /// <summary>
    /// Adds an extra row of tiles to the top/forward direction of the map
    /// </summary>
    private void ExtendForward()
    {
        // Early exit if we are at the size limit
        if(YSize >= m_MaxMapSize)
            return;

        // Save state (for undo) before changing anything
        SaveEditorState();
        YSize += 1;
        int y = YSize - 1;
        // Add a new row in the forward direction
        for (int x = 0; x < XSize; x++)
        {
            for (int depth = 0; depth < (int)Layers.COUNT; depth++)
            {
                SetEmptyBlock(x, y, depth);
            }
        }
        // Update the camera restrictions
        m_MaxZ += 1;
        m_CameraController.UpdateMinMax(m_MinX, m_MaxX, m_MinZ, m_MaxZ, m_MinHeight, m_MaxHeight);
        IsDirty = true;
    }

    /// <summary>
    /// Removes the top/forward most row from the map
    /// </summary>
    private void RetractForward()
    {
        // Early exit if we are at the size limit
        if(YSize <= m_MinMapSize)
            return;

        // Save state (for undo) before changing anything
        SaveEditorState();
        YSize -= 1;
        int y = YSize;
        // Just loop over the one row and remove the blocks
        for (int x = 0; x < XSize; x++)
        {
            for (int depth = 0; depth < (int)Layers.COUNT; depth++)
            {
                BlockUtilities.RemoveBlockFromMap(m_Map, x, y, depth, false, false);
            }
        }
        // Update the camera restrictions
        m_MaxZ -= 1;
        m_CameraController.UpdateMinMax(m_MinX, m_MaxX, m_MinZ, m_MaxZ, m_MinHeight, m_MaxHeight);
        IsDirty = true;
    }

    /// <summary>
    /// Adds a column of tiles to the left side of the map
    /// </summary>
    private void ExtendLeft()
    {
        // Early exit if we are at the size limit
        if(XSize >= m_MaxMapSize)
            return;

        // Save state (for undo) before changing anything
        SaveEditorState();
        XSize += 1;
        // We loop backwards across the x pieces, copying the one lower across
        // This is due to Tidy Tile Mapper not accepting negative indices for blocks.
        for (int x = XSize - 1; x >= 0; x--)
        {
            for (int y = 0; y < YSize; y++)
            {
                for (int depth = 0; depth < (int)Layers.COUNT; depth++)
                {
                    if (x > 0)
                    {
                        var b = BlockUtilities.GetBlockAt(m_Map, x - 1, y, depth);
                        NodeTTMBridge.Instance.SetBlockInLayer(x, y, depth, b.GetComponent<EditorBlockTag>().BlockType);
                        // We need to set the layer for raycasting if it is on the current editing layer
                        if (depth == (int)CurrentLayer)
                        {
                            var b2 = BlockUtilities.GetBlockAt(m_Map, x, y, depth);
                            b2.gameObject.SetLayerRecursively("EditorRaycast");
                        }
                    }
                    else // Place empty blocks in the left most column
                    {
                        SetEmptyBlock(x, y, depth);
                    }
                }
            }
        }
        // We move the position of the map to make it look like it is actually expanding left and update the camera restrictions
        m_Map.transform.SetX(m_Map.transform.position.x - 1);
        m_MinX -= 1;
        m_CameraController.UpdateMinMax(m_MinX, m_MaxX, m_MinZ, m_MaxZ, m_MinHeight, m_MaxHeight);
        IsDirty = true;
    }

    /// <summary>
    /// Removes a column of tiles from the left side of the map
    /// </summary>
    private void RetractLeft()
    {
        // Early exit if we are at the size limit
        if(XSize <= m_MinMapSize)
            return;

        // Save state (for undo) before changing anything
        SaveEditorState();
        XSize -= 1;
        // We loop across the x pieces, copying the one higher indexed across
        // This is due to Tidy Tile Mapper not accepting negative indices for blocks.
        for (int x = 0; x < XSize + 1; x++)
        {
            for (int y = 0; y < YSize; y++)
            {
                for (int depth = 0; depth < (int)Layers.COUNT; depth++)
                {
                    if (x < XSize)
                    {
                        var b = BlockUtilities.GetBlockAt(m_Map, x + 1, y, depth);
                        NodeTTMBridge.Instance.SetBlockInLayer(x, y, depth, b.GetComponent<EditorBlockTag>().BlockType);
                        // We need to set the layer for raycasting if it is on the current editing layer
                        if(depth == (int)CurrentLayer)
                        {
                            var b2 = BlockUtilities.GetBlockAt(m_Map, x, y, depth);
                            b2.gameObject.SetLayerRecursively("EditorRaycast");
                        }
                    }
                    else // Remove the blocks in the left most column
                    {
                        BlockUtilities.RemoveBlockFromMap(m_Map, x, y, depth, false, false);
                    }
                }
            }
        }
        // We move the position of the map to make it look like it is actually retracting left and update the camera restrictions
        m_Map.transform.SetX(m_Map.transform.position.x + 1);
        m_MinX += 1;
        m_CameraController.UpdateMinMax(m_MinX, m_MaxX, m_MinZ, m_MaxZ, m_MinHeight, m_MaxHeight);
        IsDirty = true;
    }

    /// <summary>
    /// Adds a row of tiles to the back of the map
    /// </summary>
    private void ExtendBack()
    {
        // Early exit if we are at the size limit
        if(YSize >= m_MaxMapSize)
            return;

        // Save state (for undo) before changing anything
        SaveEditorState();
        YSize += 1;
        // We loop across the Y pieces, copying the one lower indexed across
        // This is due to Tidy Tile Mapper not accepting negative indices for blocks.
        for (int y = YSize - 1; y >= 0; y--)
        {
            for (int x = 0; x < XSize; x++)
            {
                for (int depth = 0; depth < (int)Layers.COUNT; depth++)
                {
                    if (y > 0)
                    {
                        var b = BlockUtilities.GetBlockAt(m_Map, x, y - 1, depth);
                        NodeTTMBridge.Instance.SetBlockInLayer(x, y, depth, b.GetComponent<EditorBlockTag>().BlockType);
                        // We need to set the layer for raycasting if it is on the current editing layer
                        if(depth == (int)CurrentLayer)
                        {
                            var b2 = BlockUtilities.GetBlockAt(m_Map, x, y, depth);
                            b2.gameObject.SetLayerRecursively("EditorRaycast");
                        }
                    }
                    else
                    {
                        SetEmptyBlock(x, y, depth);
                    }
                }
            }
        }
        // We move the position of the map to make it look like it is actually expanding back and update the camera restrictions
        m_Map.transform.SetZ(m_Map.transform.position.z - 1);
        m_MinZ -= 1;
        m_CameraController.UpdateMinMax(m_MinX, m_MaxX, m_MinZ, m_MaxZ, m_MinHeight, m_MaxHeight);
        IsDirty = true;
    }

    /// <summary>
    /// Removes a row of tiles from the back of the map
    /// </summary>
    private void RetractBack()
    {
        // Early exit if we are at the size limit
        if(YSize <= m_MinMapSize)
            return;

        // Save state (for undo) before changing anything
        SaveEditorState();
        YSize -= 1;
        // We loop across the Y pieces, copying the one higher indexed across
        // This is due to Tidy Tile Mapper not accepting negative indices for blocks.
        for (int y = 0; y < YSize + 1; y++)
        {
            for (int x = 0; x < XSize; x++)
            {
                for (int depth = 0; depth < (int)Layers.COUNT; depth++)
                {
                    if (y < YSize)
                    {
                        var b = BlockUtilities.GetBlockAt(m_Map, x, y + 1, depth);
                        NodeTTMBridge.Instance.SetBlockInLayer(x, y, depth, b.GetComponent<EditorBlockTag>().BlockType);
                        // We need to set the layer for raycasting if it is on the current editing layer
                        if (depth == (int)CurrentLayer)
                        {
                            var b2 = BlockUtilities.GetBlockAt(m_Map, x, y, depth);
                            b2.gameObject.SetLayerRecursively("EditorRaycast");
                        }
                    }
                    else
                    {
                        BlockUtilities.RemoveBlockFromMap(m_Map, x, y, depth, false, false);
                    }
                }
            }
        }
        // We move the position of the map to make it look like it is actually retracting back and update the camera restrictions
        m_Map.transform.SetZ(m_Map.transform.position.z + 1);
        m_MinZ += 1;
        m_CameraController.UpdateMinMax(m_MinX, m_MaxX, m_MinZ, m_MaxZ, m_MinHeight, m_MaxHeight);
        IsDirty = true;
    }

    /// <summary>
    /// Sets the block at the given index to be empty.  If it's in the current editing layer, it will have an outline
    /// </summary>
    /// <param name="x">X Position index of the block to make empty</param>
    /// <param name="y">Y Position index of the block to make empty</param>
    /// <param name="depth">Depth position index of the block to make empty</param>
    private void SetEmptyBlock(int x, int y, int depth)
    {
        // If the block is in the current editing layer, make it a selectable visible empty block, otherwise make it an invisible block
        NodeTTMBridge.Instance.SetBlockInLayer(x, y, depth, ((int)CurrentLayer) == depth ? Blocks.EmptySelectableEditorBlock : Blocks.EmptyInvisibleEditorBlock);
        // If the block is in the current editing layer, set it's layer to be raycastable
        if (depth == (int)CurrentLayer)
        {
            var b = BlockUtilities.GetBlockAt(m_Map, x, y, depth);
            b.gameObject.layer = LayerMask.NameToLayer("EditorRaycast");
        }
    }

    /// <summary>
    /// Updates all the boxes to the relevant empty box (invisible or selectable) and set the layers accrodingly for all blocks
    /// </summary>
    private void UpdateHitBoxes()
    {
        // Loop through all of the blocks
        for (int x = 0; x < XSize; x++)
        {
            for (int y = 0; y < YSize; y++)
            {
                for (int depth = 0; depth < (int)Layers.COUNT; depth++)
                {
                    var b = BlockUtilities.GetBlockAt(m_Map, x, y, depth);
                    var blockType = b.GetComponent<EditorBlockTag>().BlockType;
                    // If we have an empty block
                    if (blockType == Blocks.EmptyInvisibleEditorBlock || blockType == Blocks.EmptySelectableEditorBlock)
                    {
                        // Set it to invisible or selectable block according to current editing layer
                        if (depth == (int)CurrentLayer)
                        {
                            NodeTTMBridge.Instance.SetBlockInLayer(x, y, depth, Blocks.EmptySelectableEditorBlock);
                        }
                        else
                        {
                            NodeTTMBridge.Instance.SetBlockInLayer(x, y, depth, Blocks.EmptyInvisibleEditorBlock);
                        }
                        // Get the newly update block in that location
                        b = BlockUtilities.GetBlockAt(m_Map, x, y, depth);
                    }
                    // Set the layer of the blocks for each layer
                    if (depth == (int)CurrentLayer)
                    {
                        b.gameObject.layer = LayerMask.NameToLayer("EditorRaycast");
                    }
                    else
                    {
                        b.gameObject.layer = LayerMask.NameToLayer("Default");
                    }
                }
            }
        }
    }

    /// <summary>
    /// Performs a Raycast across the blocks in the scene
    /// </summary>
    private void RaycastScene()
    {
        // Create the Ray at the mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        // Only racast against our EditorRaycast layer
        int layerMask = 1 << LayerMask.NameToLayer("EditorRaycast");
        // If we hit something
        if (Physics.Raycast(ray, out hit, 500f, layerMask))
        {
            // If we didn't hit a block, we don't care, so return
            var block = hit.transform.GetComponent<Block>();
            if (block == null)
                return;
            // If the block needs changing
            if (block.GetComponent<EditorBlockTag>().BlockType != CurrentBlockPlacementType)
            {
                // Save the state (undo) before making changes
                SaveEditorState();
                int x = block.x;
                int y = block.y;
                int depth = block.depth;
                NodeTTMBridge.Instance.SetBlockInLayer(block.x, block.y, block.depth, CurrentBlockPlacementType);
                var b = BlockUtilities.GetBlockAt(m_Map, x, y, depth);
                b.gameObject.layer = LayerMask.NameToLayer("EditorRaycast");
            }
        }
    }
}
