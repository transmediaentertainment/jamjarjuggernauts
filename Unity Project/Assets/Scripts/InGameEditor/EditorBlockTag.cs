using UnityEngine;

/// <summary>
/// A basic script to attach to blocks in the in game editor
/// to identify their type easily.
/// </summary>
public class EditorBlockTag : MonoBehaviour 
{
    /// <summary>
    /// The type of block this script is attached to.
    /// </summary>
    public Blocks BlockType { get; set; }
}
