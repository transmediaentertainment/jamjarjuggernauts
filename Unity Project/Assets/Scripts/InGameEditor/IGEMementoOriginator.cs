using TransTech.Patterns.Memento;
using UnityEngine;
using DopplerInteractive.TidyTileMapper.Utilities;

/// <summary>
/// public abstract class for the Memento Originator to return.
/// </summary>
public abstract class IGEMemento : MementoObject { }

/// <summary>
/// In game editor controller's Memento implementation.
/// </summary>
public partial class InGameEditorController : IMementoOriginator<IGEMemento>
{
    /// <summary>
    /// Creates a memento object of the current state of the editor 
    /// </summary>
    /// <returns>The memento object.</returns>
    public IGEMemento CreateMemento()
    {
        return new IGEMementoImplementation(this);
    }

    /// <summary>
    /// Restores the in game editor to the state stored in the memento.
    /// </summary>
    /// <param name="memento">The memento to use to restore the state of the in game editor.</param>
    public void SetMemento(IGEMemento memento)
    {
        var imp = (IGEMementoImplementation)memento;
        var prevX = XSize;
        var prevY = YSize;
        XSize = imp.XSize;
        YSize = imp.YSize;
        m_Map.transform.position = imp.MapPos;
        m_BoardCenter = imp.BoardCenter;
        m_MinX = imp.CameraMinExtents.x;
        m_MaxX = imp.CameraMaxExtents.x;
        m_MinZ = imp.CameraMinExtents.z;
        m_MaxZ = imp.CameraMaxExtents.z;
        m_CameraController.UpdateMinMax(m_MinX, m_MaxX, m_MinZ, m_MaxZ, m_MinHeight, m_MaxHeight);

        for (int x = 0; x < (XSize > prevX ? XSize : prevX); x++)
        {
            for (int y = 0; y < (YSize > prevY ? YSize : prevY); y++)
            {
                for (int d = 0; d < (int)Layers.COUNT; d++)
                {
                    // If the board has shrunk, clean up leftovers
                    if (x >= XSize || y >= YSize)
                        BlockUtilities.RemoveBlockFromMap(m_Map, x, y, d, false, false);
                    else
                    {
                        if (d == (int)CurrentLayer)
                        {
                            // We check if this is an invisible block, and make it a selectable invisible block
                            // if we are on the selected layer
                            var block = imp.GetBlockAtPos(x, y, d);
                            if (block == Blocks.EmptyInvisibleEditorBlock)
                                block = Blocks.EmptySelectableEditorBlock;
                            NodeTTMBridge.Instance.SetBlockInLayer(x, y, d, block);
                            var b = BlockUtilities.GetBlockAt(m_Map, x, y, d);
                            b.gameObject.layer = LayerMask.NameToLayer("EditorRaycast");
                        }
                        else
                            NodeTTMBridge.Instance.SetBlockInLayer(x, y, d, imp.GetBlockAtPos(x, y, d));
                    }
                }
            }
        }
    }

    /// <summary>
    /// Private implementation of the memento class.  Kept private so that only the
    /// In Game Editor Controller knows about it's internals.
    /// </summary>
    private class IGEMementoImplementation : IGEMemento
    {
        /// <summary>
        /// X Size of the board in the editor.
        /// </summary>
        public int XSize { get; private set; }
        /// <summary>
        /// Y Size of the board in the editor
        /// </summary>
        public int YSize { get; private set; }
        /// <summary>
        /// World position of the map in the editor.
        /// </summary>
        public Vector3 MapPos { get; private set; }
        /// <summary>
        /// World position of the center of the board.
        /// </summary>
        public Vector3 BoardCenter { get; private set; }
        /// <summary>
        /// Minimum movement position of the camera
        /// </summary>
        public Vector3 CameraMinExtents { get; private set; }
        /// <summary>
        /// Maximum movement position of the camera
        /// </summary>
        public Vector3 CameraMaxExtents { get; private set; }

        /// <summary>
        /// Array of the blocks currently in the editor board
        /// </summary>
        private Blocks[, ,] m_Blocks;

        /// <summary>
        /// Returns the block at the given position
        /// </summary>
        /// <param name="x">X Position of the block</param>
        /// <param name="y">Y (Z World) position of the block</param>
        /// <param name="d">Depth position (Y World) of the block</param>
        /// <returns></returns>
        public Blocks GetBlockAtPos(int x, int y, int d)
        {
            return m_Blocks[x, y, d];
        }

        /// <summary>
        /// Constructor for initialization
        /// </summary>
        /// <param name="ige">The In Game Editor Controller object to store the state of.</param>
        public IGEMementoImplementation(InGameEditorController ige)
        {
            XSize = ige.XSize;
            YSize = ige.YSize;
            MapPos = ige.m_Map.transform.position;
            BoardCenter = ige.m_BoardCenter;
            CameraMinExtents = new Vector3(ige.m_MinX, m_MinHeight, ige.m_MinZ);
            CameraMaxExtents = new Vector3(ige.m_MaxX, m_MaxHeight, ige.m_MaxZ);

            m_Blocks = new Blocks[XSize, YSize, (int)Layers.COUNT];

            for (int x = 0; x < XSize; x++)
            {
                for (int y = 0; y < YSize; y++)
                {
                    for (int d = 0; d < (int)Layers.COUNT; d++)
                    {
                        var b = BlockUtilities.GetBlockAt(ige.m_Map, x, y, d);
                        m_Blocks[x, y, d] = b.GetComponent<EditorBlockTag>().BlockType;
                        // We force all empty blocks into the invisible block, and restore the selectable
                        // blocks based on the layer selected when the memento is reset.
                        if (m_Blocks[x, y, d] == Blocks.EmptySelectableEditorBlock)
                            m_Blocks[x, y, d] = Blocks.EmptyInvisibleEditorBlock;
                    }
                }
            }
        }
    }

    // Undo System

    /// <summary>
    /// Caretaker stack of mementos for performing undos.
    /// </summary>
    private MementoCaretaker<IGEMemento> m_UndoStack = new MementoCaretaker<IGEMemento>();
    /// <summary>
    /// Caretaker stack of mementos for performing redos.
    /// </summary>
    private MementoCaretaker<IGEMemento> m_RedoStack = new MementoCaretaker<IGEMemento>();
    /// <summary>
    /// Are we currently saving states?
    /// </summary>
    private bool m_UndoOn = true;

    /// <summary>
    /// Saves the current state of the editor.
    /// </summary>
    public void SaveEditorState()
    {
        // Don't save if the undo system isn't turned on.
        if (!m_UndoOn)
            return;
        m_UndoStack.PushMemento(CreateMemento());
        // If there is any redo's, we need to clear them here.
        m_RedoStack.Clear();
    }

    /// <summary>
    /// Perform an undo
    /// </summary>
    public void Undo()
    {
        // If there are no states to undo, just return.
        if (m_UndoStack.Count == 0)
            return;
        // We disable to undo system while undoing so we don't store the interval state.
        m_UndoOn = false;
        var m = m_UndoStack.PopMemento();
        SetMemento(m);
        // Push the state onto the redo stack
        m_RedoStack.PushMemento(m);
        m_UndoOn = true;
    }

    /// <summary>
    /// Redo on step that has been undone
    /// </summary>
    public void Redo()
    {
        // If the stack is empty, return
        if (m_RedoStack.Count == 0)
            return;
        m_UndoOn = false;
        SetMemento(m_RedoStack.PopMemento());
        m_UndoOn = true;
    }
}