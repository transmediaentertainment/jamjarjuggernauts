using PathologicalGames;
using UnityEngine;
using System.Collections.Generic;

namespace TransTech.IGE.States
{
    /// <summary>
    /// Abstract FSM Class for a state displaying a list of buttons
    /// </summary>
    public abstract class IGEMenuButtonListState : IGEMenuState
    {
        /// <summary>
        /// Reference to the Button Prefabs
        /// </summary>
        private GameObject m_ButtonPrefab;
        /// <summary>
        /// List of the buttons in our UI list
        /// </summary>
        protected List<GameObject> m_Buttons;
        /// <summary>
        /// Reference to the grid object laying out the buttons
        /// </summary>
        protected UIGrid m_Grid;

        /// <summary>
        /// Constructor for initialization
        /// </summary>
        /// <param name="menuPanel">Reference to the UIPanel the state is controlling</param>
        /// <param name="guiManager">Reference to the GUI Manager in the scene</param>
        /// <param name="fsm">Reference to the FSM the state belongs to</param>
        /// <param name="buttonPrefab">Reference to the Button prefab</param>
        public IGEMenuButtonListState(UIPanel menuPanel, InEditorGUIManager guiManager, InGameEditorUIFSMEngine fsm, GameObject buttonPrefab)
            : base(menuPanel, guiManager, fsm) 
        {
            m_ButtonPrefab = buttonPrefab;
            m_MenuPanel.gameObject.SetActive(true);
            m_Grid = m_MenuPanel.GetComponentInChildren<UIGrid>();
            m_MenuPanel.gameObject.SetActive(false);
            m_Buttons = new List<GameObject>();
        }

        /// <summary>
        /// Creates a new button and adds it to the Grid
        /// </summary>
        /// <param name="buttonLabel">The label to put on the button</param>
        protected void AddButtonToGrid(string buttonLabel)
        {
            var go = PoolManager.Pools["GUI"].Spawn(m_ButtonPrefab.transform).gameObject;
            m_Buttons.Add(go);
            var index = m_Buttons.Count - 1;
            var label = go.GetComponentInChildren<UILabel>();

            label.text = buttonLabel;
            // Tell the button it's index within our collection
            var messenger = go.GetComponent<PopupButtonMessenger>();
            messenger.Init(index);
            messenger.ButtonPressed += HandleButtonPressed;
            go.transform.parent = m_Grid.transform;
            go.transform.localScale = Vector3.one;
        }

        /// <summary>
        /// Called by the FSM when exiting the state
        /// </summary>
        public override void Exit()
        {
            base.Exit();

            for (int i = 0; i < m_Buttons.Count; i++)
            {
                var messenger = m_Buttons[i].GetComponent<PopupButtonMessenger>();
                messenger.ButtonPressed -= HandleButtonPressed;
                m_Buttons[i].transform.parent = null;
                m_Buttons[i].GetComponent<UIButton>().isEnabled = true;
                PoolManager.Pools["GUI"].Despawn(m_Buttons[i].transform);
                m_Buttons[i] = null;
            }
            m_Buttons.Clear();
        }

        /// <summary>
        /// Abstract signature for Handling a button in the grid being pressed
        /// </summary>
        /// <param name="buttonNumber">Index of the button that was pressed.</param>
        protected abstract void HandleButtonPressed(int buttonNumber);
    }
}
