using UnityEngine;
using TransTech.System.IO;

namespace TransTech.IGE.States
{
    /// <summary>
    /// FSM State for displaying the Load Menu in the Editor
    /// </summary>
    public class IGELoadState : IGEMenuButtonListState
    {
        /// <summary>
        /// Array of all the available levels to load from disk
        /// </summary>
        private string[] m_Levels;

        /// <summary>
        /// Constructor for Initialization
        /// </summary>
        /// <param name="menuPanel">Reference to the UIPanel the state is controlling</param>
        /// <param name="guiManager">Reference to the GUI Manager in the scene</param>
        /// <param name="fsm">Reference to the FSM the state belongs to</param>
        /// <param name="buttonPrefab">Reference to the Button prefab</param>
        public IGELoadState(UIPanel menuPanel, InEditorGUIManager guiManager, InGameEditorUIFSMEngine fsm, GameObject buttonPrefab)
            : base(menuPanel, guiManager, fsm, buttonPrefab) 
        {
        }

        /// <summary>
        /// Called by the FSM System when entering the state
        /// </summary>
        /// <param name="args">Optional arguments to pass to the state (none)</param>
        public override void Enter(params object[] args)
        {
            base.Enter(args);

            m_Levels = LevelFileManager.GetLevelList();

            for (int i = 0; i < m_Levels.Length; i++)
            {
                var splits = m_Levels[i].Split('\\', '/', '.');
                AddButtonToGrid(splits[splits.Length - 2]);
            }

            m_Grid.Reposition();

            m_GUIManager.CancelLoadMapButtonPressedEvent += HandleCancelButtonPressed;
        }

        /// <summary>
        /// Called by the FSM when exiting the state
        /// </summary>
        public override void Exit()
        {
            base.Exit();

            m_GUIManager.CancelLoadMapButtonPressedEvent -= HandleCancelButtonPressed;
        }

        /// <summary>
        /// Handles a button being pressed on this popup
        /// </summary>
        /// <param name="buttonNumber">The index of the button being pressed</param>
        protected override void HandleButtonPressed(int buttonNumber)
        {
            var splits = m_Levels[buttonNumber].Split('\\', '/', '.');
            m_FSM.LoadLevel(splits[splits.Length - 2]);
        }

        /// <summary>
        /// Handles the Cancel button on the panel being pressed
        /// </summary>
        private void HandleCancelButtonPressed()
        {
            m_FSM.ShowMenu();
        }
    }
}
