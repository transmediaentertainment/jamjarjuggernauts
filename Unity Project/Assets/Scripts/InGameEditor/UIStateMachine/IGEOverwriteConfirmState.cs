using TransTech.System.Debug;

namespace TransTech.IGE.States
{
    /// <summary>
    /// FSM State for displaying a popup confirming the overwrite of a local file
    /// </summary>
    public class IGEOverwriteConfirmState : IGEMenuState
    {
        /// <summary>
        /// Constructor for initialization
        /// </summary>
        /// <param name="menuPanel">Reference to the UIPanel the state is controlling</param>
        /// <param name="guiManager">Reference to the GUI Manager in the scene</param>
        /// <param name="fsm">Reference to the FSM the state belongs to</param>
        public IGEOverwriteConfirmState(UIPanel menuPanel, InEditorGUIManager guiManager, InGameEditorUIFSMEngine fsm)
            : base(menuPanel, guiManager, fsm) { }

        /// <summary>
        /// The name of the level being saves
        /// </summary>
        private string m_LevelName;

        /// <summary>
        /// Called by the FSM System when entering the state
        /// </summary>
        /// <param name="args">Optional arguments to pass to the state (none)</param>
        public override void Enter(params object[] args)
        {
            base.Enter(args);

            var popup = m_MenuPanel.GetComponent<Popup>();
            popup.PopupButtonPressedEvent += HandlePopupButtonPress;

            if (args == null || args.Length != 1 || !(args[0] is string))
            {
                TTDebug.LogError("IGEOverwriteConfirmState.cs : Invalid arguments passed to Enter");
                return;
            }

            m_LevelName = args[0] as string;
        }

        /// <summary>
        /// Handles a button on the popup being pressed
        /// </summary>
        /// <param name="button">Index of the button being pressed</param>
        private void HandlePopupButtonPress(int button)
        {
            switch (button)
            {
                case 0: // Yes overwrite
                    m_FSM.ConfirmOverwrite(m_LevelName);
                    break;
                case 1: // Don't overwrite
                    m_FSM.DontOverwrite();
                    break;
                default:
                    TTDebug.LogError("IGEOverwriteConfirmState.cs : Invalid button number sent to handler : " + button);
                    break;
            }
        }

        /// <summary>
        /// Called by the FSM when exiting the state
        /// </summary>
        public override void Exit()
        {
            base.Exit();

            var popup = m_MenuPanel.GetComponent<Popup>();
            popup.PopupButtonPressedEvent -= HandlePopupButtonPress;
        }
    }
}
