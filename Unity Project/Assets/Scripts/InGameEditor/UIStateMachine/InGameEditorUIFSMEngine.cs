using UnityEngine;
using System.Collections.Generic;
using TransTech.FiniteStateMachine;
using TransTech.Delegates;

namespace TransTech.IGE.States
{
    /// <summary>
    /// FSM Engine driving the UI in the In Game Editor
    /// </summary>
	public class InGameEditorUIFSMEngine : FSM
	{
        /// <summary>
        /// FSMState for displaying the menu
        /// </summary>
		private IGEMenuState m_MenuState;
        /// <summary>
        /// FSMState for displaying the save menu
        /// </summary>
		private IGESaveState m_SaveState;
        /// <summary>
        /// FSMState for displaying the load menu
        /// </summary>
		private IGELoadState m_LoadState;
        /// <summary>
        /// FSMState for displaying the Unsaved Data popup
        /// </summary>
		private IGEUnsavedState m_UnsavedState;
        /// <summary>
        /// FSMState for getting the user to confirm overwriting a file
        /// </summary>
		private IGEOverwriteConfirmState m_OverwriteConfirmState;
        /// <summary>
        /// FSMState for downloading maps from the server
        /// </summary>
        private IGEDownloadMapsState m_DownloadMapsState;
        /// <summary>
        /// Default idle FSM state
        /// </summary>
        private FSMState m_IdleState;

        /// <summary>
        /// Reference to the editor controller
        /// </summary>
        private InGameEditorController m_IGEC;
        /// <summary>
        /// Reference to the GUI Manager
        /// </summary>
        private InEditorGUIManager m_GUIManager;

        /// <summary>
        /// State to go to after a check state is performed
        /// </summary>
        private FSMState m_AfterCheckState = null;
        /// <summary>
        /// List of functions to call before entering the AfterCheck state
        /// </summary>
        private List<VoidDelegate> m_AfterCheckStateFunctions = new List<VoidDelegate>();

		/// <summary>
		/// Constructor for Initialization
		/// </summary>
		/// <param name="igec">Reference to the Editor Controller</param>
		/// <param name="guiManager">Reference to the scene's GUI Manager</param>
		/// <param name="menuPanel">Reference to the UIPanel for the Menu</param>
		/// <param name="savePanel">Reference to the UIPanel for the Save menu</param>
        /// <param name="loadPanel">Reference to the UIPanel for the Load menu</param>
        /// <param name="unsavedPanel">Reference to the UIPanel for the Unsaved Data popup</param>
        /// <param name="overwritePanel">Reference to the UIPanel for the overwrite confirmation popup</param>
        /// <param name="downloadPanel">Reference to the UIPanel for the download menu</param>
		/// <param name="buttonPrefab">Reference to the Button prefab</param>
		/// <param name="busyIconPrefab">Reference to the Busy Icon prefab</param>
		public InGameEditorUIFSMEngine( InGameEditorController igec, InEditorGUIManager guiManager, UIPanel menuPanel, UIPanel savePanel, UIPanel loadPanel, UIPanel unsavedPanel, UIPanel overwritePanel, UIPanel downloadPanel, GameObject buttonPrefab, GameObject busyIconPrefab )
		{
            m_IGEC = igec;
            m_GUIManager = guiManager;
            m_IdleState = new IdleFSMState();
            m_MenuState = new IGEMenuState(menuPanel, m_GUIManager, this);
            m_SaveState = new IGESaveState(savePanel, m_GUIManager, this);
            m_LoadState = new IGELoadState(loadPanel, m_GUIManager, this, buttonPrefab);
            m_UnsavedState = new IGEUnsavedState(unsavedPanel, m_GUIManager, this);
            m_OverwriteConfirmState = new IGEOverwriteConfirmState(overwritePanel, m_GUIManager, this);
            m_DownloadMapsState = new IGEDownloadMapsState(downloadPanel, m_GUIManager, this, buttonPrefab, busyIconPrefab);

            NewState(m_IdleState);
		}

        /// <summary>
        /// Sets the state to the one stored in our After Check variable
        /// </summary>
        private void GoToAfterCheckState()
        {
            // If we haven't stored a state, go Idle
            if (m_AfterCheckState == null)
                NewState(m_IdleState);
            else
                NewState(m_AfterCheckState);
            // Call all of the stored functions
            foreach (var func in m_AfterCheckStateFunctions)
                func();
            // Clear stored data
            m_AfterCheckState = null;
            m_AfterCheckStateFunctions.Clear();
        }

        /// <summary>
        /// Shows the Menu panel
        /// </summary>
        public void ShowMenu()
        {
            NewState(m_MenuState);
        }

        /// <summary>
        /// Closes UI windows and returns to Editor
        /// </summary>
        public void ReturnToEditor()
        {
            NewState(m_IdleState);
        }

        /// <summary>
        /// Shows the Load Map menu
        /// </summary>
        public void ShowLoadMenu()
        {
            // If there is unsaved data, prompt the user
            if (m_IGEC.IsDirty)
            {
                m_AfterCheckState = m_LoadState;
                NewState(m_UnsavedState, (VoidDelegate)GoToAfterCheckState, (VoidDelegate)(() => NewState(m_SaveState)),
                    (VoidDelegate)(() => NewState(m_MenuState)));
            }
            else
                NewState(m_LoadState);
        }

        /// <summary>
        /// Show the Save menu
        /// </summary>
        public void ShowSaveMenu()
        {
            NewState(m_SaveState);
        }

        /// <summary>
        /// Creates a new map
        /// </summary>
        public void CreateNewMap()
        {
            // If there is unsaved data, prompt the user
            if (m_IGEC.IsDirty)
            {
                m_AfterCheckState = m_IdleState;
                m_AfterCheckStateFunctions.Add(() => m_IGEC.NewMap("Map", 4, 4));
                NewState(m_UnsavedState, (VoidDelegate)GoToAfterCheckState,
                    (VoidDelegate)(() => NewState(m_SaveState)), (VoidDelegate)(() => NewState(m_MenuState)));
            }
            else
            {
                m_IGEC.NewMap("Map", 4, 4);
                NewState(m_IdleState);
            }
        }

        /// <summary>
        /// Returns to the main menu of the game
        /// </summary>
        public void ReturnToMainMenu()
        {
            // If there is unsaved data, prompt the user
            if (m_IGEC.IsDirty)
            {
                m_AfterCheckState = m_IdleState;
                m_AfterCheckStateFunctions.Add(() => Application.LoadLevel("MainMenu"));
                NewState(m_UnsavedState, (VoidDelegate)GoToAfterCheckState, (VoidDelegate)(() => NewState(m_SaveState)),
                    (VoidDelegate)(() => NewState(m_MenuState)));
            }
            else
                Application.LoadLevel("MainMenu");
        }

        /// <summary>
        /// Shows the popup to get confirmation on overwriting an existing file
        /// </summary>
        /// <param name="levelName"></param>
        public void ShowConfirmOverwritePopup(string levelName)
        {
            PushState(m_OverwriteConfirmState, levelName);
        }

        /// <summary>
        /// Overwrites the given file
        /// </summary>
        /// <param name="levelName">Name of the file to overwrite</param>
        public void ConfirmOverwrite(string levelName)
        {
            SaveLevel(levelName);
        }

        /// <summary>
        /// Handles canceling of overwriting
        /// </summary>
        public void DontOverwrite()
        {
            PopState();
        }

        /// <summary>
        /// Saves the level to disk.  Overwrites any existing data
        /// </summary>
        /// <param name="levelName">Name of the level/file to save</param>
        public void SaveLevel(string levelName)
        {
            m_IGEC.SaveMapToDisk(levelName);
            GoToAfterCheckState();
        }

        /// <summary>
        /// Loads the give level.  
        /// </summary>
        /// <param name="levelName">Name of the level to load</param>
        public void LoadLevel(string levelName)
        {
            m_IGEC.LoadMapFromDisk(levelName);
            NewState(m_IdleState);
        }

        /// <summary>
        /// Shows the Download Map menu
        /// </summary>
        public void ShowDownloadList()
        {
            NewState(m_DownloadMapsState);
        }
	}
}