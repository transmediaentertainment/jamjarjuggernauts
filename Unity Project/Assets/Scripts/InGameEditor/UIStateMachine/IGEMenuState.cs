using TransTech.FiniteStateMachine;
using UnityEngine;

namespace TransTech.IGE.States
{
    /// <summary>
    /// FSM State for displaying the Menu Panel in the Editor.  Used as a 
    /// base class for all of the other FSM states.
    /// </summary>
    public class IGEMenuState : FSMState
    {
        /// <summary>
        /// Reference to the UIPanel the state is displaying
        /// </summary>
        protected UIPanel m_MenuPanel;
        /// <summary>
        /// Reference to the scene's GUI Manager
        /// </summary>
        protected InEditorGUIManager m_GUIManager;
        /// <summary>
        /// Reference to the parent FSM Engine
        /// </summary>
        protected InGameEditorUIFSMEngine m_FSM;

        /// <summary>
        /// Constructor for initialization
        /// </summary>
        /// <param name="menuPanel">Reference to the UIPanel the state is controlling</param>
        /// <param name="guiManager">Reference to the GUI Manager in the scene</param>
        /// <param name="fsm">Reference to the FSM the state belongs to</param>
        public IGEMenuState(UIPanel menuPanel, InEditorGUIManager guiManager, InGameEditorUIFSMEngine fsm)
        {
            m_MenuPanel = menuPanel;
            m_MenuPanel.gameObject.SetActive(false);
            m_GUIManager = guiManager;
            m_FSM = fsm;
        }

        /// <summary>
        /// Called by the FSM System when entering the state
        /// </summary>
        /// <param name="args">Optional arguments to pass to the state (none)</param>
        public override void Enter(params object[] args)
        {
            m_MenuPanel.gameObject.SetActive(true);
            m_GUIManager.MainMenuButtonPressedEvent += HandleMainMenuButtonPressed;
            m_GUIManager.BackToEditorButtonPressedEvent += HandleBackToEditorButtonPressed;
            m_GUIManager.TutorialButtonPressedEvent += HandleTutorialButtonPressed;
            m_GUIManager.NewMapButtonPressedEvent += HandleNewMapButtonPressed;
            m_GUIManager.LoadMapButtonPressedEvent += HandleLoadMapButtonPressed;
            m_GUIManager.OpenSaveMenuButtonPressedEvent += HandleOpenSaveMenuButtonPressed;
            m_GUIManager.DownloadMapsButtonPressedEvent += HandleDownloadMapdButtonPressed;
        }

        /// <summary>
        /// Called by the FSM when exiting the state
        /// </summary>
        public override void Exit()
        {
            m_MenuPanel.gameObject.SetActive(false);
            m_GUIManager.MainMenuButtonPressedEvent -= HandleMainMenuButtonPressed;
            m_GUIManager.BackToEditorButtonPressedEvent -= HandleBackToEditorButtonPressed;
            m_GUIManager.TutorialButtonPressedEvent -= HandleTutorialButtonPressed;
            m_GUIManager.NewMapButtonPressedEvent -= HandleNewMapButtonPressed;
            m_GUIManager.LoadMapButtonPressedEvent -= HandleLoadMapButtonPressed;
            m_GUIManager.OpenSaveMenuButtonPressedEvent -= HandleOpenSaveMenuButtonPressed;
            m_GUIManager.DownloadMapsButtonPressedEvent -= HandleDownloadMapdButtonPressed;
        }

        /// <summary>
        /// Hooked up to the Back To Editor button pressed event
        /// </summary>
        private void HandleBackToEditorButtonPressed()
        {
            m_FSM.ReturnToEditor();
        }

        /// <summary>
        /// Hooked up to the Tutorial button pressed event
        /// </summary>
        private void HandleTutorialButtonPressed()
        {
            // TODO : Tutorial system for Editor
        }

        /// <summary>
        /// Hooked up to the Load Map button pressed event
        /// </summary>
        private void HandleLoadMapButtonPressed()
        {
            m_FSM.ShowLoadMenu();
        }

        /// <summary>
        /// Hooked up to the Open Save Menu button pressed event
        /// </summary>
        private void HandleOpenSaveMenuButtonPressed()
        {
            m_FSM.ShowSaveMenu();
        }

        /// <summary>
        /// Hooked up to the New Map button pressed event
        /// </summary>
        private void HandleNewMapButtonPressed()
        {
            m_FSM.CreateNewMap();
        }

        /// <summary>
        /// Hooked up to the Main Menu button pressed event
        /// </summary>
        private void HandleMainMenuButtonPressed()
        {
            m_FSM.ReturnToMainMenu();
        }

        /// <summary>
        /// Hooked up to the Download Map button pressed event
        /// </summary>
        private void HandleDownloadMapdButtonPressed()
        {
            m_FSM.ShowDownloadList();
        }
    }
}
