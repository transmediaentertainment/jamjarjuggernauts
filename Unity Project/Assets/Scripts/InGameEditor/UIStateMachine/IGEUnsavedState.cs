using TransTech.Delegates;
using TransTech.System.Debug;

namespace TransTech.IGE.States
{
    /// <summary>
    /// FSM State for telling the user there is unsaved data
    /// </summary>
    public class IGEUnsavedState : IGEMenuState
    {
        /// <summary>
        /// Constructor for initialization
        /// </summary>
        /// <param name="menuPanel">Reference to the UIPanel the state is controlling</param>
        /// <param name="guiManager">Reference to the GUI Manager in the scene</param>
        /// <param name="fsm">Reference to the FSM the state belongs to</param>
        public IGEUnsavedState(UIPanel menuPanel, InEditorGUIManager guiManager, InGameEditorUIFSMEngine fsm) 
            : base(menuPanel, guiManager, fsm) { }

        /// <summary>
        /// Delegate called when Button zero is pressed
        /// </summary>
        private VoidDelegate m_ButtonZeroPressed;
        /// <summary>
        /// Delegate called when Button one is pressed
        /// </summary>
        private VoidDelegate m_ButtonOnePressed;
        /// <summary>
        /// Delegate called when Button Two is pressed
        /// </summary>
        private VoidDelegate m_ButtonTwoPressed;

        /// <summary>
        /// Called by the FSM System when entering the state
        /// </summary>
        /// <param name="args">Optional arguments to pass to the state (VoidDelegate, VoidDelegate, VoidDelegate)</param>
        public override void Enter(params object[] args)
        {
            base.Enter(args);

            if (args == null || args.Length != 3 || !(args[0] is VoidDelegate) || !(args[1] is VoidDelegate)
                || !(args[2] is VoidDelegate))
            {
                TTDebug.LogError("IGEUnsavedState.cs : Incorrect arguments passed into Enter Function");
            }

            m_ButtonZeroPressed = args[0] as VoidDelegate;
            m_ButtonOnePressed = args[1] as VoidDelegate;
            m_ButtonTwoPressed = args[2] as VoidDelegate;

            var popup = m_MenuPanel.GetComponent<Popup>();
            popup.PopupButtonPressedEvent += HandlePopupButtonPressed;
        }

        /// <summary>
        /// Handles a button being pressed in the popup
        /// </summary>
        /// <param name="button">Index of the button pressed</param>
        private void HandlePopupButtonPressed(int button)
        {
            TTDebug.Log("Popup button pressed : " + button);
            switch (button)
            {
                case 0: // Discard Changes
                    m_ButtonZeroPressed();
                    break;
                case 1: // Save Map First
                    m_ButtonOnePressed();
                    break;
                case 2: // Cancel
                    m_ButtonTwoPressed();
                    break;
                default:
                    TTDebug.LogError("IGEUnsavedState.cs : Invalid button number passed into handle function : " + button);
                    return;
            }
        }

        /// <summary>
        /// Called by the FSM when exiting the state
        /// </summary>
        public override void Exit()
        {
            base.Exit();
            var popup = m_MenuPanel.GetComponent<Popup>();
            popup.PopupButtonPressedEvent -= HandlePopupButtonPressed;
        }
    }
}
