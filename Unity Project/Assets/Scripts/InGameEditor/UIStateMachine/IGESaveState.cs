using TransTech.System.IO;
using TransTech.System.Debug;

namespace TransTech.IGE.States
{
    /// <summary>
    /// FSM State for displaying the Save Menu window
    /// </summary>
    public class IGESaveState : IGEMenuState
    {
        /// <summary>
        /// Constructor for initialization
        /// </summary>
        /// <param name="menuPanel">Reference to the UIPanel the state is controlling</param>
        /// <param name="guiManager">Reference to the GUI Manager in the scene</param>
        /// <param name="fsm">Reference to the FSM the state belongs to</param>
        public IGESaveState(UIPanel menuPanel, InEditorGUIManager guiManager, InGameEditorUIFSMEngine fsm) 
            : base(menuPanel, guiManager, fsm) 
        {
            menuPanel.gameObject.SetActive(true);
            m_TextInput = menuPanel.GetComponentInChildren<UIInput>();
            menuPanel.gameObject.SetActive(false);
        }

        /// <summary>
        /// The name of the level being saves
        /// </summary>
        private string m_LevelName;
        /// <summary>
        /// Reference to the UIInput the user types the name of the level into
        /// </summary>
        private UIInput m_TextInput;

        /// <summary>
        /// Called by the FSM System when entering the state
        /// </summary>
        /// <param name="args">Optional arguments to pass to the state (none)</param>
        public override void Enter(params object[] args)
        {
            base.Enter(args);

            m_GUIManager.SaveMapButtonPressedEvent += HandleSaveButtonPressed;
            m_GUIManager.OnSubmitSaveMapEvent += HandleInputSubmit;
            m_GUIManager.CancelSaveMapButtonPressedEvent += HandleCancelButtonPressed;
        }

        /// <summary>
        /// Called by the FSM when exiting the state
        /// </summary>
        public override void Exit()
        {
            base.Exit();

            m_GUIManager.SaveMapButtonPressedEvent -= HandleSaveButtonPressed;
            m_GUIManager.OnSubmitSaveMapEvent -= HandleInputSubmit;
            m_GUIManager.CancelSaveMapButtonPressedEvent -= HandleCancelButtonPressed;
        }

        /// <summary>
        /// Called by the FSM when the state loses focus
        /// </summary>
        public override void LostFocus()
        {
            base.LostFocus();

            m_MenuPanel.gameObject.SetActive(false);
        }

        /// <summary>
        /// Called by the FSM when the state regains focus
        /// </summary>
        public override void GainedFocus()
        {
            base.GainedFocus();

            m_MenuPanel.gameObject.SetActive(true);
        }

        /// <summary>
        /// Hooked up to the Save button pressed event
        /// </summary>
        private void HandleSaveButtonPressed()
        {
            // Get the name out of the UIInput
            m_LevelName = m_TextInput.value;
			if( m_LevelName == null || m_LevelName.Length == 0 || m_LevelName == "" )
			{
				TTDebug.LogWarning( "IGESaveState.cs : Blank name entered, discarding it." );
				return;
			}
			
            // Check to see if a file with that name already exists
            var currentLevels = LevelFileManager.GetLevelList();
            bool found = false;
            var fileName = m_LevelName + ".lvl";
            foreach (var level in currentLevels)
            {
                TTDebug.Log("Checking against : " + level);
                var splits = level.Split('/', '\\');
                if (splits[splits.Length - 1] == fileName)
                {
                    found = true;
                    break;
                }
            }
            if (found)
            {
                // Go into overwrite state
                m_FSM.ShowConfirmOverwritePopup(m_LevelName);
            }
            else
            {
                //save
                m_FSM.SaveLevel(m_LevelName);
            }
        }

        /// <summary>
        /// Handles the UIInput submit event
        /// </summary>
        /// <param name="levelName"></param>
        private void HandleInputSubmit(string levelName)
        {
            HandleSaveButtonPressed();
        }

        /// <summary>
        /// Hooked up to the Cancel button pressed event
        /// </summary>
        private void HandleCancelButtonPressed()
        {
            m_FSM.ShowMenu();
        }
    }
}
