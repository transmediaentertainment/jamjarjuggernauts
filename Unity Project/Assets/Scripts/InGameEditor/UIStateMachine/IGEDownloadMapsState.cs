using PathologicalGames;
using UnityEngine;
using TransTech.System.IO;
using System.Collections.Generic;
using UnityExtensions;
using System.Collections;
using Parse;

namespace TransTech.IGE.States
{
    /// <summary>
    /// FSM State for displaying the Download Maps window
    /// </summary>
    public class IGEDownloadMapsState : IGEMenuButtonListState
    {
        /// <summary>
        /// Reference to the Busy Icon prefab
        /// </summary>
        private GameObject m_BusyIconPrefab;
        /// <summary>
        /// Our instance of the Busy Icon prefab (when we are using it)
        /// </summary>
        private GameObject m_BusyIcon;
        /// <summary>
        /// Array of all of the Parse Objects referencing files on the server
        /// </summary>
        //private ParseInstance[] m_ServerInstances;
        private ParseObject[] m_ServerInstances;

        /// <summary>
        /// Constructor for initialization
        /// </summary>
        /// <param name="menuPanel">Reference to the UIPanel the state is controlling</param>
        /// <param name="guiManager">Reference to the GUI Manager in the scene</param>
        /// <param name="fsm">Reference to the FSM the state belongs to</param>
        /// <param name="buttonPrefab">Reference to the Button prefab</param>
        /// <param name="busyIconPrefab">Reference to the Busy Icon prefab</param>
        public IGEDownloadMapsState(UIPanel menuPanel, InEditorGUIManager guiManager, InGameEditorUIFSMEngine fsm, GameObject buttonPrefab, GameObject busyIconPrefab)
            : base(menuPanel, guiManager, fsm, buttonPrefab)
        {
            m_BusyIconPrefab = busyIconPrefab;
        }

        /// <summary>
        /// Called by the FSM System when entering the state
        /// </summary>
        /// <param name="args">Optional arguments to pass to the state (none)</param>
        public override void Enter(params object[] args)
        {
            base.Enter(args);

            m_BusyIcon = PoolManager.Pools["GUI"].Spawn(m_BusyIconPrefab.transform).gameObject;
            m_BusyIcon.transform.parent = m_MenuPanel.transform;
            m_BusyIcon.transform.localPosition = Vector3.back;
            m_BusyIcon.transform.localScale = Vector3.one * 100f;
            m_BusyIcon.SetLayerRecursively("GUI");

            LevelFileManager.GetLevelFileListFromServer(GetListFromServer);

            m_GUIManager.CancelDownloadMapsButtonPressedEvent += HandleCancelButtonPressed;
        }

        /// <summary>
        /// Called on completion of retrieving the file data from the server
        /// </summary>
        /// <param name="success">Was the retrieval successful</param>
        /// <param name="collection">The collection of Parse Objects returned from the server</param>
        private void GetListFromServer(bool success, IEnumerable collection)
        {
            // Destroy the busy icon because we are done now
            PoolManager.Pools["GUI"].Despawn(m_BusyIcon.transform);

            if (!success)
            {
                // TODO : Show message for failure to get list
            }
            else
            {
                // Get local file list, and only show the files we don't have.
                int count = 0;
                var enumerator = collection.GetEnumerator();
                while (enumerator.MoveNext())
                    count++;
                m_ServerInstances = new ParseObject[count];
                count = 0;
                foreach (var c in collection)
                {
                    m_ServerInstances[count] = (ParseObject)c;
                    count++;
                }
                for (int i = 0; i < m_ServerInstances.Length; i++)
                {
                    AddButtonToGrid(m_ServerInstances[i].Get<string>("name"));
                }
            }

            m_Grid.Reposition();
        }

        /// <summary>
        /// Handles a button being pressed on this popup
        /// </summary>
        /// <param name="buttonNumber">The index of the button being pressed</param>
        protected override void HandleButtonPressed(int buttonNumber)
        {
            // Spawn the busy icon on the button to show we are downloading the item
            var busyIcon = PoolManager.Pools["GUI"].Spawn(m_BusyIconPrefab.transform).gameObject;
            busyIcon.transform.parent = m_Buttons[buttonNumber].transform;
            busyIcon.transform.localScale = Vector3.one * 50f;
            busyIcon.transform.localPosition = Vector3.back;
            busyIcon.SetLayerRecursively("GUI");
            // Begin the download of the file
            LevelFileManager.DownloadFileFromServer(m_ServerInstances[buttonNumber],
                (b, s) => {
                    m_Buttons[buttonNumber].GetComponent<UIButton>().isEnabled = !b;
                    busyIcon.transform.parent = null;
                    PoolManager.Pools["GUI"].Despawn(busyIcon.transform);
                });
        }

        /// <summary>
        /// Called by the FSM when exiting the state
        /// </summary>
        public override void Exit()
        {
            base.Exit();
            m_GUIManager.CancelDownloadMapsButtonPressedEvent -= HandleCancelButtonPressed;
        }

        /// <summary>
        /// Handles the Cancel button on the panel being pressed
        /// </summary>
        private void HandleCancelButtonPressed()
        {
            m_FSM.ShowMenu();
        }
    }
}