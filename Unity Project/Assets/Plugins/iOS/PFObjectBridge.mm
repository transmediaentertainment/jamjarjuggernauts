//
//  PFObjectBridge.m
//  UnityParseBridge
//
//  Created by Simon on 6/03/13.
//  Copyright (c) 2013 Transmedia Entertainment. All rights reserved.
//

#import "PFObjectBridge.h"

@interface PFObjectBridge()

@property (nonatomic, strong) NSMutableDictionary* currentPFObjects;
@property (nonatomic, strong) NSNumber* idCounter;

@end


@implementation PFObjectBridge

- (id)init
{
    self = [super init];
    if (self)
    {
        _currentPFObjects = [[NSMutableDictionary alloc] init];
        _idCounter = 0;
    }   
    return self;
}

- (void)dealloc
{
    self.currentPFObjects = nil;
    self.idCounter = nil;
    [super dealloc];
}

- (int)createNewPFObjectWithClassName:(NSString *)className
{
    _idCounter = @([_idCounter intValue] + 1);
    PFObject* newObject = [PFObject objectWithClassName:className];
    [_currentPFObjects setObject:newObject forKey:_idCounter];
    return [_idCounter intValue];
}

- (NSString*)getPFObjectAsString:(PFObject*)pfObject withMessage:(NSString*)message withId:(NSNumber*)Id
{
    NSMutableString* returnString = [NSMutableString stringWithFormat:@"%@|%d", message, [Id intValue]];
    NSArray* keys = [pfObject allKeys];
    for (int i = 0; i < [keys count]; i++)
    {
        NSString *key = [keys objectAtIndex:i];
        id obj = [pfObject objectForKey:key];
        NSString *classType = NSStringFromClass([obj class]);
        
        // Supported types are NSNumber, NSString, NSDate, NSData, NSArray, NSDictionary
        if([obj isKindOfClass:[NSNumber class]])
        {
            NSNumber *num = (NSNumber*)obj;
            returnString = [returnString stringByAppendingFormat:@"|%@|%@|%@", key, classType, [num stringValue]];
        }
        else if([obj isKindOfClass:[NSString class]])
        {
            NSString *str = (NSString*)obj;
            returnString = [returnString stringByAppendingFormat:@"|%@|%@|%@", key, classType, str];
        }
        else if([obj isKindOfClass:[NSDate class]])
        {
            NSDate *date = (NSDate*)obj;
            returnString = [returnString stringByAppendingFormat:@"|%@|%@|%@", key, classType, [date description]];
        }
        else if([obj isKindOfClass:[NSData class]])
        {
            returnString = [returnString stringByAppendingFormat:@"|%@|%@|NSDataNotSupportedYet", key, classType];
        }
        else if([obj isKindOfClass:[NSArray class]])
        {
            returnString = [returnString stringByAppendingFormat:@"|%@|%@|NSArrayNotSupportedYet", key, classType];
        }
        else if([obj isKindOfClass:[NSDictionary class]])
        {
            returnString = [returnString stringByAppendingFormat:@"|%@|%@|NSDictionaryNotSupportedYet", key, classType];
        }
        else
        {
            returnString = [returnString stringByAppendingFormat:@"|%@|UnknownClass|NotSupported", key];
        }
    }
    return returnString;
}

- (int)fetchExistingPFObjectFromServer:(NSString *)serverId withClassName:(NSString *) className 
{
    _idCounter = @([_idCounter intValue] + 1);
    PFQuery *query = [PFQuery queryWithClassName:className];
    
    __block NSNumber* objectIdBridge = [_idCounter copy];
    [query getObjectInBackgroundWithId:serverId block:^(PFObject* obj, NSError *error) {
        if(!error) {
            // Success
            [_currentPFObjects setObject:obj forKey:objectIdBridge];
            NSString *message = [self getPFObjectAsString:obj withMessage:@"fetchObjectSuccessful" withId:objectIdBridge];
            UnitySendMessage("MUMR", "ReceiveiOSMessage", [message UTF8String]);
        }
        else {
            NSLog(@"Error: %@ %@", error, [error userInfo]);
            NSString *message = [NSString stringWithFormat:@"fetchObjectFailed|%d|%@", [objectIdBridge intValue], [error userInfo]];
            UnitySendMessage("MUMR", "ReceiveiOSMessahe", [message UTF8String]);
        }
    }];
    return [_idCounter intValue];
}

- (BOOL)setObject:(id)object forKey:(NSString *)key forObjectId:(int)objectId
{
    NSNumber* num = @(objectId);
    
    int count = [_currentPFObjects count];
    NSLog(@"count: %d",count);
    
    PFObject* pfObject = [_currentPFObjects objectForKey:num];
    if( pfObject == nil )
    {
        NSLog(@"PFObjectBridge.mm : Error: Invalid ObjectID: %d", objectId);
        return NO;
    }
    
    [pfObject setObject:object forKey:key];
    return YES;
}

- (BOOL)saveObjectEventuallyWithId:(int)objectId
{
    PFObject* pfObject = [_currentPFObjects objectForKey:@(objectId)];
    if( pfObject == nil )
    {
        NSLog(@"PFObjectBridge.mm : Error: Invalid ObjectID: %d", objectId);
        return NO;
    }
    
    __block NSNumber* objectIdBridge = @(objectId);
    
    [pfObject saveEventually:^(BOOL succeeded, NSError *error) {
        //@autoreleasepool
        //{
            if (!error)
            {
                NSString* message = [NSString stringWithFormat:@"saveEventuallySucceeded|%d", [objectIdBridge intValue]];
                UnitySendMessage("MUMR", "ReceiveiOSMessage", [message UTF8String] );
            }
            else
            {
                NSLog(@"Error: %@ %@", error, [error userInfo]);
                NSString* message = [NSString stringWithFormat:@"saveEventuallyFailed|%d|%@", [objectIdBridge intValue], [error userInfo]];
                UnitySendMessage("MUMR", "ReceiveiOSMessage", [message UTF8String] );
            }
        //}
    }];
    return YES;
}

- (NSString*)getServerIdOfObject:(int)objectId
{
    PFObject *pfObject = [_currentPFObjects objectForKey:@(objectId)];
    if(pfObject == nil)
    {
        NSLog(@"PFObjectBridge.mm : Error: Invalid ObjectID: %d", objectId);
        return @"";
    }
    
    return pfObject.objectId;
}

- (void)destroyWhenUpdatedWithId:(int)objectId
{
    [_currentPFObjects removeObjectForKey:@(objectId)];
}

@end

static PFObjectBridge* objectBridge = nil;

// Converts C style string to NSString
NSString* CreateNSString (const char* string)
{
	if (string)
		return [NSString stringWithUTF8String: string];
	else
		return [NSString stringWithUTF8String: ""];
}

// Helper method to create C string copy
char* MakeStringCopy (const char* string)
{
	if (string == NULL)
		return NULL;
	
	char* res = (char*)malloc(strlen(string) + 1);
	strcpy(res, string);
	return res;
}

void CheckObjectBridgeExists()
{
    if( objectBridge == nil )
    {
        objectBridge = [[PFObjectBridge alloc] init];
    }
}

extern "C"
{
    int CreateNewPFObjectWithClassName( const char* className )
    {
        CheckObjectBridgeExists();
        
        return [objectBridge createNewPFObjectWithClassName:CreateNSString(className)];
    }
    
    int FetchExistingPFObjectFromServer( const char* className, const char* serverId )
    {
        CheckObjectBridgeExists();
        
        return [objectBridge fetchExistingPFObjectFromServer:CreateNSString(serverId) withClassName:CreateNSString(className)];
    }
    
    const char* GetServerIdOfObject( int objectId )
    {
        CheckObjectBridgeExists();
        
        return MakeStringCopy([[objectBridge getServerIdOfObject:objectId] UTF8String]);
    }
    
    bool SetStringObjectForKey( const char* object, const char* key, int objectId )
    {
        CheckObjectBridgeExists();
        
        return [objectBridge setObject:CreateNSString(object) forKey:CreateNSString(key) forObjectId:objectId];
    }
    
    bool SetIntObjectForKey( int object, const char* key, int objectId )
    {
        CheckObjectBridgeExists();
        
        return [objectBridge setObject:@(object) forKey:CreateNSString(key) forObjectId:objectId];
    }
    
    bool SetFloatObjectForKey( float object, const char* key, int objectId )
    {
        CheckObjectBridgeExists();
        
        return [objectBridge setObject:@(object) forKey:CreateNSString(key) forObjectId:objectId];
    }
    
    bool SetBoolObjectForKey( bool object, const char* key, int objectId )
    {
        CheckObjectBridgeExists();
        
        return [objectBridge setObject:@(object) forKey:CreateNSString(key) forObjectId:objectId];
    }
    
    bool SaveObjectWithId( int objectId )
    {
        CheckObjectBridgeExists();
        
        return [objectBridge saveObjectEventuallyWithId:objectId];
    }
    
    void DestroyWhenUpdateWithId( int objectId )
    {
        if( objectBridge == nil )
        {
            return;
        }
        
        [objectBridge destroyWhenUpdatedWithId:objectId];
    }
}