//
//  PFObjectBridge.h
//  UnityParseBridge
//
//  Created by Simon on 6/03/13.
//  Copyright (c) 2013 Transmedia Entertainment. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/PFObject.h>
#import <Parse/PFQuery.h>

@interface PFObjectBridge : NSObject

- (int)createNewPFObjectWithClassName:(NSString*)className;
- (int)fetchExistingPFObjectFromServer:(NSString *)serverId withClassName:(NSString *) className;
- (BOOL)setObject:(id)object forKey:(NSString*)key forObjectId:(int)objectId;
- (BOOL)saveObjectEventuallyWithId:(int)objectId;
- (void)destroyWhenUpdatedWithId:(int)objectId;
- (NSString*)getServerIdOfObject:(int)objectId;

@end