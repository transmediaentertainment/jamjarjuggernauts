using UnityEngine;
using UnityEditor;
using System.IO;

public class iOSCompileOptions : Editor
{
	[MenuItem("Tools/Build Tools/Disable mobile incompatable DLLs")]
	public static void DisableMobileDLLs()
	{
		var assetPath = Application.dataPath;
		var pathToZlib = "/UniWeb/DLL/Ionic.Zlib.dll";
		
		if( !File.Exists( assetPath + pathToZlib ) )
		{
			Debug.LogWarning( "Ionic.Zlib.dll not found - possibly already .old" );
			return;
		}
		
		var result = Path.ChangeExtension( assetPath + pathToZlib, ".old" );
		
		var fInfo = new FileInfo (assetPath + pathToZlib);
		fInfo.MoveTo(result);
		AssetDatabase.Refresh();
	}
	
	[MenuItem("Tools/Build Tools/Enable mobile incompatable DLLs")]
	public static void EnableMobileDLLs()
	{
		var assetPath = Application.dataPath;
		var pathToZlib = "/UniWeb/DLL/Ionic.Zlib.old";
		
		if( !File.Exists( assetPath + pathToZlib ) )
		{
			Debug.LogWarning( "Ionic.Zlib.old not found - possibly already .dll" );
			return;
		}
		
		var result = Path.ChangeExtension( assetPath + pathToZlib, ".dll" );
		
		var fInfo = new FileInfo( assetPath + pathToZlib );
		fInfo.MoveTo(result);
		AssetDatabase.Refresh();
	}
}
