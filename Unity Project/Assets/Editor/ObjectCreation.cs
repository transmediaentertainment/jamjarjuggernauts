using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

public class ObjectCreation : EditorWindow
{
    [MenuItem("Tools/Object Creation/Add Popup To Scene")]
    public static void Init()
    {
        var window = EditorWindow.GetWindow<ObjectCreation>();
        window.AddButtonSlot();
    }

    private string m_Label = "";
    private List<string> m_Buttons = new List<string>();

    private void AddButtonSlot()
    {
        m_Buttons.Add("Button");
    }

    void OnGUI()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Popup Text:");
        m_Label = GUILayout.TextField(m_Label);
        GUILayout.EndHorizontal();

        GUILayout.Label("Buttons");
        var removeIndex = -1;
        for (int i = 0; i < m_Buttons.Count; i++)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Button " + i + ":");
            m_Buttons[i] = GUILayout.TextField(m_Buttons[i]);
            if (GUILayout.Button("x"))
            {
                removeIndex = i;
            }
            GUILayout.EndHorizontal();
        }

        if (GUILayout.Button("Add New Button"))
        {
            AddButtonSlot();
        }

        GUILayout.Space(10f);

        if (GUILayout.Button("Add Popup To Scene"))
        {
            Popup.CreatePopup(m_Label, m_Buttons.ToArray());
            this.Close();
        }
        if (removeIndex != -1)
        {
            m_Buttons.RemoveAt(removeIndex);
        }

    }
}
