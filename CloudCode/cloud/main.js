
// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("hello", function(request, response) {
  response.success("Hello world!");
});

Parse.Cloud.define("levelFileSaved", function(request, response) {
	var LevelFile = Parse.Object.extend("LevelFile");
	var levelFile = new LevelFile();
	levelFile.set("url", request.params.url);
	levelFile.set("name", request.params.name);
	levelFile.save(null, {
		success: function(levelFile) {
			response.success(request.params.name + " saved to DB");
		},
		error: function(levelFile, error) {
			response.error(error);
		}
	});
});
