package com.transmediaentertainment.tictacgame;
import com.parse.SaveCallback;

public abstract class IDSaveCallback extends SaveCallback {
	protected int m_ID;
	public IDSaveCallback(int id)
	{
		m_ID = id;
	}
}
